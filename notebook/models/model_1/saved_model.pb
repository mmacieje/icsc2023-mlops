å#
�&�%
D
AddV2
x"T
y"T
z"T"
Ttype:
2	��
B
AssignVariableOp
resource
value"dtype"
dtypetype�
~
BiasAdd

value"T	
bias"T
output"T" 
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
K
Bincount
arr
size
weights"T	
bins"T"
Ttype:
2	
N
Cast	
x"SrcT	
y"DstT"
SrcTtype"
DstTtype"
Truncatebool( 
h
ConcatV2
values"T*N
axis"Tidx
output"T"
Nint(0"	
Ttype"
Tidxtype0:
2	
8
Const
output"dtype"
valuetensor"
dtypetype
�
Cumsum
x"T
axis"Tidx
out"T"
	exclusivebool( "
reversebool( " 
Ttype:
2	"
Tidxtype0:
2	
R
Equal
x"T
y"T
z
"	
Ttype"$
incompatible_shape_errorbool(�
=
Greater
x"T
y"T
z
"
Ttype:
2	
�
HashTableV2
table_handle"
	containerstring "
shared_namestring "!
use_node_name_sharingbool( "
	key_dtypetype"
value_dtypetype�
.
Identity

input"T
output"T"	
Ttype
l
LookupTableExportV2
table_handle
keys"Tkeys
values"Tvalues"
Tkeystype"
Tvaluestype�
w
LookupTableFindV2
table_handle
keys"Tin
default_value"Tout
values"Tout"
Tintype"
Touttype�
b
LookupTableImportV2
table_handle
keys"Tin
values"Tout"
Tintype"
Touttype�
q
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:

2	
�
Max

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
>
Maximum
x"T
y"T
z"T"
Ttype:
2	
�
Mean

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
e
MergeV2Checkpoints
checkpoint_prefixes
destination_prefix"
delete_old_dirsbool(�
>
Minimum
x"T
y"T
z"T"
Ttype:
2	
?
Mul
x"T
y"T
z"T"
Ttype:
2	�
�
MutableHashTableV2
table_handle"
	containerstring "
shared_namestring "!
use_node_name_sharingbool( "
	key_dtypetype"
value_dtypetype�

NoOp
M
Pack
values"T*N
output"T"
Nint(0"	
Ttype"
axisint 
�
PartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring 
C
Placeholder
output"dtype"
dtypetype"
shapeshape:
�
Prod

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
�
RaggedTensorToTensor
shape"Tshape
values"T
default_value"T:
row_partition_tensors"Tindex*num_row_partition_tensors
result"T"	
Ttype"
Tindextype:
2	"
Tshapetype:
2	"$
num_row_partition_tensorsint(0"#
row_partition_typeslist(string)
@
ReadVariableOp
resource
value"dtype"
dtypetype�
�
ResourceGather
resource
indices"Tindices
output"dtype"

batch_dimsint "
validate_indicesbool("
dtypetype"
Tindicestype:
2	�
o
	RestoreV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
l
SaveV2

prefix
tensor_names
shape_and_slices
tensors2dtypes"
dtypes
list(type)(0�
?
Select
	condition

t"T
e"T
output"T"	
Ttype
A
SelectV2
	condition

t"T
e"T
output"T"	
Ttype
P
Shape

input"T
output"out_type"	
Ttype"
out_typetype0:
2	
H
ShardedFilename
basename	
shard

num_shards
filename
�
StatefulPartitionedCall
args2Tin
output2Tout"
Tin
list(type)("
Tout
list(type)("	
ffunc"
configstring "
config_protostring "
executor_typestring ��
@
StaticRegexFullMatch	
input

output
"
patternstring
m
StaticRegexReplace	
input

output"
patternstring"
rewritestring"
replace_globalbool(
�
StridedSlice

input"T
begin"Index
end"Index
strides"Index
output"T"	
Ttype"
Indextype:
2	"

begin_maskint "
end_maskint "
ellipsis_maskint "
new_axis_maskint "
shrink_axis_maskint 
N

StringJoin
inputs*N

output"
Nint(0"
	separatorstring 
<
StringLower	
input

output"
encodingstring 
e
StringSplitV2	
input
sep
indices	

values	
shape	"
maxsplitint���������
�
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape"#
allowed_deviceslist(string)
 �"serve*2.7.02v2.7.0-rc1-69-gc256c071bb28��!
�
embedding/embeddingsVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*%
shared_nameembedding/embeddings

(embedding/embeddings/Read/ReadVariableOpReadVariableOpembedding/embeddings* 
_output_shapes
:
��*
dtype0
t
dense/kernelVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*
shared_namedense/kernel
m
 dense/kernel/Read/ReadVariableOpReadVariableOpdense/kernel*
_output_shapes

:*
dtype0
l

dense/biasVarHandleOp*
_output_shapes
: *
dtype0*
shape:*
shared_name
dense/bias
e
dense/bias/Read/ReadVariableOpReadVariableOp
dense/bias*
_output_shapes
:*
dtype0
l

hash_tableHashTableV2*
_output_shapes
: *
	key_dtype0*
shared_name3913*
value_dtype0	
}
MutableHashTableMutableHashTableV2*
_output_shapes
: *
	key_dtype0*
shared_name
table_63*
value_dtype0	
f
	Adam/iterVarHandleOp*
_output_shapes
: *
dtype0	*
shape: *
shared_name	Adam/iter
_
Adam/iter/Read/ReadVariableOpReadVariableOp	Adam/iter*
_output_shapes
: *
dtype0	
j
Adam/beta_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_1
c
Adam/beta_1/Read/ReadVariableOpReadVariableOpAdam/beta_1*
_output_shapes
: *
dtype0
j
Adam/beta_2VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nameAdam/beta_2
c
Adam/beta_2/Read/ReadVariableOpReadVariableOpAdam/beta_2*
_output_shapes
: *
dtype0
h

Adam/decayVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name
Adam/decay
a
Adam/decay/Read/ReadVariableOpReadVariableOp
Adam/decay*
_output_shapes
: *
dtype0
x
Adam/learning_rateVarHandleOp*
_output_shapes
: *
dtype0*
shape: *#
shared_nameAdam/learning_rate
q
&Adam/learning_rate/Read/ReadVariableOpReadVariableOpAdam/learning_rate*
_output_shapes
: *
dtype0
^
totalVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_nametotal
W
total/Read/ReadVariableOpReadVariableOptotal*
_output_shapes
: *
dtype0
^
countVarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_namecount
W
count/Read/ReadVariableOpReadVariableOpcount*
_output_shapes
: *
dtype0
b
total_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	total_1
[
total_1/Read/ReadVariableOpReadVariableOptotal_1*
_output_shapes
: *
dtype0
b
count_1VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	count_1
[
count_1/Read/ReadVariableOpReadVariableOpcount_1*
_output_shapes
: *
dtype0
b
total_2VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	total_2
[
total_2/Read/ReadVariableOpReadVariableOptotal_2*
_output_shapes
: *
dtype0
b
count_2VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	count_2
[
count_2/Read/ReadVariableOpReadVariableOpcount_2*
_output_shapes
: *
dtype0
b
total_3VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	total_3
[
total_3/Read/ReadVariableOpReadVariableOptotal_3*
_output_shapes
: *
dtype0
b
count_3VarHandleOp*
_output_shapes
: *
dtype0*
shape: *
shared_name	count_3
[
count_3/Read/ReadVariableOpReadVariableOpcount_3*
_output_shapes
: *
dtype0
�
Adam/embedding/embeddings/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*,
shared_nameAdam/embedding/embeddings/m
�
/Adam/embedding/embeddings/m/Read/ReadVariableOpReadVariableOpAdam/embedding/embeddings/m* 
_output_shapes
:
��*
dtype0
�
Adam/dense/kernel/mVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*$
shared_nameAdam/dense/kernel/m
{
'Adam/dense/kernel/m/Read/ReadVariableOpReadVariableOpAdam/dense/kernel/m*
_output_shapes

:*
dtype0
z
Adam/dense/bias/mVarHandleOp*
_output_shapes
: *
dtype0*
shape:*"
shared_nameAdam/dense/bias/m
s
%Adam/dense/bias/m/Read/ReadVariableOpReadVariableOpAdam/dense/bias/m*
_output_shapes
:*
dtype0
�
Adam/embedding/embeddings/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:
��*,
shared_nameAdam/embedding/embeddings/v
�
/Adam/embedding/embeddings/v/Read/ReadVariableOpReadVariableOpAdam/embedding/embeddings/v* 
_output_shapes
:
��*
dtype0
�
Adam/dense/kernel/vVarHandleOp*
_output_shapes
: *
dtype0*
shape
:*$
shared_nameAdam/dense/kernel/v
{
'Adam/dense/kernel/v/Read/ReadVariableOpReadVariableOpAdam/dense/kernel/v*
_output_shapes

:*
dtype0
z
Adam/dense/bias/vVarHandleOp*
_output_shapes
: *
dtype0*
shape:*"
shared_nameAdam/dense/bias/v
s
%Adam/dense/bias/v/Read/ReadVariableOpReadVariableOpAdam/dense/bias/v*
_output_shapes
:*
dtype0
G
ConstConst*
_output_shapes
: *
dtype0	*
value	B	 R
H
Const_1Const*
_output_shapes
: *
dtype0*
valueB B 
I
Const_2Const*
_output_shapes
: *
dtype0	*
value	B	 R 
I
Const_3Const*
_output_shapes
: *
dtype0	*
value	B	 R 
��

Const_4Const*
_output_shapes

:��*
dtype0*��

value��
B��
��BtheBandBaBofBtoBisBinBitBthisBiBthatBbrBwasBasBwithBforBmovieBbutBfilmBonBnotByouBareBhisBhaveBbeBheBoneBitsBatBallBbyBanBtheyBfromBwhoBsoBlikeBherBjustBorBaboutBhasBifBoutBsomeBthereBwhatBgoodBwhenBmoreBveryBupBnoBevenBsheBwouldBmyBtheirBwhichBonlyBtimeBreallyBwereBseeBstoryBcanBhadBmeBthanBmuchBweBwellBbeenBgetBintoBbadBalsoBdoBgreatBotherBpeopleBwillBbecauseBhowBmostBfirstBhimBdontBfilmsBthenBmakeBthemBmadeBmoviesBcouldBwayBanyBafterBtooB
charactersBthinkBwatchBtwoBbeingBmanyB	characterBneverBwhereBlittleBseenBplotBloveBbestBactingBdidBknowBeverBlifeBdoesBshowBbetterByourBoffBtheseBwhileBstillBoverBendBsceneBwhyBscenesBsayBhereBmanBshouldB	somethingBsuchBgoBthroughBbackBthoseBimBrealBdoesntBnowBwatchingBthingByearsBdidntBthoughBactorsBmakesBnewBactuallyBanotherBnothingBfunnyBgoingBlookBfindBbeforeBlotBsameBfewBworkBeveryBoldBusBpartBdirectorBcantBthatsBquiteBagainBwantBcastBthingsBseemsBprettyBfactByoungBaroundBworldBgotBtakeBbothBenoughBhoweverBhorrorBdownBgiveBactionBownBbetweenBthoughtBbigBiveBmayBtheresBoriginalBwithoutBlongBisntBrightBsawBgetsBalwaysBmustBinterestingBwholeBroleBcomeBcomedyBseriesBalmostBleastBhesBtimesBbitBlastBpointBguyBscriptBmusicBdoneBminutesBsinceBmightBfeelBfarBprobablyBperformanceBanythingBfamilyByetBamBawayBkindBratherBgirlBworstBeachBsureBfoundBfunBtryingBplayedBourBhavingBwomanBtvBanyoneBmakingB
especiallyBbelieveB	differentBcomesBshowsBgoesBalthoughBhardBdayBcourseBworthBputBlooksBsenseBplaceB2BmaybeBplaysBlookingBwasntBendingBonceB
everythingBmainBsetBthreeBmoneyBreasonBscreenBduringBjohnBeffectsBdvdBsomeoneBtogetherBtakesBsaidBtrueBseemBwatchedBinsteadBamericanBactorBjobB10BeveryoneBspecialBbookBlaterBplayBhimselfBnightBseeingBaudienceBleftBideaB	excellentBsimplyB	beautifulBblackBusedByoureBwarBwifeB
completelyBsecondBdeathBhighBshotBversionBpoorBniceBhelpBkidsBhouseBfanBelseBtryBstarBdeadBmenBlessBfriendsBperformancesBreadBuseBalongBuntilBboringBgivenByearBhomeBwomenBcoupleBneedBeitherBnextBsexBmindB	hollywoodBstartBenjoyBshortBtrulyBclassicBhalfBwrongBperhapsBrestBfatherBtellBothersBmomentsBgettingBkeepBlineB
productionBstupidB
understandBcameB	recommendBletBcameraBgivesBfullBterribleBrememberBawfulBmeanB	wonderfulBdoingB
definitelyBsmallBplayingBitselfBschoolBnameBoftenBdialogueBbecomeBcouldntBwentBlostBvideoBfaceBlinesBstarsBbudgetB3BcaseBtitleBepisodeBearlyBsupposedBhopeBtopBentertainingBlikedB	certainlyBguysBpersonBliveByesBperfectBentireBhumanBseveralB
absolutelyBoverallBmotherBstyleBfinallyBagainstBproblemBsortBheadBboyBfansBfeltBpieceBwrittenBbasedBwasteBshesBseemedBworseBleadBdespiteBbecomesBlivesB	beginningB	directionBpictureBevilBfinalBhumorBfriendBcareBdarkBlaughBtotallyBwantedBwontBunfortunatelyBcinemaBidB
throughoutBlovedBohBalreadyBwantsBdramaByoullBableBunderBmrBturnBfineBlowBtriesB1BsonBchildrenBgameBdaysBwhiteBkillerBhistoryBguessBcalledBstartsBsoundBtheyreBkillBturnsBactBBcityBhorribleBgirlsBqualityBexampleBgenreBexpectBbehindBenjoyedBwritingBpartsBpastB
themselvesBamazingBflickBgaveBworksBkilledBtownBcarBfavoriteBsideBrunBdirectedBfightBdecentBmichaelB	obviouslyB	sometimesBmatterBgroupBviewerBeyesBsoonBhellBheartBstuffBthinkingBstoriesBonesBmyselfBsaysB	brilliantBexceptBpoliceBviolenceBheardBbloodBactressBchildBillBhappensB	extremelyBdaughterBcloseBtookBwouldntBleaveBhappenedBfeelingBattemptBhighlyBtakenBkidBlateBhandBobviousBcomingBlivingBsaveB	includingBetcBmomentB
experienceBtypeBlookedBrolesBinvolvedBartBokBknownBinterestBstrongBwonderBscoreBstopBcannotBnoneBmurderBrobertBitbrBtoldBhourBparticularlyBjamesBacrossBexactlyBhappenBhoursBlackBcompleteBheroBcoolBdavidBchanceBfindsBsimpleBanywayBrelationshipBletsB	hilariousBagoBendsBannoyingBsadBpleaseBnumberBusuallyBcareerBageBgoreBseriousBslowBhusbandBgodBwhoseB	importantBtodayBpossibleBorderBfemaleBopeningBstrangeBrunningBscaryBaloneBcallBshownB	basicallyBsomewhatB4B
ridiculousBcinematographyB	seriouslyBwishB
apparentlyBvoiceBpowerBcutBstartedBhugeBsayingBmostlyBcrapBdocumentaryBtakingBusualBhitBchangeBreleasedBrealityBsillyByourselfBsongBtalentBturnedBhappyBbrotherBmajorBjokesB	attentionBtalkingBroomBsequelBopinionB5BnovelBmoviebrBknewBbodyBclearlyBenglishBlevelBjackBshotsBtellsBviewBdueB	directorsBknowsBsingleBproblemsBmissBbeyondBsetsBcheapBwhatsBratingBeventsBparentsBwordsBfutureBbringBwordBtalkBlightBfourBbritishBcountryBdisappointedBeasilyBlocalBfallsBarentBappearsBgivingBcomicBaddBhaventBviewersBmodernB	storylineBlotsBsimilarBuponBfrenchBfilmbrB
supportingBwithinBtypicalBromanticBmentionBgeorgeBbunchBmusicalBaboveBsequenceBentertainmentByorkBclearBpredictableBcertainBnamedBreviewBthemeBeasyBbeginsBsongsBwaysBhateBamongB
televisionBmessageBelementsB	enjoyableBgeneralBthrillerBkeptBepisodesBkingBworkingBfilmedBmiddleBavoidBusingBstraightBfeelsBdialogBtenBwhetherBstayBpointsB	surprisedBfallBneedsBleadsBtheaterBteamBmysteryBgoneBsorryBnearlyBearthBrichardBtaleBdieBwriterBshowingBmeansBbroughtBwhosBtriedBdullBdoubtBnearBsomehowBgayBreleaseBfiveBeffortBcommentsBtomBherselfBkillingB
soundtrackBbuyBdeBmovingBcrimeBredBactualBtruthB	sequencesBpaulBladyBreviewsBpeterBimagineBclassBfastBthirdBbrothersBmaterialBeditingBdealB
eventuallyBcheckBrentBgreatestBhearBfamousBmoveBformBforgetB	fantasticBpremiseBperiodBviewingBsuspenseBpossiblyBokayBweakBpoorlyBlearnByouveBexpectedB	realisticB	animationBaverageBfigureBboysBleavesBsurpriseBspaceBwhateverBstandBamericaBfeatureBlameBforcedB	difficultB
believableBindeedBeyeBnorBsitBsisterB
particularBwritersBdogBsubjectBearlierBsexualBbecameBdeepBdecidedBtowardsB
atmosphereBcopBleeB
interestedBwaitBfollowB80sB	otherwiseBdrBstageBbeginBquestionBscifiBresultBpersonalBplusBkeepsB
filmmakersBrockBfeaturesBcheesyBreadingBmeetB
screenplayBromanceBworkedBsocietyB	situationBnoteBfootageBmonsterBminuteB	emotionalBrealizeBtotalBolderBwhomBseasonB	perfectlyBjoeB	memorableBmaleBcrewBpreviousBunlessBbattleBshameBbusinessBeffectBwesternBpowerfulBopenBstreetBremakeBappearBquicklyBsettingBmeetsBdramaticBlaughsBfreeBhotBcommentBneededB
incrediblyBbadlyBhandsBoscarBvariousBbringsBtwistBplentyBlaB
backgroundBboxBfairlyBbillBcrazyBjapaneseBcreditsBuniqueBsoundsBnatureBrichBislandB	directingBsuperbBfrontBmessBoutsideBmissingBapartBadmitBpresentBmarkBmanagesBwriteBbabyBairBinsideBsecretBmarriedB20BdevelopmentBweirdBmeantBcreateBleadingBideasBdanceBpayBreturnBunlikeBbBwilliamBactedBgunBaskB70sBfightingBtellingBholdBmasterpieceBbeautyBimdbBhardlyBfailsB	potentialBdeservesB
girlfriendBbreakBcaughtB	expectingB	politicalBescapeBformerBcleverBlaughingBcreepyBeraBforwardBdumbBnudityBcreatedB	portrayedBcuteBcopyBneitherBsuccessBagreeBplainBreasonsBdreamBviolentBvillainBattemptsBrecentlyBwastedBdoctorBpublicBlargeBboredBpartyBharryBjokeBwaitingBpureBfurtherBzombieBtalentedBfantasyBentirelyBcastingBsuddenlyBslightlyBconsideringBchoiceB	followingBspentBdecidesBvanBendedBcreditBusesBtroubleBfireBprisonBwaterBpaceB	mentionedBcoverBsadlyBcomparedBforceBoddBlistBintelligentBvalueB
ultimatelyBconceptBpopularBstoreBseesBsocialBgermanBfamiliarBcartoonB	producersBofficeB	effectiveBalienB
appreciateBmembersBitalianBcompanyBcauseBmovesBfearBsamB
incredibleBdisneyBexcitingBwroteBfollowsBflatBfilledBsweetBspendBdiedBstateB
convincingBamusingBprojectB
successfulBcommonB	portrayalBbasicB	pointlessBmissedBfitBpositiveBcollegeBproducedBscienceByoungerB12BtensionBrunsBdecideBspeakB
situationsBrecentBamountBlongerBsolidBrateBnormalBhairB	audiencesBlanguageBcultB	detectiveB7BfocusBscottBhonestBcoldBvisualBfailedB	involvingBcontrolBbooksBgangBtouchBbizarreBsilentBbiggestB8BhonestlyBmaryBthinksBwerentBrevengeBthanksB
impossibleBadultBrespectBawesomeBmatchB	questionsBstarringBshowedBcharlesBlikesBcultureBpatheticBfakeB	chemistryBbruceBbarelyBleavingBsurprisinglyBsickB	literallyB
impressiveBstudioBchangedB
disturbingButterlyBghostBdepthBkillsBconsiderBhumourBrecommendedB
appearanceBimmediatelyBshootingBasideB	generallyB30BspoilersBaliveB15B
conclusionB
mysteriousBknowingBaspectBrideBgeniusB
consideredBmasterBcenturyBmagicBfrankBfictionBboughtBtoughByeahBaccentBmadBwalkBslasherBissuesBlovesBchannelBdoorBcatBsingingBvaluesBrareBnowhereBgladBstandardB	adventureBmartinBnaturalBattackBcomediesBpickBjaneBfairBsteveBexplainBcampBsexyBtermsBimagesBwinBsouthB
historicalBchaseBsmartBabilityBtrainBplacesBminorBplanBmeaningB	somewhereBhenryBcomplexBtrashBaddedBpresenceBnakedBlikelyBtoneBbuildingBpicturesBgarbageBroadBraceBbenBvictimsBlovelyBpurposeB
personallyBcatchBanimatedBnoticeBwildBnobodyBinnocentBdancingBbandBthrownBsittingBchangesBcomputerBzombiesBtwistsBcharlieBtripBjonesBmanagedBcharmingBbeautifullyBterrificB	christmasBcentralBmakeupBspiritB	cinematicBsupposeBcostumesBwalkingByoudBhopingBwonBselfBpainfulB
governmentBblueBmakersBfestivalBarmyBmilitaryBequallyBjimBthusB	presentedBsoldiersBsmithBsubtleBmoodBbrainBslowlyBweekBunbelievableBjoanBiiB	availableB100BwestBshootBindianBgiantBbriefBjasonBmarriageBspoilerBpassBoutstandingBdetailsBfullyBtouchingBstickBdadBremainsBputsBlandB
constantlyBnewsBjourneyBheyBcolorBexcuseBdateBbotherB	happeningBaspectsBappealBwoodsB9B	everybodyBedgeBthrowBemotionsBbesidesBthankBstandsBplanetBlacksBspeakingBlondonBclimaxB	exceptionBtasteBmainlyBfindingBtonyBbillyBcontainsBactsB	narrativeBsoulBsentBdrugBdriveBtiredBsupportBpiecesBbottomBdisappointingBvampireBphotographyB	laughableBexpectationsBshipBnamesBwilliamsBstudentBlawBopportunityBloudBfallingBlikableBsurelyBinspiredBbornBaddsBsuggestBsceneryBqueenBfeelingsBthemesBintendedBagentBstuckBgreenBfilmingB
impressionBmistakeBkeyBparkBrobinB	boyfriendBbossBbobBstudentsBhotelBfacesBwowBflicksBrelationshipsBmurdersB
adaptationBcriticsBfreshBdiesBshouldntBmerelyBheavyBcaptainBtrackB	developedBconfusedBchrisBteenBgamesBfascinatingB
supposedlyBrandomBproducerBmannerB	seeminglyBallenBstunningBpullBhasntB	americansBaheadBloverBincludeBabsoluteBclubBcharmBlivedBlaughedBindustryBemotionB6BworthyBserialBdeliversBsixBshockBnegativeB	confusingBchineseBstatesBmixBdisappointmentB	impressedBbecomingBpaidBvictimBkevinBbondBoffersBheroesBjusticeBfinishB	actressesBmillionBelementB90BfolksBmikeBgradeBapproachBallowedBmomBmediocreBimageBshareBpornBremindedBheldBchristopherBofferB	apartmentBmsBmoralBfoodBclichéBmansBloseBgrantBnastyBholesBcreatureBuglyBflawsBdrawnBcgiBputtingBparisBdetailBbeatBtragicBfunniestBdreamsB
collectionBpainB
differenceBbarBrentedBdisasterBawardBringBadditionBlightingBgorgeousBdoubleBdirtyBcontentB	wonderingBprovidesBbloodyBturningBhospitalB	forgottenBforeverB
compellingBpersonalityBimpactBunusualBinformationBfollowedBdavisBadultsBteenageBmartialBdamnBratedBextremeBappearedBwarsBhurtBfellowBgroundBtwiceBtimebrBcreativeBangryBstoneBhelpsBartsBcryBblameBmemberBunknownBprinceBlocationBaliensBsystemBdyingBcarryBsearchBreadyBprovideBsecondsB	favouriteB
attractiveBasksBcontinueBquickBontoB	christianBplaneBpowersBanswerBledBcomedicB	thereforeBsummerBstepBbedBshockingBtrailerBgemBaffairB
surprisingBprovesBindependentBcopsBsuperiorBcountBmovedBteacherBhiddenBdescribeBstationBintenseB	redeemingBhatedBanymoreBeventBareaBwearingBtrustBhelpedBclichésBdesignBaccidentBrussianBintelligenceBheadsBtragedyBrarelyBgrandBwoodenBnoirBvillainsBmotionB
introducedBflyingBcarsBspotBdeliverBafraidBextraBjerryBaskedB
filmmakingBsuperBwillingBepicB	desperateBincludesBeddieB
commentaryBactionsBfellBreturnsBstreetsB
intriguingB	filmmakerBsevenBrayBnumbersBthinB	necessaryBbitsBjudgeBbeganB	standardsBrealizedBpickedB	scientistBdirectBdonBimaginationBsleepBlordBprocessBmachineBhitsBanimalsBadamBhowardBgraceBwantingBtodaysBquietBfashionBprofessionalBlimitedBfightsBdouglasBdiscoverBholdsBseaBloversBgenuineB	religiousBoperaBmemoryBliesBcriminalBsuspectBbrutalBtearsBphoneBalanBfinishedBconstantBstruggleBresponsibleBfatBunderstandingBanimalBmonthsBacceptBphysicalBdeeplyBclothesBlatterBgoldBplayersBlooseBgunsBmixedBgangsterBcrossBbrokenBallowBzeroBapparentBdennisBbatmanB	watchableBmissionBdrugsB	childhoodB	presidentBmouthBieBpilotBnormallyBnicelyBkeepingBcompareBmetBlegendBmonstersBissueBedB	dangerousBalBwittyBforcesBenergyBrapeBmentalBartistBkellyBsucksBpacingB	meanwhileBmrsB	communityBmurderedBunnecessaryBfoxB
thoroughlyBpartnerBcallsBskipBpopBhopesBtowardBeatB50BwallBvisionBbrianB
whatsoeverBvillageBscaredBplotsBnickBstephenBpassionBmooreBexplanationBdeserveBbuildBtortureBartisticBhumansBawareBtreatBskillsBproveBanimeBsurviveBstartingBremindsBpriceB	featuringBofficerBlewisBnumerousBengagingBdrivingBpleasureBsuicideB	lowbudgetBjohnnyBleaderBlackingBsmileBcreatingBconflictBabsurdB	technicalBjeanBbrightBwheneverBsatBlossBfieldB	concernedBchurchBuncleB
originallyBkillersB60sBthomasBsightBjrBheresBfloorB	knowledgeBanybodyB	accordingBanywhereBwonderfullyBtheatreBweddingBthembrBwhilstBwarningBsoldierBopensBmediaBinstanceBgottenB	explainedBwindowBlistenBshallowBcableBtreatedBpeoplesBenglandBvsBstockBregularBincludedBaccurateBworldsBseanB
remarkableB	naturallyBforeignBjeffBgonnaBiceBgrowingBradioBpatrickBfranklyBmemoriesBdatedB
generationBfredBsavingBmanageBoppositeBdesireBboatBbelowBanthonyB	locationsBsomebodyBrealismBpulledBmineBluckyBjoyBhorseBfathersBemptyB	teenagersBbiggerBbehaviorB	reviewersBlengthBidentityBeffortsBlovingBdrunkBdogsBblandBrevealedBrecordBlearnedBeuropeanBdeanBcapturedBwitchBoccasionallyBhadntBsavedBterriblyBgaryB
comparisonBacademyBwinningBprivateBholmesB
connectionBsequelsBpsychologicalBnonsenseBbrownBbeachBwoodyBultimateBtimBrubbishBboardBspanishBsignBroseBparodyBhigherBcontextBbreaksBsheerBdealingBvhsBownerBjumpBfailByouthB
underratedBordinaryBhumanityBtwentyBsingerB	discoversBnationalBkindaBfinaleBfactsBjenniferBdebutBplayerBlakeBweveBvoteBreceivedBmorningBgraphicBflyBdeservedBresultsBmeetingBessentiallyBballBstandingBrangeBclassicsBtalentsBperspectiveBlosesBevidenceBtheyveBhumorousBdealsBarthurBwindBunfunnyBtravelBsuitBexploitationBeditedB
discoveredBstudyBgoryB	contrivedB	continuesBcaptureBfridayBcameoBandyBakaBadviceBrescueBinternationalBcutsBvisitBstevenBreachBoverlyBtapeBnoticedB	nightmareBcB
unexpectedBcurrentBbankBunrealisticBsatireB
depressingB	treatmentB
referencesBpageBfinestBcapableBbearBneverthelessBfaultBladiesBinvolvesB	genuinelyBeatingBblindBcandyBmoonBkongBfeetBrogerBthisbrBwiseBstewartBprovedB
friendshipBjacksonBinsaneBawkwardBhunterBcrappyBluckBtillBheroineBgagsBdreadfulBdesertBwayneB	deliveredBstereotypesBrollB
flashbacksBfameB	professorBpostBreliefBexecutedBdraculaBmagnificentBpassedBunableBryanBrelateB	hitchcockBexistBcrashBcombinationBfailureBbombBattitudeBvehicleBgeneBbrilliantlyBmajorityBbodiesBspectacularBfateBdecisionBblondeBsheriffB	virtuallyBmarryB1010B	sufferingBheckB
commercialBtalksBpairBfreedomBcrowdBtypesB50sBscreamBpretentiousBmurphyBonebrB	hopefullyBfamiliesBdressedBdevilBdecadeBdannyBallowsBsingBportraysB	painfullyBkateBfordBcartoonsBaskingBalexBtheatersBsegmentBrulesB
rememberedBericBfaithBcrawfordB40BsoapBsistersBhillBthoughtsButterBtediousBmexicanBgrowBsellBranBpromiseBportrayB
gratuitousBwoodBwellbrBreactionBhorriblyBhauntedBnorthBcastleB90sBframeBnativeBmattBasianBweeksBsympatheticBsakeB	describedBcuriousB	screamingBmaxBbringingBstrengthBroundBremainBpracticallyBformulaBcausedB1970sBseatBlogicBlevelsBteensBdevelopBvisualsBgrewBdinnerBclaimBterrorBrussellBpaperBcleanBvisuallyB
individualBhandsomeB1950sB
theatricalBsuckedBspeedBshopBlessonBtexasBskinBrBendlessBstealBlargelyBenemyBcaresBsuddenBstoppedBhimbrBprotagonistBproductBdickBdestroyBbelievesBunitedBtestBstronglyB
refreshingBgoldenBfavorBdragBassumeBmattersBkickBhearingBforestBdeathsBbarbaraBresearchBproduceBthatbrBembarrassingBpleasantBchooseBbeastBbetBmodelBcenterB13B	subtitlesBlosingBholdingB
delightfulBcreatesBcashBlearnsBbuiltBwerewolfBviewedBversionsBreviewerBplansBwalksBdisplayB	creaturesBgordonBappropriateBshockedB
satisfyingBhideBcornyBemotionallyBdriverBbmovieB	slapstickBrecallBproperBmebrBfleshB	existenceBdanielBprogramBgasBcircumstancesB
technologyBedwardBancientBtaylorBspeechBsidesBsendBscareB	qualitiesBmaskBchuckBsourceBsaturdayBmetalBfillBtwistedBfactorBsiteBhangingBextrasBlaughterBwitnessBtrappedBrobotBriverBjulieBagesBveteranBusaBteenagerBstolenBspeaksBprovidedBpresentsBjapanBcontrastBstorybrB
relativelyBdollarsBcostBcorrectBrealizesBpullsB
irritatingBhauntingB
disgustingBspoilBrentalBjackieBheavenBeuropeBarmsBukB
propagandaBlousyBstealsBdryBblowBsupernaturalB
portrayingBfranceB	executionBdropBmassacreBcuttingBauthorBallbrBstorytellingBsimonBseasonsBlesbianBexactBspendsBriseBexcitedB810BreporterBcloserBclaimsBbugsBandersonBwinnerBunfortunateBpriestBmothersBlowerBassBwideBtendBsurrealBsupermanBdBsusanBserviceBsafeBcostumeBjosephBdangerBgrantedBcanadianBannaBtraditionalBstudiosB	flashbackBruinedBfitsB	convincedBprimeBwarmBtargetB	highlightBjimmyBdrawBchosenBawardsBtinyB	legendaryBexperiencesBcourtBcostsBannBvarietyBstereotypicalB
overthetopBguiltyBfocusesBskyBproudB	committedBbreakingBwearBeveningBstayedBpityBhandledBdirectlyBanneBpsychoBmilesBfeaturedBprincessBlatestBinsultB	influenceBgrittyB
excitementBpacedBholeBforgettableB	abandonedBswordBremotelyBrainBchickB	appealingBvampiresBgoofyBdeadlyBwalkedB	universalBsurfaceBidiotBdeviceB	destroyedBwitB	promisingBpoliticsBjBconversationBbuddyBangelBtouchesBsunBrightsBfrighteningBdrinkingBsonsBmagicalB	interviewB	elizabethBconvinceB
amateurishBsuspensefulBsuffersBsouthernBforthBdeeperBwilsonBweaponsBservesBprintBfareBclueBnaziBharrisBchiefBwelcomeBvietnamB	structureBmobB	offensiveBdrivenBwealthyBrobBreligionBpileBmultipleBlucyB
interviewsBexperiencedBentertainedBaccidentallyBnetworkBlonelyBlauraBexplainsBenterBdonaldBuniverseBsympathyBpraiseBinnerBindiaBhongBhiredBhelenB	encounterBcruelBvincentB	substanceB
mainstreamBjesusBdegreeB
californiaBamazedBwhoeverBsubplotB	regardingBlightsBripoffBlouisBfbiBdepictedBamateurB710B410B1930sBsoftBfootballBclichédBchangingB
adventuresBvoicesBtableBdanB
previouslyBnudeBnaiveBgreaterBdressBdeliveryBabuseBurbanB	statementBpowellB	performedBkindsB	downrightBbelievedBvictorBsectionBregretBmindsBdubbedBcontemporaryBbirthBwarnerBquirkyBhoodBtrekBcoreBaccentsB
strugglingB	sensitiveB
reputationBgrownBsecurityBrevealBsharpBmarketBinitialBcryingBsharkBrequiredBrawBbotheredBruinBobsessedBmovementB	initiallyBcoveredBcouldveBaccountBwaybrB	surprisesBmarieBcampyBroutineBraisedBhalfwayBenjoyingBuninterestingBtightB
thankfullyBseekBjungleBinsightBpeaceBkeatonBfootBdozenBartistsBarrivesBwouldveBharshBcapturesBcallingBaintBrepeatedBjoinBfunnierBfalseB
continuityBscenarioB	melodramaBmassiveBlloydBheavilyB	halloweenBrobertsBpreferBmurdererBmereBisbrBfishBasleepBspiteBpayingBforgotBdesperatelyBcomplicatedBunconvincingBtalesBsufferBscaresBsarahBlifebrBineptBhuntBexamplesBafricaB1980sBturkeyBsirBplacedBnuclearBjobsBwearsBsBreminiscentBoriginalityBjuliaBstatusB	searchingBhallBguestBclarkB	childrensB	atrociousBdragonB
departmentBbusBamongstBmorganBfiguredBremoteBanglesBsortsBprotectB
occasionalBbuyingB
australianBripBherebrBchoicesBchargeBangerB
everywhereBbroadwayBwalterBproductionsBeroticBdecadesB	daughtersBcriticalBaddingBruleBclosingBunlikelyBserveBscaleBroughBpriorBoBjailBfuB	bollywoodBfoolBafricanB
worthwhileBsummaryBkimBandorBalbertBpassingBoliverBfB
expressionBblockbusterBmildlyBmansionB	criminalsBcarriesB	amazinglyBweekendBtrainingBpathBcolorsBcategoryBachieveB
washingtonBraiseBnovelsBinternetBdarknessBcagneyB25BmatureBhedBeightBdrewBattackedBsportsBpoolBoutbrB	obnoxiousB	nominatedBghostsBexpressBuncomfortableBtitlesBridingBlawyerBendbrBdesignedBblahBrelatedBkissBignoreBcombinedBchoseBbaseB
attemptingBviaBthrowsBonbrBnonethelessBhorrificBfabulousBcooperBbradBspoofB	screeningBparBirishBservedBrevealsBnotableBnecessarilyBleslieBbaseballBstarredBparkerBlynchBindieBflynnBcarriedBsanB	referenceBbeliefBtrickBstBofferedBjunkBhillsBgrimB	entertainB
determinedBanswersBmountainBlearningBwellesBcurseBmexicoBfaithfulB	challengeBwriterdirectorBextraordinaryBcontainBpositionBfiguresBcausesB	strangelyBstopsBsignificantBsandlerBracistB	fictionalBboreBagainbrBtrilogyBsucceedsBsinisterB	recognizeBgodzillaBgermanyBeastBtechnicallyBsumBrollingBquestBfocusedBjewishBflightBdemonBcageBwesternsB
understoodBtimingBtwinBtermBskillBmgmBlindaBexpertBblownBattacksBuBtreasureBtouchedBsuckBproperlyBpreparedBlieB	franchiseBflawedBeffectivelyB2000BthrowingBronBmindlessBfreddyB	criticismBtallBprotagonistsBpresentationBpregnantBmistakesBfortunatelyBdrinkBsundayBsocalledBgrossBalbeitBringsBrachelBformatBflawBcynicalBangleBspyBshutBpicksBextentB	countriesBcomicalBchinaBstomachBkungBhonorBbetteBauntBratingsB	narrationBkhanBeverydayBbearsB110BoddlyB	favoritesBaliceBshadowBmariaBhandleBfacialBbasisB11BundergroundBtreeB
regardlessBjohnsonBholidayBtourBstaysBsoundedBsleepingB	listeningBinspirationBenvironmentBbitterBscriptsBsantaB	depictionBculturalBturnerBperformBgruesomeBblairB	strugglesBbraveBwarnedBglassBcontactBconcertBbreathBoilBhintBbrooksBworryBsettingsB	ludicrousBlettingBlarryBguardBgreatlyB	disbeliefBtruckB	obsessionBangelsB	advantageB310BlifetimeBdudeBcaryBflashBdollarBlosBgraveBgainBfallenBdawnBviewsBstylishBleagueBimageryBfourthBdailyBcowboyBcellBcasesBandrewBtommyBmillerBhangBgoalBtaskB
frequentlyBcolorfulBashamedBpersonsBflowBburnsBsilverBhelpingBexpressionsBalasBwinsBwhereasBvonBpunchB	innocenceBclipsBrentingBpaintBmassBhidingBgraphicsBdescriptionBbalanceBsuspectsBpurelyBnowadaysB	marvelousBlugosiBentryB	assistantB	afternoonBpianoBembarrassedBdelightBwastingBtheoryBtenseBsurroundingBstrangerBlesserBintroductionBxBremindBpinkBlucasBlazyB	kidnappedBexistsBdukeBcannibalBvirusBshootsBseekingBhappilyBgoodbrBescapesBatmosphericBweaponB	techniqueBsnowBestablishedBequalBsleazyBpoignantB	happinessBagedBspoiledBrogersBrivalBhatBcrimesBteethBstealingBsilenceBlockedBironicBcourageBclaireBalrightBguessingBcrudeB	survivorsBstruckBrepeatBregardBrefusesB
intentionsBcousinBbarryB30sB14BthiefBstatedB
performersBmidnightBrevolvesBmillionsBhenceB	conditionBcarryingBcaringBbelongsB	thousandsBnonBgrowsBdrivesBbelovedBbagBstrikeBnotedBinterpretationBbrokeB
miniseriesBmadnessB	confusionBsuggestsBsuccessfullyBstrikingBstoodBspendingBmichelleBinfamousBhaBfriendlyBexceptionalBchasesBwatersB	traditionBquoteBninjaBjumpsBglimpseB	elsewhereBcivilBcabinBwannaBtricksBstuntsBintellectualBidioticBconveyBmarchBlukeBcharismaBvacationBtherebrBshapeB
restaurantBplasticBpersonalitiesBcheeseBbadbrBsophisticatedBsoldBslightBshinesBretardedBdunneBappreciatedBtedBnightsBkoreanBkirkBgrippingBdislikeBdisagreeBcodeBbuckB910BindiansBbridgeBtributeBseparateBscreenwriterBriskBlaneBfarmBegB	connectedBacceptedBwolfBupsetB
universityB
uninspiredBtopicB	stupidityBstormBspokenBnancyBlyingB
inevitableB
experimentB	attemptedBworkersB
revolutionB
physicallyBgreekBunintentionallyBrushBcredibleBcredibilityBchillingBaforementionedB80BtoyBtoobrB	terroristBsolveB	sexualityBroyBmedicalBhundredBburnB	thrillingBlisaBincreasinglyBgiftBfxB	expensiveBensembleBtarzanBsetupBpittBmirrorB	essentialBburtBzoneBwwiiBrockyBproofBbritainB	authenticB	ambitiousBairedB	thrillersB
pleasantlyBobscureBkicksBkarloffBindividualsBhostBchanBbreathtakingBrippedB
repeatedlyBnonexistentBhomageB	carefullyBtiedBshakespeareBironyBemB
conspiracyBcanadaBbrazilBtroubledBsmokingBroyalBnineBneatBmatthewBhundredsBconcernsBchasingBanticsBtriteBteachBovercomeBknifeBkillingsBhandfulB
explosionsBcloselyB1960sBtuneBreplacedBpovertyB
perfectionB	newspaperB	inspectorB	curiosityB	competentBcoachBboredomBunforgettableBtornB
techniquesB
scientistsBportraitB	insultingBianBhusbandsBhorrorsBforgiveB
associatedBadaptedBmickeyBmafiaBleBfondaBempireBcorruptBcommitBboundBtrialB
prostituteBpetBkaneBhitlerBfosterB
encountersBburningBpsBeerieBdemonsBchaplinBcarolBstanleyBlovableB
importanceBheatBconsistsBburiedBagentsB
acceptableBshortsBrobotsB
repetitiveB
reasonablyBprideBlessonsBlegsBinvestigationBharoldBfreaksBcomedianBbusyBbetterbrBarriveB45BtoddBshedB
revelationB
redemptionB
overlookedBopenedBlaidBbrideBviciousBunbelievablyBtwinsBsentimentalBsavesBpacinoBouterBjerkB
highlightsBweightBshoesBpressB
outrageousBhammerBgottaBgardenB	countlessBcornerBcatsBsingsB	prisonersB	notoriousBmeatBkenBdroppedBcouplesB2001BwishesBvideosBstiffB
reasonableBneilBidentifyBgutsBcardBappearancesBwallsBneedlessBessenceBemmaB	cardboardB16BwomansBsuitsBstuntBsizeBsecretsBpackedBoccurBnationBmoviesbrB	instantlyBguiltBgodsBdareBchainsawBbrieflyB
admittedlyB	abilitiesB	remainingBralphBmonkeyBbeatingBallowingByoubrBprofoundBpersonaBherbrBgloryBfairyBdearBthreatB	territoryBsitcomBsexuallyBseagalBrocksBracismBmitchellBmentallyBkurtBignoredB
hystericalBhealthBcolumboBbbcB
attractionB	appearingB40sBsufferedBperBnavyB
meaningfulBjessicaB	intensityBgagBcraftedBtheydBpredatorBhopkinsBeditorBangelesB3dBuselessBshyBshortlyB	onelinersBofficersB	miserablyBexerciseB	enjoymentBdraggedBwatchesBupbrBspotsBsplitBreturnedBhorsesBhardcoreBcarterBtitanicBsucceedBstripBsellingBplagueBlibraryBkennedyBimaginativeBelBdubbingBwakeB	typicallyBtrapBsatanBresponseBmelodramaticBlBireneBgBfifteenBdrunkenBdramasBcreatorsBcomicsBblowsBsadisticBobjectBimportantlyBdocumentariesBchildishBwonderedBshallBnobleBheartsBachievedB18BtravelsBthirtyB
stereotypeBsomeonesBpatBopposedBmanagerBlogicalBhopedB
frustratedBfortuneBcreationBbeerBbattlesBaccomplishedBstrikesB	sacrificeBphilipBofferingB	gangstersBcookBbogartBbobbyBarrestedBwillisBtorturedB
subsequentBsmokeBshowerBsendsBranksBpatientB	invisibleBhookedBgenresBfarceBdollBchairBthousandB
terrifyingBsidekickBrushedBpointedBneckBmediumBgroupsBfrankensteinBbrandBwaveBstrongerBprojectsBjumpingBitalyBdevelopsBcontroversialBchancesBcamerasBcameosB24BterryB
surroundedBstretchBstepsBpennBfieldsBdestructionBbayBsignsBrageBnotchB	intentionB
afterwardsBwarnBrealiseBjayBgenerousBagingBwondersB	revealingB	resemblesBremovedB	producingB
overactingBmenacingBdialogsBbreastsBbeatenB1stBtoiletBtheyllBtextBtalkedBswedishBrobberyB	returningB
presumablyBhuntingB	holocaustB	graduallyB
cameraworkB	believingBadBwardB
tremendousBtradeBsubplotsBstrictlyBstoleBreducedBnurseBnB
motivationBloserBlargerBjawsBincidentBgenericBfuneralBfreakBdragsBdocBcharacterizationBunionBspecificBshellB	possessedB	miserableBmiscastBloadBhittingBhardyBelderlyBchicagoBcatholicBbucksB2006BthrillsB
representsBquaidBneighborhoodBliberalBkingsBbulletsB2005B
populationBoccursBmusicalsBhipBharderBfilmsbrBwBshadowsBnotesBlouBhireBflawlessBdigBcurtisBcrisisBcharismaticBstaffBscreamsBpreciousBpaintingBnazisBmarksBhookB	endearingBdiseaseBcruiseBcareersBspookyB	spiritualB	providingBextendedBcrackBcircusBbeneathBtaxiBtBridBreallifeBpreventBposterBplannedBmst3kBmonthBinvestigateBhypeBfighterBemilyB	continuedBconneryBcompetitionBbackdropBattachedBundoubtedlyBsurvivalBmustseeBletterBfunnybrBfacedBeveBdirectorialBdigitalBdifferencesBclownB13thBpickingBmildBjonathanBgreyBdoomedBcraigB	catherineBbirthdayBbeatsBadequateBwarrenBtonsB
terroristsBstringBscriptedBrelevantBpossibilityBmBlackedBgrandfatherBdefiniteBcampbellBbuildsB	reactionsBmatrixBjeremyBellenBdeliberatelyBwannabeBrequiresBplanningB	ourselvesBjonBidiotsBhudsonB
horrendousBcoversBcaineBarnoldB1940sBrowBreportBpassesBpackBofficialBnoiseBmouseBmakerBmagazineBfiredBeasierB	decisionsBconsequencesBbushBbleakB	attractedBarmBticketBpushBphotographedBerrorsBcraftBstanwyckBsinatraB	manhattanBkudosBexploreBetBdisplaysBcombatBcheckingBblendBbibleBannoyedB210B
unpleasantBluckilyBlegBhboBdoctorsBcrystalBruralBguideBdvdsBdancerBcorpseBbeingsBargueBworriedBpopcornBmeaninglessBlistedBknockBjetBheroicBcitizenB	broadcastBbasementB
altogetherBwickedBvastBspinBsloppyBpulpBpropsBinstallmentBevidentBdealtBdarkerB
brillianceBwreckB
performingBgentleBdrivelBcomplainBadamsB	travelingBruthB	lifestyleBengagedBdevoidBbuttonBbatB	australiaBargumentBalikeBwingBurgeBtigerB	symbolismBshineBridiculouslyBpretendB
philosophyBoccurredBiiiBgrayBexposedBcluesBabsenceBnarratorBhorridBhatesBgrabB	dialoguesBconversationsBcausingB911B0BmeritBmelB	godfatherBfearsBdiscBairplaneBpartlyBneighborBmessagesBjesseBisolatedBironBimplausibleBfittingBderekBaccusedB	worthlessB	strangersBreachesBracialBordersBintentBfrancoB	financialBestateBentersBdemandsBcaredBsuperficialBselfishBrootBresemblanceB	overratedBlustB	landscapeB
helicopterB	appallingBtunesBsmallerBrapedBlouiseBheadedBcaptivatingBamyB
unoriginalBruthlessBonscreenBmoralityBminimalB	everyonesBdistanceB
depressionBdaringBconnectB	complaintBclumsyBbroadBareasBaffectedBtcmB	spielbergB
remarkablyBrelativeB	primarilyBnervousBinvolveB
explainingBenemiesBdynamicBdutyBchainBsubjectsB
scientificBproceedingsBkickedBillegalBeB	currentlyBcontractBcharactersbrBcardsBalltimeBahBvagueBsoulsBsinB
resolutionBpullingBpleasedBnelsonBharrisonBfurthermoreBescapedBenjoysB	elaborateBdefenseB	celluloidBwinterBthroatBpolishBnoseBmenaceBlikingBknightB
ironicallyBhonestyBhatredB	franciscoBfatalB
concerningBcomfortableBclintBbellBwatchbrB	tarantinoBsticksBspecificallyBsoleBshipsBpuppetBoscarsBnotablyBmidBmastersBkitchenBjealousBhilariouslyBbathroomB	barrymoreBanywaysBworkbrBwebsiteBwarriorBwallaceBunwatchableBstagedBsmoothBrankBowenBmodelsBirelandBholyBfrancisBblatantBvictoriaBthumbsBshowbrB	psychoticBlawrenceBkarenBintrigueBfreemanBexBenormousBdutchB
discussionBdireBchoreographyBbuttB20thBtransformationBthrewBsuperblyBstayingBsegmentsBpoliticallyBjudyBjakeB	intriguedB	inspiringBharveyBexaggeratedBconBcolourBblockBadmireBwilderBtreesBtonightB
sutherlandBsubBspareBpushedBoutcomeB	murderousBmissesBmiikeBmeasureBmaintainBlionBinteractionBfacingB	executiveB	eccentricBdivorceBdiscussB	depressedBdanielsBcontraryBconsistentlyBcoffeeBcloseupsBvirginBswearB	similarlyBschemeBpurchaseBprimaryBmaggieBconventionalBcommercialsBachievementBworldbrBshiningB
incoherentBillnessBexplicitBeightiesBeastwoodBdetailedBbakerBarrivalBaffordBwesBwalkerBtearBscorseseBrossBreunionBportionBitllBinvolvementBfailingBdoorsBdistantBdaveBchannelsBwackyBtimelessB
suspiciousB
spoilersbrBslickBsadnessBmotivesBlooselyB
homosexualBgothicBexoticBerrolB
convolutedBbirdB	vengeanceBupperBtrioBreachedBphantomBoverdoneBjuvenileB
disjointedBdiamondB	commentedBborderB2004BunfoldsBtracksB	slightestBrapBpotentiallyBpaysBiraqB
innovativeB
hollywoodsBhoffmanBhanksBgrandmotherBgialloB	disturbedB
cinderellaB	celebrityBcameronBbulletBbladeB2ndB1980BstanBreynoldsBoffendedBjamieBidealBhuhBguestsBdevotedB
deliveringBciaBchaneyBbrainsBbettyBalfredBswimmingBsplendidBrecordedB
progressesBorangeBnormanBmethodBloyalBinaneBendingsBdrawsBdignityBcriedBcommandBbenefitB60BteaB	secretaryBpitchBnearbyBmyersBmixtureBlabBdistractingB	containedBburnedBblowingBbeverlyBbebrBbandsBwidowBtieBromanBrecognitionBdorothyBamountsBunevenBtadBspellBsidneyBnotionBnotbrB
developingBbareBaudioBtaughtBstreepBspainBsolelyBreceiveBpracticeB	movementsBjennyBhughBgingerBdamageBbannedBwooBvalleyBsubtletyBsovietBsallyBrickBoddsBmitchumBlightheartedBgermansBclothingBcakeBarmedBangelaByaBtrailersBsentenceBreactB
passionateBoughtBonedimensionalB	inventiveBfingerBagreedB1999BsitsBsherlockB	seventiesBjuneB	formulaicBdrawingB
creativityBcivilizationBcaveB	buildingsBboxingBbelongBaimedBwomensBwindsBthugsB
thoughtfulBpsychiatristBprisonerB
pretendingBmileBkiddingB
influencedBfooledBetcbrBadorableBtitledBthrillB	suggestedBspringBmargaretBhBfancyBfactoryB	displayedBchallengingBwatsonBshelfBresortB
recognizedBhighestB	companionB	classicalBcalmBbeatlesBandrewsBaidB2003BwedBunintentionalBrobinsonB
nominationBjazzBignorantBexchangeBdollsB	dinosaursBdianeBdelBcliffBcentersBbrutallyBadvanceB17BtriumphBtrainedBtiesBsimilaritiesBoceanBlinkBlegalBfondBvividBthreateningBpressureB	plausibleBmaidBjewsBformsBcopiesBcitiesBbangBballsBaccompaniedB	voiceoverBstellarBsquareB
simplisticBsamuraiBproceedsBplantBphotographerBpartiesBmayorBloadsBlayBinstantBimprovedBdozensBcupBchristBarrogantBannieBwetBusefulBupsBtreatsBroomsBpoeticBpanicBnycB	comparingBvegasBsliceBseenbrBscoresB	realizingBratBpunkBphilosophicalB	performerBhoB
futuristicBexploredB
disappointB	describesBchaosBbuddiesBbiteBstinkerBsolutionB	senselessBprogressBphantasmBpagesB
irrelevantBhintsBfingersBendureBearnedBchestBbonusBbedroomBagreesBvirginiaBtrashyBreliesBpigBnamelyBmummyBhurtsBfolkB	explosionBdownhillB	dimensionBdaddyBcoherentBclaimedBcarlBbluesBwebBunitBteachingBsurvivedBslaveB	resultingBreadsBpeoplebrBmightyB
introducesBimpressBgraspBfliesBduoBdoomBdirectsBunderstatedBthickBspikeBreceivesBonlineB
journalistBegoBcriticBcoastBchickenBunderstandableBtribeBtameBshirleyBrivetingBrepresentedBquitBpieBopinionsBmorebrBlowestBhughesB	disappearBdimensionalBcrucialBboBasylumB	alongsideB3rdB	yesterdayBstaringBsnakeBslapBrebelBraymondBoverwhelmingBjustifyBinvasionBgregoryBgoodnessB	discoveryBdianaBconstructedBconfrontationBcitizensBandreB	alternateBwalkenBvaluableBtoplessBsullivanB	succeededBpushingBpantsBnormaBinappropriateBhousesBhomelessB
equivalentBduckBcringeBcleverlyBcheBchapterBbottleBarrivedBairportBsunsetBstylesBstevensBscenesbrB	representB	principalBpornoBleoBgenerationsB	fairbanksBexistedBdefeatB	dedicatedB	communistBburtonBbelaB	attitudesBaccessBabysmalBsteelBframedB	equipmentBclosedBchasedBblankB
apocalypseBweakestBvisibleBviceBvaguelyB
unsettlingBsettleBplotbrB	operationBmoodyBmarcBlyricsBliBheartbreakingBfeedBearsB
corruptionBcomposedBcasualBbutlerBbernardBtomorrowBspokeBsecondlyBsavageBsappyBruinsBrichardsBreedB	provokingBpoetryBpanBnailBjudgingBglobalBflopB	corporateBassassinBwoundBwivesBuserBtranslationBsynopsisBstinksBspreadBsellersBromeroBrisingBparentBmonkBmaniacB	educationBdevicesBbinBaustinBvisitingBspiritsBshowcaseB	nostalgiaBnonstopBinbrBhumbleB
enterpriseBeditionBcountrysideBconcernB	charlotteBalternativeByellowB
unbearableB	neighborsBlipsBinferiorBimproveBconservativeB	climacticBcinematographerBattorneyBtriangleBsurvivorBnedBmurrayBmotivationsB	imitationBhollowBexceptionallyBdesperationBdesiredBdammeBconsiderableBcomfortBchoreographedBcarreyBcandymanBantsB	alcoholicBtroopsBtimothyBtastesBstoogesBshirtBpsychicBobjectsBnicholasBmasonBkickingBhideousBcatchingB	carpenterB2002BtwelveBtrailBteamsBratsBpotBmistakenBfaultsBdropsBdisappearedBchoosesBcatchesBboldBassignedBwrappedBwizardBtongueBtempleBsuitedBsensesBquestionableBparallelBorsonBoccasionB	nostalgicBmontageBfestB	conflictsBbusterBavoidedB2007B	witnessedBweaknessBtwilightB	survivingB	superheroBstraightforwardBseeksBrottenBlolBlitBjoinsBincompetentB	forbiddenBcarefulBberlinBbaldwinB	satisfiedBrouteBremainedBpromisesB
popularityBpatienceB
nightmaresBmtvBmatthauB	immenselyBhollyBfoulBfoughtBdinosaurBcureBcrushB
conditionsBclosestBcircleB1000BworeB
underlyingBtoysBthompsonBspeciesBpromisedBpaintedBmiracleBhandedBembarrassmentBdemonstratesBcoxB
cassavetesBadvertisingBwishedB
vulnerableB
transitionBtracyBteachersBstiltedBquotesBpursuitBplightBmorrisBmankindBimaginedBhostageBgloriousBfixBcombineBtankBseverelyBsafetyBpurpleB
punishmentBphotoBmundaneB	masterfulBlikewiseBkingdomBhuntersBguessedBengageBeasternBbronsonBblondBblewB	alexanderBaidsBsecretlyBresponsibilityBresidentBrelyBphillipsB	mountainsBmoreoverBlennyB	laughablyBkeithBhistoricallyBeatenBdepictsBcorpsesB70BworkerB
terminatorBshakeBsatisfyBroofBpropertyBopportunitiesBnationsBlettersBjeffreyBinexplicablyB	grotesqueBelegantBeaseBdevilsBcontestBconstructionBconanB	comediansBappreciationBwoundedB	witnessesBwildlyBvisitsBsuitableBriotBracingBpossibilitiesBlawsBlanceBjoinedBinvestigatingBgiftedBdancesBclaudeBcheatingBblastBtiresomeBthruBtendsBskullB	secondaryBrootsBrequireBreplaceBprizeBpeakB	nicholsonBfrequentBduvallBbullBamericasBwaitressBwaitedBstarkBsinkBscenebrBrubberBoffbrBnutsB	mysteriesBgreedBfocusingB	fashionedBdatingBcluelessBclassesBcancerBbrooklynBadoptedBaboutbrBtenderBswitchBsincereBsharesBraisesBphotosBniroBmutantBmetaphorBmateB	madefortvBleighBlastedBjoeyB	excessiveB
destroyingBbusinessmanBbuffBbehaveBawakeBallensBalcoholB	affectionBwealthBremadeB	recordingBreachingBraisingBmethodsBmarioBjoshBgrahamBemphasisBeagerBdesiresB	capturingBbumblingBbetrayalB	authorityB
artificialBviewingsBunhappyBunawareBsunshineBsportBsendingBrussiaB
horrifyingBguardsB	greatnessB
disappearsBcharacterbrBbambiBastonishingBaprilBaffectB
underneathBretiredB	purchasedB	policemanBpalBleonardBfloridaBelvisBdistinctBcinemasBcheatedBbootBapeBalertBabusiveBversusBscopeBrevolutionaryBrenoBrandyBrandomlyBkapoorBhankBgableBfrustratingB
expositionBdefendBburkeBwithbrBwishingBtownsBstonesBsagaB
reflectionB	pricelessBpopsBplanesBpirateBperryBpartnersBmoronicBmilkBlimitsBlegendsBleadersBlandsBheistBheartwarmingBhackBgirlfriendsBfloatingBfedBdementedBdemandBcreamBcheckedBbrandoBbenjaminBbeliefsBalbumBstandoutBsethBpotterBnonsensicalB	marketingBliftedBlatelyBingredientsBhopelessBhomosexualityBfrustrationBdespairB
confidenceB	commanderBcolinBbergmanBbabeBadviseB35BsurfingBsmilingBsignificanceBservingBrabbitBpiratesBmachinesBjuniorB	illogicalBheadingB	correctlyBbunnyBbuffsBbackgroundsB	ambiguousBwrongbrB	wanderingBtunnelBsueBstaleB	resourcesBreflectBpunBmiseryBloadedBlengthyBlaurelBholdenBedgarBconvincinglyB
consistentB	carradineBborisBaweBarguablyBadvancedB19thB1990sBturkishBtransferBtimesbrBtechnicolorBsuspendBseriesbrBrocketBreevesBrecommendationB	nightclubBminimumBgriffithBeducationalBdaBcueBcrowBcostarBchildsBchicksBblacksBbitchBamazonBabusedB	trademarkBteachesB	respectedB	relationsB	primitiveBphilBminorityBlivelyBimprovementBgreedyBdirtBcomposerBassaultBstressBsharedBscreenbrBrelationBpreyBpreviewBpearlB	occasionsBnathanBmarsBintentionallyBeugeneB
controlledBcampaignB	behaviourB	wellknownBvulgarBsevereBschoolsBquentinBprepareBpBnooneBmatchesBlosersBinsanityB
fascinatedBfamilysBernestBcontinuallyBappropriatelyBwritesBwasbrB
undercoverBseldomBrompB	rochesterBquietlyBoliviaBmerylBkyleBhopperB
hitchcocksBflairB	courtroomBcliveBbonesBassumedBaffairsBwrapBwhollyBunpredictableBtravestyBthievesB	slaughterBsandraBsamuelBremakesBmasterpiecesBloneBhamletB
detectivesB	compelledBcannonBboneB
additionalBwifesBwarmthBvisionsBunseenBtrackingBtackyBsubparBshowdownBphraseB	musiciansBinvitedB	householdBglennBglassesB
frightenedBfirstlyBexplorationBearlBclockBaliBwidelyBwakesBvBstoresBscreenwritersBremarksB	relativesBreelBpassableBownedBmuchbrB
lacklusterBjerseyBinsipidBeitherbrBdustBdriveinBdrearyBdoubtsB
derivativeBcrashesBclicheBastaireBassassinationBagencyB3000BveinBunfairBsufficeBrolledBphonyB	murderingBmessedBmentionsB	interestsBhalBfuryBfasterBdebateBadaptationsB
activitiesBwellwrittenBsimultaneouslyB	sillinessBrebeccaBrandolphBoutfitBoffbeatBmythBlatinBkayBindianaBguitarB
enthusiasmBelectricBderangedB	backwardsBaimBaccuracyBabsentByellingBunexplainedB	supportedBsharonBpolishedBnowbrBmonkeysBmistressBminiB
mentioningBlockBintimateBdorisBcusackBbombsBtowerBtobrBspiderB	speciallyBpuzzleB	minutesbrBmapBjillBinterestinglyBhokeyBfemalesBdisguiseBdiehardBdancersBcloseupB	admirableBaceB1968B	unlikableB	threatensB	subjectedBstuartBstaticBsketchBrudeBridesBregardsBpettyB
lonelinessB
literatureBlindsayBexploresBevaBcrazedBclosetBchristieB	brutalityBawhileB1984BwellsBtripeB	spaghettiBregardedBperformsBoverlookBneuroticBjoelB
inevitablyB
hopelesslyBfiringBfavourBempathyBdarnBculturesBclipBchampionB
challengesBagendaB1997B
widescreenBtrendB	testamentBsungBshoreBshakyB	screwballBromeB
respectiveBrefuseBparadiseB	immediateB	guaranteeBglowingBexperimentsBexperimentalB
definitionB
complaintsBcolonelBblakeBbiasedBtrafficBsometimeBslavesBsixtiesBreferredB	prominentBpg13B
misleadingBmadebrBincomprehensibleBhornyBcreditedBcomplainingBbloomB	wrestlingBwornBrisesBreleasesBreflectsBprequelBpennyBmonksBmodeBmeritsBliteraryBlemmonB	inabilityBhumphreyBhearsBfrancesBfirmBensuesBdisneysBdiscoveringB	depictingBcoleBcastsBangstBweatherBsupplyBstickingBsoftcoreBrecordsBpantherBolivierBmarriesBlionelBinvitesBinventedB	integrityBfiftyBfeverBcoBbilledBvisitedBsoccerBshoddyBrexBrepresentationBplacebrBpitBoptionBmegBmayhemBmanipulativeBmallBloyBlinersBknockedBimmatureBhighwayBexpenseBearnBdifferentlyBdeafBcountsB	conceivedB
compassionB1970BwisdomBwardrobeBvictoryBunfoldBstillerBpreachyB
obligatoryBnewlyBnephewBhippieBgangsBexorcistBemperorBdidbrBdesignsBdeceasedBdaybrBcoincidenceBborrowedBanywaybrByearsbrBvoyageBuncutBsleazeB
presentingB	preciselyBmusicianBmorallyBmiamiBleesBlabelBheightsBgorillaBfistBdefinedBcostelloBconceptsBcarrieBwavesBsparkBsignedBscrewedBquinnBpostedBozBminusBmindedB
middleagedBlastsBjumpedB	introduceBhealthyBhayesBgoldieBedwardsB
directionsBdentistB	crocodileBcormanB
convictionB
christiansBbowBawfullyB23BwretchedB
underworldBthingbrBtagBsumsB
simplicityBscoobyBroommateB	repeatingB	reluctantBrecognizableBpackageBnicoleBnatalieBmoralsBmarshallBliftB	hackneyedBfishingBeternalBendingbrBdeletedBcompeteB75B1973B
threatenedBstagesBscreensBsaleBresistBredeemBratesB
phenomenalBpatriciaBorderedB	objectiveBmuseumBmarvelBkubrickBkennethBfortyBforcingB	exploringB	evolutionBdrinksB	disgustedB
discussingBdemiseBboomBbirdsBattendBarcBantonioBamusedBallanBwongBtromaB
sympathizeBpremiereBpatientsBparadeBoutingBmalesBhistoricBhestonBhartBhammyBgarnerBflagBfillerBfemmeBexposeBentitledB
engrossingBdisgustBdifficultiesBdealerBcomedybrBcatchyBauthoritiesB	acclaimedB1998B1987B
underwaterBstrandedBsopranosBrothBrearBrealmBqB	publicityBproBpitifulBpierceBnailsBmixingBjuanBinteractionsBinsistsBhungBhulkBhersBgriefBgoldbergBcubeB	computersBbrosBbravoBbewareBbelleB
audiencebrBassumingB	amusementBalleyBacidB1989B	upliftingBtravoltaBtopnotchBtopicsBsuburbanBsquadBsharksBrodBresembleBracesB	misguidedBmachoBlesBiconBhugelyBheightBhamiltonBfascinationBedgyBdudBdressesBdolphB	disguisedBburstBbalancedB	attackingB1939BvainBsixthBshoutingBshootoutBsandBrefusedBrefersBrangersB
positivelyBpalaceBownsBmuddledBminsB	insuranceBgemsBfiftiesBexpectsBdobrB	companiesBclaustrophobicBbollBbatesB1993BwouldbeBthrilledBscottishBnativesBmuslimBmarilynBlocalsBkeenB	ignoranceBfrightBfogBdragonsB
difficultyBdanishBchargedBcgBbathBaddressBactingbrBzBstairsBsoloBsneakBsimpsonsBscotlandBrightbrBremoveBpurposesB	preferredBorleansBmadonnaBlaborBinexplicableBfunbrBelephantBdistributionBdestinyBdatesB
conscienceBcharliesB
challengedBbutcherBbesideBbabiesBaspiringBaccountsBabruptB1969BunexpectedlyBsharingB	septemberBrejectedBpoemBkissingBinfectedBhelloBdwarfBauthenticityB
adolescentBsketchesBshortcomingsB	residentsB	renditionBreferB	prejudiceBpredecessorBnorthernBlastingBhannahB	generatedBfrozenBfreddieBflowersBfirmlyBexposureBenteringBelmBdestroysBdamnedBcreatorBclaimingBwalshBvitalBsubtlyB	stretchedBspiritedBsoundingBslaterBshakingB	referringBprostitutionB	profanityBpilotsB
passengersBoverlongBoldestB
mechanicalBmassesBloyaltyBlifelessBlegacyBlanaBincidentallyB	heartfeltB	glamorousBgibsonBgalBfuriousBdougBdesertedBcubaBbradyB
basketballBbargainBbanalBaxeBambitionBamandaB1985B1979BtroublesBthoughtprovokingBstumbledBsteamBsplatterBsnipesBservantBpeculiarBparanoidBminBjacketBitemsBhungryBhookerBhelplessBhawnBhangsBgarboB
continuingBcomparisonsBcenteredBattractB1992B1972BwrightBvehiclesBunBtokenBrootingBplayboyB
phenomenonBmollyBlilyBjordanBiqB
indicationBherosBgrabsBgloverB
exceptionsBeditBdroppingBdressingBdameBcopiedB
accomplishBabcBvoicedBventureB
translatedBtailB
suspensionBstumblesBpunchesBnoisesBkarateBhandheldBgoldblumBgatherBfeastBdysfunctionalBdroveBbudB	breakfastBbountyB	blatantlyB	biographyBbeowulfB	befriendsBautomaticallyBanyonesBwhinyBwarmingBshiftBromeoB
richardsonBregionB	partiallyBoutfitsBnyBjanBinjuredBinhabitantsB	identicalBhomesBhaplessBgrainyBgainedBfoolishB	expressedB
expeditionBdoseBdislikedBdeniroBcrueltyBcoupledBcollinsBchuckleBcheerBcapitalBawaybrB
accuratelyB1995BwheresBwhaleBsingersB	satiricalBropeBriderBrerunsBpacificBnormBnerdBmutualBmasksBmarkedBkungfuBinconsistentBgamblingB	fantasiesB	establishB	endlesslyB	dependingBdemonicBcreepBcloneBcdBbrosnanB1978BwidmarkBwarriorsBvotedBvolumeBtemptedBtapBswearingBsteadyBritaBresolvedBreignBrecycledBpossessBpaleB
noteworthyBmillBmaeBjudgmentB	ingeniousBheavyhandedBgoodbyeBelevatorBeatsBdonnaBdesignerBdenzelBclichesBbugBbmoviesBauthorsBansweredB1990BwesleyB	victorianBuweBstunnedB	strongestBstalloneB	spectacleBrussiansBprestonBpathosB
lieutenantBkurosawaBivBhilarityBgreatbrBgateBeconomicBdickensB
describingBdescentBcrispBconsiderablyB
complexityBcombinesBcoffinBcampusBbridgetBbarsBbanksBangusBacceptsBwindowsBwelldoneBvanessaBumaBtriviaBrepliesBpostersBnorrisBnewmanBmodestBministerBmedievalBinternalB
insightfulBhelpfulB	gentlemanBgearBfluffBflashyBfiB	encourageBemergesB
containingBconfessBcemeteryBbeholdBashleyBapesBadoreB	addictionBtransformedBsymbolicBsurvivesBsunnyBshtBshouldveBsciB	scenariosB	realitiesBpursuedBprovocativeBparanoiaBotherbrB
motorcycleBmatchedB
mannerismsBlifesBkidnapBhatsBhandlingBhandlesB
guaranteedBgimmickBfunctionB	evidentlyBdomesticBdiscoB
disastrousBdamagedBcravenB
confrontedB	confidentBclanBcaliberBbourneBbanterB1976BwigBunrealB
togetherbrBtacticsBswimBsteerBrespectableBpeteB	paramountBjulianBitiBgenderB	exquisiteB	explosiveBdubiousBdisgraceBcostarsBconveysBconventionsBconvenientlyB
contributeB
caricatureBboastsBbarrelB200B1994ByardBwillieBvotesBshorterBscrewBrippingBrickyB
relentlessBpuppetsBprovingBpgBpassageBoldfashionedBmarxB
landscapesBkansasBimpliedBharmlessBerrorBdestinedBbluntBauditionBactiveBabruptlyB99B1920sBwaltBvastlyBunsuspectingBuniformBshieldsBseemingBsafelyBquarterB
psychologyBobrienBmuteBlonBloisBleatherBkoreaBjustinBjewelBintroducingBhitmanBhindiBheartedBgloriaBemergeBdreckBdilemmaBdelightfullyBcrashingBconfrontBbullyBbernieBbeeByouthfulBwoundsBveteransBslipB	shouldersB	repulsiveBrampageBprostitutesBpointbrBpauseB
noticeableBmysteriouslyBmortalBmorbidBmalcolmBlongingBlethalBinstinctBhootBgroundsBdiverseB
despicableBcycleB	cigaretteBchampionshipBbridgesB	brainlessBbethBalecB	absurdityBaboardB1988B1986B
wildernessBwhoreBvintageB
unlikeableB	uniformlyBslyBshearerBrolebrB
restrainedBrealisedBrapidlyBramboB	publishedBproducesBplottingBownersBnervesBnemesisB	lightningBleonBinteractBglaringBgatesBfoolsBfeelgoodBexteriorBexploitBdistrictB	convincesB
conventionBconnectionsBbeforebrBapproachingBaircraftB2008BwintersBsubsequentlyBstumbleBstoppingB	startlingBsomedayBsmilesB	sentimentBpreviewsBpatternBoharaBnieceBnerveB
misfortuneBmartyBmarthaBlimitBintroBhockeyBharmBhackmanBgregBexaminationBelsebrBeliteBduelBcoreyBconsiderationBcollectBchoppyBbeattyBanalysisB1974BwellmadeBwandersBsurgeryBsubstantialBstudyingBstorysBsidBshocksB	separatedBselectedBsaintBpromoteBpreposterousB
portrayalsBpointingB	organizedBninjasBlandingBjulietB	instancesBflashesBfifthBescapingB	energeticBdivorcedBdepthsB	demandingB
commitmentBclassyBbreedBboringbrBakinB
aggressiveB28ByearoldBtipB	terrifiedBsweptBsubtextBstudiesBshoulderBshelleyBsensebrBrainyBquestioningBpamelaBmildredBlushBlasBjealousyBinsultsBhepburnBfisherBexploitsB	deservingBconcentrateBcommunicateBbroodingBbellyBadrianBabbottBwingsB
weaknessesBtroyBtapedBswingBsundanceB
suggestionBsourcesBshadesBseniorBseebrBronaldBrhythmBragingBpryorBpraisedBperformancebrB	obsessiveBmcqueenBlunchB
legitimateBlaurenBkathyBitemBhoganBhidesBhamBgluedBghettoBfillingBfamilybrBeducatedBearBdeeBcopeB	christineBbratBbostonBaudreyBarrestBabortionBwireB	unrelatedBunderstandsBtopsBswampBreBpoisonBoutdatedBominousBnodBnbcBmoronsB	monologueBlongtimeBlimitationsBjarringBinstitutionBguruBdubBcrossingBcouchBchristianityBchillsBcaricaturesBbulkBbillsBalteredB1971BwanderBvoodooBvisitorBunsatisfyingBtokyoBthurmanB	tastelessBstandupB	spidermanBsobrBshiftsBsergeantBrespectivelyBpostwarBpetersB	parallelsBoriginBmessyBgarlandBfreakyBforrestBfelixB
emmanuelleBdaisyBcontestantsBballetBattendedBanticipationB
acceptanceBtierneyBtaxBsuckerB	streisandBshawBseedyBringuBrenderedBpushesBprogramsBpredictBphonesBowesBoriginsBmaloneBmadmanBlicenseBlayersB
kidnappingBjolieBinspireB
imaginableBherbertBgeorgesBflipBfarmerBelsesBdeterminationBderBdenyB
colleaguesBcattleBbuysBbeltBaltmanB	afterwardBadditionallyByoungestBwitchesBwineBwartimeBtorontoBtitularBtapesBsymbolBsurroundingsBsternBslimyBservicesBrestoredBreportsBqueensBprayBpleasingBnaughtyBmumBmooresBjohnsBjadedBjacquesB	isolationBinspirationalBheatherBgoodsBgeekBfiancéBenBeggBdylanBdramaticallyBcabaretBartsyBarthouseB1977BwiderBwashedBunsympatheticBunimaginativeBtossedBspockBsnlBsimpsonBsergioBriceBrescuedBrememberingBraveBmcdowellBmarineBmangaBgratefulBgodawfulBexcessBexcellentlyBdumpedBdanteBcushingB
commentingBclerkBbetrayedB
astoundingB
approachesB95B4thBtoolBtakashiBsuitablyBskilledBseymourB	selectionB	sarcasticBradicalBpoliticiansBpcBpaltrowBolBmelissaBmealBmadsenBmacabreBmacBkeysBjulietteBintentionalB
inaccurateBimmortalBimmenseBgripBgeorgiaBfeatBeverettBdismalB	deliciousBdeathbrBcountyB	consciousB	clockworkBcanceledBbtwBbillieBbikeBberryBbennyBbeggingB	associateBactivityB1996B1932BvinnieBusersBundeadBultraBtrialsBtrainsBthoughbrBstringsBspreeB	sincerelyBshotgunBrollsBrobbinsBpromptlyB
professionB
pedestrianBneroBnarrowBmccarthyBmarionBlastlyBhustonBgutBgiganticB	fortunateBexperiencingBdrakeBdivineB	departureBdakotaBcounterBcontributionBbattlingBapplaudB1983BtoxicBsydneyBstabbedBsmellBsiblingsBsheepBscriptbrBredneckB
psychopathBpalmaBmotiveBmiaBmanbrBlocatedB
highlanderBherculesBgrantsBfullerBfayBexplodeBemployedBdownbrBdonebrBdiaryBdefeatedB	decidedlyBcrookedBcoatBchorusBcharmsBbabesB
advertisedB	acceptingBxfilesBvileBupcomingBunderdevelopedBsmarterBsenatorBsanityB	salvationBrobbersBrobbedBrelaxB	receivingBrealisticallyBreadersBpocketBoutrightB	originalsBnutBmindbrBlovebrBliamBletdownBlandedBkarlBinmatesBgrudgeBfeministBdependsBdebtBdeadpanBdafoeBcrackingBcoppolaBbuffaloBbentB20sBwhereverBupdatedBtendencyBstylizedB
statementsBstalkingBslaveryB	repressedBreminderBopenlyBmotelB	mentalityBlombardBlaurenceBitselfbrBindependenceBgloomyBflavorBeventualBdustinBdanaB	completedBcheekBcareyBcampsB	breakdownBbittersweetBbackbrBaugustBarizonaB	argumentsBaddictedBBvirtualBsylviaBstareBsleepsBshelterBsheenB	reviewingBpeckBpayoffBnopeBmontyBlendsBlendBlangeBiconicBhydeBhippiesBhabitBfollowupBflowerBfactorsBdumberBdoyleB
deliberateBcomebackBcarnageBcapBbitesBavidBasiaBamitabhBalterB1991B10brBupdateB
unfamiliarBsterlingBstardomBsorelyBsophieBsoonerBresumeBrespectsBreliableBpokemonB	paintingsBnewerBnetBmannersBlowkeyBlooneyBknowbrBkeitelBjacksBfrankieBeverbrBenduringBegyptianBdumpBdepictBcowBcostnerBcorporationBcommitsBcladBcindyBchillBbishopBatlantisB21stB1950BwhiningBvocalBsinkingBsaraBsandyB	regularlyB
protectionBpeacefulBmisunderstoodBmidstBmickBmercyBmeantimeBjuryBjawBinsightsBinformedB	imaginaryBignoringB
hellraiserBgroundbreakingBgarageBfeedingB	fastpacedBfadeB	exploitedBexitBenteredBconveyedBconcludeBbwBbudgetsBbreastBbillingBashBagathaB	affectingBadmitsB1940BwiselyBviolinBverdictBtongueincheekBtonBsugarBstartersBsoupBsosoBsomethingbrBsmashBskitsBsfBschlockBscarlettBrunnerBrollerB	programmeB	principleBpretendsB
possessionBposingBposeBploddingBphillipBparodiesBneatlyB
moviegoersBmailBlavishB	intricateBhystericallyBhybridB	hungarianBglimpsesBfranklinBensureBdynamiteBdisorderBdcB	customersBcurlyB	criticizeBcolumbiaBchowBchoosingBchessBcharacterizationsB	cannibalsBbachelorBartworkBappleBanyhowBaidedB	aestheticB1981B1975BtylerBtrampB	tormentedBtombBtaBswallowB
stunninglyBstudiedB	offscreenBmysticalBmensBinterviewedBinherentBgraduateBforgivenBforbrBfiancéeBelliottBdreadBdownsBdelicateB
definitiveBdecidingBcoveringBclausBcellsBbrookeBbrettBboxerBvinceB	tolerableBtheoriesB	stephanieBscratchBscoredB	scarecrowBsarandonBsaneBpursueBnoraBlincolnBidolBhiresBheelsBgeeksBfulciBfartBentiretyBdisappointedbrBdazzlingBdarrenBcrooksBconfinedB
comparableBcommunicationBclashBcherBcentreBcelebritiesBcelebrationB	candidateBbordersBbegB
approachedB1936BweakerBtowersB	terrorismBsublimeB	spaceshipBsnakesBsamanthaBrivalsBritualBritterB	remembersBranchB	orphanageBofbrBmouthsBlongestBlighterB	incorrectB	incidentsB
improbableBhooperBfiresBfelliniBexpertlyBexcruciatingBdocumentB
distractedBcleaningBcheaplyB
celebratedBbrunoBblamedBbittenBbearingBbaddiesB	assembledBaddictB
accessibleB	absorbingBwhitesBwaitsBvibrantBupbeatBtrivialBtraitsBsupremeB	submarineBstatingBsleptBscariestBsatanicBroundsB	patrioticBparkingB	neglectedBmesmerizingBlensBleapBknockingBiraBindicateBhowlingBglanceBfuelBfodderBflimsyBfacilityBeternityBenhancedBdurationBdumbestBdistractionBdevastatingBdeclineBcreekB	convictedB
complimentBclerksB
chroniclesBcbsB
cartoonishBassureB	anthologyBallyBabandonB1982B1933B19BBwarnersBtrollBtraceBtherapyB	suspicionBstreamBshanghaiBrewardedB
resistanceBreaderB
perceptionBnoveltyBmonroeBmatesBluisBjobbrBjarBhavocB	harrowingBgremlinsBgenrebrBgalleryBehBedieBcrownBcriesBcontroversyBclimbingBchucklesBcharacteristicsBcarnivalBbranaghBbettieBavoidingB	attendingBapplyBangelinaBachievesB300BtvsBticketsBthreadBstrokeBspeechesBsoughtBsessionBscroogeBscottsB
resemblingBreplacementB	redundantBrangerBpolicyBphysicsBnivenBmillionaireBmiceB
industrialBhostileBformedB
forgettingBfeebleBfederalBexperiencebrBexpectationBethnicBengineBeggsBdanesBclimbBcastbrBcasinoBbikerBbeardB	aftermathB85B22B
witnessingBwheelBwendyBtripleBswingingBspencerBsimmonsBsensibleBschemingBrupertBroundedB	relevanceBrehashBprehistoricBpredictablyBpimpB	pervertedBpansB	overblownBorganizationBnetflixBmoneybrBlesbiansBlegionBkumarB	justifiedBjudgedBinfoBhopefulBframesBfillsB	estrangedBdinerB	christinaBblessedBassuredB	admissionB21ByoursB	wellactedBwangBswordsB
suggestingBstrictBstrayBspiceBsettledBroughlyB	releasingB	reflectedBrecoverB	preparingBpalmBordealBmyrnaBmoviemakingBmobsterBlaunchB	lancasterBkathleenBinvestedBinterruptedBinfluentialBheadacheBfundingBfightersBdebbieBcritiqueBcontrollingBcontributedB	combiningB
collectiveB	colleagueBcolbertBblobB
antagonistB–BwaynesBvomitB
undeniablyB	throughbrBthirtiesB	suspectedBstakeBspanB	sickeningBservantsBsensualBsatisfactionBsangBsaltBroutinesBrealizationBprofitBpivotalBpayneBpalanceBpacksBoutlawBnovelistBmaskedBmartinsBlimpBlevyBjoiningBintactBimoBharrietBgrowthBgadgetBfluidB	firstrateB	festivalsBencounteredBdooBdemonstratedBdemonstrateB	curiouslyBconfuseBcoloursB	centuriesBbuffyBbrentBbonnieBbestbrBbenefitsBandreaBaffectsBaaronB48BveronicaBswitchedB
sufficientBstuffbrBstationsBsnoopyBseriousnessBsellsBseatsBsalmanB
sacrificesBripsB
principalsBpoundB
optimisticBnoticesBnominationsBnewcomerBmusicbrBmedicineBinvestigatorB	intenselyBinjuryBignoresBhuntedBgenerateB	gatheringBgapB	furnitureBfreakingBenidBenhanceBemployeeBembarrassinglyBearnestB	dominatedBdisappearanceBdaysbrB
cronenbergBcountingBchefBcentsBburntBbegsBarrayBarkinBamidstB
wheelchairB
watchingbrBwashBwardenB	variationBvalidB	unnaturalBtouristBsonnyBsmithsBseveredBsaifB	remindingBpythonBpoppingBplateBpistolBphotographsBpaintsBownbrBnunBnolanBnannyBmoronBmeltingBlynnBltBkittyBizoBinterestingbrB	inclusionB	himselfbrB
farfetchedBexpertsBdrabBalotBallegedBafricanamericanBabominationB	worldwideBwallyBupsideBunsureBunstableBuniformsBuhBughBtruthsBsurgeonBstereotypedBselfindulgentBruiningB	rewardingBrewardBrelentlesslyBreadilyBpalsBoverbrBmelodyBlorettaBlinearBlikebrBlesterBkeatonsBjulyBjaggerBjacksonsB	housewifeBgrassBgoalsBfalkB	explodingBethanBerBenoughbrBdownfallBdistractBdietrichBdestinationBdamonBcontemptBcaperBbustBbaconBalaBaboundBwagnerBvividlyBvalB
unansweredBswitchesBspinningBspiesBsparksBsmugBslimBslappedBshoutBseduceBseBrubyBrifleBpromotedB	practicalB	possessesB
politicianBpianistBpaulaBparticipateBoverusedBmaureenBmagnumBlurkingBlolaBlinkedBlibertyB	inventionBinformativeBhopBhkBgolfBglobeBginaBevokeBentriesBenthusiasticBembraceBelevenB	discussedBdeedsBcrippledBcookingBcocaineBchopsBcassidyBcapacityB	breathingBboobsBbitingBappealsB1959BwtfBvolcanoBverbalB
similarityB
shockinglyBshawnBseinfeldBseedBsalesmanBrejectsBporkyB	populatedBoccupiedBnarratedB	murderersBmichaelsBlizBlaysBjudgesBinteriorBinsertBinchBflowsBfilthyBexplodesB	employeesBelliotBdriversBdeputyB	delightedBcursedBcunningBcubanBcrapbrBcourtesyBcoolestBconsistB	confrontsBcomaB	collectorBcirclesBcabBbuzzBbrodyBbowlBbinocheB
assignmentBadvancesByawnB	viewpointBvaughnBuneasyBtrevorBtransportedB	transportBtoothBtoniBthailandBtechBsweatBsuaveBstuffedBspitBslideBskinnyBsensationalBrousingBrogueBreviewedBresolveB
rebelliousBprofileBpoleBpaulieBoverbearingBoffendB	obliviousBmuslimsBmirrorsB
meanderingBmarryingBmannB	maintainsBmaBkissesB	katherineBjessBirvingBinsistBhelmBfritzBentranceBebertBduBdividedBcornB
convenientBconcentrationBclichedBbasketBaussieBatticBarabB	vigilanteBupstairsBturtlesBtodaybrBsustainBsportingBsolvedBsinsBshoppingB	semblanceBsalvageBrooneyBothersbrB	mythologyBmockBmidgetBmasterfullyBmarcusBliteralBkBjockB	impendingBhooksBglossyBgabrielBfugitiveBeyreBeffortlesslyBedmundBdraggingBdespiseB
denouementBdawsonBcounterpartsBconclusionsB
commandingB
cigarettesBchamberB
captivatedB	camcorderBblockbustersBawaitingBanticipatedBangieB
admirationBacquiredB1934BvoidBvapidBtheatresB	superstarBstripperBstalkerB	shamelessBsadakoBreverseBrepresentativeBraidBpuppyBpizzaBperverseBpaycheckBnunsBmixesBmentorBlohanBlistenedBlayingBjanetB	horrifiedBhomerBheavensBharlowBgesturesBgerardBgeeBfuzzyB
foundationBcracksBcontributesB	considersBconsBcinemabrBcheeringBbrendaBbegunBbeeryBbattlefieldBauraBanytimeBanguishBaimingBwweBunderstatementBtormentBtightlyB
substituteB	strengthsBstrainBstatueBstagingBstabB	sentencesBsailorBrealisesBprotestB
progressedBpreciseBpoppedBpoirotBninaBnerdyBmondayBlikeableBliedBladderBindifferentBhandicappedBgrandmaBgilbertBgeneralsBgarfieldBforemostBfordsBfarrellBfactualBexcusesB	enigmaticBebayBeagleBdvdbrBduncanBdukesBdrownedBdeliciouslyBdegreesB	crawfordsBconsequentlyBcheersBchaoticB
casablancaBcaroleBcarmenBbuscemiBbaldBbacksBautoBalisonBagonyBadmiredBactorsbrBwastesBunattractiveBtuckerBtonesB	toleranceB	sincerityB	seductionBscheduleBqualifyB
profoundlyBpressedBpompousBplantsBpbsBparksBoutlineBoldsBmaximumB	magicallyBmadameBlundgrenBlangBknocksBkentBjoshuaB	immigrantBgreatsBgreaseBgoodmanBfranticBfadedBentertainingbrBemailBdodgeBdisabledBcrossedB
complainedBboyleBbiblicalB	backstoryBadaptByepB	underusedBtunedB	translateBtalkyBtabooBstellaBspoilingBspellingBscarfaceBsandersBrukhBpoundsBpenisB	passengerBomenBnoahBnetworksBmontanaBmodestyBmeredithBislandsBinsultedBinsertedBinnBinformsB
immigrantsB	historybrBharborBgriffinBgarciaBfreezeBexcruciatinglyB
displayingBdetractBdemilleBdefineBdaviesBdaffyBcohenBclickBclayBcautionBcanyonBbrickBatrocityBarguingBargentoB	anonymousBadmittedBww2B
witchcraftBwagonBvetBvariedB
soderberghBsnappyBshinyBshahBscandalBrkoBreunitedBreservedBrampantBpokerBmonicaBlipBliarB	languagesB
infinitelyBheroinBhaydenBgoodlookingBgandhiBevansBestablishingBdjangoBdadsBchristyBchadBbasilBbarkerBavoidsBarrivingBanxiousBampleBadsBzanyBworsebrBunclearBturdBtubeBtoolsBthugBsteeleBssBspiralBsensibilitiesBremarkBproductsB	permanentBpenelopeBoutdoorB	operatingBolsenBollieBoffenseB
montgomeryBmidlerB
liveactionBjointBillusionB
identifiedBhoneyBhomicideBgrislyBgapsBfortBflameBfixedB	finishingB	extensiveB
executivesBdashingBcircaBchevyBcarlosB	cameramanBbuildupBbastardBbacallBaptB
youngstersBwarrantBvergeB	unwillingBtremendouslyBthaiBsurfBsonicBskitB	shootoutsBsexistBsecureBseasonedBresultedBreggieBoctoberBobtainBmutantsBmobstersBmanipulationBliuB	libertiesBlaterbrBjoseBisraelB	irritatedBiranBinadvertentlyBhunkB
highschoolBhallmarkBfanaticBeconomyBdistressBcyborgBcrossesBcrawlBconfederateBcerebralBcandleBbelievabilityBartyBaliciaB1931BweeklyBtobyBthrustBsubwayBstackBsororityBsitcomsBserialsBsentimentalityBsensitivityBrepeatsBrecipeBrationalBrathboneBpornographyBomarBodysseyBniftyBmomentumBlaserBknightsBjediBingridB
increasingBincestBhurryBgalaxyBfruitBflickbrBfleshedBdaltonBcrosbyBcomplicationsB
boundariesBawfulbrB	ambiguityB1935BwhybrBwhoopiBwaxBwarnsB
villainousBturtleBtheftBswedenBsuppliesBsummedBsubgenreBsourBshoeBshieldBrunawayB	rodriguezBrightlyB	retellingBprosBprogrammingBotooleB	officialsBobservationBnigelBmermaidBmanicBloweBklineBjurassicBjulesBhomebrBgigB	gentlemenBgaysBfrogsBestherBenglundBedgesBdynamicsBdudleyBdistinctiveBdevotionBdeerB
comprehendBchoppedBchewingBbryanBbashingBbaronBappliedB1964ByoutubeBvoightB	vignettesBthoB
stopmotionBstapleBstabbingB	skepticalBshannonBsealBsalesBrepresentingBreaperBraniB	radiationBprofessionalsB
overweightB	overboardBottoBnuancedBnightmarishBneesonB
melancholyBmarlonBlunaticBlorenzoBkubricksBincreaseBhatingBguardianBgrabbedBfrontalBfragileBexamineB
enchantingBdodgyBcountedBconsciousnessBcompassionateBcmonB	butcheredB	brazilianBbleedingBblackandwhiteBbiasB	awarenessBarcherBanalyzeB1944ByarnB
wonderlandB	villagersBusbrBunconventionalBthroneB	telephoneBsunkBsubduedBshakespearesBshadyB	seductiveBsarcasmBsaloonBruledBripperBreluctantlyBramblingBpsycheB
protectingBpeersBpamBpairingB	norwegianBmustveBmorgueBmimicB	lookalikeBleopardBleatherfaceBkinskiBinviteBhardestBhaleBgrandsonBgovernorB	footstepsBfilmographyBfeminineBfataleBexcelsBelviraBegyptBearliestB	distortedBdimB	determineBdefiesBcroweB
compensateBchaplinsBcarsonB
carpentersBcannesBbreadBblondellBarrowBarquetteB	armstrongBaladdinBaffleckBactionbrB34BxmenBwillemBvisceralBveraB	underwearBunappealingBturmoilBthreadsBstarsbrBsmoothlyBsleepyB	signatureBscreenedB
scratchingBsacredBpsychedelicB	picturebrBpattonBpartbrBongoingBmulderBmotionsBmirandaBmarleneBleanBkathrynBjoannaB	homicidalBheapB	geraldineBgatheredBframingBflowingBflashingBfenceBfamedBelectionBdrillB
distinctlyBdistinctionBdaylightBcrashedBcousinsBcommendableBclientBboutBartistryBarebrBannoyB1954B1942BwoefullyBwipedBwhoveB	wholesomeBunnecessarilyBtransitionsBtouristsBtireBtargetsBtablesBsystemsB	sylvesterBsixteenBshaftB	sensationBscriptwriterB	schneiderB	satelliteBromancesBrenaissanceB	qualifiesBpuzzlesBpierreBphoenixBperiodsBpercentBpapersBpadB	obstaclesBmomsBmelvinBmccoyBmanipulatedBmacyBlynchsBlumetBlizaBhenchmenBgratingBghostlyBfundamentalBfulfillBfilthBfierceB
expressiveBenvyBelaineBdustyBdiggingB
criticizedBcookieBconradBcolmanBbrandonBbookbrBbogusBbackedBawryB	awakeningBassumesBassistB	ambitionsB1945B1941BvirtueB	valentineBuptightB	unfoldingB
uncreditedBumBtrapsB	transformB
tragicallyBtomsBtobeBtinaBtangoBsuckingBstraighttovideoB	spotlightB
spielbergsBspectrumB
somethingsBsnuffBskippedBrowlandsBrobbingBreeveB	reasoningBpursuingBproportionsBprogressionBproceedBpredecessorsB	pleasuresBpaxtonBpathsB
originalbrB	obscurityBmormonBmississippiBmishmashB	milestoneBmandyBkindlyBivanB	honeymoonBhometownBhauntBgobrBgenieBfaintBextremesBevokesBepicsB
electronicBearpB	disregardBdaytimeBdarthBdarklyB
courageousB	civiliansBcheaperBcainBburyBburialBborrowBboardingB
astronautsBarticleBaroundbrBappliesBamoralB	warehouseBwarbrBvicBtraumaBtherebyBthebrB	spoilerbrBsociallyBslowerBsissyBsinksB	singaporeBsignificantlyBshelvesBshapedBsassyBromerosBroddyB	robertsonBrecreateBrapistBposesBpoetBpiBperkinsBpausesBnuancesBnaschyB	motivatedBmistyB
mediocrityBmechanicBmarvinB
managementBlucBkellysBjokerBjoBjafarBinjectB
influencesB	incapableBhatefulBgymBgravesBgailBfrombrBforgetsBfoilBfleeBfarrahBexwifeBexistingB
excellenceBdistributedBcountrysBconvictBconnieB
confessionBclooneyBcharacterisationBbothersBbiopicB	arroganceBapocalypticBamelieB500BzohanByellBwakingBvcrBvaderB	traumaticBtossBthemselvesbrBspaceyBsophisticationBshockerBsheridanBshepherdBscarletB
retirementBrepublicBreportedB	rebellionBprospectB	perceivedBpattyBobserveBobservationsB	necessityBmurkyB	moustacheBmillsBmillandBmanagingBmagicianBlureBlivesbrBlionsBlightlyBlambertBintendB
imprisonedBhollandBhaywardBgusBfortiesB	followersBfacebrB
engagementBdarlingBconnorBcompositionB	competingBchewBcheadleB	boxofficeBbootsB	bloodshedBbavaBbarneyBbanderasBbachchanB
accidentalB2009BwearyB	unusuallyBuncannyBtyroneBtravisB
temptationBsyndromeB	subtitledBstrungB	stretchesBsteppedBsteamingBspineBsleeperBsitesBsfxBscarierBsanjayB	remainderBrelatesBregimeBpfeifferBoldmanBnutshellBmobileB	mercenaryBmarisaBinformBideabrBhardenedBgrannyBgereB	emphasizeB	emergencyBdowneyBdismissBdangersBdallasBcustodyBcorporationsB	chocolateBcharltonBbreatheB
borderlineBboostBboardsB
battlestarBaustenB
atrocitiesB	animatorsB	andersonsBakbarBacknowledgeBabstractB1947B	woodstockBwhipBvanityBuncoverBunconsciousBspidersBslaughteredBsightsBreverendBretrieveBrandallBrailroadBraftBpsychologistBproudlyBprologueB	pregnancyBprecodeBplayfulB	paragraphBowlB
ostensiblyBnicolasB	moderndayB
millenniumBmeyerBmamaBluxuryBlopezBlocateBlaunchedBladBkennyBisraeliBimpliesBimhoBhiphopBhesitateBgravityBflewBenduredBelderBechoB	defendingBdeadbrBconsequenceBcoloredBchopBchipBcheerfulBchatBchargesBchaptersBcampingBbombingBbloodthirstyBbikersB
beforehandBaustrianBamidBtomeiBtolerateBtemperBtaraBsuicidalBstirringBstarshipBstalkedBspoofsB	showcasesBshepardBrodneyBrisksB	revolvingBrevelationsB	replacingBrebelsBrajB
possiblebrBpoeBpennedBpairedBpainterBmovieiB
monotonousBmelanieBmadisonB	lingeringBlandmarkBlampoonBkirstenBkatieBjanitorBinsignificantB
improvisedBgillianBfashionsBfailuresBelectricityBearnsBdrowningBdoesbrBdistinguishedBdisappearingB
diabolicalBcrushedB	condemnedBcloudBcarolineBbritsBbossesBblessBbitchyBbennettBbearableBathleticBabductedBzealandBwrathBtestingBswissBsurroundB	stupidestBstanceBsqueezeBsnapsBshamefulBsaybrBreuniteB	restraintBradioactiveBprolificBpaddingBoverwhelmedB	overtonesBoutlookB	orchestraB
officiallyB
occupationBnotingBnikitaBneedingBnearestBmessingBmerryBmaintainingBlinebrBkidnapsBjudithBjuddBjedBinterpretationsB	instinctsB	inhabitedBhypedB
happeningsB
goodfellasBfinishesBfanningB	fairytaleB	efficientBdruggedBdixonBdiveBdiscussionsBderivedB
depictionsBdependBcynthiaBcrawlingBcoworkerBcosmoBcontinuouslyB
conflictedBclarkeBcircuitBceremonyBcapeBbuseyBbsBbondsB	announcedB250B1938B010BzorroBwyattBwovenBwipeBwellerBvaryingBustinovBterminalBtargetedBsweepingBstrippedBstonedBspringerBsparedBslashersBshovedB	scriptingBsatisfactoryBrosesBrewriteB
retrospectBrelaxedBreidBquintessentialBpsychologicallyBpolarBpickfordBobservedBnolteBnielsenBnealBmuppetBminnelliBmindsetBmichelBmeyersBmasseyBmarpleB
manipulateB	magazinesBlabeledBkidmanBkellerB	interiorsB	injusticeB
incidentalBhumBgypsyBgreeceBgoodingBfryBfarrowB	expressesB	espionageBdnaBdeniseBdeedB	creationsBcockyBclaudiusBcharacteristicBcarrollBbubbleBbrassB
boyfriendsBboogieBblamesBbendBballoonBateBambianceBaimsB1967B1951BwolvesBwillyBvitusBviennaB	verhoevenBvanishedB	unleashedBunderstandablyBtriggerBtomatoesBthelmaB	switchingBswansonBstargateBstableBsighBromanianBriversBrespondBrefreshinglyB	recurringBreaganBquaintBpropB	preachingBpolanskiB
photographBperspectivesBoverwroughtBorphanBninetyBneutralBmiraculouslyBmarathonB	malkovichB	macmurrayBlennonB
laboratoryBknivesBkeeperB	intellectBindigoB
impeccableB
illustrateBhitchBhintedBguinnessBgroundedBgripsBgoofsBghastlyBfunkyBfannyBexploitativeBexclusivelyBevelynB	europeansBdreamedBdemeanorBcynicismBclunkyB	celebrateBbuttonsBbeesBbauerBawardedBapplauseBantiheroB
annoyinglyBallegoryB26BwhodBvegaB	unnervingBtorchBtheirsBsymbolsB	stylisticB
structuredBshakesBsgtBseattleBsailorsBrespondsB	renderingB	promotionBpromBpinBpickupBpassiveBparticipantsBpalmerBmustacheBmudBmoldBmarisBmaniacsBluridBlargestBlansburyBkhannaBinsecureBinsanelyBinaccuraciesB	honorableBhawkeB
hardboiledBglowBfreelyBflippingBexplanationsB
encouragedB
eliminatedBduhBdreamyBdevelopmentsBdaleBcrowdedBcontrolsB
collectingBclubsBcheatBcecilBcarlaB	caribbeanBcaesarBbutchBbucketBbrendanBbakshiB	argentinaBafghanistanB	addressedB1958B1953BvibeBtroopersBtitsBthreatsBstrainedBstirBsteamyBspelledBselfcenteredBromaniaBreasonbrBraimiBrabidB	procedureBprayerBporterBplatoonBplanetsBphaseBpenBoweB
outlandishBnotionsBneonBnashBmockeryB
mastermindB	marijuanaBlongbrBlistsBlengthsBlarsBkrisBknackBkeaneBjustificationBinconsistenciesBhiringBharlemBhappierBhansBgraysonBgrandparentsBgradyBgouldB	galacticaBfairnessBepitomeBdutiesBdummyBdudesBdualBdopeyBdirecttovideoBdiceBdeskBdeniedBdecencyBdarioBcowboysB
committingBcombsBcollapseBcokeBcoincidencesBbreakthroughBborrowsBbensonB
beckinsaleB	architectBadventurousB
adequatelyBachievementsBwatcherBvotingB
vaudevilleBunravelBunluckyBtrumanB
transformsBtilB	surpassesBstilesBstereotypingBstaresBsorrowBshatnerBsegalBsectionsBrollercoasterBrolesbrBrolandBroadsBredfordBrapingBrapidBpresidentialB	overactedBounceBoccultB	monstrousBmissileBmauriceBleapsB
instrumentBidiocyB
idealisticBhoustonBhitlersBhawkBharperBhaiBgirlbrBfratB
favouritesBfarmersBexhibitB	documentsBdivingB	dismissedBdetachedBdeemedBcrassBconwayB
censorshipBcenaBcathyBbullockB	broderickBbondageBbaleBassetBasokaBapproximatelyBanniversaryBalarmBadoptB	admirablyBabrahamB55B1957BwinnersB	weirdnessB
vietnameseB	videotapeBvengefulBuninspiringB
undertonesBtrustedB
travellingBtraitorB	throwawayBthankfulBtackleB
supportiveBspringsBslipsBshowtimeBseventhB	sentencedBscullyBsaxonBrivalryBritchieBrichlyB	recogniseBqueerBpredictabilityBplethoraBplaguedBplacingBpetersonBpeanutsBpatchBoddballBnoticingBnewtonBnavalBmuscleBmuppetsBmichealB	macdonaldBlightweightB
investmentBinvestBhungerBhostelBhareBgwenBgiftsBgamebrBfrontierBflamesBfieryBfableB
expressingBdwightBdeltaBdashBcounterpartBconnellyBcoenBcherryBblendsBbingBarrangedB
animationsB	addressesB	accompanyBabroadB93ByakuzaByBvinnyB	versionbrBtreatingB
transcendsB	tombstoneBthumbBsubmitBstupidbrBstinkBspellsB
slowmotionBskippingBshiversBselfconsciousBsamsBruggedBridersBrevivalBretainBpriestsBprayingBposeyBpokeBovertBoutrageouslyBmortonBmiikesBmidwayB
marvellousBlordsBlenaBlatinoBjunkieBjokingBjaredB	insteadbrBinclinedBidealsBiconsBhousekeeperBhmmBhiBhermanBhectorB	heartlessBhairyBgustoBgrodinBgrandpaBfawcettBexaggerationB
enthralledBentertainerB
enormouslyBengineerBemployBdrumBdiariesBdiBdesBdeliveranceBdataBdangerouslyBcrumbB
continuousB	colourfulBcohesiveBclydeB	childlikeBceciliaBcaptBcapsuleB	blackmailBbelieverBbcBbadassBassociationBartisticallyBariseBarielBapolloB	annoyanceB	announcesBakiraBagnesBadaptingBacclaimB1963BwrestlerBwillsBwhereinBunknownsBtransparentBtracesBtonedBtackedB
successionBstoicBsparseBsordidB
shoestringBsharpeBsaddestBrussBrhysBrelyingBrejectBregainBpornographicBpigsBnovakB
nauseatingBmaturityBmaritalBlubitschBkruegerBitdBirisBinterspersedB	interplayBhunkyBhmmmBhisherBhealingBharrysBhalfhourBgwynethBgrinB	graveyardBgracesBgestureBfussB
fulfillingBfiendBfetchedBeyesbrBevolvedBensueBenforcementBeditsBedithBdealersBcottonBconstraintsBconcentratedBclownsBcheerleaderBcaseyBcarlitosBbritBbombedBbertBavengeB	automaticBappalledBalonzoBalliesBakshayBagobrBactionpackedBaccompanyingB65B1949ByellsBwiresBwieldingB
werewolvesBwarfareBvulnerabilityBtripsBthatllB	sweetnessBsupportsBsupermarketBseducedBscreenplaysBschemesBriskyBrangingBpulseB	premieredB	predatorsBpostapocalypticBperilBpatrolBpaigeBoutsBmatchingBlovinglyBlonerBlinksBknoxBintendsBintegralBinsomniaBhoskinsB
hitchhikerB
historiansBhallucinationsBgundamBgovernmentsBfuturebrBfundsBfanaticsBeagerlyBdrasticallyBdisdainBdiamondsBdestructiveBdemiBdeformedBdarkestBdamBcowardBconquerBconcreteBcliffhangerBchopraBcaptiveBboozeBbillionBadrienBadaptionBaccomplishmentB	abundanceB1956ByetiBworshipBwormBtribesBtraveledBthreatenBtensionsB	temporaryBtannerBtaglineB	stumblingBsoberBslowsBsensibilityBrustyBrushesBrookieBrobbieBrewindBpunishedBpubBpsychiatricBpovBportionsBperformancesbrBpainsBoutputBorientedBopposingBninetiesBmitchBmimiBmeanspiritedBmaidenB	madeleineBlowbrowBlivBlewtonBlaurieBladdBjuiceBjudeBjanuaryBinhabitBillustratesB	hypocrisyBhowardsBheritageBheirBgoshBgoersBgiantsBgentlyBfraudBextraordinarilyBexposingBexpandedBerinB
employmentBdictatorBdesolateBdenyingBdenisBdefyBcondescendingBclosureBclarityBcharityBcasuallyBcarpetBcareerbrBbumpBbumBbrothelBbotchedBboothBbimboBbadnessB	awfulnessBatlanticB
assistanceBallstarBadministrationB1937B
williamsonBvolumesB
tournamentB	torturingB	superiorsBstrawBstampBspinalB
skillfullyBshredBsaintsBsadieBrhamesBrelatingBpuzzlingB
presidentsB	practicesBplummerBplatformBouttakesBnorwayBmuggingBmeaningsBlucilleBlocustsBleoneBleonardoBlaughtonBkalBjoesBjewBit´sBirwinB	inheritedBimplicationsB	imitatingBhenchmanBheiressBhawaiiBhahaBgreeneBgoddessBgainsBgadgetsBflemingBfewerBexistentialBepisodicB
elementaryBdunnBdriftBdreamingBdishB
directorbrBdiegoBdecemberBdeathstalkerBcurtainB	crouchingBcoupBconvictsB
companionsBclosesBcleefBclaraBcensorsBblaxploitationBbaddieBanxietyB	allegedlyBablyB1952BwwiBwidowedBwheelsButilizedB	tragediesB
traditionsB
thereafterB	thanklessBstadiumBspontaneousBshrinkBserumB	sebastianB	scatteredBsackBroboticBrioBreincarnationBreeksBrecallsBravenBraunchyBraidersBpremierBpreacherB	positivesBpixarBpiperBphiladelphiaBpepperB
paranormalBovershadowedBoutsetB	opponentsB	momentsbrBmoeBmeltBmauryBmarinesBlimbsBliftsBkinnearBjoyceBjjBintelligentlyBinnuendoBinducingBimitateB
identitiesBhysteriaBhandyBgretaBgameplayBfrogBforgivenessBfilesBfeistyBfearedBfalconBeyedBeasterB	dreamlikeBdraculasBdowntownBdownsideBdownbeatB	dominatesBdemographicBdandyBcryptBcringingBcorrectnessBconventBconfinesBclimateB	christiesBchandlerBblinkBbleedBbelushiBbaxterBbartonB	assassinsBarnieBabsorbedBvillaBvanishesBvalerieBtropicalBthirteenBthanksgivingBtautBtasksB
suggestiveBstreakBstacyBstabsB	spreadingBspawnedBslippedBrigidBrequestBrapesBradarBpotentB	policemenBpesciBpeaksBnerdsBnamBmoleBmockingBmissionsBmiguelBloopBlinesbrBlilBlifelongBkindnessB	johanssonBjodyBjamBinvestigatesBhypnoticBhumiliationBhostsBhostagesBheroismBheroinesBheflinBhappenbrBgraffitiBgossipBgeneticBgaspBfreakedBflagsBfarewellB	exchangesBeliBechoesB	dimwittedBdialBdenialBdeclaredBdavidsBdarylB	conveyingBcockneyBcanonBcannedBbyeBburdenB	botheringBblocksB	bickeringBbgradeB	bartenderBautobiographyBapprovalB
anythingbrBallisonBadvisedBadulteryB27B1961B101BzhangByearbrBwarpedB	versatileBunremarkableB	uncertainBtrickedBtransferredBtoroBtimonBsplashBsopranoBsnippetsBsmackBslutBsentinelBscriptwritersBrourkeBrestorationB
republicanBreliedBrefugeBratioBpuzzledBpresumeB	preservedBprankB	positionsBoutsiderBoptimismBnessBmotifBmisfitsBmifuneBmiddleclassB
mercifullyBmathieuBmarketedBmaineBlifeboatBlectureBlaughbrBkitBkazanBjodieBjanesBisabelleBideologyBhomosexualsBgrosslyBgrinchBgoatB	friendsbrBfleetingBfinanceBfastforwardB
everybodysBelevateBdepartedBcutterBcurryBcsiB	cowrittenBcontinuationBconnectsBcolonyBcloudsBchongBchamberlainBceilingBbunuelBboxesBbehavingBbatsBbashBbaitB
assumptionB
antoinetteB
annoyingbrBangBaamirB1960B1943BwisecrackingB	virginityB
untalentedBtrunkBtribalBtepidBtentB
tendernessBteddyBtearingBspecialsB
specialistBspadesBsniperBsnapBsciencefictionB	reportersBrantB
prejudicesB
playwrightBplainlyBpardonBovertlyBoperateBoneillBnursesBnightbrBmonotoneB
monologuesB	misplacedBmarloweBlocksBlocaleBlayerBlawyersBkinkyBirresponsibleBinterpretedBinexperiencedBhousebrBhispanicB	hendersonBgaloreBfriendshipsBferrellBevolveBequalsB	episodebrB
entertainsBeklavyaBeditorsBdownwardBdistinguishBdetroitB
dedicationBcoverageBcopyingBconsumedBcongratulationsBcoasterBclimaticBbondingBbackingB
assortmentBantiBanitaBamnesiaBamateursByuenB
yourselvesBwryBworriesBwormsBwokeBwaltersBwadeBviggoB	valentinoBtruthfulBtonysB
surrealismBstupidlyBskatingBshoutsB	setpiecesB
sentimentsB	scorsesesB
schindlersBsammyBsamebrBripoffsBriddenBreviveBproductionbrB
principlesBprefersBpouringBphrasesBpermanentlyBpeggyBpatsyBoneselfBoblivionBnikkiB
neighboursBnarniaBmadridBlambsBkneesB
interestbrBinsistedBinheritanceB
incompleteBilkBhurtingBhumorbrBhorizonBhaircutBgothBgenaBfundB
fulllengthBfontaineBfleetB
flamboyantBfiascoBfadesBfabricB	evocativeBethicsBenforcerBdunstB
dreadfullyBdreadedBdisconnectedBdirectionbrBdescendsBdernBdeepestBddBdaylewisBdanielleBcrewsBchoirBcentB	bodyguardBblazingBbigfootBbellamyBbeanBaveryB	avalancheBatomicBashtonBarenaBalonebrBalluringB	accidentsBtwentiesBthenbrBteresaBtempoB	surpassedB
stretchingBsoullessB	smalltownBskiBshermanBshaunBshamelesslyBscarredBsaviniBrosalindBrerunB	receptionB	promotingB	privilegeB	prevalentBpodBphilippinesBoutbreakBorlandoBoptionsB	opinionbrBmutatedB	monasteryB	materialsBmasturbationB
marginallyBmadelineBlocalesBisleB
irrationalB
integratedB	indulgentB	indicatesBhelmetB	harrelsonBgrumpyBgeoffreyB	functionsBfriedBflynnsBfionaBfetishBfeaturelengthBfascistBfallonBexpandB	exhaustedB	eastwoodsBdrippingBdraftBdominateBdefiningBdebraBdaresBcustomsBcrummyBcoyoteB	coworkersBcowardlyBcontentsB
connectingBcinematographicBchoreBcastedB	cancelledBcalendarBburstsBbrigitteB	boulevardBavengerB	attendantB
associatesBantonBacademicB1946BwitsB	willinglyB	whimsicalBwashingBwalmartBvingBversaBvernonBunsuccessfulBunfairlyBtroupeBtestedB
terriblebrBteaseB	summarizeB	struggledBspottedBsnowyBshadeBsessionsBseriouslybrBselectBschwarzeneggerBschizophrenicBsandlersBrumorsB	rosemarysBresurrectedB	requisiteBregisterBpreparationBpredicamentBowningBoscarwinningBorgyBnicholsBnestBmythicalBmuddyB
moderatelyBmeteorBmcconaugheyBmasterpiecebrBmasqueradingBmarvelouslyBmabelBloBlaughoutloudBlandauBjessieBinuyashaB
instructorBimpressionsBichiBherbieBharpBhailBgenBgeekyBfreneticBforsytheBferrerBeyebrowsBestablishmentB	enchantedB	empathizeBdwarfsBdonnieBdistributorsBdistributorBdimeB	democracyB	deceptionBdarcyBcursingBcrackedB	costumingBcosB	consistedB	complainsBcollaborationBcoincidentallyB	coherenceBclinicBcasperBcasebrBbuddingBbrushBbrunetteBbrinkBboilsB
biologicalBbagsBauntsBarisesBarchBantBaltmansB
alienationBadmirerBabuB73B1955ByouthsB
yourselfbrBwymanBworryingBwhippedBweeBvisitorsB
unemployedB
undeniableBtrendyBtoesBteriBtemporarilyBsuperfluousBsunriseBstunkBstrategyBstarvingBsmashedBsinglehandedlyBsiegeBsenBruntimeBrobberBringoBresurrectionBrestoreB
recoveringBprowessBplugBplantedB	placementBpalpableBpaddedBoperaticBnuttyBmurphysB
monumentalBmeekBlogBlingBlineupBlimbBkerryBjosieBintimacyBindulgeBhoundBheterosexualBhenrysBhelicoptersBhayworthBgroceryBgilmoreBfrostBentertainmentbrBemmyBdoublesBdivisionBdigsBdaredBdaftB	confirmedB	confessesB
compromiseBcomparesBclientsBchemicalBbranchBbradleyBblanketBbirchBbeastsBbassBbarnB	backwoodsBarrowsBarkBarchitectureBannualBanchorBallianceBairingBwitlessBvoyagerB	violentlyBusageBunhingedB
unbearablyBtrickyBthereofB
strikinglyB	stockwellB
staggeringBsomberBsolvingBsmarmyB
smallvilleBslayerBslamBsicknessBshrillBshaneBsemiBsealedBscumBscoopBrewardsB
respectfulBravingBrarityB
questionedB
proverbialB
protectiveB	prolongedB
postmodernBportBpopeB	poignancyBpmBpenaltyBpatriotB
oppositionB
operationsBobsceneBmockumentaryBminingBmessbrBmatterbrB
maintainedB	labyrinthB	judgementBindifferenceBilBhoodsBherringsBhauntsBharmonyBhaltBgripeBgoofBfoleyBfernandoBfayeB
encouragesBdomBdirectorwriterBdesiBdeadwoodBdaneBcropBcrookBcountessBcooksBconvenienceBconclusionbrB	classicbrBchloeB
calculatedBbruteBbrownsB
braveheartBblokeBarmorBaptlyB	amusinglyBamazesBalmightyB	alexandraBwimpyBweirdoBwarningsBvirgilBunwantedBtwobrBtopperBtoppedBtickB
tendenciesB
tarantinosB
sweetheartBstylebrB	strangestB
stepmotherB	staircaseBsquidBsmashingBsixtyBshadowyBsequelbrBscarB
sacrificedBrevoltBretroBrestlessB	rehearsalBrealbrBrangesBquotedBpunksBpunchingB
projectionB
professorsB
privilegedB	preventedB
powerfullyBpoliteBpokesBplottedBplotlineBpicnicBpenchantB
oppressiveB	occurringBnuanceBnemoBmovietheBmontagesBmaxwellBmanufacturedBmackBlamestBkingsleyBkeanuBjewelryBiranianBintellectuallyBheadbrBhavebrBgunfightBgrievingBgielgudBfortressBformerlyBforeheadBfauxBexternalB	exclusiveBentityBearthsBdramabrBdominoBdinBdemandedB
cunninghamBcrowdsB
criminallyBcrazeBconcentratesBcommandoBclaytonB	classroomB
childrenbrBcharmedBcateB
breathlessB	boogeymanBbicycleBberengerBbelongedBbehalfBbeefBballadBaxelB
attributesB
astonishedBassortedB
amityvilleB
alcoholismBactorsactressesB88B51B1966BweedBuniquelyBuncompromisingBufoBtwodimensionalBtucciBtruebrBtrierBtraumatizedBtediumBsufficientlyB
standpointBsortaBslingB	shawshankBshahrukhBsansBrudyBriotsB
rightfullyBrigBricoBrewatchB
reportedlyBregularsB
recreationBrecklessBraysBrainesBpunsBprintsBpremisesBpillsB	personnelBoverseasBoveractsBoveractBorganBoperatorBnickyBmisogynisticBmilieuBmexicansBlacedBitaliansB
invariablyBinsectsB
inherentlyBimmoralBimdbsBhousingBhicksBharvardBhardingBhairedBgruffBgrainBgoonsBgladlyBginoBgardnerBfrenzyBfirstbrBfincherBexistentBexceedinglyBexaminedBenvironmentalBenvelopeBemergingBdrawingsB	diversityBdisappointsB	dignifiedBdepravedBdenverBdeborahBcreepsBcoopersBcompilationBclouseauB	chinatownBcherylBcatastropheBcagneysBbumpsBbulliesB	braindeadBblendingBbikiniBbasingerBbalconyB
babysitterBauteurB
articulateBamokBactbrB
abominableB400B1948B150BwreckedBvelvetBunstoppableB
unfinishedBundevelopedBunderappreciatedBtvbrBtinBthingsbrBtellyBtastyBtanksBswayzeB
subjectiveBstillsBstewartsBspectacularlyBsimplerBsimonsBsearchesBrundownBringingBretireB	resonanceBrepriseBremorseBpursuesBpuriBpukeBprofessionallyBprestigiousB	pleasanceBpedroBpaulyBpartialB	paralyzedBouttaBonenoteBoconnorBnoonBnoisyB
needlesslyBmyriadB	metaphorsBmclaglenBmagneticBlootBlevelbrBkristoffersonBknockoffBjumbledBjoysBjerkyBjeepBislamB	interpretBincompetenceBincomeB
incestuousBhorrorbrBhartleyBgullibleBguineaBglenB
forgivableB	fireworksBexcellentbrBernieBenthrallingBellisB	eliminateB	elephantsBdoveBdogmaBdicaprioB
deservedlyBdelpyBczechBculpritBculminatingBcouncilB	convertedB
consistingBconfrontingB	concludesBcompetentlyBcolletteBcolemanBclothBcleanerB
classmatesBchunkBchinB
capitalizeBcaanB	butterflyBbonBbelievablebrBbastardsB	barbarianB	apologizeBandbrB	aimlesslyB
addressingBactivistB	abductionBwilburBwheelerBwernerBwalletB
voiceoversBvaliantB
unresolvedBtrucksBthunderB	therapistBtenantsBtalkiesBstimulatingBsnatchBslugsBsirkBsecludedBscoringBscorceseBscamBsaviorBrosieBrhymeBrenownedBreillyBrefusingB	recoveredBpuertoBpubliclyBpitsBpenguinsBolympicB	nefariousBnatashaBnapoleonBmossBmisterB	miniatureBmashBmanosBmaniacalBlogoBlockeBliquorBliottaBleastbrBlawlessBlapBladenBkareenaB
infidelityBhornB	hillbillyBhilariousbrBgrangerBgameraBgainingBfreddysB	forgivingBfloodBflipsB
fitzgeraldBfiguringBfathomBencouragingBdreyfussBdomainBdjBdifferentbrB	depardieuBdenmarkBdamatoBcuesBcontributionsBconductBcomebrBcolossusBclumsilyBclintonB
classifiedBcarebrBcanalBbisexualBbikoBbarrierBaxBastonishinglyB	answeringBamuseBalternatelyBalistBacquireB5thB510B18thBzombiB	wrenchingBwhodunitBweiszBvirtuesBvenusBveniceBuniversallyBtribulationsBtremorsBtobaccoBthroughoutbrBthorntonBthBtenantBtalibanBswintonB	suspendedBstudBsteadilyBspoonBslewBsignalBsidewalkBshopsBshirtsBshenanigansB
sensualityBretreatB
reflectingBreeseBredgraveB
reanimatorBrazorBransomBramonesBprophecyBprimalBpondBpolandB
plantationBpatronsBpalmasBopusBopponentBohioB	nothingbrB	neighbourBnailedBmorseBmollBmeetingsBmeasuresBmarcelBmagnificentlyBmadhuriB	logicallyBlodgeBlamasBkramerBjeanneBjadeB	intrusiveBhustlerBheartwrenchingBhayekBgranddaughterBgrabbingBglossBgleasonBglamourBgiggleBgarsonBfundedBfoxxBfloatB
fictitiousBescortBenhancesBemployerBemotionlessBemergedBelusiveBeerilyBdugBdrawbackBdownloadBdigestB	dickinsonB	degradingBdefinesBdecadentB
criticismsBcoursebrBcontrastingBconsistencyBcollarBclawBcivilianBcircumstanceBcannibalismBcafeBbyronBburgessBbreakupBbounceBbaffledBaubreyB
aristocratBaddamsB	achievingByorBwuBwidowerBvotersBvikingsB
variationsBunforgivableBtradingBtiringBtighterBtibetB	throwbackBterrainB	tennesseeBteamingBtalkieBsurfersBstubbornB
sophomoricBsnoopBsmBshueBshrekBsheetB	shatteredBserbianBsearchedBsabrinaB	roommatesBprogressiveB	predictedB
portugueseBpollyBplausibilityB
permissionBpassionsBpassagesB	offspringB	observingBnotwithstandingBmingBmarysBlorreBloganBlandisB	knightleyBkeystoneBkaufmanBjerksBjeansBirresistibleBineffectiveBhookersBhesheB	henriksenBgrindB	glorifiedBgaleBfluffyBflockBfletcherBfleeingBfileBfckB
exploitingBeverymanBeuroBerikaB	effectsbrBeducateB
distraughtB
dimensionsBdifferBdiebrBdialectBdeviousB
devastatedBdeckBcrusadeBcronenbergsB
creepinessBcrammedBcowroteBcornersB
consummateBconfidentialB	comicallyBcoburnB	civilizedBchansB
categoriesB
capitalismBcallahanBbowlingBbarbraB
attributedBashesBangelopoulosBalliedB	alexandreB	aishwaryaBaimlessBadvertisementBadoredB1965BwronglyBworksbrBvikingBventuraB
uneventfulBtimelineB	surrenderB
subversiveBstingB
stepfatherBspouseBspeedingBsmittenBslotBsierraBshakespeareanBshaggyBrushingBrootedBritualsB	revoltingB	resortingBrescuesBrenegadeBredeemedBraulBrainsBquirksB	purposelyBproneBprincesBpricesBpreventsB	premingerBpioneerBpersonbrBpennsBoxygenBorientalBomB	offeringsBnorthamBnixonB
newspapersBmutedBmuscularB	morriconeBmomentbrBmindnumbingB	mercilessBmanorB	mandatoryBmadlyBloungeBlossesBlipstickBjuicyBinjectedBinheritBimpersonationB	imaginingBhuttonBhottieBhonourBhgB
happenedbrBguybrBgroovyBgroanBgatorBfuturamaB
formidableBfilipinoBexposesB	emptinessBduaneBdosesBdollyBdistastefulBdiscountBdilBdepartmentsBdelveBdefBcypherBcomplexitiesBcommandsBclarksonBcarelessB	canadiansBbooneBboatsBbeaversBbathtubBbathingBbasicsBatkinsB	arbitraryB
apartmentsBamiableB
accustomedBwrongedBwoosBwhatsoeverbrBweaverBvetsB	upsettingBunrecognizableB
unfaithfulBtransformersBtollBtitlebrBtheronBtendedBteamedBtastefulB
talentlessBtalentbrBsuppliedBsubstandardBstatureBsplicedBspearsBsloaneBslangBskiesB
seamlesslyBryderBrufusBrougeBrobocopBrobertoB	resurrectB
researchedB	rejectionBrainbowBquartersB	publisherB	projectedBprinzeBprecededBpraisingBperkyBpartnershipBpanacheBpakistanB	overnightB	outsidersBonealB
nononsenseBnicheBnewestBneverendingBnarratesBmotherbrBmcgregorBmcadamsBmanneredBlsdBlotbrBloonyBlithgowBlistingBimplyBhumiliatingB	hindsightBhealBhabitsBgrossoutBgenocideBfurBforumB	firsttimeBexudesBexaggeratingBevilsBestablishesBescapistBepidemicBennioBemulateBemploysBdumpsBdittoBdisgruntledBdetBdeppBcraneB
contestantBconquestB	connivingB	comprisedBcolumbusBcoalBcitysBcitybrB
cheesinessBchabrolBcanvasBcagesBbunkerBbridesBboydBbouncingBbeersBbartBbafflingBbadgeBbackyardB	awkwardlyBaspirationsBarticlesBarchiveBannoysBanilBactorbrBacknowledgedB2brB1962ByankeeBwoefulBwelcomedB	wednesdayBviewerbrBvaultBvacuousBunscrupulousBunratedBtysonBtunnelsBtildaBtigersBthinkbrBsyncBstalksB	squeamishBspacesBsocietalBsizedBsingularBshaolinBseamlessBsandsBrileyBridiculeBriddledBrefinedBrankedBramplingBpunchedBprovokeB	protectedB	precisionBpranksBpostsBponderBpitchedB
patriotismBosamaBonesidedBnellBmusterBmummiesBmoodsBmoderateBmerchantBmeganBmeadowsBmathB	macarthurBluciaBloriBlistensBlestBlavaBlaraB	kurosawasBkristenBjoanneBintertwinedBinstrumentsB	instituteB	increasedB	inbetweenBillustratedBhypocriticalBhottestB
homophobicBheydayBhansonBfranzBfiennesBeverythingbrB	eroticismBelevatedBelectedBearningBdreamerBdiscernibleB
dialoguebrBdevBdeftB	decoratedBdavidsonBdantesBdamselBcruzB
criticallyBcontemporariesBcompositionsB	communismBcommonlyB	collectedB	collapsesBcheatsBcharleyBchaneysBcaféBcaBbrightonBbooBbogartsB	bloodbathBblissBbehavesB	astronautB	ambulanceBalexisBacquaintanceB7thB1930ByetbrByardsBwritingsBwomanbrBwhitBwaysbrBvowsBvinBursulaBunrelentingBtowBthoraBtestsBterenceBstalwartBsquirmB
speechlessBsoxBsolondzBsocietysBsiblingBsg1BselfabsorbedBrunofthemillBrubBrevisitB
repetitionBrankingBramBputridB	punchlineBprogressivelyBproblematicB
preferablyB
perversionBperceiveBpeasantB	oversizedBoperasBnovemberBnobrBmonstrosityB
missionaryBmischievousBmeandersBmasteryB	lovecraftB	louisianaBloudlyBlingerBlexBlawnB	latenightBkitschBkillerbrBjewelsBinhumanBinfantBincBimmortalityBhustleBhollywoodbrBhillbilliesBherdBheartbrB	hardshipsBguidedBgospelBgongB	gleefullyBgigglingBgearedBgarneredBgapingBfredricB
forebodingBfinelyBfatherbrBfantasticallyBfansbrBenlightenedBdundeeBdrifterBdominantBdivaB	disastersBdesmondB
democraticBcurtizBcravensBcramBcowriterB	conductorB
conceptionBcommunitiesBclonesBcheechBcavesB	cassandraBcarrierBcardinalBbythenumbersBbudgetbrBborgnineBblindlyBblendedB
bewilderedBbergenBbenigniBbellsBbarnesBbarkinBbachB
apprenticeB	applaudedBantiwarBamberB	alligatorBafterthoughtB
affectionsBaerialBabelBwellmeaningBweavesBvermontBunsuccessfullyB	twentiethBtubBtriadB	threesomeBtextureBtennisBtaylorsBswarmBsuperlativeB	sumptuousBsubconsciousBsteppingBstalinBspittingBsphereBsophiaBsolarBsmallestBskeletonBshtickBsewerBseasideBsealsBscanBsavvyBroscoeBrooftopBrendersBrenderB	precedingBpowderBpicBpersuadeBpenguinBpatheticallyBparticipatingBoutcastBoursBoprahBomegaBnodsBnasaBnarcissisticBmpaaBmistBmarshBluredBlizardBjohnnieBjoansBislamicBisaacBinsufferableBhiltonBhighlightedBherzogB	halfbakedBhailedBgypoBguessbrBgrendelBgeniusesBgamblerBfreshmanBfoulmouthedBfondlyBfloorsBflirtingBfinancedBfidoB
featuretteBfearlessBfaresBfadingBexperimentingBexorcismBexconBevolvesBensuresBelmerB
earthquakeBdupedBdoylesBdopeBdisillusionedB
disfiguredBdigressB	dependentBdecentlyB	corruptedB	contenderB	chauffeurBchancebrBcastroB	caretakerBcarellBbrashBboiledBblessingBberkeleyBbeauBavaBalphabetBactivelyBabrB98ByankeesBwillingnessB	wastelandBventuresB	unnoticedBunflinchingBtypecastBtshirtBtriumphsBtownspeopleBtoolboxBtimmyB
timberlakeBthereinBtheodoreBtextbookBtellerBtechnologicalBtailsBtacklesBsuzanneB	supremelyB
supportersB
subtletiesBstealthBstakesB	sprinkledBspeakersB	sparklingBspadeBsooooB	snowbloodBslumberBsluggishBslashBsheetsBserviceableBse7enBscaringBscantilyBsacrificingBrussellsBridiculousbrBresortsBremovingBreckonB	realitybrBquotingB	qualifiedBpseudoBpreachBpraisesBposhBponyBpokingBplacidBpicturesqueB
perceptiveBparticipationBothelloBoceansB	newcomersBmovieandBmisunderstandingBmiloBmillersBmenuBmcBmaverickBmartianBmarcoBmaclaineBlupinoB
leadershipBldsBlaughsbrBlampBknockoutBkeyboardBkerrBkentuckyBjangBinvitingBinconsequentialB
humiliatedBhorrorthrillerBholidaysB	havillandBhacksBgomezBgilBgetawayBformalBferrariBfarcicalB
explicitlyBexgirlfriendBemilBelicitBeileenB
effortlessBeffortbrBduskBdunawayBdiskBdefenceBdeclaresBdecayBcontendBcontactsBconnorsB
conferenceB
complementB	claudetteBclassifyBcarusoBcarefreeBcannibalisticB
candidatesBbustedBbrittanyBbriskBbrilliantbrBbraveryBbradfordBbloatedB	blanchettBbickfordBartfulBartbrB	arrestingBarrangementBargentosB	amsterdamB8thByuznaByoungsByeeByeaBweaveBwbBurgencyBuntimelyB
uneducatedBunclesBtrophyB
transplantBtideBthemedBthematicBsuiteBsterileBsoreBsofiaBsocksBsleuthBsleeveBsinbadBshotbrB
separationBscreamedBschtickB	scheduledBsabotageBrussoB	rooseveltB	righteousBreliveBreginaBrefugeeBrecapBrallyBpuerileBpsychopathicBpreserveBpourBperiodbrBpeekBpaulsBparrotBpannedBpackingBoccurrencesBnevilleBnamebrBmythsB	mortensenB
mismatchedBmisfitBminesB
metropolisBmaudlinBmantleBloanB
liberationBleungBlauBkristinBjoyousBjolsonBjollyBjigsawBinvadingBinterminableBinspiresBindyBindepthB	increasesBicingBhornsBhomelandBhilaryB
hesitationBhereinB
hairstylesBgraphicallyB	graduatedBgoogleBgoodnaturedBfinchBfilmnoirBfetchingBfeeBfartherBfamiliarityBexhilaratingBexcessesBerabrBequippedBequalityBenginesBdrumsBdominicB
documentedB	diversionB
delusionalBcutoutsBcrotchBcrispinBcringeworthyB	corridorsBcopingB	componentBclutchesBclunkerBchunksBchockBchampBblurryBbethanyBbeenbrBaplombB	anotherbrBandroidBalongbrBalarmingBaheadbrB	adulthoodBadeptBactionadventureByearningBwondrousB
winchesterBwhilebrBweepBwavingBwateredB	vanishingBvanceBunpretentiousBunderdogBtimelyBthesisBtelegraphedB
suspicionsBstitchesBsprayBspotonBspoiltBsituationbrBshellyB
screeningsBscrapBscoutB
scarecrowsB	sasquatchBsandwichBsampleBrulerBrosannaBrockedBrobertaBroaringBroachBrevolveB	repellentBremovesBrelievedBrelaxingBrefugeesB	recaptureB	ravishingBrackBquantumB	protectorBproposalBprofBpresumedBpreityB	pleasenceB	physicianBpetsB	perpetualBpaBoutlawsB	negativesBmotleyB
mistakenlyB
miscastingBmirrenB
mesmerizedBmercilesslyBmenbrB	mcdonaldsBmaltinBluminousBlingersBkindergartenBjfkBisabellaB
invitationBintercutB	inflictedB	indicatedBincorporatedBincorporateBincarnationBhugBhightechBheartilyBhawksBhamillBhainesBhadbrBgunfireBgregorysB	gladiatorBgiBgellarBgarrettB	frameworkBfinesseBfilmicBfearfulB	favorableB
fassbinderBextravagantB	extensionBerasBensuingBenlistsBendsbrBendeavorBdrainB	discardedBdiazBdenominatorBdecapitatedBdeannaBdaphneBcrocBcoughBconnollyBcompromisedB
communistsBcobraBcleeseBchimpBchainsBcentresBcelebratingBcargoBcalBcairoBbruckheimerBbrawlB	bourgeoisBarguedBappetiteB	anymorebrBanistonB	andromedaBamazeB	allusionsBallureB	agreementBadmirersBadjustBabundantB44BzoeBwwBwrapsBwillardBwildersBwellcraftedBweepingBvargasB
ubiquitousBtoeBtimedBthunderbirdsBswashbucklingB	sustainedBsurrealisticBsuburbiaBstripesBspunkyBsoylentB	societiesBsneakingBsidedBshabbyBscenicBrumorBrosarioBromanoBroamingBritzB	resembledBreplyB	relegatedBreformBreelsBrecommendingBreaBpunishB
punctuatedBpsychosB
pronouncedBprominentlyBpineB	pervasiveB	northwestBnoelBnatBmyraBmiyazakiBmillaBmelvynB	melbourneB	masculineB	marriagesBmarinaBlukasBlugosisBlotrBliterateBlindseyBlilaB
lesbianismBlegallyBlamBintimidatingBinjuriesB
impossiblyBimmersedBicyBhomemadeB	hideouslyBhesitantBgunshotBguidanceBgrandeurBgrafBgodardBgloveBglancesBfutileBfoeBfloraBfinanciallyBfarleyB
faithfullyB	expertiseBenjoybrB	engrossedBeleanorB	edinburghBdungBdrownBdoeBdisturbBdelightsBdeftlyBdeclareBdeceitBdasBcriticizingBcopiousBcoolerB	constructBcomradesB
compulsiveBcomingofageBcodyBcocktailBclareBchurningBcheeredBcancelBcalebBburstingBbulworthBbottlesBboobBbobsB	bigbudgetB	behaviorsBballroomBaykroydBawakensBassetsBarousedBanyonebrBanticlimacticBalvinBalphaB	actualityB
acquaintedBaccompaniesBabsurdlyB97BzoomBwrecksBwolfmanBwinklerBwhineBvietBviableBvaryButteredBtycoonB	travelersB	trappingsBtornadoBtimidBthirstBtedsBtaintedBswingsBstrickenBstantonBspinsBspeakerBsoooBsondraBslappingBskirtBskillfulBsilkBsicklyBshuttleB
shortlivedBshiftingBshackBsettlingBscarybrBrudolphBroyaleBrottingBripeBretainsB
restrictedBrefusalB
reflectiveBradiantB
prosecutorBpretensionsBpreparesB
powerhouseBpoorestBplaybrBphyllisBpeytonBpatternsBpartyingB	outburstsBorganicB
oppressionBoddityBocB	notorietyBnewfoundBmonitorBmommyBminionsBminersB	memorablyBmemoirsBmegaB
meditationBmcdonaldBmatinéeB
materialbrB	mastersonBmarjorieB
malevolentBlyricalBlapsesBkingpinB
kidnappersBkiddieB
innovationBimprovesBiagoBhuntsBhrsB
housewivesB	haphazardBhanBhamperedBhackingBgunplayBguiseBgrouchoBgrooveB
grindhouseBgoddardBgilliamBgeishaBgardenerBfreedBfondnessBflingBfleesBfakedBfacelessBexquisitelyBexcessivelyBexaminesBessayBeraseBelsaBeclecticBdownerB	dishonestBdillonBdentalBcutesyB	continentBcontemplateBconfirmsBconfessionsBconceitBcomplimentsBcensoredBcavalryBcarloBbrowningB	brightestBblurredBblanksB	bizarrelyBawaitsBautisticBastorBassumptionsBarcsB	apologiesB	ancestorsBamosBalaskaBadoptionByodaBwraithBwinstonBwinkBwatchersB	volunteerBvanillaBunitsBtravelerBtraitBthroatsBsynthesizerBsydowBsuspiciouslyB	surroundsB	successesB
succeedingBsuburbsBstarletB
stagecoachBsrBspoutingBspellbindingBsnobBsleepwalkingBslapsBskipsBsigningB	shootingsBsheriffsBsettlesBscarcelyBsantiagoBsaddledBsaddenedBrratedBrobsBrobeB	rightwingBriffBridleyBricherBrestsBreneBreinerBrantingB
programmesBpreBpoachersBpigeonBphilippeBpalestinianB	oppressedBofficesB
occurrenceBnosesBnobudgetB
nihilisticBnaïveBmuseBmisfireBmeagerBmasturbatingBlusciousBlumpBlodgerBliberalsBlandlordBlairBlabourBkajolBjournalBjensenBjekyllBjasonsB
jacquelineBjacobBittheB
involvedbrBinventorBinterrogationB	incessantBimplicationBhumaneBhugoBhostedBhinesBharmonBgrimyBgimmicksBgertrudeB
gandolfiniB	freshnessB	frequencyBformingBfinneyBfinerBfilmtheB	exhusbandB	excaliburBewanBeveningsBenticingB	elisabethBeleganceBdrainedBdowntoearthBdispleasureB
dispatchedBdenseBdancedBdacascosB	creditsbrBcompetitiveBcommendBchoppingBcaptainsBcandidBbraBbetraysBbessonBbehemothBbeansBbavasBbackwardBbackupBatopBasiansBarchieBapprovedB
antichristB
ambassadorBalbumsBaileenBaiB
adrenalineBadoptsB42B20000BzoomsBzaneByupByoBvivekB
violencebrB	viewingbrBunnamedB
unexcitingB
undertakerBtuesdayBtransvestiteBtownbrBtidyB	testimonyBsymptomsBswitzerlandBsuperheroesBstereoBspoofingBsplendorB	socialiteBsneaksBsimoneBshortbrBsendupB
secondrateBscrewingBrulingBriflesBrestrictionsB	responsesBresourcefulBrescuingBrepairBrefundBrecruitBratsoBrainerB	pressuresB
practicingBpowBposedBpoetsBpipeBpierBpickyBpercyBpaulineBparentalBpaletteBoverheadBonesbrBneillBneglectBnaturalisticBmusclesB
mulhollandBmoriartyBmmB
microphoneBmeasuredBmansonBlyleBlongsufferingBllBlinkingBlickBlayeredBlaundryBlaidbackB
lacklustreB
journalismBinventB
insistenceB	impressesB	hurricaneBhungaryBhellbrBhelenaB
heartbreakBhazelBhamsBgrailB	generatesBfranticallyBforensicBfoggyBferrisBfavorsBfatefulBeyecandyBexhibitsBenvironmentsBenjoyablebrBeinsteinB
effeminateBdivideBdisappointmentsB	dillingerBdescriptionsBdeprivedB
deplorableBdeluiseBdefeatsBcurtainsBcromwellBcrimebrBcourtneyBconvertB
contrastedBconchitaB	composersBcoldbloodedBcloakBclimbsBchurnedBcheezyB	caucasianB	burlesqueBbuddhistB	borrowingBbiographicalBbetsBbeautiesBbarrageBavailBattractsBatbrBaristocraticBappallinglyB	antonioniBandieBanarchyBanachronisticBaloudBalabamaBajayBabandonsB86B64B43B36ByenBwhoresBwholeheartedlyBvillagesBvesselBvertigoBverneButmostB
upbringingBuntrueBuninvolvingB	unfocusedBtreacherousBtheoB
tearjerkerBswiftBsurveillanceBsurferB	stevensonBsquarelyB
spellboundBsnowmanBsmirkBslackB	showcasedBsharplyBsexploitationBsexismBseasBscorpionBsardonicBrutgerBrusselBrocketsBrepublicansBrecruitsB	recruitedB
recognizesBrecitingBreciteB	realisingBrapperBquipsBportmanBporkysBpoisonedBpervertBperpetuallyBpastbrBparoleBowingB
overplayedBorphanedBoptedBobserverB
obligationBnobodysBnextbrBneedleBnaomiBnamelessBmountedBmiriamB
minimalistB	mechanicsBludicrouslyBloosesBlinerBlentBleansBkolchakBkilmerB	kickboxerBjtBjokebrBjackmanBiowaB
invincibleBinstrumentalBinstructionsBinputB
ingredientBindividuallyB
inadequateBimpressivelyBimmigrationBhauerBgungaBgumpBgazzaraBgardensB	garbagebrBgalsB	gabrielleBgabeB	frederickBfreakinBfosseBfolkloreB	fleischerBexecuteBevokedBerraticB	eponymousBenthusiastsBemperorsBembarkBearthbrBdismayBdisappointmentbrBdieselB	desirableBdemonstrationBdelvesB	decadenceBdebtsBcrusadesBcourtsB	countrybrBcoproductionBcontributingB	constanceBcongressBcommentatorsBcomedydramaBcleanedBclarenceBchubbyBchicoBchalkBcatalystBcaneBcanbrBcampfireBcampersBbutterBbullyingBbulliedBbrowBbramBbrainwashedBborgBbookieBberserkBbelgianBbeckhamBbananaBawaitBattentionbrB
attachmentBastroBarthursBarabsBarabicBapproveBanaBaliasBakshayeB	airplanesBabigailB610BzemeckisByorksByokaiBwiserBwaldemarBvocalsBvampBurgesBunspokenBunsatisfiedBuncomfortablyBthouB	televisedBtailorBsurpassBsultryBstoltzBstarbrB	splittingBsomersetBsolitaryBsnapshotBsnailsBsluttyBskimpyB	silvermanBsilentlyBsidewaysBsetupsBscrewsB	saturatedB	rewrittenBrevivedBrevelBreservationsB
resentmentB	rereleaseBreplacesBregretsBrednecksB
recreatingBreactsBpurseBprofitsB
productiveBprintedBpowsB	ponderingBplodsBpinheadBperiodicallyB
percentageBparsonsBpacinosBopenerBoffsBobstacleBnumbingBnukeBnoviceBnortonBneoBnaishB	mutilatedBmoscowBmissilesBmcshaneBmajesticBlucioB	loathsomeBkitanoB	katharineBjawdroppingBjanisBisabelBinsiderBinfuriatingBinfernoB
infatuatedBindeedbrBimposedBhomeworkBhobbyBhewittBhelpbrBhellsBheathBhathawayBhardworkingBhammingBhairdoBgoingsBglendaBfulcisBfrontsB
fragmentedBfoundedB	foreverbrB	foreignerBfloydBflorenceBfistsB	exhibitedBexceededB	examiningBethicalBetherealB	escapadesBerikBengagesBenableBdykeBdwBdrollBdon´tBdirkB	digitallyBdevoutB	depravityBdemonstratingBdebacleBdarkenedBcringedBcottenBcorbinBcoolnessBcontradictionB	commandosBcollageBcollaboratorBchibaBcherishBchenBcheekyBchairsB
capitalistBcamerabrBcallousBbyrneBboormanBbomberB	blandingsBbioBbergmansBbeachesBbeaBawkwardnessBarcticB	americabrBambushBalteringB	alienatedBalainB	addictiveBacB32BzenBxtroBwifebrBwhereaboutsBweddingsBwcBvivaBvacuumBunwittinglyBunimpressiveBunimportantBtruthbrBtroughB	troublingBtodBtheresaBtackledBsummersBstrivesB	strangledBstrainsBspiritualityBsoftwareBsodaB	socialistB
slowmovingB
shatteringBsesameBseminalBschoolbrBscarsBsaluteB
saccharineBrumbleBridiculousnessB	residenceBreserveBreplayBreflectionsBreallybrBreactingBrazzieBrapistsBrabbitsBquotableBpurityBpumbaaBprofessionalismBppvBpostmanBpostingBpoorerBpointlesslyBpleaBpillB	petrifiedB	perfectbrB	perennialBpeggBpayedBparisianBparasiteBowedBotherworldlyBoralBoopsBomittedBoldbrB
noticeablyB	nighttimeBnicknameBneveBmosBmikesBmicheleBmerlinBmenjouBmedalBmaysBlobbyBliquidB
lighthouseBliftingB
leprechaunBlenoBkieferBkayeBkarloffsBjenBinterviewingB	innocentsB	infantileBinduceBillfatedB	humorlessBhubbyB	historianBheatedBhatchetBharbourBhangedBhallsBguevaraBgrenadeBgorillasBgigglesBgarishBfredericBfortunesBfoolingBfondasBfirthBfeudBfarbrBfantasticalBernstBealingBduttBdurningBdudsBdonkeyB
dominationB	docudramaBdishesBdilemmasBdienB	deliriousBdecapitationB	deathtrapBdahliaBcrushingB	crosswordBcradleBcoolbrBcommentariesB
collectorsBchuckyBcharlizeBchainedBcellarBcastlesBcarlinBbranaghsBbitterlyBbitbrBbetsyBbernsenB	belongingBbanditBaustriaBaustensBattenboroughBarguesBappealedBandreasBanatomyBanakinBailingBafricansBadamsonB6thB31BworldlyBwomenbrBwickedlyBwhimBwarpBwaistB
vocabularyB	viewersbrBusherBunquestionablyB
unorthodoxB
unbalancedBtrinityBtorturesBtorsoB
threadbareBtemptingBswatB
sunglassesBstripsBstalkB	solutionsBsofaBslugBslidingBslashingB	sidekicksBshooterBshempBsevenbrB	separatesBseducingBseducesBrubinBronnieBrishiBrifeBreneeBrelishB	reasonsbrBrapportBquincyBpuppiesBprospectiveBproposesB
problemsbrB	problembrBpreteenB	precursorB
precociousBpoweredBpoundingBpoopBployBpeeBpasticheBpartsbrB	palatableBorientationBnc17BnaggingB
moviegoingBmourningBmountB	mistakebrBmiraclesBmidgetsB	mccartneyBmayaBmartiansBmanipulatingBmacbethBlouieBlongsBlockerBlillianB
lifestylesBleadenBladysBkneeBjumpyBjerichoBjeopardyBjennaBjarmuschBitbutB
interludesBincomparableB
illiterateB
heightenedBhaleyBhadleyBhackedBgutwrenchingBgritBgreggBgoslingBgooseBgeezBgamutBfreezingB
foreignersB	fishburneBferryBfencingBfearingB	fathersonBespB
englishmanBembarksBelishaBelevatesB
electricalBeastmanB	dumplingsB	dominiqueB	disparateB
disciplineBdetractsBdespisedBderridaBdelonBdarrylBcreepingB	crazinessBcrankBcoopBconfirmB	commendedBcoarseBchillerBchangBcarbonBcansBcabinetBbumsBbritainsBbreezyBbreakoutBboresBbloodedBbensBbemusedB
beginningsBbasesBbaileyB	backstageBastuteBarmiesBarchaeologistBanticipatingBanalBamcBaloofBagebrBaffluentB	accoladesBabbyB999B68B61B1800sB10000ByachtBworkingsB	wisconsinBwildeBwattsB
volunteersBvogueBunheardBuncertaintyBuglinessBtypingBturgidBtrackedBtoastBthirdsB
terrorizedB
tastefullyBtakeshiBsweepBsuperiorityBsunlightB
submissionBstuffyBstorytellerB
storylinesBstonerBstarskyB
squanderedBspewingB	spectatorBspecializedB	societybrB	showgirlsBsheilaBserlingBsalmaBsadismBreviewbrBreusedBresearchingBremakingB
registeredB
referencedBquoBpullmanBpristineBpretentiousnessBpretenseB
preferableBposseBpapaBowensBoutrageBorgansBomgBoleBnattyBmumblingBmorrisonBmonaBmolBmisunderstandingsB
minoritiesBmightveBmchughBmarredB	mansfieldBmainlandBlungBlaughablebrBknottsBkidsbrBkiddiesBjourneysBjocksBitandBinvadersBinvadedB	intervalsBinmateBinfestedBinfernalB
ineptitudeBinbredBimposingBimaginationsBidyllicBicebergBhopelessnessBhoopersBhiltBhaggisBgrownupBgradualBgownBgooBgetgoBgearsBgambleBfrighteninglyB	frenchmanBfentonBfailingsB
fabricatedBethelBeroticaBepilogueBemberBelectrifyingBeldestBdudikoffB
dreamworksB
dictionaryBdeniesB	dastardlyBcuzBcontrivancesB	contrastsB	condensedB	concludedBconcentratingBcomprehensionB	committeeBcomeuppanceBcomboBcobbB	cleopatraBclearerBcinemaxBchordBchokeBcheungBchavezBceasesB
cannonballBburtonsBbritneyBbrattyBbratsBboundsBblindedBbenoitBbarkingBbankerBbanditsBbaggageBbackboneBbabylonBavenueBauroraBathleteB	amazementBairwolfB83B71B1929BzooByorkersBxxxBwrongsBwonderfulbrBwillbrBwildlifeB	whicheverBweaponryBwaiterBviiiB	viciouslyBvh1BverveBunderwrittenBuncontrollableBuncommonBturturroBtroopB
translatesB
trademarksBtipsBthespianBterrorizingBtackBsymphonyB	strippersBstormsBsteigerB	spongebobB
splendidlyBsolaceBslicedB	skywalkerBsimplemindedBsimBsilkwoodBsilentsBsidebrBshunnedBseeingbrBsankBsalaryBrockerBriddleB	reservoirB
repressionBreginaldBredeemsB	recreatedBraoulBqaB	prototypeBprofaneB	potboilerBpoltergeistBpoliciesBpipesBpantheonBorgasmB	operativeBonsetBobtainedBobeseBoatesBnumbBnovarroB	nastassjaBnamingBmosesBmisadventuresBmiliusBmgmsBmealsBmckenzieBmarshalBmarchingBmanuBmalteseBlowsBkottoBkombatB	justifiesBjeromeBiwoB
irritationBinvestigationsBinvadeBintoleranceBintercourseBinsBinnateBimperialBidaBhutchBhrithikBhonoredBhelmedBheinousBgumBguardedB	griffithsB	grandioseB
graduationBgoingsonBgiamattiBgeraldBgeneticallyBgenesisBgarrisonBgabbyBfundamentallyB	fulfilledB
forewarnedBforbidBforayBfollowbrBflareBflamingBfinishbrBfendBfashionableBexileB
everyonebrBescapismB
enthusiastBenlistedBenlistBenigmaBelfenBeddiesBduvallsBdungeonsBduffBduetBdonsBdockBdisturbinglyBdeusB
descendantBdaggerBcuredB	crumblingB
creativelyBcravingBcontradictionsBcontemplatingBconnecticutB
concludingBconceivableBcomparisonbrB
commissionBchucksBchesBchambersB
challengerBcaronBcalculatingBbuckarooBbrotherhoodBbrennanBboyishBbootlegB
bitternessBbettanyBbenicioB	benefitedB
believablyBbehavedBbeginningbrB	befuddledBbedsBbeamBbannerB	backdropsBbabysBatwillBastinB	artifactsB
armageddonBapostleBanalogyBalbaBairlineB	adversaryB
accountantB84B57B5000B1927B007BzachByuppieBwrappingBworkingclassBwigsBwhollB
whisperingBweirdestBweiB	vulgarityBversatilityBveilB
upperclassBunsurprisinglyBunsavoryB	undermineBunavailableB	turbulentBtrexB	treasuresBtrashedBtraditionallyB	townsfolkBtougherBtoriBtopicalBtolerantBthoroughBterrenceBteenagedB
stutteringBstriveBstarrBspyingBspawnBsmartestBslobBslimeBskullsBsinatrasBsilvaBsignalsBshowersB
separatelyBselmaBscorebrBrowanBrichieB	reprisingB	reportingB
repertoireBrelicB	releasebrBrefrainBrantsBrancidB
purchasingBpumpBprisonsBpresleyBpioneersBphilosopherB	phenomenaB
persistentB	pendletonB	peckinpahBpaymentB	patriarchBpastedBoscarworthyBoozesBodonnellBnursingBnicksBnewhartBnadiaBnaB	mysticismB	mushroomsBmotorB
moralisticBmoBmetropolitanBmccareyB	massivelyBmachinationsBloweredBlookbrBlightenBleaningBleakBlampoonsBknowlesBkleinBjoviB
jeanclaudeBjayneBjackassB
irreverentBinteractingBimminentBifcBhivBhijackedBhessBhellbentBheavenlyB
headstrongBhardshipBhandymanBguysbrB
guillotineB
gloriouslyBgaynorBfrustrationsB
foregroundBfinnishBfilterBfilmiBfeedsBfanbrBeyeballsBevolvingB
evaluationBettingBenragedB	enjoyablyB
embodimentB	embarrassBellaBelizaBegotisticalBeaterBdynastyBdwellB	duplicateBdunnoBdullbrBdtvBdoggB	disguisesBdisappointinglyBdidntbrBdevilishBdearestBdamagingBcutoutB
culminatesBconspiraciesBconnerysBcondemnBconcealBcommerciallyBcolonialB
clevernessBclancyBcharacterdrivenBceaseBcavemenBcabotBbuttsB
butcheringBburrBburnettBbsgBbrolinBboyerBboilBbetrayBbankruptBbanalityBavengingBatypicalB	attackersBagreeingBaffinityB79B72B56B29B16mmByossiB	womanizerBwimpBwideeyedBwhackedBwendigoBwendersBwelshBwelfareBwarholBwallachBvomitingB
voluptuousBvivianBunsungBundemandingBtvmovieBtransformingBthinlyB	theoristsBtattooBsykesB	surrogateB	successbrBstormyBstardustBspinsterBspinoffB
spacecraftBsmacksBslumsBsleevesBslashedBskirtsBskaterBsinghBsimplestBsillybrBsexesBsexbrBsemataryBrevolverBreversedBretributionBretardB	religionsBrehashedBraptorBrappersBquartetBpumpingB
preferenceB	portraitsB	pollutionB	poisonousBpiercingBphotographersB
perplexingBperlmanBpeasantsBpazBpaddyBoverwhelminglyBoutdoorsB
originatedB
orchestralBoklahomaBnukieBnubileBmultiBmstB	moonlightBmindblowingBmidlifeBmichiganB
meticulousBmcclureBmarquisBmalloryB	macdowellBlurksBlowlifeBlisBlimboB	liberatedBlemonBkiraBjournalistsBjimaBintrospectiveBinlandB	ingenuityBinformerBinducedBidealismBhulkingBhoodlumsBhobbitBhidBheavyweightBhallucinationBgutterB	guerrillaBgrippedBglovesBgiovannaBgenitalsBgazeBfraserB	foolishlyBfloppedB	fishermanBfeldmanBfatesB	farmhouseB	explorersBexperimentationB	excrementBengineeringBembodiesBeightbrBeffectbrB	easygoingBduchovnyBdouglassBdisputeBdisinterestedBderiveBdereksB
definatelyBcurtBcrustyB	criterionBcowsB	counselorB
contentionBconsumptionB	constableBconflictingB	conductedBcoinBchipsBcheerleadersBchartsBchantingBchantBchairmanBcapabilitiesBcandlesBbuffoonBbrightlyBbodilyBblurbBblossomsBblackieBbeaverBbarrelsBazumiBautobiographicalBaudieB
archetypalBarakiBarabianBannetteBangeredBaielloBafricanamericansBadvertisementsBadmiralBaccomplishesBabyssB78B77ByorkerBwreakBwindsorBwhitakerBwhisperBweirBwayansBwaryBwaltzBvoyeuristicBviscontiBvirginalButilizeBunprofessionalBunprecedentedBunjustlyB
uniquenessBuneaseBunbornBumbrellaB	ultimatumBugliestBtwistingBtrainerBtowelBticksB
terminallyB	swordplayBsurlyB	successorB	strippingB	storiesbrBstintB	stallonesBspansBsorceryBsockBsinclairBshudderBsharifBselvesBsellerBseagalsBscrutinyBrosemaryBrosaBrichesBrethinkB	resultsbrB
rereleasedBrentsBrenoirBredsBrecoversBrachaelBpromotionalB	pratfallsBplotwiseB	plentifulBplatesBpicturedB	persuadedBpersecutionB	perplexedBpareBpalesBoverkillBoverdoseBoutragedBoccupyBnevadaBncisB	nastinessBmysticB	multitudeBmovieitBmisogynyBmindnumbinglyB
masterworkBmartinoBmakingsB	magnitudeBluluB
lovemakingBlorenBlomBlimeBkrugerBjetsBintolerableB
interwovenBinterventionBinstallmentsBinsensitiveBinjectsB
infectiousB
indirectlyB
indicativeBinchesB	improvingBillustrationB
hodgepodgeBhoaxB	headlinesBhatchBhannaBhallwayBhagenBgunpointBgroinBgreasyBgistBgimmickyBgershwinBfurryBfranksBfourteenBfittedBfilmandB	ferociousBfellinisBfancifulBeyebrowBextendBevilbrBeuropaBelwoodBeduardoBedisonBedenBdumbedBdroolingBdivesB
disserviceB
disrespectBdilapidatedBdeludedBdegeneratesBdebutedBcroniesBcrampedBcraftsmanshipBcormansB
conversionBcompoundBcompanionshipBcommentatorB	comicbookB
comedienneBcomatoseB
collapsingBcolBchronologicalBchicB	cheesiestBchecksBcedricBcasselB	carandiruBcaponeBcalamityBbusinessmenBburyingBbretBboldlyBbloodbrBblondesBblaBbelgiumBbeardedBbarrenB	balancingBautumnBautopsyBaudacityBarrangeBappreciatesBantiqueBamritaBalienateB
aggressionBagenciesBadviserB	activistsB96B39B2dByuckB	youngsterByadaB	wrestlersBwhipsBwanderedBvoorheesBvideobrBvenomBveersBvaljeanBvaldezB	untouchedBuntouchablesBunspeakableB
underminedB
underbellyB	uncoveredBtrustingB
triumphantBtossesBtitansBtibetanBthornBtheatricallyBtaxesBtateBtangledBtamblynBtajBtabloidBsybilBsweatyBsurebrB
superhumanBsullenB	submittedBstrivingBstrangeloveBstoogeBstahlBspoilsBspeculationBsoundtracksBsoonbrB	somebodysBsmiledBslackerBsketchyBshuBshowsbrB	shortenedBshoBshakenBselfishnessB
schoolgirlBsavalasB	satyriconBsailingBsailBrvBrotBromansBrodmanBreyBrehabBredheadB
redeemableBrecoveryBrecognizingB
recognisedBrangBrailwayBpowerbrB
possessingBpoppinsB	poisoningBpittsBpingBperceptionsBpeerBpawnBpassionatelyBparableBpairsB	overthrowBoffsetBnypdBmumblesB	moviegoerBmoaningBmessesBmementoBmckeeBmarcosBmaiBmagnoliaBmadamB	machineryBlukewarmBloreBloosingBlonesomeBloachBlistlessBlearyBlaceBknowledgeableBjunkiesBironsideB
intestinesBinsectBinjokesB
infiltrateBineptlyBincongruousBimpressionableBimpoverishedBhorrorcomedyBhorrendouslyBhodiakBhistrionicsB	hemingwayBhelsingBheadquartersB
hauntinglyBgunshotsBguidesBgreetedBgracieBgracedBglimmerBgleeBgibbsB
geographicBgangstaBfutilityBfrailBforbesBflopsBfloatsB	financingBfacetsBfacadeBextravaganzaBexecsBexcelB	entrancedB	entourageBenlighteningBembeddedBeffectivenessBeasiestBdyeBdvBdumpingBdrummerBdrillingBdoubtfulBdormBdorkyBdisposalB
discomfortBdiffersBdevitoBdepalmaB
degenerateBdeansBdangerfieldBcredentialsBconjureB	collisionB	collapsedBclimaxesBclimaxbrBcleavageBchiB
ceremoniesBcementBcamaraderieBblaiseBbestsellingB	believersBbeggarsB	bargainedBbaresBbarbieBbagdadBaztecBawardwinningB	attributeBattractionsBassemblyBasinineBantwoneBanchorsBaffectionateB	adversityB
accompliceBabeB180B11thBwoodysBwolfeB
wisecracksBwinonaBwilsonsBwhomeverBwhitneyBwelldevelopedBweirdlyBwaywardBwannabesBwalesBvixenB	visionaryBvickyBvickiBverballyBveiledBvariesBunsubtleBunavoidableBunacceptableBturnersB	transcendBtrainspottingBtoutedBthematicallyBteeBtakerB
sympathiesBsukiyakiBstylisticallyBstrangenessBstinkingB	specialbrBsolidlyBsoilBsoderberghsBsobaditsgoodBsobBsnyderBsneakyB	smugglingBsmokesBsidesplittingBshavedB	seventeenBselfrighteousBscriptwritingB	schmaltzyBscathingBsashaBsapBrothrockBrightfulBridebrBrewritesBreservationBrequirementBrebornB	qualitybrBproposedB	propheticBpromoBpromiscuousB
profitableBprodigyBprancingBprBpoorbrB	ponderousBpoloBplutoBpistolsBpinnacleBpilesBphotographicBpatronizingBoutingsB	oppositesBodeBnomineeBnoirsBnoirishB
narrativesBnanonBmurderbrBmodicumBmcclaneBmarginalBmallratsBlyricB	lumberingBlouderBlivenBlisterBliliBlessbrBleachmanBlandladyBkudrowBkeiraBjuxtapositionBiturbiBingmarBindestructibleBincorporatesB	imperfectB	impatientBimaginesBhuggingBhorsemenB	horsebackBhearseBharshlyBhandbrBhackersBhackerBgunmanBgrizzlyBgrinningBgretchenBgracefulBglossedBgelBfwordB
franchisesBfourbrBflippedBfiltersBfidelityBfebruaryBfavoursBfatallyB	fanaticalBfalselyBfakingB
explosivesBexecBericaB	enduranceB	empathiseBduryeaBdrownsBdrossBdrasticBdoomsdayBdisrespectfulBdisposeBdisgracefulB
disabilityBdietB	descendedBdenchBdeadonB	daredevilBdamianB	customaryBcusterBculkinBcrypticBcrittersBcriteriaBcopycatBcopoutB	continualBconsumerBconroyBcommonplaceBcommissionerB	churchillBchunBchewsBchesterB
charminglyBcharacterizedBchapBcatesBcarveyBcarreysBcarolinaBcapoteBcamBbygoneBbusesBburrowsBbumpingBbudapestBbronxB	brokebackBbogglesBboastBbladesBbeginsbrBbatemanBbangingBbackseatBavengersB	audaciousBattainBassesB	assaultedBarrangesBarabiaBalternatingBaltarBalexsB	alejandroBaeBadvocateBadolescenceBaddictsB	actressbrBaccordinglyBabrasiveB33B17thBzintaByvonneByokoByikesBwwfBwroughtBwipingBwindingBwhippingBwhatbrBwarholsBvitalityBvisiblyB	veritableBvantageBunworthyBunityB
unintendedBundressBunderwhelmingBunaBtrippyBtrenchBtransportationB
thinkingbrBtestifyBtessBtensBtemplateBswornBswankB
surrealistBsummonedBsummonBsuedBstuntmanBstinkersBstaceyBspreadsBspousesBspeeBspearBsorcererBsoapsBslitBslaughterhouseBskeltonBsituatedBsinglesB
showcasingBshookBshamblesBselfservingBscholarshipBschizophreniaBsaturnBsadbrBruthsBrubbingBroombrB
rewatchingB	revengebrBresonateBresidesBrepliedBrenamedBreinholdBregrettablyBreduceBraBpsychopathsBprovokesBprovokedBprizesBpretextBpressingBpreconceivedBpoursBpostproductionBpossessionsBpollackB	polanskisBpoitierBpeepingB
patheticbrBpantiesBpactBpackagedB	overcomesBoperatesBoohBokeefeB
offputtingBoclockBobservesBnobilityBnadirBmyselfbrBmorrowBmorleyBmoresBmoreauBmolinaBmodifiedBmisleadBmercedesBmcgavinBmannerbrBmajorsB	mackenzieBmacheteB	luxuriousBluthorBloomingBlondonsBlochBlistbrBlinedB	lethargicBlesleyBleftwingBlatebrBlassieBladsB	kusturicaBkissedBkirbyBkerseyBkerriganBjovovichBjossBjeunetBjacobiB
inventionsBinsecuritiesBineffectualB
indicatingBimpulseBimportBidleBhotelsBhollidayBhokumBhighlightingBherobrBhaysB	hamfistedB
gyllenhaalBguidingBgrindingB	gratitudeB	goldsmithBglueBglamorBgavinBflirtB
flawlesslyBfixingBfixationB	firefliesBfingBfakesBexitingB
exhaustingBesteemBenlightenmentB
emphasizedBelenaBegosBedwinBdriftingBdoingbrBdixieB
disposableBdevisedB
dependableBdenB	delusionsBdejaB	defeatingBdanglingBdahmerBcurveBcruisingBcrackerB	courtshipBcounterpointBcortezBcorbucciB	considineBconsciouslyBcolumnBcinemascopeBchoreographerBchimpsBcheapestBcesarB
celebratesBcassieBcarbrBcaligulaBcainesBbusbyBbumpedBbullsBbudgetedBbrucesBbroadcastingBbrandosBboybrBbotsBbothbrBbomanBboggedB	bloodlessB
blackadderBbigtimeBbigotryBbennetBbeatriceB	awesomelyBassistedB
antisocialBantagonistsBangelicB	amazingbrBalsobrBaholeB	agonizingBabusesBabbeyB700B54B35mmBzuccoBzipBzackByunByaphetB	wolverineBwizardsBwipesBwhimsyBwhatnotBwestsB
waterfrontBwardsBvolatileBvinylB
viewpointsBvacantButtersBunknowinglyBunfulfilledBunfoldedBundergoBuncoversBunbeknownstBtyingBtuningBtsuiB	treacheryBtranceBtailoredB
sympathiseBswannB
suppressedBstrokesBstrappedBstickyBstemsBstartbrBsparkleBsoulfulB	slowpacedBsloanBsizemoreB	singletonBshowyBshoveBsexiestBselfdestructiveBscruffyB
scandalousBrobustBrobsonBrivetedBresponsibilitiesBrequirementsBrepulsedB	repugnantBreprehensibleBrepleteBrenB
rememberbrB
recordingsBratingbrBraptureBquantityBpuristsBpublicsB
provincialB
proportionBpredictablebrBpooBpolesBpocketsBpidgeonB	perfectedBpepperedBpammyBoveruseBoutletBouchBottBorientBobsoleteB	nonlinearBnileBnaryBmystiqueBmumbaiB
moonstruckBmiraBminotaurBmercuryB
melodramasBmelodiesBmelindaBmcdowallBmassachusettsBmarkingBmaguireBlutherBlunaBluggageBlowellBlovehateBlotteryBlolitaBlizardsB	librarianBlenziBlennieB	leisurelyBleeringBlaputaBlapdBlagoonBkookyB	knowinglyBkhansBjustineBjohnsonsBjeanetteB
janmichaelBjaguarBintrepidB	interludeBinterferenceBinstitutionsB
indictmentB
impeccablyBimpaledBillicitB	illegallyBidiosyncraticBhourbrBhortonBhomelyBholmBhijackBhendrixBheleneBhecticBhazzardBhammersBgwynneB
gunslingerBgruelingBgrownupsBgossettBgodbrBgladysBgildaBgetbrBgarthBfrightenBfrigginBfoxesBforsterBfollyBflavourBflatoutBflatbrBferraraBeyeballB
exploitiveB
executionsBeppsB
episodesbrBenticeBembracedBeighthBecstasyBduranteBdrawnoutB
downstairsBdorkBdorffB	distractsBdiscernB
diminishedB	detailingBdescendantsB
demolitionBdefendsBdeclinedBdeclarationBdecayingBdealingsBcyborgsBculpBcrudB
costarringBcosbyBcorpsBcorbettB
consultantBconquersBcompanysB	commenterB
comfortingBcolossalBclaustrophobiaBclaudiaBcigarBchildbrBchickensB	champagneB	cellphoneBcarlsonBcalibreBbutchersB
businessesB	budgetaryBbuchananBbrowsingB	brotherbrBbroaderBbravuraBbogieBblurBblastingBbenchBbelowbrB
befriendedBaugustusBapplyingBantiamericanBannounceB	almodovarBallinallBalivebrB	afterlifeBadaBabsorbBabortedB1928B120B�ByelledBxmasBwreckingBwildingBwickerBwhistleBwarrantsBwarlordBvistasBviBvaluebrB
valentinesBuntoldB
unrequitedBunleashB
truthfullyBtreyBtownsendB
timetravelBtelevisionbrBtechnoBtbsBtappedBsweepsB
surprisebrBstressedBsteinBstartledBspunBspillingBspeedyB	specialtyBsophiesBsoakedBsnobbishBsneakersBslummingBslowingBslippingBshrewdBshaveBshapesBshahidBseedsB
screechingBscrappyBscantBsammoBsaidbrBropesBromanticallyBrockingBroamBrisquéBricciBrhymesB	rewatchedBretreadB
researcherBreeferBreconciliationBrashBraptorsBraiBquickieBpyramidBpumpedBprosecutionB	pronounceBpromosB	proceededB
pretensionB
preferringBpouredBportalBporkBpointsbrBpitcherBpinnedBpillowB
pickpocketBpicardBpervertsBperilsBparticipatedB	ownershipB	oversexedBoutbackB	osullivanBorganizationsBorderingBorchestratedBoperatedB
openmindedBojBobjectivelyBnonameBnickelodeonBneedyBnecklaceBmugBmostelBmetaphoricalBmeshBmayerBmaudeBmatteredBmastroianniBmanifestBmaniB	maliciousBlungsBlunacyB	lucrativeBlongoriaB	limelightBlifelikeBlearntBleaB	launchingB	landmarksBkittenBkahnBjunglesBjudiBjodhaaB	jerusalemBjamaicaBiraniB
inyourfaceB	intriguesB
innocentlyBindoorBindividualityBimmuneBhurleyBhoursbrBhooverBhoodlumB
homoeroticB	herselfbrBheightenBheelBhavenBharvestBharpoBharassedBhandingBhalleBhaasBgrammarBgovindaBgiddyB	gibberishB	geographyBgamingBfurlongBfriendbrBfrancosBfostersBforgivesBflesheatingBfellowsB
federationBfaustBfascismBfartingB
farnsworthBfaheyBexboyfriendB	examplebrBevanBesotericBenzoB
embitteredBelvesBeloquentB
elaboratedBdungeonBdreyfusBdowntroddenBdourBdoorwayBdonovanBdonnerBdolbyB
distributeB	discussesBdiningBdialogbrB	developerB	designersBdellaBdegradationBdefendedBcrystalsBcrowningBcosmicB	copyrightB
componentsB
completingBcomfortablyB	columbineBcloyingBclorisBclocksBcliffsBclawsBclashesBcivilisationBchurnB	chronicleBchagrinBcartoonyBbrigadeBbrashearBboomerBblamingBbenderBbehindthescenesBbeckBbeatingsBbarriersBbamBbakshisBbailBawaitedBattireBattendsBapplesB	applegateBapacheBannouncementBandresBahemB
adulterousBaccusationsB90210B1925B1912B16thBzuckerBzizekBzapB
womanizingBwinceB	warrantedBwanBvuB
verhoevensB	vancouverButahB	unscathedBunderplayedBumbertoBtwinkleBtomboyBtomasBtillyB	throughlyBthreebrBtenthBtemperatureB	talkativeB	swallowedBsuitorBsubtitleBsturdyBstorylinebrBstefanB	standoutsBsquallBspacebrBsonbrBsolarisB	soapoperaB	smalltimeB	sleeplessBslackersB	skinheadsBskewedBshuffleBshoutedBshortageB
shamefullyB
sequencebrBselleckBselenaB	scratchesB	scorpionsBsatansBsarahsBsaferB	routinelyBrodeBroastB	ridiculedB	reworkingBrevueBretainedB	reproduceB
remasteredBreiserBreefBrebuildBreactedBramonBpurportsBpriorityBprimaBpopulaceBpoesBplainsB
pittsburghBphysiqueBpessimisticB
peripheralB	pennilessB	patientlyBpartingBparkersB
paraphraseBpangB
overworkedB	overhypedBonstageBolympiaB
offthewallB
nonfictionBneighbourhoodB
negativelyBnanaBnadaBmutinyBmouthedBmoranisBmontrealBmodelingBmissouriBmetaphysicalBmercenariesBmemorablebrBmeanbrBmatteiBmarianneBmanipulatesBmadcapBmaceBlonglostBloatheB	lecherousBlambBkinBjosefBjohnstonBjimsBjezebelBjenkinsB	jeffersonBivyBititB	insistingBinhabitsBinfatuationBinexcusableBimplyingBimpersonatingBhypnosisBhumdrumBhoytBhillaryBhijinksBhigginsBhemanBhellishBheedBhechtBhatersBhartmanBharmedB
harassmentBgreerBgrandfathersBgrandeB
gracefullyBgownsBgordonsBgaulBfunctioningBfringeB	fragmentsBforthcomingBforeshadowingBfledBfkBfilmitBfictionalizedBfaultyBfangsBfamouslyBfactbrB	exemplaryB	excursionB
escalatingB
eraserheadBerasedBembodiedBelamBeightyBecstaticBdyanBdukakisBdrawerBdragoBdotBdominickB
dominatingBdiscriminationB
disclaimerBdetourBdeneuveBdaytodayBdalyBcutenessBcuckoosBcrimsonBcrennaB	creepiestBcozyBcottageBcorkyBcompletistsBcompensatedBcolleenBcollectivelyBcliffordB	clevelandBclearedBchopperBchokingBchokedB	championsB	certaintyBcartmanBcambodiaBburgerBbureauBbubblyBbreezeBbreedingBbreathtakinglyBboorishBbondiBblowupBblancheBbidBbeggedBbeautifulbrB	bandwagonBbambisBbalancesBbakedB
avantgardeBautomobilesBartfullyBandersBalloutB	allaroundBalibiB	admittingBaccomplishmentsB810brB1brB	writingbrBwolfgangBwildestBwaspBwalkersBwaiB	voyeurismBvirtuousBvirginsBvinodB
victimizedBvendettaButteringB	utilizingBupscaleB	unlimitedBunemotionalB
underminesBunbelievablebrBtwisterBtuckBtrimmedBtransformationsBtotoBthundercatsB
thrillerbrBterrificallyBtappingBtalbotBtaboosB	swordsmanBswellBsweeneyBswayBsugaryBsuckeredBsturgesBstrutBstrollBstephensBstatuesBsquirrelBspacekBspB	sorceressBsoooooBsolitudeBsobieskiBsnotB	simulatedBsillierB	sigourneyBshoresBsewellBselznickB
selfesteemBseizeBscottyBsanchezB	saawariyaBroyaltyB	romancebrBrobardsBritesBrhapsodyBresignedBrequiemB	repulsionBrenewedBregalB
recruitingBreconstructionBrecalledB	radicallyBrachelsBracerBquitsBquarryBqualmsBpussyBpumpkinheadBpublishBpromptedBprohibitionBprobableBprivacyBprequelsBpopulateBpointofviewBpointlessnessB	permittedBpenultimateBpentagonBparkedBpaquinBoversB
overcomingBonemanB	nonactorsBnintendoB	narratingBnainaBmyrtleBmultilayeredBmoogBmoanBmetroBmediterraneanBmeatyBmarkbrBmariusBmariesBmargoBmachinaB
longwindedBlindyBlindenBkilledbrBkatrinaBjeezB
jeanpierreBjeBivoryBissuedBirsBirritateBiraqiB	inventingBintellectualsBimaxBidentificationBhowsBhowellBhordesBhopebrBholtBhohumB
hobgoblinsBhickBheathersBheartyBheartbrokenBheapsBhardbrBhannibalBhallwaysBgunnedB	gunfightsBgulliverB	greetingsB
gorehoundsBgloomBglibBgirlsbrBgidgetB
functionalB
fraternityBfoxsB	forefrontBforcefulBfilmmakingbrBfilmbutBfiendsBfalcoB	exuberantBexemplifiesB	evidencedB	embracingBelitistBdumBdramatizationBdraftedBdoughBdoorstepBdizzyingBdizzyBditchB
distortionBdislikesB
discerningB	diagnosedBdevelopmentbrBdetectB
detachmentBdelayBdefiantBdecorBdecoBdebatingBdashedBdariusBcustomBcorleoneB
coproducedBcopperBcooperationBconteBconstitutionBconstitutesBconfusesBconfrontationsBcommuneBcoloradoB	colombianBclothedBclauseBclansBchristensenB
cheerfullyBcbcBcavemanBcaptorsBcapraBcameronsBcamelBcadBbrighterBbonanzaBbohemianBblastedBbinderBbeetleBbedtimeBbarrieBazariaBatheistB
assistantsBartifactBarsenalBantidoteB	anchormanB	americanaBafoulB	advancingBachinglyB91B89B8510B7510B58B42ndByulByrB	withdrawnBwinsletB	whodunnitBwhoaBwhiskeyBwhackB	wellpacedBwelldeservedBvolBvioletBunsophisticatedBunmistakableB	unleashesBunintelligentBtwitchBtvseriesBturksBtugBtudorBtrishB
travelogueBtossingBtomatoBtoddlerBtitillationB	tightropeBthwartedBthriveB	thresholdBtexBtelevisionsBtatumBtashanBtamilBsyrupyBsucksbrBstrideBstreepsB	storywiseBstathamBstandardsbrBsprayingBspilledBsolB	sociopathBsmutBsmashesB	sleepawayBskinnedBshavingB
schumacherBschoolteacherBsavagesBsatiricBryansB	rubbishbrBriggBrickmanBricardoBreveredBrereadB	requiringBreprisesBremarkedB	reconcileBrecommendedbrBreclaimB	recallingBrebBranbirBquaidsBqiBpusherBpupilsBprophetBpromptsBprestigeBpredatesB	powerlessB	posturingB
plagiarismBpimpsBpilingBpiaB
persuasionBpasteB	partitionB	parodyingBpareshBpanamaBpacoBoneilB	oldschoolB	obligatedBobeyBoberoiBnoddingBmythologicalBmythicB	mysterybrBmotherinlawBmonotonyBmockedBmischiefBmindbogglingBmeticulouslyBmeiBmcgrawBmazeBmayanBmathesonBmametBluciferBlillieBleapingBlargerthanlifeBkimberlyBkidbrBkarmaBkarensBkaranBironsB	investorsBinvestigatedB	innuendosB	indemnityB	indelibleB	impromptuB	illusionsBidahoB	hystericsB	hypocriteB
hypnotizedB	hypnotistB
horriblebrBhitchcockianBhippyBhappeningbrBhammeredBgroverBgrierBgreenstreetBgravitasBgerryBgardeniaBgamersBfulfillmentBfrancoisBfractionBfountainB	forwardedBfoldBfidelBffolkesBfetusBfeathersBfactionsB	execrableBewoksBeruptsBeffectivebrBeddyBdiscsBdisconcertingBdependedBdeodatoBdenholmBdelicatessenB
decorationBdearlyBcustomerBculturedBcrankyBcozBcoyBcornballBcoppolasBconmanBcongratulateB
conductingB
completionBcompetitorsB
competenceBcompensationB
commentersBcollideBcohortsBcinematicallyBchoresB	centurybrBcelineB
cautionaryBcarlyleBcapsBbustingBbrewsterBbrayB	blindnessBbikesBbebopBbeamsBbashedBbakersBaviationBauntieB
attentionsB
attendanceBarmoredBardenBappreciativeBapologyBaplentyB
antithesisB
anticipateBambientB
alienatingBalertbrBairsBafloatBadriftB	additionsBactionerBaccentbrB87B53BzodiacBwowbrBwoodcockBwebbBwandaBvladBviiBviewbrB	victoriasBvarneyBussB	unpopularB	unmarriedB	undergoesBundercurrentBulteriorBtraceyBtoweringBtiresB	thoughtbrBthirstyB	terrorizeBtacklingBsyrupBswearsBsuperficiallyBsubstitutedBstyledB
streetwiseBstrandsBspottingBspontaneouslyBspeedsBspaderBsolvesBsmellsB
singularlyBsinginBsimbaBshotsbrBshinBsherryBshearBsharmaBshaktiB	scoobydooBscarceBscandinavianBsaddlesBsadderBrubbleBrpgBrookerBromanticismBrodgersB	ridiculesBrichnessBrelationshipbrB	reinforceBrediscoveredBracyBracketBpyunB
preventingBpotatoBplungesBpleasebrB
pioneeringBpinchBpikeBpiecedBphilipsBperpetratedBpearlsBpatronB	paperthinBpanelB	packagingBoutweighBoncebrBokbrBoffendsBnightlyBnetherlandsBneneBnauseaBnapBmunroBmuniBmoranBmonumentBmondoB
miraculousB
medicationBmaughamBmatthewsBmasteredB	masochistBmarshaBmareB
manuscriptBmanteeB	mannequinBmanhoodBmaldenBmaidsBlumleyBlowlyBlowgradeBlloydsBlaptopBlanesBlamourBkormanBketchupBjumboBjrsBjonessBjebBjcBjapansBjanieBjacobsBinvestigatorsBintruderB	insertingBinlawsBinkBincarnationsBimprovBimpotentBimpactbrB
imitationsBimdbcomBifansBhummingBhuesBhotterBhordeBhomagesBhogBhideoutBherringBhelplessnessBhebrewBhastilyBhasbeenBharmfulBhardwareBhaphazardlyBhangoverBhackettBgrunerBgraderBgoodheartedBglorifyBgisB	gilligansB
generatingBfundamentalistB	fracturedBfractureB
forwardingBforteBforgedBforestsBfocalBflukeBfloodingBflaviaBflamencoB	firsthandBfingernailsB	feinstoneBfeatherBexplodedB	excludingBexciteBexceptionbrBevokingBequationB	employingB	eminentlyBembassyBeaglesBdwellingBdunnesBdrunksBdrankBditsyB
disasterbrB	dichotomyBdianneB
developersBdevastationB
descendingB	decliningBdeceptivelyBdawsonsBcwBcumBcruxB	creepshowBcorridorB
coronationBcookiesB
constituteB
concoctionBcomplementsBcommunicatingBcoltraneBcohensBcoachingBclutterBclarifyBclairB	chucklingBchelseaBcheapoB	cheapnessBchangebrBcelticBcaterBcatalogBcarriageBcalmlyBbutbrB	burroughsBbullshitBbulldogBbuggedBbrutishBbronsonsBbrockBbrandedBbogeyBblossomBblockingBbenignBbeltsBbeckerBbaywatchBbatteredBbarbaricB	bannisterBbackgroundbrB	autopilotB	auschwitzBauditioningBatmospherebrB	arthurianBarmandBardentBarchivesBaquaticB	apartheidBankleBancestorBanaisB
ammunitionBalmaBallegoricalB	agreeableBaficionadosBaffableB
aestheticsBadversariesBaccusesBabusingB
abandoningB81B666B3brB1922B110thB	zabriskieB
yugoslaviaByearnByawningBwristBworkoutBwordsbrBwithersBwellmanBwellcastBweberBwaterbrBwarlockBvincenteB	villainbrBveneerBveinsBurgingBunsolvedBunravelsB
unpreparedB
universalsBuninterestedBunforgivingB	undertoneBunderstandbrBunconvincinglyBtyrantBtradesBtourdeforceBtopbrB	toleratedBtoadBtivoBtissueBtickingBthwartB	thirdrateBthirdlyBthinnerBtequilaBtenorB
telepathicBteasingBswapBsusieB	supporterBsuicidesB
stupendousBstuffingBstorebrBstewBstemBstarvedBstarstuddedB	stanwycksB	sponsoredB
spectatorsB	specificsB	southwestBsonyBsongbrBslumBslaughteringB	skeletonsBsirksBsincebrB	shyamalanBshrewsBshredsBshovingBshellsBschlockyBsaucyBsandmanBruddBrowdyBroshanBronnyBromyBripleyBrevolutionariesBrestroomBrestaurantsBresolvesBresentBremembranceBrelianceBreimaginingB
rehearsalsB	regrettedB
reconsiderBrecognisableBreadingsBrappingBradiosB	quicklybrBquibbleBpubertyB	psychosisBprovenBproseB
proprietorBpromotesB	projectbrB
prioritiesB	prematureBpredominantlyBprecinctB
polarisdibBplungeBplumberBpluckyB	plotholesBplazaBpitfallsBpeskyBperpetratorsBpeelB	parasitesBpaoloB	pantomimeBpaganBpacesBorvilleBorbitBolgaB
objectionsBnovelloBnovellaBnondescriptBninthBnaivetyB	musicallyBmullerBmovieitsBmorelandBmomentarilyBmjBmilianBmerrillBmeltsBmckayBmayorsBmanualB	makeshiftBmaggotsBlitteredBlingoBlingerieBlinBlimoBleonesBledgerB
laughingbrBlabuteBlaboredBkristelBkellsBjoyfulBjaneaneBjakobBjakesBizzardBinvestigativeB
inimitableBinescapableBindiesBimprovisationBimitatedBhumanoidBhoppingBhavanaB
hasselhoffBhartleysBgrapesB	goodnightBgibsonsBgacktBfusionB	funlovingBfullestBfrodoBfreemansBformulasBfoesBfluentBfleaB
flashlightBfinlandBfastestBfarrellyBfargoBfamilialB
extraneousBextendsBexamB
endearmentBencounteringBenablesB	eisenbergBefficientlyBednaBeclipseBearthyBducksBdroneBdotsBdoloresBdinoBdilutedBdictatorshipBdeviantBdefianceBdeceivedB	debatableBcummingsBcrumbsBcrapfestBcraftyBcoronerB	cooperateB
contradictB	confessedBconceiveBcometBcockroachesBcoastalB
claymationB	cherishedBchattingBcategorizedBcasualtyBcarvedBcarlitoBcapricaBbucketsB	bubblegumBbrodieBbravelyBbravadoBbowieBbourgeoisieBbouncesBboltsBbogBbobbieBblancBbizBbimbosBbetaB
bestsellerBbentonB
benevolentBbendixBbeingbrBbanBbadlybrBbabysittingB
automobileBattachBathletesB	astoundedB	assailantBaspireBarrangementsBaquamanBapathyBanthropologistBangeloB	anastasiaB	anarchistBamesB	amazoncomBamanBajBagendasBadrienneB8mmB1916ByuB	workplaceBwoolBwhaleyBweighB	waterfallBvoyeurBvolcanicBvitoBvieBvanishBuniteBunionsBunintelligibleB	unhealthyB
unengagingBulmerBtyneBtrumpBtroublesomeBtrollsB	treasuredB
transpiresBtraderBtonguesBtickedBtexanBtautouBtattooedBtangibleBtangB
synopsisbrBswainBsuitcaseBsubzeroBsuburbBstupiderBstraysB
standaloneBstallionBsqualorBspunkBspermBsorvinoBsooBsnobbyB	snatchersBslidesB
skyscraperBsilverstoneBshriekBshittyBshippedB	shelteredBshedsBshanksBshamebrBselfimportantBseamsBscrollBsavingsBsaberBroundingBrollinBrohmerBriskingBreyesBreversalBreunitesBrepB
rendezvousB
releasedbrB	recreatesBrecommendationsB
randomnessBrainingBrailsBragBquigleyBquickerBpuckB	protégéBprivatesBpreludeBpottyBpotionB
poignantlyBpoB
playgroundBphiloB
pedophiliaBpatentlyBparadoxBpancakeBoverdramaticBoscarnominatedBokaybrBofferbrB	offcameraBoctopusB
obsessionsB	objectionBnooseBnoamBnaturedBmurdochBmoviebutBmoulinBmotifsB	mooreheadBmonteBmoneysBminerBmilliganBmidwestBmid80sBmessiahB	messengerB	messagebrBmergeBmeltedB
mankiewiczBmalikBmaestroBlucindaBlostbrBlookoutBlockingBlizzieBlivebrBlistingsBlinaBlewBleviBleiaBleaguesBlabouredBklausB	justicebrBjuhiBjaimeBjailedB	irritatesBiotaB	interfereB
instructedBinheritsBinflictBinfiniteBinfantryB
indulgenceB	improviseBimprovementsBigorBidentifiableB	hospitalsBhidalgoBheidiBheftyBhdBhayBhattonBharronBhariBhappygoluckyBhappybrB	happensbrB	hairstyleBgroomBgrizzledBgratedBgrandmothersBgradBgoofballBgonebrBgokuBgogoBgoerBgiovanniBgiorgioBgermsB	generatorB	generalbrBgaspingBgarbBfryeBfreshlyBfrenziedBfrankensteinsBfranchotBfrailtyB	formationBfeminismBfedsBfalloutBeyebrB
extinctionBexpressionlessBexiledBexcerptsBeventsbrBensuringBenslavedBeelBdurisBdurbinBduneBdummiesBdrenchedBdomineeringBdocumentingB	divertingBdistinguishesB	dissolvesB	disgracedBdetestB	detentionB	demeaningBdelusionBdelayedB	deceptiveBdebatesBdanzaBcurbBcruisesBcrudupBcreepedBcreakyBcrawlsBcostsbrBcontradictsBconsumeB	concoctedBcommunitybrBcoloniesBcollegesBcollectsBcollaborationsBcodesB	clayburghBclaptrapBchatterBceoBcarrolB	captivateBcanineBcalvinB
businessbrB
broadcastsBbreslinBbosssBboobiesBbonkersB	bombasticBblacktopBbisonBbingoBbigotedBbiancaBbhorrorB	betrayingBbeckyBbbBbawdyBbangkokBbakeBaweighBateamBartificiallyBaristocratsBappreciatingBanybodysB
anticlimaxBanticlimaticBandysBalucardBadoringBacquaintancesBaboveaverageB76B747B100000B	zetajonesByukiBwynorskiBwistfulB
widespreadBwhizB	whirlwindBwhewBwhalesBwerebrBwebsterBwebsitesBwatchablebrBwashingtonsBvortexBvillainyB
vegetarianBussrBupsetsBunmotivatedBundoneBtwohourBtubesBtruerBtrousersB	troublebrBtransylvaniaBtouchyBtoonsB	tomlinsonBtobackB
thoughtoutB	thespiansBtavernBsystematicallyBswashbucklerBsutherlandsB
suspensebrB
summarizedBsuccumbsB
subcultureBstirredBstevesBstaredB	sprawlingBspoutsBspoutBspiralsBsmokedBsmartsBsloppilyBsisterinlawBserbsBsectBsalemsBsaladBruseBroryBrobinsB
revisitingB	retainingBrequestsBrebirthBragsBprudeBpropositionB	projectorBprettierBpowellsBpointlessbrB	plasteredBpjBpinupBpinpointBpersonifiedBpersonasB
personableBperpetratorBperfunctoryB	pedophileBpeckerB
parliamentB	parentsbrB	pakistaniB	overwhelmBoverviewBoverlookingBoutcastsBorphansBoptingBonwardsB	offendersBobligedBnymphomaniacBnicebrB
neorealismBneighborhoodsBneglectsBneedntBnaturesBnapierBmoraleBmocksBmobyBmisledBmikiBmelancholicBmeadowB	mcdermottBmataBmarleyBmarcieBmanchuBmaltaBluigiBlucidBlowrentBlobsterBlinneyBliningB
likabilityB	lightnessBlightedBlensesBlelouchBlecturesBlawmanBlanguidBkineticBkindbrBkidukBkernBkeachBkaBjoplinBjonathonBjokesbrBinventsBintrovertedBintertitlesB	ineptnessBindistinguishableB
indigenousBincorporatingBimpulsesBillinoisBillconceivedBhumilityBhpBhorrificallyB
homophobiaBhogansBhoffmansBhinduBhikingBhighbrowBhellmanBhelensBhealedBheadyBhawkinsBhattieBhastingsBhamburgBhaggardBguildBguffmanBgreensBgothamBglobalizationBgathersBgasolineBgallonsBfrostyBfridgeBfreakishBforkBforciblyBflownBfischerB	fireplaceBfirearmsBfinchersBffBfaveBfastbrBfartsBfangoriaBfaltersB
expectedbrBexitsB	everytimeBeverythingsB	eternallyBenthusiasticallyBenglandsB	endeavorsBendearinglyB
emphasizesB	elegantlyB
efficiencyB
economicalBechoedBdushkuBdrillerBdourifBdiverB	dislikingB
diplomaticBderivesB
daughterbrBdarkoBdammesBcursesBcrusoeBcouldbrBconcedeBcomposeB	completesB	companybrB	communionBchurchesBchoicebrBcanoeBbybrBbushesBbunuelsBbuenosBbubbaBbrynnerB	browningsBbrotherinlawB	borderingBbonhamBbogdanovichBblackmailedBbewildermentBbeulahBbeggarBbatteryBbatchBbanningBayresBawakenedBatlantaBassassinatedBassassinateBarbuckleB	appointedBappeaseB
anamorphicBamericanizedBallamericanBaideBaggravatingBageingBagarBafterbrBadoresBadoptingBadolescentsBaccuseBaccommodateBabbotB66B52B38B1914B10thBzappaBzacByrsByearnsByaddaBwyomingBwtcBworstbrB	withstandBwiestBwendellBwelldirectedBwebberBweavingBwastedbrBwartsBwarsawBvladimirB	violationBvinciBveriteBvaletBunderscoresB
uncensoredBunbiasedB
unassumingB	unabashedBufosBtshirtsB	truncatedBtrumpetBtruffautB
truetolifeBtrejoBtreatbrB	tragedybrBtolkiensBtohoB	theaterbrBtemplesBtemperedBteaserBteasedBteBtattoosBtanBtakeoffB	syntheticB
synonymousBswiftlyBsweetlyBsuperstitiousBsuperpowersBsuggestionsBsuchbrBsubstantiallyB
subliminalBstrodeBstressesB	stranglerBstorageBstoopBstepdaughterB
statisticsBsporadicallyBspillsBspecialeffectsBsparkedBsiodmakBsimulateBsilliestB
silhouetteBsilencesBshovelBsheikBshefferBsheeshBshandBshalhoubBsetbrBsereneBsepiaBselfindulgenceB
selectionsB	secretiveBscholarsBsawyerB	sanitizedBsanctimoniousBsalvadorBsagetBrwandaB	robinsonsBrigetBrhetoricBrevolvedB	revisitedB
respectingBresnaisB	replicateB
reinforcesBregionalBreformedBreducesBrecorderBrecollectionBraquelB	ramblingsBraiderBquotientB
quatermainBpurestBpumpkinBpropelB
programmerB	proclaimsB
proclaimedBpriyankaB
popcultureB	plutoniumB	plotlinesBplantingBpinsB	pertinentBperfectlybrBperezBpeppardBpenetratingB	penetrateBpauletteBpainedBozonBoverlappingBoutwardBopiumB
oneofakindBonelinerBonboardBoliviersBolenBodditiesBobjectionableBnothingnessBnitpickB	nicknamedB
neorealistB
neglectingBneelBneedlesBnearingBnadjaBnaaBmunchiesB	multiplexBmultimillionBmultidimensionalBmphBmorganaBmoonsB	miyazakisBmitchumsB
mistreatedBmerkBmcmahonBmarinBmarBmanlyBmangledBmanagersBmalBmakeoverBmahoneyBlunaticsBlucianoBlombardiBloathingBliverBlittlebrBlitterBlillardB	liberallyBleroyBleftoverBleftistBleelaBlamentBlaceyBkeelerBjuonB	jennifersBjanewayBirinaB
instalmentBindulgesBindoorsBinconvenientBimposeBimplausibilityBimmerseBideologicalBidentifyingB
identifiesB	idealizedBickyBicelandBibrBhoundsB	hostilityBhingesB	hijackingBhersheyBheadlineBhawaiianBhatcherBhasbrBhardhittingBhammondBhalfbrBhairsBguttedBgutsyBgrandmasBgrandchildrenBgramsBgraciousBgoodiesBgoblinsBgigliBghoulsB
generosityBgarrBganzBfruitionBfrigidB	françoisBfoxyBforsyteBforsakenB	footlooseBflowedB
flourishesBfloodedBflightsBfinebrBfeyBfennB	expansionB	expandingBexcelledBevaluateBerichBentertainersBengulfedBendowedBeltonBelicitsBebertsBdunBduchessBdroolBdroningB	drawbacksB
dramatizedBdispatchBdippingBdiplomatBdiminishBdevosBdevganB
detractorsBdentistsBdehavillandBdefenderBdecrepitBdeathlyB	davenportBdariaBdansBdanikaBcynicBcutestBcultsBcuddlyBcubBcrushesB
crossroadsBcrockettB	correctedBcontradictoryB	contactedB	conqueredB	coneheadsBconcertsB	commentbrBcommandmentsB	closenessBcleaverBcleancutBchuckledBchronicBcheeryB
charitableBchargingBcharacterisationsBcertificateBcensorBcarrotBcarolynBcaresbrB
capabilityBbulbBbubblesBbratzBboilingBboastingBblvdBbluntlyBblaineB	blackfaceB	benjaminsBbelialBbatwomanBbarberB	baltimoreBbaghdadBbadguyBbackstabbingBawakenBattainedBaslanB	argentineB	apatheticBanthemB
announcingB	alreadybrBalfredoBalbertoBairesBahmadB	acrobaticBaccompanimentBabnerB74B47B37B30thB2000sBzunigaBzatoichiByeByayBwrestleBworonovBwitherspoonB	whistlingBwendtBwellintentionedBwelchBwarmedBwallowBvincenzoBventBveidtButilizesBurgentBuprightB	unwelcomeBuntoBunthinkableBunrestB
unofficialBunimaginableB
uncoveringBturkeysBturboBtrippingB	triggeredBtrentBtosBtoiletsBtitillatingBthoughtfullyBthereforBtenuousB	tentaclesBtenementBtapestryBtanyaBtacticBsurprisedbrB	supremacyB
superstarsB	sullivansB	strategicB
stranglingBstompingBsteinerBstageyBsporadicBsplitsBsoundtrackbrBsonatineB	someplaceBsolomonBsobbingBsmuggleBslammedBsizesBsilasBshortestBshiaBseventyBsergeiBserenityBserbiaBsensuousBsecularB	secondsbrBseatedBscrimmBscornBschwartzmanBscannersBsarneBsalonB
ruthlesslyBrushmoreBrugBrotatingBroarBroanBringerBrimmerBrichterB	rewritingBretitledB	respondedB
resoundingB	resonatesBrepetitiousB
reinforcedBreincarnatedB	registersBregionsBrecountsBrecommendableBquebecB	pseudonymBprochnowBprewarBpresentationsBpragueB	practicedBportugalB	pitifullyBpicsBperversionsBpencilBpaybackBpauloB	parentingBoverrunBoverpoweringBoverloadBoswaldBorganisationBoptsBomnipresentBomissionBoliveBofficebrBoffensivelyBoeuvreBnotoriouslyBnighyBnicerB	negotiateBnecksBnataliBnachoB
mutilationBmultifacetedBmozartBmormonsBmoniqueBmodeledBminnieB	minnesotaB
methodicalBmeinBmcqueensBmaturedBmasculinityBmaraBmansionsBmamieBlustingBluresBloafBlinusB	lettermanBlendingBlazinessBlattersBlapseBlamarrBlabelsB	kornbluthBkochBkermitBkennedysB
kaliforniaBjuliesBjabbaB	intrinsicB	intendingB
insatiableBinnumerableB
injusticesB	injectingB
inflictingBimprisonmentBimpersonateBillustriousBillegitimateBidealistBhyperactiveBhutBhusseyB	hunchbackB	howeverbrBhousemanBhoboBhighpoweredBhensonB	heartlandBhalfheartedB
guidelinesBgrowlingBgroaningBgraspingBgorefestBgoingbrBglitterBgizmoBgilaBgestapoBgenesBgaudyB	gallagherBgaBfryingBfrustratinglyBfreudianBforeBfluteBfluB
fiorentinoBfingerprintsB	filmgoersBfellaBfeelbrBfarisBfantasticbrBfanciesBfacetBextractBextinctBexplorerB
exhibitionB	exchangedBexceedsBenvironmentalistB	entangledBencompassesBemotiveBelsieBeliminatingBelectronicsBeldersBeighteenBedmondBearthlyBdyerBdumpsterBdullestBdriedBdribbleBdrawlBdpB	doubtlessBdocumentarybrBdisturbsB	disturbiaBdispositionBdisposedBdiggsBdialougeB	diagnosisBdeterioratedBdemillesBdazedBdauntingBdaisiesB
crocodilesB	credulityBcrankedB	countdownBcookiecutterBcookedBcontinentalBcontaminatedBconstructiveBconspicuousBconformB
condemningBcomprehensiveBcomplicatingBcomparativelyBcolouredBcoloringBclinicalBclickedBchieflyBchanningBcauldronB	catharsisB	catalogueBcarnalBcandiceBcamillaBbrundleBbrittBbriggsBbreathesBboomersB
boisterousBbogardeBblokesBbillionsBbillionaireBbeijingBbarmanBbarfB
barbariansBbananasBbabybrBbabbleBauteuilB	attentiveBaspiresBarseBappointmentB
apocalyptoBannasB	annabelleBammoBalonsoBalludedBairborneBadmiringBacknowledgesB	absurdistBabidingB82B63B1000000BzhaoByimouByeagerByanksBwsBwordplayBwingerBweirdosBwageBurBupandcomingBunwillingnessBunreasonableBunrealisticallyB
unravelingBunfathomableB
undeservedB
undergoingBuncaringB	twothirdsBturfBtrumpyBtrashingB
trainwreckBtoughestBtotalitarianBtongBtolkienBtngBthreedimensionalBthisiBtheeBthankedBtestosteroneBterrorsB	teachingsBtchaikovskyBtawdryBtallerBsymbolicallyBsweatingBsuspiriaBsurgicalBsuppressBsuccumbBsubtitlesbrB	subjectbrBstirsBstaplesBstandinBstallBstagyBsquireBsquadronBspitsBspikesBspartansBspallBsombreBsociologicalBsoaringBslicingBsledgeBslantBsithBsinfulB	sidewalksBshiftedBsheedyBsheddingBsharpesBsettlersBsensitivelyB	selfawareB	scriptureBscifihorrorBscholarBschmidtBsauceB
sabretoothBrubenBrtsBrosterB
rollickingBroccoBrobesonBrobbyBriccoBrevelsBresistsBrepercussionsBreparteeBremovalB	relatableBreinventBredoneB	recyclingB
recountingBrealmsB
rankinbassBrahulBracebrBpuffB
publishingBprowlB	promptingBpromisinglyBprogrammersB
proceduresBpopeyeBpoelzigBplumpBphonedBphilosophiesBpertweeB
persecutedBpermitBpeakedBpatentedBpatchyBpastorBpasoliniBpasBparodiedBpapBoxfordBoverdueBoverdoesBorthodoxBorderbrBopsBoffsideBoddestBoasisBnuffB	nowherebrBnosyBnormsBnonebrBnewsreelBnecrophiliaBnastiesBnarrowlyBnaivetéBmuckBmpBmortgageBmonitorsB	minghellaBmimzyB	mimickingBmiltonBmilB
midwesternBmeddlingBmatthausB	matriarchBmarielBmannsBmanifestationBmaniaBmammothBmalleBmaintenanceBmacmahonBlynneBlupinBlpBloveableBlommelB
lighteningBlibbyBlemmonsBlegionsBlaconicBkickassBkiBkeyesBkatBkaryoBkaraokeBkaninBkaliBjudasBjoltBjaniceBjamesonB	islandersBinvisibilityBintoxicatedBintervieweesBinterracialB
insecurityB
industrybrBinconsistencyBinconceivableB	implantedBilonaBideasbrBhustonsBhrB	hooligansB
histrionicBhintingBhickockBhelmsBhedyBheaviesBheavierB	heartacheBhealerBhazyBhartnettB	harshnessBhardysBgungB
gunfighterBguinessBgtoBgtaBgroundbrBgripesBgreetingBgratesB	graduatesBgorebrBgimmeBgigoloBgetsbrBgandalfBgageBfunhouseBfueledBforbinBflawsbrBfiremenBfilteredBfiercelyBfiddleBfecesBfeatsBfattyBfatherinlawBfaginBfadB
facilitiesB
fabulouslyB	extremistB
expendableBexecutionerBexcusedBevadeB	eurotrashBericsB	enlightenBenduresB	employersBembracesBeffectedBechoingBeatersBduvalB
duplicatedBdumbfoundedBdulyBdrainingBdontbrBdocsBdingyB	differingBdifferencebrBdicksB	detractedB	democratsBdelilahBdefyingBdecipherBdebrisBdanningBdaninskyB	damnationBcurrencyB	culturebrB	criticiseBcreedBcreasyBcraftsBcowardsBcourtingBcorneredBcordBconvictionsB	consensusB	conquerorBcommunicationsBcomfortsBcoinsBcobbledBcoatsBclingingBclassbrBclarksBcitedBcheckbrBcharmerBcaviteB
casualtiesB
castellariBcashingBcasaBcartBcarlisleBbuxomBburglarBbubblingBbroodBbricksBbribeBboyhoodBbowelsBbouncyB
bothersomeBboopB	bookstoreBbooksbrBboliviaBbodybrBbloopersBblockedB	blasphemyB	bernhardtBbergerBbereftB	benchmarkBbayouBbaringB	barcelonaBbambooB	ballerinaBbalkanB
backtobackBayeshaBaudibleBaudiardB
assessmentBartieBaristocracyBarchivalB	archetypeBaquariumBandréBalteregoBaldrichBalbinoBaggressivelyBaestheticallyBadvancementBackroydBachillesBabsurditiesBabjectB9thB94B92B50000B49B2010B1900sB14thBzillionB	yorkshireByeohB
wrongfullyBwristsBwrayB
workaholicBwordbrBwongoBwinstoneB	winningerBwilyB
whateverbrBweaklyBwaspsBwarmheartedBviolatedBverbatimBvenueB	unwittingB
unexploredBunendingBunderestimatedB	unbridledBunarmedBtyraB
typewriterBtweetyBtupacBtreasuryBtreadBtoursBtourneurBtoshiroBtoddsBtimerBtiffanyBthamesBtextsBtemperamentalBtauntsBtapingBtaimeBtabB	syndicateBsympatheticallyBswimsBsvensonBsuvB	survivebrB	supergirlBsuperbbrBsummingB	summationBsultanBstuporB
streisandsBstraussBstrandBsteepedBstashBstarzBstarbuckBstacksBsrkBspillBsparklesB	sophomoreBsohoBsnobsBsneeringBsnappedB
smolderingBsmartlyB
slipstreamBsleekBslainBsimplebrBshakespearianBsewersBseussBserpentBsequencesbrB
seductressB	schwimmerBschildkrautBsaylesBsantosB
sanitariumBsadakosBrossiB	romasantaBromanticizedBromaBrockwellBrizzoB	reverenceBrevereBretrospectiveB	reptilianBreminiscingBremakebrBrelivingBrelieveB	rehearsedB	rehashingBreedsB	reappearsB	rapidfireBrajeshBrafBquinnsBpushyBpurposefullyBpublicbrBproyasBprotocolB
prostheticBproposeBproclaimB	probationBprixB	priscillaBpreservationB	presenterBprematurelyB
prejudicedBpoodleBpleadingBplatinumBpiesBpiecebrBphoebeBpetrilloBpetiteB	penthouseBpenaBpedigreeBpecksBpavedBpastsB
paperhouseBpaintersBpackardBpacifistBoverzealousB	outwardlyBoutpostBontarioBonetimeBoldtimeB
nonsensebrB
nitpickingBneighboringB	nashvilleBnahBmusingsBmurielBmundaeBmummysBmulliganB	mukherjeeBmotivateBmostbrB	mortalityBmooseBmonogramBmichellBmicBmiBmellowB	mcdormandBmaynardBmatBmarianBmandarinBmalignedBmakebrBlumiereBlorraineBlongtermBlongedBlernerBleeleeBlawsuitBlaudedBkylesBkutcherBkrishnaBkowalskiBkkkBkikiBkherB
judgmentalBjoséBjohnnysBjasmineBjaceyBisntbrBinvalidBintrospectionBintricaciesBinsertsB	informingBinertBindiasBinanityBhyperBhusseinBhudsonsB
horrorfestBhormonesBhorizonsBhoorayBheartstringsB
headmasterBhaulBharmsB	hamiltonsB	guillermoBgroupbrBgrosseB	greenawayBgrauBgratuitouslyBgradersB	governessBgoebbelsBglimpsedBgilliamsB	giancarloBformbrBforgottenbrBfochBflirtsBflightyBflatsBfirebrBfinalebrB	fictionbrBfenechB
fastmovingBfanfareBextraterrestrialBextensivelyBexpensesBexclaimsBeruptionBepBentertaininglyBemotingBemoteB
eloquentlyBelijahBelfB	electionsB	egregiousB	earlierbrBduplicitousBdumbbrBdriftsBdreambrBdoubledBdisturbingbrBdissatisfiedBdiseasesBdisappointingbrBdipBdiarrheaBdevineB	detrimentB
demolishedB
delinquentBdefaultBdecorationsB	declaringBdatabaseBdangBdamienBcylonsBcusacksBcurvesBcurioBcrossdressingBcropsBcrichtonBcraterBcostumedBcookbookBconvertibleBconservativesBconsentB
conformityB	concealedBcomputergeneratedBcompromisingB
compressedBcommunicatedB	colorlessBclosebrBclassicallyBclashingBclairesBcirculationBcircularBchimneyB
chillinglyBchewedBchestnutBcheerleadingB	centeringBcelesteBcautiousBcarvingBcapitolBcan´tBcandysBcallerBbustyBburiesBbundleBbowsBbookerBblastsBbirdcageBbiographiesBbillysBbentleyBbellaBbeginbrBbeckettBbeattysBbangsBbacklashBatrociouslyBarnoldsBarnBapachesBannsBamarBairbrBageoldBadventB	adorationBacneBachingBaccusingBaajaB67B360B1917B	12yearoldB12thB1010brB½BzgradeBzeldaBzelahByumaBxviBwylerB	writtenbrBwobblyBwittedBwinfieldBwildsBwholebrB	wellbeingBweissmullerBweakenedBwalkensBwagerB
villainessBvidhuB	victimsbrBversesB	venerableBvaginaB
uproariousBuntouchableB
unsuitableBunneededB	unearthedBunderageB
unaffectedBunadulteratedB
tyrannicalBtwitBtumblingBtrybrBtrustsBtrifleBtriersBtrendsBtransmittedBtractorB	torturousBtomlinBtiniestBthursdayBthoughtlessB	thompsonsB
theatersbrBthawBthalbergBterranceBtearjerkingBtartBtaggedBtactBsynchronizedB	supplyingBsuperimposedBsuffocatingBsuccubusBsubstitutingBstrifeBstiggsBstellanB	stationedB
starvationB	stapletonBstairwayBsqueakyBspirituallyBspewB	speculateB
spaceshipsB
southernerBsoulbrBsnootyB	smugglersBsledgehammerBshowgirlBshiverBshippingBshaqBshBsexierBselfloathingBseitaBseasonbrBscumbagBscreenplaybrBscheiderBsatanistB	sarsgaardBsantasBsabuB
rutherfordBrueBropedBrodeoBresumesB
respondingB	resentfulBremnantsB	remarriedBreloadedBreligiouslyBreignsB	reclusiveBreactionaryB	rampagingBrammedBrailBraggedBquidBqueasyB	purportedB
punchlinesBpulitzerBptBprovinceBprotestsB
propertiesB
proceduralBprissyB	priestessBpredictionsB
predictionBpreconceptionsBprecedesBplodB	pinocchioBphallicB
persuasiveB	persuadesBpersonificationB	permeatesBparttimeBparticipantBparlorBparadoxicallyBpapillonBpanningB	onslaughtBoftenbrBoconnellB	occupantsB	observantBnuisanceBnotchesBnormasBnoodleBnirosBnipplesBnikhilBnihilismBnewbornBnephewsBnationalityB
narcissismBnakataB
musketeersBmusicsBmountingBmoroseBmoritaB
monochromeBmommaBmissyBmishapsBmiramaxBmiddlingBmeterBmetamorphosisB	metallicaBmedalsBmdBmcpheeBmcgovernBmcgBmatingBmarvBmarginBmajestyBmacrossBlukesBluiseBlucysBlornaBlolbrBlogansBlickingBleverBlazarusBlaurentBlatinosBlandonBkunalBkristyBkincaidBkimsBkibbeeB	kellermanBkattBkamalBkajiBjumbleB	jonestownBjefferyBjazzyBivanhoeBitvBistanbulBirritatinglyBirishmanBintertwiningB
intertwineB
interruptsB	interruptBinterBintegrationB	insidiousBinklingB
inhumanityB	infectionBinfantsBincriminatingBincessantlyB	impulsiveBimdbbrB
illuminateBideallyB
humorouslyB
humanisticBhuhbrBhostessBhomoBhindsBhildyBhighsBherrBherbB
helplesslyBhellgateBhearstBhastyBharriedBharkBharilalBhandgunBgunningBgulfBgrendelsB
greenlightBgoldsworthyBglitzB	glaringlyBgillisBgillBgigsBgeniusbrBgembrBgedBgardeB	frivolousBfriedmanBfrictionBfreewayBfreebrBfoundingBfounderBforwardsBforgettablebrBfootnoteBfollowerBflickerBflashedB
firstclassBfireflyBfilmitsBfigurativelyB	favorablyB
eyeopeningB	expansiveB	exercisesB
exchangingB	evacuatedBespositoB
envisionedBenamoredBenactedBemilysBeliasBehleBdwellsBdublinBdoubtingBdosentBdoorbrBdolphinsBdogmeBdodgingBditzyBdistressingB
distressedB
disneylandB	discreditBdisBdexterBdelvingB
delicatelyBdeliaB
delectableBdebonairBdazzleBdavissBdaltonsBcusakBcurtinBcruellyBcrosscountryBcreepierBcraftingBcoverupBcovertBcoursesB
conqueringBcompassBcompactBcommunicatesB
commandantBcollectionsBcollaboratedBcoedBclearbrBcleansBcillianBcigarsBchiefsBcheeksB
channelingBchabrolsBcenterpieceBceliaBcdsBcaviezelB	catholicsBcartersBcarolsBcaroBbuttermakerBbumperB	bulldozerBbulgariaBbtkBbrushesBbrookBbrimmingBbrandyBboxedBborneBbootyB	bombshellBbmwBbleachedBbevyBbettisBbessonsBbertieBbeheadedBbefriendBbaumbachBballoonsBbakulaBbainterBaweinspiringBavantBavalonBauthenticallyBaustraliansBauctionBauBattestBasteroidBassaultsBasidesBarmlessB
anywherebrBannihilationBandertonB
anarchistsB	amusingbrBamicusB	amendmentBamateurishlyBalltheBaidanBafterwardsbrBadoptiveBadityaBactoractressBacknowledgingBabackBabB1520B1015B
zombielikeByvetteBxratedBxenaBwrithingBwreckageBworkmanlikeBwoodwardBwoB
westernersBweirsBwateryBwarringBwallopBwackoBvoicingB	vivaciousBvittorioBvariableBvalidityBuproarBupdatingB	unhappilyBunflatteringBundyingB	underwoodBuncontrollablyBulrichBtwentysomethingBtweedB
treatmentsBtreasonBtouringB	toughnessBtouchbrBtoreBtoddlersBtitoB	timeframeBtiltBtendingB	targetingBsystembrB
supervisorB	supermansBsuperficialityB
structuresBstrongwilledBstrangleBsteelyB	stabilityBspitfireBspedBsouthernersBsongsbrBsoftenBsociopathicBsnatchedBsnackBsmokyBslowedBslicesBslabBskinnerBsirensBsirenB
simplifiedBshrugBshowbizBshiftyBshettyBsheraBshelbyBshamBsetsukoBserenadeBselfrespectingB	selectiveBscissorsB	scepticalBsamirBsadeBsaathiyaBrumoredBrudolfB
rosselliniBroperBromainB	robberiesBrestbrB
reluctanceBrekindleBreenactmentBreducingBreckonedBreavisBrealisticbrBrealisationBqueueBquestionsbrBprudishB	propelledBpromoterB
programmedBproclaimingB	processbrBprivyB
prevailingBprepBprairieBpostcardBpolitelyBpokémonBplotlessBpleadsBplankBpiningB
photogenicBpetuliaB
peppermintBpennsylvaniaBpedanticBpatronisingBpassportBpamperedBpabloBoverprotectiveBoutcomesBorgiesBoregonBoozingBolympicsBoccupiesB	obtainingBobscuredBnoonanB	nevermindB	neverlandBnauseumBnaturebrBmushyBmovieaBmotorcyclesBmorphsBmonarchyBmonarchBmkBmisusedBmishapB	minusculeBmilkedBmendozaBmendesB	memorizedBmemorialB	mccormackBmawkishBmaterialisticBmatchbrBmassageB	massacredBmascotBmartinezBmarketsBmarkerBmardiBmanureBmagrewBmachismoBlustfulBlusterBloopyBloboBliveinBlistenerBlillyBlightbrBlesserknownBleighsBlatifahBkinoBkibbutzBkettleBkeriBkeiBkariBk9B
juxtaposedBjkBjerrysBjennysBjaystonBjavierBjaffeBjackiesBjacketsBititsBirreversibleB	installedB	insomniacBinoffensiveBinnovationsB	innocuousBinishB
ingloriousBinfusedBinexperienceB
industriesB	indulgingBincredulousBimperfectionsBicetBhushB	husbandbrB	humiliateBhoweBhotsBhonorsBholdensBhinderedBhickokBhelsinkiBhazardBhatchesBharemB	hardnosedBhansenBhangerBhanekeBhandicapBhandedlyB
hammerheadBhaloBhaimBguidoB	greenwoodBgrahameBgordonlevittBgonzalezBgoesbrBghoulBghaiBgftB	genevieveB
generouslyBgarofaloBfulltimeBfullfrontalBfriarBforsythBforlornBflushedBfloB	flamingosBfixesB	fistfightBfilmedbrB	feelingbrBfeedbackBexpressionistB	executingB	ethnicityBerrBendorsementB	emotionbrBemeraldBembodyBeliaBelectrocutedB
egocentricBedwigeBeditionsB	editingbrB
ecologicalBeasybrBdysfunctionBduffyBdreamsbrB
downloadedBdoublyBdoggedB	disordersBdismayedBdisingenuousBdisgustinglyBdiscoveriesBdirectingbrBdiesbrBdidacticBdiatribeBdevouredBdevoteesBdeterBdesertsBdepressinglyB	defiantlyBdefectsBdeemBdeadlineBdapperBdannysBdamningBdaggersBcurrieBcupsB
culturallyBculminationBcsBcruellaBcrowesB
criticisedBcristinaBcostnersB
coscarelliBcorsiaB	consumingBconstitutionalBconnoisseursBcongratsBcomradeB
completistBcomplementedBcoincidentalBcockpitBclowningBcliqueBclimbedBclicksBchopinBchillyB	chevalierB	chemicalsBcerealB	catatonicBcashinBcarneyBbuñuelBburmanB
burgeoningBbroadlyBbranchesBboyzBbottomsBbombayBboardedBbloodlettingBbloodiedBblithelyBblenderBbleedsB
blackmailsB
blackboardBbiologyBbettingBbehindbrB	beethovenBbattyB
battleshipBbaseketballB
barrymoresBbarredBbanzaiBbanaBbaldingBbaioBbabarB	awesomebrBawedBawakesBauteursB
attractingBassuresB	assertionBassertBarjunBapologizingBapartbrBantonyB
antonionisBantelopeBanothersBannesBannabelBalicesBalfonsoBairheadBafiB	afflictedBafarB
adventurerBadolphBadmiresBadlibbedB	adjustingBacuteB	abhorrentB80sbrB600B1924B1920ByummyByankedBxavierBwrenchBwrapupBwilfredB	wikipediaBwhispersBwhirlBwhinesBweirderBweepyBwasterBwalrusBwagesBvogelB
videostoreBvictorsBverseB
versaillesB	venturingB	vegetableBvariantBvalliBupstateBupfrontBunusedBunkindBunimpressedBunharmedB
unfriendlyB	unethicalB
undisputedBtwistyBtwistbrBtwentysomethingsBtutorBtudorsBtrustyBtrippBtrickingB	travoltasBtrailsBtobeyBthyBthreesB	tentativeBtempBtelBtearfulBteambrBsweetestBswanB
surpassingBsuckersBsubstitutesB	stylishlyB
strathairnBstevieBsteroidsBsteepBstateoftheartBstankBstainedBstaidBsquaresBsplashedB	spiralingB	spinelessB
soundstageBsorbonneBsonamBsommerBsolesBsolemnBsnappingBsludgeBsleepbrBslayBslatedBskiingBskeletalBsinnersBsideshowBshroomsBshowingsBsheldonBsharperBsexinessBsensationalistBsenileBscreenwritingBscrawnyB	scratchedBscrapedBscalpelBsantB	salaciousBsalBsaffronBrumoursBroweB	romancingBrockinBrochonBrisibleBriftBricanBreversesBresourceBresearchersBrennyB
renditionsBreappearanceB
reanimatedBrawalBravesBravenousBravagedB	rationaleBrandBrambleBrafiBradiumB	purposebrBpuristBpuddleBpseudointellectualB
protractedB
projectingBprisonbrBprimBpriestlyB
presentdayBprerequisiteBpreoccupiedB
premingersB	predatoryBpplBporchBpokedBpoemsBpluckedBpleadBplaystationB
platitudesBplaguesBperuBpersianBpeachB	pattersonB
paramedicsB
pantolianoB	painfulbrBozzyB
overstatedB
outofplaceBoutdoBorganizeBoperettaBogreB	obviousbrBnuggetB	nobrainerBnobelBninebrBnicBnegroBnecessarybrBmythosBmylesBmultiplayerBmuddleBmoviemakersBmoresoB
moralizingBmolestationBmodineBmobsB	mitchellsBmistreatmentBmirroredBministryBmimeBmilitiaBmidairBmerrickBmcbainBmatteroffactBmatadorBmasochisticBmasalaBmarykateBmarxistBmarsdenBmarcyBmantegnaBmakingbrBmadonnasBmadeupB	macgregorBmacaulayBlyndaB	loopholesBloggiaBlocustBlockwoodBliviaBlicenceBlevantBleifBlaurasB	latterdayBlatexBlamebrBlagaanBlabeoufBknightlyB
kickboxingBkendallBkarismaBjustlyBjustifiablyBjugglingB	judgmentsBjesterBjeffsBjammedBjaffarB	investingBinevitabilityBindependentlyBinappropriatelyBimpregnatedBimplicitBimpassionedBimaginativelyBimaginationbrB	hyperboleBhullB	hotheadedBhorrorscifiBhopsBhookingB
honourableBhiveBhighpitchedBhiatusBheroicsBhenriB	heightensB	heartbeatBharnessBhandsbrB
handcuffedBhamptonB	hairsprayBhagBhacheBgunghoB	gulliversBgruntsB
gratifyingBgratificationBgrasBgoriestB
gorgeouslyBgopalB
goosebumpsBgoonB	glorifiesBglitchesBgibbonsBghostbustersBgeenaBgarbosBgallBfullersB	fullblownBfulfillsBfrickerBfreespiritedB	fragilityBforlaniB
forcefullyBflungBflorianeBflirtatiousB
flickeringBferoxBfergusonB	fashionbrBfarinaBfaintsBexistencebrB
exhaustionBexemplifiedBexecutesB
excitingbrBevenlyBernestoBemilioBelmoBellieBeleniakBdwellersBduranBdumasBdrummondB
dragonballBdosB	doolittleBdolphinBdocumentationBdiversBdistractionsBdiscreetBdermotBderelictBderailedBdepartsBdementiaBdefenselessBdatebrBdarkwingBdarksideBdaddysBcuthbertBcushingsBcrossbowBcritterBcrippleBcraziesBcouplingBcountercultureB	costarredBcontrivanceBcontestsB	contentbrBcontemplativeBconsolationBconnoisseurBconjuredBconjunctionBconductsB
complicateB
compatibleB	commodityBcollaboratorsBcoldheartedBcockBclimberBcinematographybrBchingBchileBcheyenneBcharadeBchapelB	changedbrBchahtaBcateringBcassavettesB
cartwrightBcaptionsBcamilleBcaleBbrushedBbrisklyB
brightnessBbrainwashingBbossyBbootheBbootbrBboltBbollsBbogglingB
blossomingBblaringBblackmailingB
binocularsBbingeBbikinisBbignameBbianchiBbeyBbewilderingB
bestialityBbesetB	believebrBbeleagueredBbegleyBbassettBbasingBbaroqueBbarkersB
bankruptcyBbanishedB	averagebrBattackerBasapBaryanBartificeBarouseBarmourBargentinianBarchaeologicalBarcaneB
antarcticaB	analyzingBamickBambushedBallusionBahhB
afternoonsBaffordedBadventurebrB
advantagesBadeleB	actionsbrBactioncomedyBacresBacklandB
accountingBabhishekB910brB710brB310brB247BzeusBzeniaBzendaB	zellwegerB
yesteryearByawnbrBwrestlemaniaBwoesBwhoppingB
wellingtonBweissBweighedBweaselBwarmlyB	warburtonBwailingBvillonBvenomsBvaultsBvarmaBuprisingBupgradeBunscaryBunsaidB
unreliableBunparalleledBunmemorableBunjustB
ungratefulBuneditedBundiscoveredBundervaluedBunconnectedB	unchartedBunceremoniouslyB
unbeatableBulyssesBtyrannyB
twentyfiveBtwainBturnoffBtsBtristanBtransportingB
translatorBtraffickingBtradedBtraciBtommysBtntBtissuesBtintinBtightsBthewlisBthesebrBtempestBtemperamentBtayeBtauntingBtaskinBtaiBsusannahBsuckedbrB	succumbedB
successiveB
submissiveB	submergedBstrengthenedBstonersBstodgyBstiersBstepsistersBsteeringB	squirmingBsprayedBspocksBsplinterBspatB
softspokenBsofterBsoakBsnottyBsnoozeBsnipersBsniffingBsniffBsnickerBsnailBsmearedB
smatteringBslothBslopBskepticBsizzlingBsickbrBsicBshrineB	shriekingBshoneBsherwoodBserpicoB
selfparodyBsearingB
screentimeBscrapsBscrapingBscrapesBschoolgirlsB	schoolboyBscentBscalesBsavorBsavagelyBsaulBsaucerB	satanistsBsaskiaBsammiBsageBrusticBrunbrB	roundtreeBrhodesBretiringBrespiteBresistedBreproductionB
repressiveBreplacementsB
rehearsingBreeditedB	redheadedBreboundB
reassuringBreappearBrealtimeBramseyBragtagBraggedyBquieterBpursuitsBps2B
protestingBprosaicBpromiscuityB
proceedingBprobingB	primetimeB	pretendedB	presentlyBprayersBportableBpleasesBplattBpitiableBpillarBpiggyBpicassoBphoningBphillipeBphilanderingBperuvianBpekingBpegBpawlikBpawBpattiB	patchworkBpatchesBpartybrB	panderingBpairingsBpainstakinglyBoutburstB
ossessioneBopulentBopportunisticBolsonBolinBoilyBogdenBobsessivelyBobnoxiouslyBnunsploitationBnothingsBnotebookBnormalcyBnominalBnobodiesBnoblemanB
nicholsonsBnewcombeBnewbieBneonoirB	narratorsBnarrativebrBnarrateBnancysBnachleBmunichBmulberryBmootBmonBmissbrBmischaBmilitantBmiklosBmikeyBmetheB	meteoriteBmenziesB	megalodonBmatteBmathisBmassimoB	massacresBmarlowBmargotB
margheritiBmarchesBmanybrBmanuelBluzhinBlustyBlustsBlumièreB	lovestoryBlovelessBlorneBliaisonBleopoldBlegitBlawsonBlatentBlanzaB
languagebrBkrigeBkoiB	kidnapperBkgbBkelloggBkeatingBkatesBkagomeBjuliusBjuliasBjpB	johnathanBjoharBjoaquinBjimmysBjellyBjanssenBjaggedBjabsBjabBitchyB	irritableBinvokeBinventivenessBintroductionsBintricatelyBintimidatedBinsultinglyBinstructionBinquisitionB	injectionB	initiatedBingramBingeniouslyBinflatedBindescribableB	inaudibleB	inanimateB
inaccuracyBimitatesBhoseBhilliardBhideoBhersholtBhelmetsBheeB
hedonisticBharryhausenBhanzoB	hamburgerB	hallmarksB	halfassedBhaircutsBgunnBgsBgrinsBgoodtheBgollumBgoliathBgoldwynBgialliBgastonBgarryBgamblersBgabinBfumblingBfreudBfreezerBfrasierBfranB	forensicsBforemanBflyboysBflamethrowerB	fittinglyBfertileBfellasBfavBfaredBfamineB	failurebrBfailsbrBfabB
eyepoppingBexpressionisticBexcisedBeskimoB
engineeredBendearBemphasizingB
empatheticBemmasB	embroiledBeliotBeganBdullnessBdrippedBdrekBdreamtBdoodleBdonenB
dominatrixBdomeBdogbrBdoesntbrBdixitBdisregardedB	dismissesB	discourseBdictionBdictatesBdiaBdevourBderrickBdentonBdentBdenizensB	deerfieldBdeathbedBdarwinBdarkmanBdamatosBdahlBcydBcybillBculledBcrisesB	crescendoBcreepersBcreeperBcrazierBcosetteBconverseB	contagionBconsumerismBconnedBconfinementBconfidentlyBconciseBcompriseBcomprehensibleBcompelsB	commodoreBcomerBcolaBcohortBcnnB	clutteredBclockingBclichériddenBclapBclandestineBcircumstancesbrBchunkyBchordsBchocolatBchilesBcheetahBchartBceteraBcementedBcatholicismB	catharticB
categorizeBcatastrophicBcatandmouseB	captivityBcaptivesBcapablyB	campaignsB
camouflageB	cambodianBcadillacB
bystandersBburdenedBbulgingBbreakbrBboringlyB	boredombrBboastedBbluthBblunderB	bleaknessBblasphemousBblandlyB	blackoutsBbitchesBbiddingBberniesBberkleyBbelBbartholomewBbarrettBbainBbaiBbaftaBbadtheBassignmentsBassholeBarnazBarchaicBarcadeBannalsBanimationbrBangersBanandBanalyzedBamiBalastairBafroBadvisesBadolfBadelaideBaccomplicesBabandonmentB410brB1926B1915B1500BzoomingBziyiBzestByukByorkbrBxboxBwriterbrB
worldwearyBwindyB
windshieldBwhiteyBwheelchairboundB	waterstonBwashesBwashedupB	wallpaperBvowBvinsonBvaluedButilityBupwardsBupstagedBuproariouslyBuntamedBunobtrusiveBunfitBunemploymentB	umpteenthBtruestBtrojanBtrinaBtrimBtrickeryBtranslatingB	transfersBtorrentBtoppingB	tigerlandBtheologyBthemebrBtenkoBtemporalBtellbrBtastebrBtantalizingBsynthBsweetsBswayedBsustainsBsunkenBsuitorsBstubbyBstroheimBstokersBstereotypicallyBsqueezedBspurnedBspongeBspicesBsparringB	southeastBsorrowsBsoninlawBsnideBsmileyBslenderBslayingBslapdashB
skepticismBskatesBsituationsbrBsituationalBsimsBsickeninglyBsiameseBshutsBshortyBsheppardB	sergeantsB
separatingBseguesBsectorBschellBsamaraBsalemBsaddamBrunwayBruffaloBrudimentaryBromcomBroadsideBriddickBrichestBrgvBreworkedBretailBrestoresB	resolvingB	requestedBreputationsB	rejectingBrehabilitationBreferencingBrecycleBreapBraoBranmaB
ramshackleBramisBracistsBquizBquittingBquibblesBpyrotechnicsBpupilBpunisherB	prospectsB	processesBprimerB	pressuredBpremBprefaceBpredicamentsBpottsBpottersBpotentialbrBpostureBplumBpleasurableBplaymateB	playfullyBplanetbrBpitchingBpitaBpinoBpingpongBpickerB
perpetuateBperilousBpenalBpeetB
peckinpahsBpearceBpeachesB
payperviewBpattenB	parachuteBpanahiBpaltryBpainstakingBpacebrB	overpowerB	overdriveBoverdoB	outshinesBoutrunBotherwisebrBosB	openendedBocdBobjectivityBoathB	numbinglyBnovelbrBnovaBnottingBnotreBnotalentBnonethelessbrBnicestBnepotismB
negligibleBnavajoB
naturalismBnascarB	narcoticsBmélièsB	murderessB
mumbojumboBmotoBmotherlyBmortimerBmorgansBmononokeBmommieB	momentaryBmogulBmisuseBmisunderstandB
mindlesslyBmimicsBmilkingBmightilyB	middletonBmibBmemorybrBmcewanBmayoBmarqueeBmariasBmapleBmanningB	manifestsBmalibuBmalaBmagnetB	magiciansBmaddyBmacreadyBlurchesB	loyaltiesBlookerB
longhairedBloftyBloadingB
livingstonBlibreBlewtonsBlevinsonBlennysBleafBlawbrBlastbrBlassBlangsBlamerBkitschyB	kiddingbrBkickerBkesslerBkeenanBjunctionBjulianneBjoggingBjiltedBjhorrorBjawdroppinglyBinvestsBinvadesB	intuitiveBinterviewerBinterpersonalBinternationallyB
innerspaceB
initiationB
inhabitingB	indicatorBimpressivebrBimploreBimplantBimbuesBillsBidiosyncrasiesB
ideologiesBhumpingBhumanlyBhowlBhomewardB
homecomingBhollowayBhollisBhitechB	hierarchyB	hellworldBhellboyBheadmistressB	headachesBhauledBhassleBhappenstanceBhammettBhalvesBhallucinatingBhainBhagarBgushing
��	
Const_5Const*
_output_shapes

:��*
dtype0	*��	
value��	B��		��"��	                                                 	       
                                                                                                                                                                  !       "       #       $       %       &       '       (       )       *       +       ,       -       .       /       0       1       2       3       4       5       6       7       8       9       :       ;       <       =       >       ?       @       A       B       C       D       E       F       G       H       I       J       K       L       M       N       O       P       Q       R       S       T       U       V       W       X       Y       Z       [       \       ]       ^       _       `       a       b       c       d       e       f       g       h       i       j       k       l       m       n       o       p       q       r       s       t       u       v       w       x       y       z       {       |       }       ~              �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �                                                              	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �       	      	      	      	      	      	      	      	      	      		      
	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	      	       	      !	      "	      #	      $	      %	      &	      '	      (	      )	      *	      +	      ,	      -	      .	      /	      0	      1	      2	      3	      4	      5	      6	      7	      8	      9	      :	      ;	      <	      =	      >	      ?	      @	      A	      B	      C	      D	      E	      F	      G	      H	      I	      J	      K	      L	      M	      N	      O	      P	      Q	      R	      S	      T	      U	      V	      W	      X	      Y	      Z	      [	      \	      ]	      ^	      _	      `	      a	      b	      c	      d	      e	      f	      g	      h	      i	      j	      k	      l	      m	      n	      o	      p	      q	      r	      s	      t	      u	      v	      w	      x	      y	      z	      {	      |	      }	      ~	      	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	      �	       
      
      
      
      
      
      
      
      
      	
      

      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
       
      !
      "
      #
      $
      %
      &
      '
      (
      )
      *
      +
      ,
      -
      .
      /
      0
      1
      2
      3
      4
      5
      6
      7
      8
      9
      :
      ;
      <
      =
      >
      ?
      @
      A
      B
      C
      D
      E
      F
      G
      H
      I
      J
      K
      L
      M
      N
      O
      P
      Q
      R
      S
      T
      U
      V
      W
      X
      Y
      Z
      [
      \
      ]
      ^
      _
      `
      a
      b
      c
      d
      e
      f
      g
      h
      i
      j
      k
      l
      m
      n
      o
      p
      q
      r
      s
      t
      u
      v
      w
      x
      y
      z
      {
      |
      }
      ~
      
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
      �
                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                             	      
                                                                                                                                           !      "      #      $      %      &      '      (      )      *      +      ,      -      .      /      0      1      2      3      4      5      6      7      8      9      :      ;      <      =      >      ?      @      A      B      C      D      E      F      G      H      I      J      K      L      M      N      O      P      Q      R      S      T      U      V      W      X      Y      Z      [      \      ]      ^      _      `      a      b      c      d      e      f      g      h      i      j      k      l      m      n      o      p      q      r      s      t      u      v      w      x      y      z      {      |      }      ~            �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �      �                                                                      	       
                                                                                                                                                                  !       "       #       $       %       &       '       (       )       *       +       ,       -       .       /       0       1       2       3       4       5       6       7       8       9       :       ;       <       =       >       ?       @       A       B       C       D       E       F       G       H       I       J       K       L       M       N       O       P       Q       R       S       T       U       V       W       X       Y       Z       [       \       ]       ^       _       `       a       b       c       d       e       f       g       h       i       j       k       l       m       n       o       p       q       r       s       t       u       v       w       x       y       z       {       |       }       ~              �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �       �        !      !      !      !      !      !      !      !      !      	!      
!      !      !      !      !      !      !      !      !      !      !      !      !      !      !      !      !      !      !      !      !      !       !      !!      "!      #!      $!      %!      &!      '!      (!      )!      *!      +!      ,!      -!      .!      /!      0!      1!      2!      3!      4!      5!      6!      7!      8!      9!      :!      ;!      <!      =!      >!      ?!      @!      A!      B!      C!      D!      E!      F!      G!      H!      I!      J!      K!      L!      M!      N!      O!      P!      Q!      R!      S!      T!      U!      V!      W!      X!      Y!      Z!      [!      \!      ]!      ^!      _!      `!      a!      b!      c!      d!      e!      f!      g!      h!      i!      j!      k!      l!      m!      n!      o!      p!      q!      r!      s!      t!      u!      v!      w!      x!      y!      z!      {!      |!      }!      ~!      !      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!      �!       "      "      "      "      "      "      "      "      "      	"      
"      "      "      "      "      "      "      "      "      "      "      "      "      "      "      "      "      "      "      "      "      "       "      !"      ""      #"      $"      %"      &"      '"      ("      )"      *"      +"      ,"      -"      ."      /"      0"      1"      2"      3"      4"      5"      6"      7"      8"      9"      :"      ;"      <"      ="      >"      ?"      @"      A"      B"      C"      D"      E"      F"      G"      H"      I"      J"      K"      L"      M"      N"      O"      P"      Q"      R"      S"      T"      U"      V"      W"      X"      Y"      Z"      ["      \"      ]"      ^"      _"      `"      a"      b"      c"      d"      e"      f"      g"      h"      i"      j"      k"      l"      m"      n"      o"      p"      q"      r"      s"      t"      u"      v"      w"      x"      y"      z"      {"      |"      }"      ~"      "      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"      �"       #      #      #      #      #      #      #      #      #      	#      
#      #      #      #      #      #      #      #      #      #      #      #      #      #      #      #      #      #      #      #      #      #       #      !#      "#      ##      $#      %#      &#      '#      (#      )#      *#      +#      ,#      -#      .#      /#      0#      1#      2#      3#      4#      5#      6#      7#      8#      9#      :#      ;#      <#      =#      >#      ?#      @#      A#      B#      C#      D#      E#      F#      G#      H#      I#      J#      K#      L#      M#      N#      O#      P#      Q#      R#      S#      T#      U#      V#      W#      X#      Y#      Z#      [#      \#      ]#      ^#      _#      `#      a#      b#      c#      d#      e#      f#      g#      h#      i#      j#      k#      l#      m#      n#      o#      p#      q#      r#      s#      t#      u#      v#      w#      x#      y#      z#      {#      |#      }#      ~#      #      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#      �#       $      $      $      $      $      $      $      $      $      	$      
$      $      $      $      $      $      $      $      $      $      $      $      $      $      $      $      $      $      $      $      $      $       $      !$      "$      #$      $$      %$      &$      '$      ($      )$      *$      +$      ,$      -$      .$      /$      0$      1$      2$      3$      4$      5$      6$      7$      8$      9$      :$      ;$      <$      =$      >$      ?$      @$      A$      B$      C$      D$      E$      F$      G$      H$      I$      J$      K$      L$      M$      N$      O$      P$      Q$      R$      S$      T$      U$      V$      W$      X$      Y$      Z$      [$      \$      ]$      ^$      _$      `$      a$      b$      c$      d$      e$      f$      g$      h$      i$      j$      k$      l$      m$      n$      o$      p$      q$      r$      s$      t$      u$      v$      w$      x$      y$      z$      {$      |$      }$      ~$      $      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$      �$       %      %      %      %      %      %      %      %      %      	%      
%      %      %      %      %      %      %      %      %      %      %      %      %      %      %      %      %      %      %      %      %      %       %      !%      "%      #%      $%      %%      &%      '%      (%      )%      *%      +%      ,%      -%      .%      /%      0%      1%      2%      3%      4%      5%      6%      7%      8%      9%      :%      ;%      <%      =%      >%      ?%      @%      A%      B%      C%      D%      E%      F%      G%      H%      I%      J%      K%      L%      M%      N%      O%      P%      Q%      R%      S%      T%      U%      V%      W%      X%      Y%      Z%      [%      \%      ]%      ^%      _%      `%      a%      b%      c%      d%      e%      f%      g%      h%      i%      j%      k%      l%      m%      n%      o%      p%      q%      r%      s%      t%      u%      v%      w%      x%      y%      z%      {%      |%      }%      ~%      %      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%      �%       &      &      &      &      &      &      &      &      &      	&      
&      &      &      &      &      &      &      &      &      &      &      &      &      &      &      &      &      &      &      &      &      &       &      !&      "&      #&      $&      %&      &&      '&      (&      )&      *&      +&      ,&      -&      .&      /&      0&      1&      2&      3&      4&      5&      6&      7&      8&      9&      :&      ;&      <&      =&      >&      ?&      @&      A&      B&      C&      D&      E&      F&      G&      H&      I&      J&      K&      L&      M&      N&      O&      P&      Q&      R&      S&      T&      U&      V&      W&      X&      Y&      Z&      [&      \&      ]&      ^&      _&      `&      a&      b&      c&      d&      e&      f&      g&      h&      i&      j&      k&      l&      m&      n&      o&      p&      q&      r&      s&      t&      u&      v&      w&      x&      y&      z&      {&      |&      }&      ~&      &      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&      �&       '      '      '      '      '      '      '      '      '      	'      
'      '      '      '      '      '      '      '      '      '      '      '      '      '      '      '      '      '      '      '      '      '       '      !'      "'      #'      $'      %'      &'      ''      ('      )'      *'      +'      ,'      -'      .'      /'      0'      1'      2'      3'      4'      5'      6'      7'      8'      9'      :'      ;'      <'      ='      >'      ?'      @'      A'      B'      C'      D'      E'      F'      G'      H'      I'      J'      K'      L'      M'      N'      O'      P'      Q'      R'      S'      T'      U'      V'      W'      X'      Y'      Z'      ['      \'      ]'      ^'      _'      `'      a'      b'      c'      d'      e'      f'      g'      h'      i'      j'      k'      l'      m'      n'      o'      p'      q'      r'      s'      t'      u'      v'      w'      x'      y'      z'      {'      |'      }'      ~'      '      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'      �'       (      (      (      (      (      (      (      (      (      	(      
(      (      (      (      (      (      (      (      (      (      (      (      (      (      (      (      (      (      (      (      (      (       (      !(      "(      #(      $(      %(      &(      '(      ((      )(      *(      +(      ,(      -(      .(      /(      0(      1(      2(      3(      4(      5(      6(      7(      8(      9(      :(      ;(      <(      =(      >(      ?(      @(      A(      B(      C(      D(      E(      F(      G(      H(      I(      J(      K(      L(      M(      N(      O(      P(      Q(      R(      S(      T(      U(      V(      W(      X(      Y(      Z(      [(      \(      ](      ^(      _(      `(      a(      b(      c(      d(      e(      f(      g(      h(      i(      j(      k(      l(      m(      n(      o(      p(      q(      r(      s(      t(      u(      v(      w(      x(      y(      z(      {(      |(      }(      ~(      (      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(      �(       )      )      )      )      )      )      )      )      )      	)      
)      )      )      )      )      )      )      )      )      )      )      )      )      )      )      )      )      )      )      )      )      )       )      !)      ")      #)      $)      %)      &)      ')      ()      ))      *)      +)      ,)      -)      .)      /)      0)      1)      2)      3)      4)      5)      6)      7)      8)      9)      :)      ;)      <)      =)      >)      ?)      @)      A)      B)      C)      D)      E)      F)      G)      H)      I)      J)      K)      L)      M)      N)      O)      P)      Q)      R)      S)      T)      U)      V)      W)      X)      Y)      Z)      [)      \)      ])      ^)      _)      `)      a)      b)      c)      d)      e)      f)      g)      h)      i)      j)      k)      l)      m)      n)      o)      p)      q)      r)      s)      t)      u)      v)      w)      x)      y)      z)      {)      |)      })      ~)      )      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)      �)       *      *      *      *      *      *      *      *      *      	*      
*      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *      *       *      !*      "*      #*      $*      %*      &*      '*      (*      )*      **      +*      ,*      -*      .*      /*      0*      1*      2*      3*      4*      5*      6*      7*      8*      9*      :*      ;*      <*      =*      >*      ?*      @*      A*      B*      C*      D*      E*      F*      G*      H*      I*      J*      K*      L*      M*      N*      O*      P*      Q*      R*      S*      T*      U*      V*      W*      X*      Y*      Z*      [*      \*      ]*      ^*      _*      `*      a*      b*      c*      d*      e*      f*      g*      h*      i*      j*      k*      l*      m*      n*      o*      p*      q*      r*      s*      t*      u*      v*      w*      x*      y*      z*      {*      |*      }*      ~*      *      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*      �*       +      +      +      +      +      +      +      +      +      	+      
+      +      +      +      +      +      +      +      +      +      +      +      +      +      +      +      +      +      +      +      +      +       +      !+      "+      #+      $+      %+      &+      '+      (+      )+      *+      ++      ,+      -+      .+      /+      0+      1+      2+      3+      4+      5+      6+      7+      8+      9+      :+      ;+      <+      =+      >+      ?+      @+      A+      B+      C+      D+      E+      F+      G+      H+      I+      J+      K+      L+      M+      N+      O+      P+      Q+      R+      S+      T+      U+      V+      W+      X+      Y+      Z+      [+      \+      ]+      ^+      _+      `+      a+      b+      c+      d+      e+      f+      g+      h+      i+      j+      k+      l+      m+      n+      o+      p+      q+      r+      s+      t+      u+      v+      w+      x+      y+      z+      {+      |+      }+      ~+      +      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+      �+       ,      ,      ,      ,      ,      ,      ,      ,      ,      	,      
,      ,      ,      ,      ,      ,      ,      ,      ,      ,      ,      ,      ,      ,      ,      ,      ,      ,      ,      ,      ,      ,       ,      !,      ",      #,      $,      %,      &,      ',      (,      ),      *,      +,      ,,      -,      .,      /,      0,      1,      2,      3,      4,      5,      6,      7,      8,      9,      :,      ;,      <,      =,      >,      ?,      @,      A,      B,      C,      D,      E,      F,      G,      H,      I,      J,      K,      L,      M,      N,      O,      P,      Q,      R,      S,      T,      U,      V,      W,      X,      Y,      Z,      [,      \,      ],      ^,      _,      `,      a,      b,      c,      d,      e,      f,      g,      h,      i,      j,      k,      l,      m,      n,      o,      p,      q,      r,      s,      t,      u,      v,      w,      x,      y,      z,      {,      |,      },      ~,      ,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,      �,       -      -      -      -      -      -      -      -      -      	-      
-      -      -      -      -      -      -      -      -      -      -      -      -      -      -      -      -      -      -      -      -      -       -      !-      "-      #-      $-      %-      &-      '-      (-      )-      *-      +-      ,-      --      .-      /-      0-      1-      2-      3-      4-      5-      6-      7-      8-      9-      :-      ;-      <-      =-      >-      ?-      @-      A-      B-      C-      D-      E-      F-      G-      H-      I-      J-      K-      L-      M-      N-      O-      P-      Q-      R-      S-      T-      U-      V-      W-      X-      Y-      Z-      [-      \-      ]-      ^-      _-      `-      a-      b-      c-      d-      e-      f-      g-      h-      i-      j-      k-      l-      m-      n-      o-      p-      q-      r-      s-      t-      u-      v-      w-      x-      y-      z-      {-      |-      }-      ~-      -      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-      �-       .      .      .      .      .      .      .      .      .      	.      
.      .      .      .      .      .      .      .      .      .      .      .      .      .      .      .      .      .      .      .      .      .       .      !.      ".      #.      $.      %.      &.      '.      (.      ).      *.      +.      ,.      -.      ..      /.      0.      1.      2.      3.      4.      5.      6.      7.      8.      9.      :.      ;.      <.      =.      >.      ?.      @.      A.      B.      C.      D.      E.      F.      G.      H.      I.      J.      K.      L.      M.      N.      O.      P.      Q.      R.      S.      T.      U.      V.      W.      X.      Y.      Z.      [.      \.      ].      ^.      _.      `.      a.      b.      c.      d.      e.      f.      g.      h.      i.      j.      k.      l.      m.      n.      o.      p.      q.      r.      s.      t.      u.      v.      w.      x.      y.      z.      {.      |.      }.      ~.      .      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.      �.       /      /      /      /      /      /      /      /      /      	/      
/      /      /      /      /      /      /      /      /      /      /      /      /      /      /      /      /      /      /      /      /      /       /      !/      "/      #/      $/      %/      &/      '/      (/      )/      */      +/      ,/      -/      ./      //      0/      1/      2/      3/      4/      5/      6/      7/      8/      9/      :/      ;/      </      =/      >/      ?/      @/      A/      B/      C/      D/      E/      F/      G/      H/      I/      J/      K/      L/      M/      N/      O/      P/      Q/      R/      S/      T/      U/      V/      W/      X/      Y/      Z/      [/      \/      ]/      ^/      _/      `/      a/      b/      c/      d/      e/      f/      g/      h/      i/      j/      k/      l/      m/      n/      o/      p/      q/      r/      s/      t/      u/      v/      w/      x/      y/      z/      {/      |/      }/      ~/      /      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/      �/       0      0      0      0      0      0      0      0      0      	0      
0      0      0      0      0      0      0      0      0      0      0      0      0      0      0      0      0      0      0      0      0      0       0      !0      "0      #0      $0      %0      &0      '0      (0      )0      *0      +0      ,0      -0      .0      /0      00      10      20      30      40      50      60      70      80      90      :0      ;0      <0      =0      >0      ?0      @0      A0      B0      C0      D0      E0      F0      G0      H0      I0      J0      K0      L0      M0      N0      O0      P0      Q0      R0      S0      T0      U0      V0      W0      X0      Y0      Z0      [0      \0      ]0      ^0      _0      `0      a0      b0      c0      d0      e0      f0      g0      h0      i0      j0      k0      l0      m0      n0      o0      p0      q0      r0      s0      t0      u0      v0      w0      x0      y0      z0      {0      |0      }0      ~0      0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0      �0       1      1      1      1      1      1      1      1      1      	1      
1      1      1      1      1      1      1      1      1      1      1      1      1      1      1      1      1      1      1      1      1      1       1      !1      "1      #1      $1      %1      &1      '1      (1      )1      *1      +1      ,1      -1      .1      /1      01      11      21      31      41      51      61      71      81      91      :1      ;1      <1      =1      >1      ?1      @1      A1      B1      C1      D1      E1      F1      G1      H1      I1      J1      K1      L1      M1      N1      O1      P1      Q1      R1      S1      T1      U1      V1      W1      X1      Y1      Z1      [1      \1      ]1      ^1      _1      `1      a1      b1      c1      d1      e1      f1      g1      h1      i1      j1      k1      l1      m1      n1      o1      p1      q1      r1      s1      t1      u1      v1      w1      x1      y1      z1      {1      |1      }1      ~1      1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1      �1       2      2      2      2      2      2      2      2      2      	2      
2      2      2      2      2      2      2      2      2      2      2      2      2      2      2      2      2      2      2      2      2      2       2      !2      "2      #2      $2      %2      &2      '2      (2      )2      *2      +2      ,2      -2      .2      /2      02      12      22      32      42      52      62      72      82      92      :2      ;2      <2      =2      >2      ?2      @2      A2      B2      C2      D2      E2      F2      G2      H2      I2      J2      K2      L2      M2      N2      O2      P2      Q2      R2      S2      T2      U2      V2      W2      X2      Y2      Z2      [2      \2      ]2      ^2      _2      `2      a2      b2      c2      d2      e2      f2      g2      h2      i2      j2      k2      l2      m2      n2      o2      p2      q2      r2      s2      t2      u2      v2      w2      x2      y2      z2      {2      |2      }2      ~2      2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2      �2       3      3      3      3      3      3      3      3      3      	3      
3      3      3      3      3      3      3      3      3      3      3      3      3      3      3      3      3      3      3      3      3      3       3      !3      "3      #3      $3      %3      &3      '3      (3      )3      *3      +3      ,3      -3      .3      /3      03      13      23      33      43      53      63      73      83      93      :3      ;3      <3      =3      >3      ?3      @3      A3      B3      C3      D3      E3      F3      G3      H3      I3      J3      K3      L3      M3      N3      O3      P3      Q3      R3      S3      T3      U3      V3      W3      X3      Y3      Z3      [3      \3      ]3      ^3      _3      `3      a3      b3      c3      d3      e3      f3      g3      h3      i3      j3      k3      l3      m3      n3      o3      p3      q3      r3      s3      t3      u3      v3      w3      x3      y3      z3      {3      |3      }3      ~3      3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3      �3       4      4      4      4      4      4      4      4      4      	4      
4      4      4      4      4      4      4      4      4      4      4      4      4      4      4      4      4      4      4      4      4      4       4      !4      "4      #4      $4      %4      &4      '4      (4      )4      *4      +4      ,4      -4      .4      /4      04      14      24      34      44      54      64      74      84      94      :4      ;4      <4      =4      >4      ?4      @4      A4      B4      C4      D4      E4      F4      G4      H4      I4      J4      K4      L4      M4      N4      O4      P4      Q4      R4      S4      T4      U4      V4      W4      X4      Y4      Z4      [4      \4      ]4      ^4      _4      `4      a4      b4      c4      d4      e4      f4      g4      h4      i4      j4      k4      l4      m4      n4      o4      p4      q4      r4      s4      t4      u4      v4      w4      x4      y4      z4      {4      |4      }4      ~4      4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4      �4       5      5      5      5      5      5      5      5      5      	5      
5      5      5      5      5      5      5      5      5      5      5      5      5      5      5      5      5      5      5      5      5      5       5      !5      "5      #5      $5      %5      &5      '5      (5      )5      *5      +5      ,5      -5      .5      /5      05      15      25      35      45      55      65      75      85      95      :5      ;5      <5      =5      >5      ?5      @5      A5      B5      C5      D5      E5      F5      G5      H5      I5      J5      K5      L5      M5      N5      O5      P5      Q5      R5      S5      T5      U5      V5      W5      X5      Y5      Z5      [5      \5      ]5      ^5      _5      `5      a5      b5      c5      d5      e5      f5      g5      h5      i5      j5      k5      l5      m5      n5      o5      p5      q5      r5      s5      t5      u5      v5      w5      x5      y5      z5      {5      |5      }5      ~5      5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5      �5       6      6      6      6      6      6      6      6      6      	6      
6      6      6      6      6      6      6      6      6      6      6      6      6      6      6      6      6      6      6      6      6      6       6      !6      "6      #6      $6      %6      &6      '6      (6      )6      *6      +6      ,6      -6      .6      /6      06      16      26      36      46      56      66      76      86      96      :6      ;6      <6      =6      >6      ?6      @6      A6      B6      C6      D6      E6      F6      G6      H6      I6      J6      K6      L6      M6      N6      O6      P6      Q6      R6      S6      T6      U6      V6      W6      X6      Y6      Z6      [6      \6      ]6      ^6      _6      `6      a6      b6      c6      d6      e6      f6      g6      h6      i6      j6      k6      l6      m6      n6      o6      p6      q6      r6      s6      t6      u6      v6      w6      x6      y6      z6      {6      |6      }6      ~6      6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6      �6       7      7      7      7      7      7      7      7      7      	7      
7      7      7      7      7      7      7      7      7      7      7      7      7      7      7      7      7      7      7      7      7      7       7      !7      "7      #7      $7      %7      &7      '7      (7      )7      *7      +7      ,7      -7      .7      /7      07      17      27      37      47      57      67      77      87      97      :7      ;7      <7      =7      >7      ?7      @7      A7      B7      C7      D7      E7      F7      G7      H7      I7      J7      K7      L7      M7      N7      O7      P7      Q7      R7      S7      T7      U7      V7      W7      X7      Y7      Z7      [7      \7      ]7      ^7      _7      `7      a7      b7      c7      d7      e7      f7      g7      h7      i7      j7      k7      l7      m7      n7      o7      p7      q7      r7      s7      t7      u7      v7      w7      x7      y7      z7      {7      |7      }7      ~7      7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7      �7       8      8      8      8      8      8      8      8      8      	8      
8      8      8      8      8      8      8      8      8      8      8      8      8      8      8      8      8      8      8      8      8      8       8      !8      "8      #8      $8      %8      &8      '8      (8      )8      *8      +8      ,8      -8      .8      /8      08      18      28      38      48      58      68      78      88      98      :8      ;8      <8      =8      >8      ?8      @8      A8      B8      C8      D8      E8      F8      G8      H8      I8      J8      K8      L8      M8      N8      O8      P8      Q8      R8      S8      T8      U8      V8      W8      X8      Y8      Z8      [8      \8      ]8      ^8      _8      `8      a8      b8      c8      d8      e8      f8      g8      h8      i8      j8      k8      l8      m8      n8      o8      p8      q8      r8      s8      t8      u8      v8      w8      x8      y8      z8      {8      |8      }8      ~8      8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8      �8       9      9      9      9      9      9      9      9      9      	9      
9      9      9      9      9      9      9      9      9      9      9      9      9      9      9      9      9      9      9      9      9      9       9      !9      "9      #9      $9      %9      &9      '9      (9      )9      *9      +9      ,9      -9      .9      /9      09      19      29      39      49      59      69      79      89      99      :9      ;9      <9      =9      >9      ?9      @9      A9      B9      C9      D9      E9      F9      G9      H9      I9      J9      K9      L9      M9      N9      O9      P9      Q9      R9      S9      T9      U9      V9      W9      X9      Y9      Z9      [9      \9      ]9      ^9      _9      `9      a9      b9      c9      d9      e9      f9      g9      h9      i9      j9      k9      l9      m9      n9      o9      p9      q9      r9      s9      t9      u9      v9      w9      x9      y9      z9      {9      |9      }9      ~9      9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9      �9       :      :      :      :      :      :      :      :      :      	:      
:      :      :      :      :      :      :      :      :      :      :      :      :      :      :      :      :      :      :      :      :      :       :      !:      ":      #:      $:      %:      &:      ':      (:      ):      *:      +:      ,:      -:      .:      /:      0:      1:      2:      3:      4:      5:      6:      7:      8:      9:      ::      ;:      <:      =:      >:      ?:      @:      A:      B:      C:      D:      E:      F:      G:      H:      I:      J:      K:      L:      M:      N:      O:      P:      Q:      R:      S:      T:      U:      V:      W:      X:      Y:      Z:      [:      \:      ]:      ^:      _:      `:      a:      b:      c:      d:      e:      f:      g:      h:      i:      j:      k:      l:      m:      n:      o:      p:      q:      r:      s:      t:      u:      v:      w:      x:      y:      z:      {:      |:      }:      ~:      :      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:      �:       ;      ;      ;      ;      ;      ;      ;      ;      ;      	;      
;      ;      ;      ;      ;      ;      ;      ;      ;      ;      ;      ;      ;      ;      ;      ;      ;      ;      ;      ;      ;      ;       ;      !;      ";      #;      $;      %;      &;      ';      (;      );      *;      +;      ,;      -;      .;      /;      0;      1;      2;      3;      4;      5;      6;      7;      8;      9;      :;      ;;      <;      =;      >;      ?;      @;      A;      B;      C;      D;      E;      F;      G;      H;      I;      J;      K;      L;      M;      N;      O;      P;      Q;      R;      S;      T;      U;      V;      W;      X;      Y;      Z;      [;      \;      ];      ^;      _;      `;      a;      b;      c;      d;      e;      f;      g;      h;      i;      j;      k;      l;      m;      n;      o;      p;      q;      r;      s;      t;      u;      v;      w;      x;      y;      z;      {;      |;      };      ~;      ;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;      �;       <      <      <      <      <      <      <      <      <      	<      
<      <      <      <      <      <      <      <      <      <      <      <      <      <      <      <      <      <      <      <      <      <       <      !<      "<      #<      $<      %<      &<      '<      (<      )<      *<      +<      ,<      -<      .<      /<      0<      1<      2<      3<      4<      5<      6<      7<      8<      9<      :<      ;<      <<      =<      ><      ?<      @<      A<      B<      C<      D<      E<      F<      G<      H<      I<      J<      K<      L<      M<      N<      O<      P<      Q<      R<      S<      T<      U<      V<      W<      X<      Y<      Z<      [<      \<      ]<      ^<      _<      `<      a<      b<      c<      d<      e<      f<      g<      h<      i<      j<      k<      l<      m<      n<      o<      p<      q<      r<      s<      t<      u<      v<      w<      x<      y<      z<      {<      |<      }<      ~<      <      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<      �<       =      =      =      =      =      =      =      =      =      	=      
=      =      =      =      =      =      =      =      =      =      =      =      =      =      =      =      =      =      =      =      =      =       =      !=      "=      #=      $=      %=      &=      '=      (=      )=      *=      +=      ,=      -=      .=      /=      0=      1=      2=      3=      4=      5=      6=      7=      8=      9=      :=      ;=      <=      ==      >=      ?=      @=      A=      B=      C=      D=      E=      F=      G=      H=      I=      J=      K=      L=      M=      N=      O=      P=      Q=      R=      S=      T=      U=      V=      W=      X=      Y=      Z=      [=      \=      ]=      ^=      _=      `=      a=      b=      c=      d=      e=      f=      g=      h=      i=      j=      k=      l=      m=      n=      o=      p=      q=      r=      s=      t=      u=      v=      w=      x=      y=      z=      {=      |=      }=      ~=      =      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=      �=       >      >      >      >      >      >      >      >      >      	>      
>      >      >      >      >      >      >      >      >      >      >      >      >      >      >      >      >      >      >      >      >      >       >      !>      ">      #>      $>      %>      &>      '>      (>      )>      *>      +>      ,>      ->      .>      />      0>      1>      2>      3>      4>      5>      6>      7>      8>      9>      :>      ;>      <>      =>      >>      ?>      @>      A>      B>      C>      D>      E>      F>      G>      H>      I>      J>      K>      L>      M>      N>      O>      P>      Q>      R>      S>      T>      U>      V>      W>      X>      Y>      Z>      [>      \>      ]>      ^>      _>      `>      a>      b>      c>      d>      e>      f>      g>      h>      i>      j>      k>      l>      m>      n>      o>      p>      q>      r>      s>      t>      u>      v>      w>      x>      y>      z>      {>      |>      }>      ~>      >      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>      �>       ?      ?      ?      ?      ?      ?      ?      ?      ?      	?      
?      ?      ?      ?      ?      ?      ?      ?      ?      ?      ?      ?      ?      ?      ?      ?      ?      ?      ?      ?      ?      ?       ?      !?      "?      #?      $?      %?      &?      '?      (?      )?      *?      +?      ,?      -?      .?      /?      0?      1?      2?      3?      4?      5?      6?      7?      8?      9?      :?      ;?      <?      =?      >?      ??      @?      A?      B?      C?      D?      E?      F?      G?      H?      I?      J?      K?      L?      M?      N?      O?      P?      Q?      R?      S?      T?      U?      V?      W?      X?      Y?      Z?      [?      \?      ]?      ^?      _?      `?      a?      b?      c?      d?      e?      f?      g?      h?      i?      j?      k?      l?      m?      n?      o?      p?      q?      r?      s?      t?      u?      v?      w?      x?      y?      z?      {?      |?      }?      ~?      ?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?      �?       @      @      @      @      @      @      @      @      @      	@      
@      @      @      @      @      @      @      @      @      @      @      @      @      @      @      @      @      @      @      @      @      @       @      !@      "@      #@      $@      %@      &@      '@      (@      )@      *@      +@      ,@      -@      .@      /@      0@      1@      2@      3@      4@      5@      6@      7@      8@      9@      :@      ;@      <@      =@      >@      ?@      @@      A@      B@      C@      D@      E@      F@      G@      H@      I@      J@      K@      L@      M@      N@      O@      P@      Q@      R@      S@      T@      U@      V@      W@      X@      Y@      Z@      [@      \@      ]@      ^@      _@      `@      a@      b@      c@      d@      e@      f@      g@      h@      i@      j@      k@      l@      m@      n@      o@      p@      q@      r@      s@      t@      u@      v@      w@      x@      y@      z@      {@      |@      }@      ~@      @      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@      �@       A      A      A      A      A      A      A      A      A      	A      
A      A      A      A      A      A      A      A      A      A      A      A      A      A      A      A      A      A      A      A      A      A       A      !A      "A      #A      $A      %A      &A      'A      (A      )A      *A      +A      ,A      -A      .A      /A      0A      1A      2A      3A      4A      5A      6A      7A      8A      9A      :A      ;A      <A      =A      >A      ?A      @A      AA      BA      CA      DA      EA      FA      GA      HA      IA      JA      KA      LA      MA      NA      OA      PA      QA      RA      SA      TA      UA      VA      WA      XA      YA      ZA      [A      \A      ]A      ^A      _A      `A      aA      bA      cA      dA      eA      fA      gA      hA      iA      jA      kA      lA      mA      nA      oA      pA      qA      rA      sA      tA      uA      vA      wA      xA      yA      zA      {A      |A      }A      ~A      A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A      �A       B      B      B      B      B      B      B      B      B      	B      
B      B      B      B      B      B      B      B      B      B      B      B      B      B      B      B      B      B      B      B      B      B       B      !B      "B      #B      $B      %B      &B      'B      (B      )B      *B      +B      ,B      -B      .B      /B      0B      1B      2B      3B      4B      5B      6B      7B      8B      9B      :B      ;B      <B      =B      >B      ?B      @B      AB      BB      CB      DB      EB      FB      GB      HB      IB      JB      KB      LB      MB      NB      OB      PB      QB      RB      SB      TB      UB      VB      WB      XB      YB      ZB      [B      \B      ]B      ^B      _B      `B      aB      bB      cB      dB      eB      fB      gB      hB      iB      jB      kB      lB      mB      nB      oB      pB      qB      rB      sB      tB      uB      vB      wB      xB      yB      zB      {B      |B      }B      ~B      B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B      �B       C      C      C      C      C      C      C      C      C      	C      
C      C      C      C      C      C      C      C      C      C      C      C      C      C      C      C      C      C      C      C      C      C       C      !C      "C      #C      $C      %C      &C      'C      (C      )C      *C      +C      ,C      -C      .C      /C      0C      1C      2C      3C      4C      5C      6C      7C      8C      9C      :C      ;C      <C      =C      >C      ?C      @C      AC      BC      CC      DC      EC      FC      GC      HC      IC      JC      KC      LC      MC      NC      OC      PC      QC      RC      SC      TC      UC      VC      WC      XC      YC      ZC      [C      \C      ]C      ^C      _C      `C      aC      bC      cC      dC      eC      fC      gC      hC      iC      jC      kC      lC      mC      nC      oC      pC      qC      rC      sC      tC      uC      vC      wC      xC      yC      zC      {C      |C      }C      ~C      C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C      �C       D      D      D      D      D      D      D      D      D      	D      
D      D      D      D      D      D      D      D      D      D      D      D      D      D      D      D      D      D      D      D      D      D       D      !D      "D      #D      $D      %D      &D      'D      (D      )D      *D      +D      ,D      -D      .D      /D      0D      1D      2D      3D      4D      5D      6D      7D      8D      9D      :D      ;D      <D      =D      >D      ?D      @D      AD      BD      CD      DD      ED      FD      GD      HD      ID      JD      KD      LD      MD      ND      OD      PD      QD      RD      SD      TD      UD      VD      WD      XD      YD      ZD      [D      \D      ]D      ^D      _D      `D      aD      bD      cD      dD      eD      fD      gD      hD      iD      jD      kD      lD      mD      nD      oD      pD      qD      rD      sD      tD      uD      vD      wD      xD      yD      zD      {D      |D      }D      ~D      D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D      �D       E      E      E      E      E      E      E      E      E      	E      
E      E      E      E      E      E      E      E      E      E      E      E      E      E      E      E      E      E      E      E      E      E       E      !E      "E      #E      $E      %E      &E      'E      (E      )E      *E      +E      ,E      -E      .E      /E      0E      1E      2E      3E      4E      5E      6E      7E      8E      9E      :E      ;E      <E      =E      >E      ?E      @E      AE      BE      CE      DE      EE      FE      GE      HE      IE      JE      KE      LE      ME      NE      OE      PE      QE      RE      SE      TE      UE      VE      WE      XE      YE      ZE      [E      \E      ]E      ^E      _E      `E      aE      bE      cE      dE      eE      fE      gE      hE      iE      jE      kE      lE      mE      nE      oE      pE      qE      rE      sE      tE      uE      vE      wE      xE      yE      zE      {E      |E      }E      ~E      E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E      �E       F      F      F      F      F      F      F      F      F      	F      
F      F      F      F      F      F      F      F      F      F      F      F      F      F      F      F      F      F      F      F      F      F       F      !F      "F      #F      $F      %F      &F      'F      (F      )F      *F      +F      ,F      -F      .F      /F      0F      1F      2F      3F      4F      5F      6F      7F      8F      9F      :F      ;F      <F      =F      >F      ?F      @F      AF      BF      CF      DF      EF      FF      GF      HF      IF      JF      KF      LF      MF      NF      OF      PF      QF      RF      SF      TF      UF      VF      WF      XF      YF      ZF      [F      \F      ]F      ^F      _F      `F      aF      bF      cF      dF      eF      fF      gF      hF      iF      jF      kF      lF      mF      nF      oF      pF      qF      rF      sF      tF      uF      vF      wF      xF      yF      zF      {F      |F      }F      ~F      F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F      �F       G      G      G      G      G      G      G      G      G      	G      
G      G      G      G      G      G      G      G      G      G      G      G      G      G      G      G      G      G      G      G      G      G       G      !G      "G      #G      $G      %G      &G      'G      (G      )G      *G      +G      ,G      -G      .G      /G      0G      1G      2G      3G      4G      5G      6G      7G      8G      9G      :G      ;G      <G      =G      >G      ?G      @G      AG      BG      CG      DG      EG      FG      GG      HG      IG      JG      KG      LG      MG      NG      OG      PG      QG      RG      SG      TG      UG      VG      WG      XG      YG      ZG      [G      \G      ]G      ^G      _G      `G      aG      bG      cG      dG      eG      fG      gG      hG      iG      jG      kG      lG      mG      nG      oG      pG      qG      rG      sG      tG      uG      vG      wG      xG      yG      zG      {G      |G      }G      ~G      G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G      �G       H      H      H      H      H      H      H      H      H      	H      
H      H      H      H      H      H      H      H      H      H      H      H      H      H      H      H      H      H      H      H      H      H       H      !H      "H      #H      $H      %H      &H      'H      (H      )H      *H      +H      ,H      -H      .H      /H      0H      1H      2H      3H      4H      5H      6H      7H      8H      9H      :H      ;H      <H      =H      >H      ?H      @H      AH      BH      CH      DH      EH      FH      GH      HH      IH      JH      KH      LH      MH      NH      OH      PH      QH      RH      SH      TH      UH      VH      WH      XH      YH      ZH      [H      \H      ]H      ^H      _H      `H      aH      bH      cH      dH      eH      fH      gH      hH      iH      jH      kH      lH      mH      nH      oH      pH      qH      rH      sH      tH      uH      vH      wH      xH      yH      zH      {H      |H      }H      ~H      H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H      �H       I      I      I      I      I      I      I      I      I      	I      
I      I      I      I      I      I      I      I      I      I      I      I      I      I      I      I      I      I      I      I      I      I       I      !I      "I      #I      $I      %I      &I      'I      (I      )I      *I      +I      ,I      -I      .I      /I      0I      1I      2I      3I      4I      5I      6I      7I      8I      9I      :I      ;I      <I      =I      >I      ?I      @I      AI      BI      CI      DI      EI      FI      GI      HI      II      JI      KI      LI      MI      NI      OI      PI      QI      RI      SI      TI      UI      VI      WI      XI      YI      ZI      [I      \I      ]I      ^I      _I      `I      aI      bI      cI      dI      eI      fI      gI      hI      iI      jI      kI      lI      mI      nI      oI      pI      qI      rI      sI      tI      uI      vI      wI      xI      yI      zI      {I      |I      }I      ~I      I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I      �I       J      J      J      J      J      J      J      J      J      	J      
J      J      J      J      J      J      J      J      J      J      J      J      J      J      J      J      J      J      J      J      J      J       J      !J      "J      #J      $J      %J      &J      'J      (J      )J      *J      +J      ,J      -J      .J      /J      0J      1J      2J      3J      4J      5J      6J      7J      8J      9J      :J      ;J      <J      =J      >J      ?J      @J      AJ      BJ      CJ      DJ      EJ      FJ      GJ      HJ      IJ      JJ      KJ      LJ      MJ      NJ      OJ      PJ      QJ      RJ      SJ      TJ      UJ      VJ      WJ      XJ      YJ      ZJ      [J      \J      ]J      ^J      _J      `J      aJ      bJ      cJ      dJ      eJ      fJ      gJ      hJ      iJ      jJ      kJ      lJ      mJ      nJ      oJ      pJ      qJ      rJ      sJ      tJ      uJ      vJ      wJ      xJ      yJ      zJ      {J      |J      }J      ~J      J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J      �J       K      K      K      K      K      K      K      K      K      	K      
K      K      K      K      K      K      K      K      K      K      K      K      K      K      K      K      K      K      K      K      K      K       K      !K      "K      #K      $K      %K      &K      'K      (K      )K      *K      +K      ,K      -K      .K      /K      0K      1K      2K      3K      4K      5K      6K      7K      8K      9K      :K      ;K      <K      =K      >K      ?K      @K      AK      BK      CK      DK      EK      FK      GK      HK      IK      JK      KK      LK      MK      NK      OK      PK      QK      RK      SK      TK      UK      VK      WK      XK      YK      ZK      [K      \K      ]K      ^K      _K      `K      aK      bK      cK      dK      eK      fK      gK      hK      iK      jK      kK      lK      mK      nK      oK      pK      qK      rK      sK      tK      uK      vK      wK      xK      yK      zK      {K      |K      }K      ~K      K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K      �K       L      L      L      L      L      L      L      L      L      	L      
L      L      L      L      L      L      L      L      L      L      L      L      L      L      L      L      L      L      L      L      L      L       L      !L      "L      #L      $L      %L      &L      'L      (L      )L      *L      +L      ,L      -L      .L      /L      0L      1L      2L      3L      4L      5L      6L      7L      8L      9L      :L      ;L      <L      =L      >L      ?L      @L      AL      BL      CL      DL      EL      FL      GL      HL      IL      JL      KL      LL      ML      NL      OL      PL      QL      RL      SL      TL      UL      VL      WL      XL      YL      ZL      [L      \L      ]L      ^L      _L      `L      aL      bL      cL      dL      eL      fL      gL      hL      iL      jL      kL      lL      mL      nL      oL      pL      qL      rL      sL      tL      uL      vL      wL      xL      yL      zL      {L      |L      }L      ~L      L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L      �L       M      M      M      M      M      M      M      M      M      	M      
M      M      M      M      M      M      M      M      M      M      M      M      M      M      M      M      M      M      M      M      M      M       M      !M      "M      #M      $M      %M      &M      'M      (M      )M      *M      +M      ,M      -M      .M      /M      0M      1M      2M      3M      4M      5M      6M      7M      8M      9M      :M      ;M      <M      =M      >M      ?M      @M      AM      BM      CM      DM      EM      FM      GM      HM      IM      JM      KM      LM      MM      NM      OM      PM      QM      RM      SM      TM      UM      VM      WM      XM      YM      ZM      [M      \M      ]M      ^M      _M      `M      aM      bM      cM      dM      eM      fM      gM      hM      iM      jM      kM      lM      mM      nM      oM      pM      qM      rM      sM      tM      uM      vM      wM      xM      yM      zM      {M      |M      }M      ~M      M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M      �M       N      N      N      N      N      N      N      N      N      	N      
N      N      N      N      N      N      N      N      N      N      N      N      N      N      N      N      N      N      N      N      N      N      
�
StatefulPartitionedCallStatefulPartitionedCall
hash_tableConst_4Const_5*
Tin
2	*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *#
fR
__inference_<lambda>_57621
�
PartitionedCallPartitionedCall*	
Tin
 *
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *#
fR
__inference_<lambda>_57626
8
NoOpNoOp^PartitionedCall^StatefulPartitionedCall
�
?MutableHashTable_lookup_table_export_values/LookupTableExportV2LookupTableExportV2MutableHashTable*
Tkeys0*
Tvalues0	*#
_class
loc:@MutableHashTable*
_output_shapes

::
�*
Const_6Const"/device:CPU:0*
_output_shapes
: *
dtype0*�*
value�*B�* B�*
�
layer_with_weights-0
layer-0
layer_with_weights-1
layer-1
	optimizer
	variables
trainable_variables
regularization_losses
	keras_api

signatures
"
	_lookup_layer

	keras_api
�
layer_with_weights-0
layer-0
layer-1
layer-2
layer-3
layer_with_weights-1
layer-4
	optimizer
	variables
trainable_variables
regularization_losses
	keras_api
 

1
2
3

0
1
2
 
�
non_trainable_variables

layers
metrics
layer_regularization_losses
layer_metrics
	variables
trainable_variables
regularization_losses
 
3
lookup_table
token_counts
	keras_api
 
b

embeddings
 	variables
!trainable_variables
"regularization_losses
#	keras_api
R
$	variables
%trainable_variables
&regularization_losses
'	keras_api
R
(	variables
)trainable_variables
*regularization_losses
+	keras_api
R
,	variables
-trainable_variables
.regularization_losses
/	keras_api
h

kernel
bias
0	variables
1trainable_variables
2regularization_losses
3	keras_api
v
4iter

5beta_1

6beta_2
	7decay
8learning_ratemnmompvqvrvs

0
1
2

0
1
2
 
�
9non_trainable_variables

:layers
;metrics
<layer_regularization_losses
=layer_metrics
	variables
trainable_variables
regularization_losses
PN
VARIABLE_VALUEembedding/embeddings&variables/1/.ATTRIBUTES/VARIABLE_VALUE
HF
VARIABLE_VALUEdense/kernel&variables/2/.ATTRIBUTES/VARIABLE_VALUE
FD
VARIABLE_VALUE
dense/bias&variables/3/.ATTRIBUTES/VARIABLE_VALUE
 

0
1

>0
?1
 
 

@_initializer
LJ
tableAlayer_with_weights-0/_lookup_layer/token_counts/.ATTRIBUTES/table
 

0

0
 
�
Anon_trainable_variables

Blayers
Cmetrics
Dlayer_regularization_losses
Elayer_metrics
 	variables
!trainable_variables
"regularization_losses
 
 
 
�
Fnon_trainable_variables

Glayers
Hmetrics
Ilayer_regularization_losses
Jlayer_metrics
$	variables
%trainable_variables
&regularization_losses
 
 
 
�
Knon_trainable_variables

Llayers
Mmetrics
Nlayer_regularization_losses
Olayer_metrics
(	variables
)trainable_variables
*regularization_losses
 
 
 
�
Pnon_trainable_variables

Qlayers
Rmetrics
Slayer_regularization_losses
Tlayer_metrics
,	variables
-trainable_variables
.regularization_losses

0
1

0
1
 
�
Unon_trainable_variables

Vlayers
Wmetrics
Xlayer_regularization_losses
Ylayer_metrics
0	variables
1trainable_variables
2regularization_losses
][
VARIABLE_VALUE	Adam/iter>layer_with_weights-1/optimizer/iter/.ATTRIBUTES/VARIABLE_VALUE
a_
VARIABLE_VALUEAdam/beta_1@layer_with_weights-1/optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUE
a_
VARIABLE_VALUEAdam/beta_2@layer_with_weights-1/optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUE
_]
VARIABLE_VALUE
Adam/decay?layer_with_weights-1/optimizer/decay/.ATTRIBUTES/VARIABLE_VALUE
om
VARIABLE_VALUEAdam/learning_rateGlayer_with_weights-1/optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUE
 
#
0
1
2
3
4

Z0
[1
 
 
4
	\total
	]count
^	variables
_	keras_api
D
	`total
	acount
b
_fn_kwargs
c	variables
d	keras_api
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
4
	etotal
	fcount
g	variables
h	keras_api
D
	itotal
	jcount
k
_fn_kwargs
l	variables
m	keras_api
OM
VARIABLE_VALUEtotal4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUE
OM
VARIABLE_VALUEcount4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUE

\0
]1

^	variables
QO
VARIABLE_VALUEtotal_14keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUE
QO
VARIABLE_VALUEcount_14keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUE
 

`0
a1

c	variables
fd
VARIABLE_VALUEtotal_2Ilayer_with_weights-1/keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUE
fd
VARIABLE_VALUEcount_2Ilayer_with_weights-1/keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUE

e0
f1

g	variables
fd
VARIABLE_VALUEtotal_3Ilayer_with_weights-1/keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUE
fd
VARIABLE_VALUEcount_3Ilayer_with_weights-1/keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUE
 

i0
j1

l	variables
��
VARIABLE_VALUEAdam/embedding/embeddings/mWvariables/1/.OPTIMIZER_SLOT/layer_with_weights-1/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
�~
VARIABLE_VALUEAdam/dense/kernel/mWvariables/2/.OPTIMIZER_SLOT/layer_with_weights-1/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/dense/bias/mWvariables/3/.OPTIMIZER_SLOT/layer_with_weights-1/optimizer/m/.ATTRIBUTES/VARIABLE_VALUE
��
VARIABLE_VALUEAdam/embedding/embeddings/vWvariables/1/.OPTIMIZER_SLOT/layer_with_weights-1/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
�~
VARIABLE_VALUEAdam/dense/kernel/vWvariables/2/.OPTIMIZER_SLOT/layer_with_weights-1/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
~|
VARIABLE_VALUEAdam/dense/bias/vWvariables/3/.OPTIMIZER_SLOT/layer_with_weights-1/optimizer/v/.ATTRIBUTES/VARIABLE_VALUE
�
(serving_default_text_vectorization_inputPlaceholder*#
_output_shapes
:���������*
dtype0*
shape:���������
�
StatefulPartitionedCall_1StatefulPartitionedCall(serving_default_text_vectorization_input
hash_tableConstConst_1Const_2embedding/embeddingsdense/kernel
dense/bias*
Tin

2		*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *,
f'R%
#__inference_signature_wrapper_57038
O
saver_filenamePlaceholder*
_output_shapes
: *
dtype0*
shape: 
�	
StatefulPartitionedCall_2StatefulPartitionedCallsaver_filename(embedding/embeddings/Read/ReadVariableOp dense/kernel/Read/ReadVariableOpdense/bias/Read/ReadVariableOp?MutableHashTable_lookup_table_export_values/LookupTableExportV2AMutableHashTable_lookup_table_export_values/LookupTableExportV2:1Adam/iter/Read/ReadVariableOpAdam/beta_1/Read/ReadVariableOpAdam/beta_2/Read/ReadVariableOpAdam/decay/Read/ReadVariableOp&Adam/learning_rate/Read/ReadVariableOptotal/Read/ReadVariableOpcount/Read/ReadVariableOptotal_1/Read/ReadVariableOpcount_1/Read/ReadVariableOptotal_2/Read/ReadVariableOpcount_2/Read/ReadVariableOptotal_3/Read/ReadVariableOpcount_3/Read/ReadVariableOp/Adam/embedding/embeddings/m/Read/ReadVariableOp'Adam/dense/kernel/m/Read/ReadVariableOp%Adam/dense/bias/m/Read/ReadVariableOp/Adam/embedding/embeddings/v/Read/ReadVariableOp'Adam/dense/kernel/v/Read/ReadVariableOp%Adam/dense/bias/v/Read/ReadVariableOpConst_6*%
Tin
2		*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *'
f"R 
__inference__traced_save_57729
�
StatefulPartitionedCall_3StatefulPartitionedCallsaver_filenameembedding/embeddingsdense/kernel
dense/biasMutableHashTable	Adam/iterAdam/beta_1Adam/beta_2
Adam/decayAdam/learning_ratetotalcounttotal_1count_1total_2count_2total_3count_3Adam/embedding/embeddings/mAdam/dense/kernel/mAdam/dense/bias/mAdam/embedding/embeddings/vAdam/dense/kernel/vAdam/dense/bias/v*#
Tin
2*
Tout
2*
_collective_manager_ids
 *
_output_shapes
: * 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� **
f%R#
!__inference__traced_restore_57808��
�l
�
G__inference_sequential_1_layer_call_and_return_conditional_losses_57144

inputsO
Ktext_vectorization_string_lookup_none_lookup_lookuptablefindv2_table_handleP
Ltext_vectorization_string_lookup_none_lookup_lookuptablefindv2_default_value	,
(text_vectorization_string_lookup_equal_y/
+text_vectorization_string_lookup_selectv2_t	?
+sequential_embedding_embedding_lookup_57128:
��A
/sequential_dense_matmul_readvariableop_resource:>
0sequential_dense_biasadd_readvariableop_resource:
identity��'sequential/dense/BiasAdd/ReadVariableOp�&sequential/dense/MatMul/ReadVariableOp�%sequential/embedding/embedding_lookup�>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2Z
text_vectorization/StringLowerStringLowerinputs*#
_output_shapes
:����������
%text_vectorization/StaticRegexReplaceStaticRegexReplace'text_vectorization/StringLower:output:0*#
_output_shapes
:���������*6
pattern+)[!"#$%&()\*\+,-\./:;<=>?@\[\\\]^_`{|}~\']*
rewrite e
$text_vectorization/StringSplit/ConstConst*
_output_shapes
: *
dtype0*
valueB B �
,text_vectorization/StringSplit/StringSplitV2StringSplitV2.text_vectorization/StaticRegexReplace:output:0-text_vectorization/StringSplit/Const:output:0*<
_output_shapes*
(:���������:���������:�
2text_vectorization/StringSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        �
4text_vectorization/StringSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       �
4text_vectorization/StringSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      �
,text_vectorization/StringSplit/strided_sliceStridedSlice6text_vectorization/StringSplit/StringSplitV2:indices:0;text_vectorization/StringSplit/strided_slice/stack:output:0=text_vectorization/StringSplit/strided_slice/stack_1:output:0=text_vectorization/StringSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:���������*

begin_mask*
end_mask*
shrink_axis_mask~
4text_vectorization/StringSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: �
6text_vectorization/StringSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
6text_vectorization/StringSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
.text_vectorization/StringSplit/strided_slice_1StridedSlice4text_vectorization/StringSplit/StringSplitV2:shape:0=text_vectorization/StringSplit/strided_slice_1/stack:output:0?text_vectorization/StringSplit/strided_slice_1/stack_1:output:0?text_vectorization/StringSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask�
Utext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCast5text_vectorization/StringSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:����������
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Cast7text_vectorization/StringSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: �
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShapeYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:�
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
^text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdhtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0htext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: �
ctext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreatergtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0ltext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: �
^text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCastetext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: �
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMaxYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0jtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: �
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :�
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2ftext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0htext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: �
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMulbtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximum[text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimum[text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 �
btext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincountYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0jtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:����������
\text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumitext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:����������
`text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R �
\text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2itext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:����������
>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2LookupTableFindV2Ktext_vectorization_string_lookup_none_lookup_lookuptablefindv2_table_handle5text_vectorization/StringSplit/StringSplitV2:values:0Ltext_vectorization_string_lookup_none_lookup_lookuptablefindv2_default_value*	
Tin0*

Tout0	*#
_output_shapes
:����������
&text_vectorization/string_lookup/EqualEqual5text_vectorization/StringSplit/StringSplitV2:values:0(text_vectorization_string_lookup_equal_y*
T0*#
_output_shapes
:����������
)text_vectorization/string_lookup/SelectV2SelectV2*text_vectorization/string_lookup/Equal:z:0+text_vectorization_string_lookup_selectv2_tGtext_vectorization/string_lookup/None_Lookup/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:����������
)text_vectorization/string_lookup/IdentityIdentity2text_vectorization/string_lookup/SelectV2:output:0*
T0	*#
_output_shapes
:���������q
/text_vectorization/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R �
'text_vectorization/RaggedToTensor/ConstConst*
_output_shapes
:*
dtype0	*%
valueB	"���������       �
6text_vectorization/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor0text_vectorization/RaggedToTensor/Const:output:02text_vectorization/string_lookup/Identity:output:08text_vectorization/RaggedToTensor/default_value:output:07text_vectorization/StringSplit/strided_slice_1:output:05text_vectorization/StringSplit/strided_slice:output:0*
T0	*
Tindex0	*
Tshape0	*(
_output_shapes
:����������*
num_row_partition_tensors*7
row_partition_types 
FIRST_DIM_SIZEVALUE_ROWIDS�
sequential/CastCast?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0*

DstT0*

SrcT0	*(
_output_shapes
:����������x
sequential/embedding/CastCastsequential/Cast:y:0*

DstT0*

SrcT0*(
_output_shapes
:�����������
%sequential/embedding/embedding_lookupResourceGather+sequential_embedding_embedding_lookup_57128sequential/embedding/Cast:y:0*
Tindices0*>
_class4
20loc:@sequential/embedding/embedding_lookup/57128*,
_output_shapes
:����������*
dtype0�
.sequential/embedding/embedding_lookup/IdentityIdentity.sequential/embedding/embedding_lookup:output:0*
T0*>
_class4
20loc:@sequential/embedding/embedding_lookup/57128*,
_output_shapes
:�����������
0sequential/embedding/embedding_lookup/Identity_1Identity7sequential/embedding/embedding_lookup/Identity:output:0*
T0*,
_output_shapes
:�����������
sequential/dropout/IdentityIdentity9sequential/embedding/embedding_lookup/Identity_1:output:0*
T0*,
_output_shapes
:����������|
:sequential/global_average_pooling1d/Mean/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :�
(sequential/global_average_pooling1d/MeanMean$sequential/dropout/Identity:output:0Csequential/global_average_pooling1d/Mean/reduction_indices:output:0*
T0*'
_output_shapes
:����������
sequential/dropout_1/IdentityIdentity1sequential/global_average_pooling1d/Mean:output:0*
T0*'
_output_shapes
:����������
&sequential/dense/MatMul/ReadVariableOpReadVariableOp/sequential_dense_matmul_readvariableop_resource*
_output_shapes

:*
dtype0�
sequential/dense/MatMulMatMul&sequential/dropout_1/Identity:output:0.sequential/dense/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:����������
'sequential/dense/BiasAdd/ReadVariableOpReadVariableOp0sequential_dense_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
sequential/dense/BiasAddBiasAdd!sequential/dense/MatMul:product:0/sequential/dense/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������p
IdentityIdentity!sequential/dense/BiasAdd:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp(^sequential/dense/BiasAdd/ReadVariableOp'^sequential/dense/MatMul/ReadVariableOp&^sequential/embedding/embedding_lookup?^text_vectorization/string_lookup/None_Lookup/LookupTableFindV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:���������: : : : : : : 2R
'sequential/dense/BiasAdd/ReadVariableOp'sequential/dense/BiasAdd/ReadVariableOp2P
&sequential/dense/MatMul/ReadVariableOp&sequential/dense/MatMul/ReadVariableOp2N
%sequential/embedding/embedding_lookup%sequential/embedding/embedding_lookup2�
>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2:K G
#
_output_shapes
:���������
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�C
�
__inference_adapt_step_57274
iterator

iterator_19
5none_lookup_table_find_lookuptablefindv2_table_handle:
6none_lookup_table_find_lookuptablefindv2_default_value	��IteratorGetNext�(None_lookup_table_find/LookupTableFindV2�,None_lookup_table_insert/LookupTableInsertV2�
IteratorGetNextIteratorGetNextiterator*
_class
loc:@iterator*#
_output_shapes
:���������*"
output_shapes
:���������*
output_types
2]
StringLowerStringLowerIteratorGetNext:components:0*#
_output_shapes
:����������
StaticRegexReplaceStaticRegexReplaceStringLower:output:0*#
_output_shapes
:���������*6
pattern+)[!"#$%&()\*\+,-\./:;<=>?@\[\\\]^_`{|}~\']*
rewrite R
StringSplit/ConstConst*
_output_shapes
: *
dtype0*
valueB B �
StringSplit/StringSplitV2StringSplitV2StaticRegexReplace:output:0StringSplit/Const:output:0*<
_output_shapes*
(:���������:���������:p
StringSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        r
!StringSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       r
!StringSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      �
StringSplit/strided_sliceStridedSlice#StringSplit/StringSplitV2:indices:0(StringSplit/strided_slice/stack:output:0*StringSplit/strided_slice/stack_1:output:0*StringSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:���������*

begin_mask*
end_mask*
shrink_axis_maskk
!StringSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: m
#StringSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:m
#StringSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
StringSplit/strided_slice_1StridedSlice!StringSplit/StringSplitV2:shape:0*StringSplit/strided_slice_1/stack:output:0,StringSplit/strided_slice_1/stack_1:output:0,StringSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask�
BStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCast"StringSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:����������
DStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Cast$StringSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: �
LStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShapeFStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:�
LStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
KStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdUStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0UStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: �
PStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : �
NStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreaterTStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0YStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: �
KStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCastRStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: �
NStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: �
JStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMaxFStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0WStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: �
LStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :�
JStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2SStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0UStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: �
JStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMulOStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0NStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: �
NStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximumHStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0NStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: �
NStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimumHStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0RStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: �
NStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 �
OStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincountFStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0RStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0WStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:����������
IStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : �
DStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumVStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0RStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:����������
MStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R �
IStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : �
DStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2VStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0JStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0RStringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:����������
UniqueWithCountsUniqueWithCounts"StringSplit/StringSplitV2:values:0*
T0*A
_output_shapes/
-:���������:���������:���������*
out_idx0	�
(None_lookup_table_find/LookupTableFindV2LookupTableFindV25none_lookup_table_find_lookuptablefindv2_table_handleUniqueWithCounts:y:06none_lookup_table_find_lookuptablefindv2_default_value",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*
_output_shapes
:|
addAddV2UniqueWithCounts:count:01None_lookup_table_find/LookupTableFindV2:values:0*
T0	*
_output_shapes
:�
,None_lookup_table_insert/LookupTableInsertV2LookupTableInsertV25none_lookup_table_find_lookuptablefindv2_table_handleUniqueWithCounts:y:0add:z:0)^None_lookup_table_find/LookupTableFindV2",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*
_output_shapes
 *(
_construction_contextkEagerRuntime*
_input_shapes

: : : : 2"
IteratorGetNextIteratorGetNext2T
(None_lookup_table_find/LookupTableFindV2(None_lookup_table_find/LookupTableFindV22\
,None_lookup_table_insert/LookupTableInsertV2,None_lookup_table_insert/LookupTableInsertV2:( $
"
_user_specified_name
iterator:@<

_output_shapes
: 
"
_user_specified_name
iterator:

_output_shapes
: 
�[
�
G__inference_sequential_1_layer_call_and_return_conditional_losses_56865

inputsO
Ktext_vectorization_string_lookup_none_lookup_lookuptablefindv2_table_handleP
Ltext_vectorization_string_lookup_none_lookup_lookuptablefindv2_default_value	,
(text_vectorization_string_lookup_equal_y/
+text_vectorization_string_lookup_selectv2_t	$
sequential_56857:
��"
sequential_56859:
sequential_56861:
identity��"sequential/StatefulPartitionedCall�>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2Z
text_vectorization/StringLowerStringLowerinputs*#
_output_shapes
:����������
%text_vectorization/StaticRegexReplaceStaticRegexReplace'text_vectorization/StringLower:output:0*#
_output_shapes
:���������*6
pattern+)[!"#$%&()\*\+,-\./:;<=>?@\[\\\]^_`{|}~\']*
rewrite e
$text_vectorization/StringSplit/ConstConst*
_output_shapes
: *
dtype0*
valueB B �
,text_vectorization/StringSplit/StringSplitV2StringSplitV2.text_vectorization/StaticRegexReplace:output:0-text_vectorization/StringSplit/Const:output:0*<
_output_shapes*
(:���������:���������:�
2text_vectorization/StringSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        �
4text_vectorization/StringSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       �
4text_vectorization/StringSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      �
,text_vectorization/StringSplit/strided_sliceStridedSlice6text_vectorization/StringSplit/StringSplitV2:indices:0;text_vectorization/StringSplit/strided_slice/stack:output:0=text_vectorization/StringSplit/strided_slice/stack_1:output:0=text_vectorization/StringSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:���������*

begin_mask*
end_mask*
shrink_axis_mask~
4text_vectorization/StringSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: �
6text_vectorization/StringSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
6text_vectorization/StringSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
.text_vectorization/StringSplit/strided_slice_1StridedSlice4text_vectorization/StringSplit/StringSplitV2:shape:0=text_vectorization/StringSplit/strided_slice_1/stack:output:0?text_vectorization/StringSplit/strided_slice_1/stack_1:output:0?text_vectorization/StringSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask�
Utext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCast5text_vectorization/StringSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:����������
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Cast7text_vectorization/StringSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: �
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShapeYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:�
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
^text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdhtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0htext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: �
ctext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreatergtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0ltext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: �
^text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCastetext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: �
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMaxYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0jtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: �
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :�
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2ftext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0htext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: �
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMulbtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximum[text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimum[text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 �
btext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincountYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0jtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:����������
\text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumitext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:����������
`text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R �
\text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2itext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:����������
>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2LookupTableFindV2Ktext_vectorization_string_lookup_none_lookup_lookuptablefindv2_table_handle5text_vectorization/StringSplit/StringSplitV2:values:0Ltext_vectorization_string_lookup_none_lookup_lookuptablefindv2_default_value*	
Tin0*

Tout0	*#
_output_shapes
:����������
&text_vectorization/string_lookup/EqualEqual5text_vectorization/StringSplit/StringSplitV2:values:0(text_vectorization_string_lookup_equal_y*
T0*#
_output_shapes
:����������
)text_vectorization/string_lookup/SelectV2SelectV2*text_vectorization/string_lookup/Equal:z:0+text_vectorization_string_lookup_selectv2_tGtext_vectorization/string_lookup/None_Lookup/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:����������
)text_vectorization/string_lookup/IdentityIdentity2text_vectorization/string_lookup/SelectV2:output:0*
T0	*#
_output_shapes
:���������q
/text_vectorization/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R �
'text_vectorization/RaggedToTensor/ConstConst*
_output_shapes
:*
dtype0	*%
valueB	"���������       �
6text_vectorization/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor0text_vectorization/RaggedToTensor/Const:output:02text_vectorization/string_lookup/Identity:output:08text_vectorization/RaggedToTensor/default_value:output:07text_vectorization/StringSplit/strided_slice_1:output:05text_vectorization/StringSplit/strided_slice:output:0*
T0	*
Tindex0	*
Tshape0	*(
_output_shapes
:����������*
num_row_partition_tensors*7
row_partition_types 
FIRST_DIM_SIZEVALUE_ROWIDS�
"sequential/StatefulPartitionedCallStatefulPartitionedCall?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0sequential_56857sequential_56859sequential_56861*
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *N
fIRG
E__inference_sequential_layer_call_and_return_conditional_losses_56777z
IdentityIdentity+sequential/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp#^sequential/StatefulPartitionedCall?^text_vectorization/string_lookup/None_Lookup/LookupTableFindV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:���������: : : : : : : 2H
"sequential/StatefulPartitionedCall"sequential/StatefulPartitionedCall2�
>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2:K G
#
_output_shapes
:���������
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
o
S__inference_global_average_pooling1d_layer_call_and_return_conditional_losses_56447

inputs
identityX
Mean/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :g
MeanMeaninputsMean/reduction_indices:output:0*
T0*'
_output_shapes
:���������U
IdentityIdentityMean:output:0*
T0*'
_output_shapes
:���������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :������������������:\ X
4
_output_shapes"
 :������������������
 
_user_specified_nameinputs
�
T
8__inference_global_average_pooling1d_layer_call_fn_57488

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *\
fWRU
S__inference_global_average_pooling1d_layer_call_and_return_conditional_losses_56447`
IdentityIdentityPartitionedCall:output:0*
T0*'
_output_shapes
:���������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :������������������:\ X
4
_output_shapes"
 :������������������
 
_user_specified_nameinputs
�

a
B__inference_dropout_layer_call_and_return_conditional_losses_57478

inputs
identity�R
dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  �?q
dropout/MulMulinputsdropout/Const:output:0*
T0*4
_output_shapes"
 :������������������C
dropout/ShapeShapeinputs*
T0*
_output_shapes
:�
$dropout/random_uniform/RandomUniformRandomUniformdropout/Shape:output:0*
T0*4
_output_shapes"
 :������������������*
dtype0[
dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *��L>�
dropout/GreaterEqualGreaterEqual-dropout/random_uniform/RandomUniform:output:0dropout/GreaterEqual/y:output:0*
T0*4
_output_shapes"
 :������������������|
dropout/CastCastdropout/GreaterEqual:z:0*

DstT0*

SrcT0
*4
_output_shapes"
 :������������������v
dropout/Mul_1Muldropout/Mul:z:0dropout/Cast:y:0*
T0*4
_output_shapes"
 :������������������f
IdentityIdentitydropout/Mul_1:z:0*
T0*4
_output_shapes"
 :������������������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :������������������:\ X
4
_output_shapes"
 :������������������
 
_user_specified_nameinputs
�
`
B__inference_dropout_layer_call_and_return_conditional_losses_57466

inputs

identity_1[
IdentityIdentityinputs*
T0*4
_output_shapes"
 :������������������h

Identity_1IdentityIdentity:output:0*
T0*4
_output_shapes"
 :������������������"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :������������������:\ X
4
_output_shapes"
 :������������������
 
_user_specified_nameinputs
�%
�
E__inference_sequential_layer_call_and_return_conditional_losses_56777

inputs	4
 embedding_embedding_lookup_56747:
��6
$dense_matmul_readvariableop_resource:3
%dense_biasadd_readvariableop_resource:
identity��dense/BiasAdd/ReadVariableOp�dense/MatMul/ReadVariableOp�embedding/embedding_lookupV
CastCastinputs*

DstT0*

SrcT0	*(
_output_shapes
:����������b
embedding/CastCastCast:y:0*

DstT0*

SrcT0*(
_output_shapes
:�����������
embedding/embedding_lookupResourceGather embedding_embedding_lookup_56747embedding/Cast:y:0*
Tindices0*3
_class)
'%loc:@embedding/embedding_lookup/56747*,
_output_shapes
:����������*
dtype0�
#embedding/embedding_lookup/IdentityIdentity#embedding/embedding_lookup:output:0*
T0*3
_class)
'%loc:@embedding/embedding_lookup/56747*,
_output_shapes
:�����������
%embedding/embedding_lookup/Identity_1Identity,embedding/embedding_lookup/Identity:output:0*
T0*,
_output_shapes
:����������Z
dropout/dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
dropout/dropout/MulMul.embedding/embedding_lookup/Identity_1:output:0dropout/dropout/Const:output:0*
T0*,
_output_shapes
:����������s
dropout/dropout/ShapeShape.embedding/embedding_lookup/Identity_1:output:0*
T0*
_output_shapes
:�
,dropout/dropout/random_uniform/RandomUniformRandomUniformdropout/dropout/Shape:output:0*
T0*,
_output_shapes
:����������*
dtype0c
dropout/dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *��L>�
dropout/dropout/GreaterEqualGreaterEqual5dropout/dropout/random_uniform/RandomUniform:output:0'dropout/dropout/GreaterEqual/y:output:0*
T0*,
_output_shapes
:�����������
dropout/dropout/CastCast dropout/dropout/GreaterEqual:z:0*

DstT0*

SrcT0
*,
_output_shapes
:�����������
dropout/dropout/Mul_1Muldropout/dropout/Mul:z:0dropout/dropout/Cast:y:0*
T0*,
_output_shapes
:����������q
/global_average_pooling1d/Mean/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :�
global_average_pooling1d/MeanMeandropout/dropout/Mul_1:z:08global_average_pooling1d/Mean/reduction_indices:output:0*
T0*'
_output_shapes
:���������\
dropout_1/dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
dropout_1/dropout/MulMul&global_average_pooling1d/Mean:output:0 dropout_1/dropout/Const:output:0*
T0*'
_output_shapes
:���������m
dropout_1/dropout/ShapeShape&global_average_pooling1d/Mean:output:0*
T0*
_output_shapes
:�
.dropout_1/dropout/random_uniform/RandomUniformRandomUniform dropout_1/dropout/Shape:output:0*
T0*'
_output_shapes
:���������*
dtype0e
 dropout_1/dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *��L>�
dropout_1/dropout/GreaterEqualGreaterEqual7dropout_1/dropout/random_uniform/RandomUniform:output:0)dropout_1/dropout/GreaterEqual/y:output:0*
T0*'
_output_shapes
:����������
dropout_1/dropout/CastCast"dropout_1/dropout/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:����������
dropout_1/dropout/Mul_1Muldropout_1/dropout/Mul:z:0dropout_1/dropout/Cast:y:0*
T0*'
_output_shapes
:����������
dense/MatMul/ReadVariableOpReadVariableOp$dense_matmul_readvariableop_resource*
_output_shapes

:*
dtype0�
dense/MatMulMatMuldropout_1/dropout/Mul_1:z:0#dense/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������~
dense/BiasAdd/ReadVariableOpReadVariableOp%dense_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
dense/BiasAddBiasAdddense/MatMul:product:0$dense/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������e
IdentityIdentitydense/BiasAdd:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp^dense/BiasAdd/ReadVariableOp^dense/MatMul/ReadVariableOp^embedding/embedding_lookup*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*-
_input_shapes
:����������: : : 2<
dense/BiasAdd/ReadVariableOpdense/BiasAdd/ReadVariableOp2:
dense/MatMul/ReadVariableOpdense/MatMul/ReadVariableOp28
embedding/embedding_lookupembedding/embedding_lookup:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
b
D__inference_dropout_1_layer_call_and_return_conditional_losses_56454

inputs

identity_1N
IdentityIdentityinputs*
T0*'
_output_shapes
:���������[

Identity_1IdentityIdentity:output:0*
T0*'
_output_shapes
:���������"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime*&
_input_shapes
:���������:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs
�
o
S__inference_global_average_pooling1d_layer_call_and_return_conditional_losses_56411

inputs
identityX
Mean/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :p
MeanMeaninputsMean/reduction_indices:output:0*
T0*0
_output_shapes
:������������������^
IdentityIdentityMean:output:0*
T0*0
_output_shapes
:������������������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*<
_input_shapes+
):'���������������������������:e a
=
_output_shapes+
):'���������������������������
 
_user_specified_nameinputs
�
.
__inference__initializer_57574
identityG
ConstConst*
_output_shapes
: *
dtype0*
value	B :E
IdentityIdentityConst:output:0*
T0*
_output_shapes
: "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes 
�	
�
@__inference_dense_layer_call_and_return_conditional_losses_56466

inputs0
matmul_readvariableop_resource:-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������_
IdentityIdentityBiasAdd:output:0^NoOp*
T0*'
_output_shapes
:���������w
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs
�	
c
D__inference_dropout_1_layer_call_and_return_conditional_losses_57527

inputs
identity�R
dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  �?d
dropout/MulMulinputsdropout/Const:output:0*
T0*'
_output_shapes
:���������C
dropout/ShapeShapeinputs*
T0*
_output_shapes
:�
$dropout/random_uniform/RandomUniformRandomUniformdropout/Shape:output:0*
T0*'
_output_shapes
:���������*
dtype0[
dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *��L>�
dropout/GreaterEqualGreaterEqual-dropout/random_uniform/RandomUniform:output:0dropout/GreaterEqual/y:output:0*
T0*'
_output_shapes
:���������o
dropout/CastCastdropout/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:���������i
dropout/Mul_1Muldropout/Mul:z:0dropout/Cast:y:0*
T0*'
_output_shapes
:���������Y
IdentityIdentitydropout/Mul_1:z:0*
T0*'
_output_shapes
:���������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*&
_input_shapes
:���������:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs
�
�
E__inference_sequential_layer_call_and_return_conditional_losses_56703

inputs	4
 embedding_embedding_lookup_56687:
��6
$dense_matmul_readvariableop_resource:3
%dense_biasadd_readvariableop_resource:
identity��dense/BiasAdd/ReadVariableOp�dense/MatMul/ReadVariableOp�embedding/embedding_lookupV
CastCastinputs*

DstT0*

SrcT0	*(
_output_shapes
:����������b
embedding/CastCastCast:y:0*

DstT0*

SrcT0*(
_output_shapes
:�����������
embedding/embedding_lookupResourceGather embedding_embedding_lookup_56687embedding/Cast:y:0*
Tindices0*3
_class)
'%loc:@embedding/embedding_lookup/56687*,
_output_shapes
:����������*
dtype0�
#embedding/embedding_lookup/IdentityIdentity#embedding/embedding_lookup:output:0*
T0*3
_class)
'%loc:@embedding/embedding_lookup/56687*,
_output_shapes
:�����������
%embedding/embedding_lookup/Identity_1Identity,embedding/embedding_lookup/Identity:output:0*
T0*,
_output_shapes
:�����������
dropout/IdentityIdentity.embedding/embedding_lookup/Identity_1:output:0*
T0*,
_output_shapes
:����������q
/global_average_pooling1d/Mean/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :�
global_average_pooling1d/MeanMeandropout/Identity:output:08global_average_pooling1d/Mean/reduction_indices:output:0*
T0*'
_output_shapes
:���������x
dropout_1/IdentityIdentity&global_average_pooling1d/Mean:output:0*
T0*'
_output_shapes
:����������
dense/MatMul/ReadVariableOpReadVariableOp$dense_matmul_readvariableop_resource*
_output_shapes

:*
dtype0�
dense/MatMulMatMuldropout_1/Identity:output:0#dense/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������~
dense/BiasAdd/ReadVariableOpReadVariableOp%dense_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
dense/BiasAddBiasAdddense/MatMul:product:0$dense/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������e
IdentityIdentitydense/BiasAdd:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp^dense/BiasAdd/ReadVariableOp^dense/MatMul/ReadVariableOp^embedding/embedding_lookup*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*-
_input_shapes
:����������: : : 2<
dense/BiasAdd/ReadVariableOpdense/BiasAdd/ReadVariableOp2:
dense/MatMul/ReadVariableOpdense/MatMul/ReadVariableOp28
embedding/embedding_lookupembedding/embedding_lookup:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
E
)__inference_dropout_1_layer_call_fn_57505

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *M
fHRF
D__inference_dropout_1_layer_call_and_return_conditional_losses_56454`
IdentityIdentityPartitionedCall:output:0*
T0*'
_output_shapes
:���������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*&
_input_shapes
:���������:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs
�	
�
#__inference_signature_wrapper_57038
text_vectorization_input
unknown
	unknown_0	
	unknown_1
	unknown_2	
	unknown_3:
��
	unknown_4:
	unknown_5:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCalltext_vectorization_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5*
Tin

2		*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *)
f$R"
 __inference__wrapped_model_56401o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:���������: : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:] Y
#
_output_shapes
:���������
2
_user_specified_nametext_vectorization_input:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�	
�
,__inference_sequential_1_layer_call_fn_57057

inputs
unknown
	unknown_0	
	unknown_1
	unknown_2	
	unknown_3:
��
	unknown_4:
	unknown_5:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5*
Tin

2		*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *P
fKRI
G__inference_sequential_1_layer_call_and_return_conditional_losses_56712o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:���������: : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:K G
#
_output_shapes
:���������
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
*__inference_sequential_layer_call_fn_56599
embedding_input
unknown:
��
	unknown_0:
	unknown_1:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallembedding_inputunknown	unknown_0	unknown_1*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *N
fIRG
E__inference_sequential_layer_call_and_return_conditional_losses_56579o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":������������������: : : 22
StatefulPartitionedCallStatefulPartitionedCall:a ]
0
_output_shapes
:������������������
)
_user_specified_nameembedding_input
�[
�
G__inference_sequential_1_layer_call_and_return_conditional_losses_56959
text_vectorization_inputO
Ktext_vectorization_string_lookup_none_lookup_lookuptablefindv2_table_handleP
Ltext_vectorization_string_lookup_none_lookup_lookuptablefindv2_default_value	,
(text_vectorization_string_lookup_equal_y/
+text_vectorization_string_lookup_selectv2_t	$
sequential_56951:
��"
sequential_56953:
sequential_56955:
identity��"sequential/StatefulPartitionedCall�>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2l
text_vectorization/StringLowerStringLowertext_vectorization_input*#
_output_shapes
:����������
%text_vectorization/StaticRegexReplaceStaticRegexReplace'text_vectorization/StringLower:output:0*#
_output_shapes
:���������*6
pattern+)[!"#$%&()\*\+,-\./:;<=>?@\[\\\]^_`{|}~\']*
rewrite e
$text_vectorization/StringSplit/ConstConst*
_output_shapes
: *
dtype0*
valueB B �
,text_vectorization/StringSplit/StringSplitV2StringSplitV2.text_vectorization/StaticRegexReplace:output:0-text_vectorization/StringSplit/Const:output:0*<
_output_shapes*
(:���������:���������:�
2text_vectorization/StringSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        �
4text_vectorization/StringSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       �
4text_vectorization/StringSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      �
,text_vectorization/StringSplit/strided_sliceStridedSlice6text_vectorization/StringSplit/StringSplitV2:indices:0;text_vectorization/StringSplit/strided_slice/stack:output:0=text_vectorization/StringSplit/strided_slice/stack_1:output:0=text_vectorization/StringSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:���������*

begin_mask*
end_mask*
shrink_axis_mask~
4text_vectorization/StringSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: �
6text_vectorization/StringSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
6text_vectorization/StringSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
.text_vectorization/StringSplit/strided_slice_1StridedSlice4text_vectorization/StringSplit/StringSplitV2:shape:0=text_vectorization/StringSplit/strided_slice_1/stack:output:0?text_vectorization/StringSplit/strided_slice_1/stack_1:output:0?text_vectorization/StringSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask�
Utext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCast5text_vectorization/StringSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:����������
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Cast7text_vectorization/StringSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: �
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShapeYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:�
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
^text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdhtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0htext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: �
ctext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreatergtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0ltext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: �
^text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCastetext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: �
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMaxYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0jtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: �
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :�
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2ftext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0htext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: �
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMulbtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximum[text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimum[text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 �
btext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincountYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0jtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:����������
\text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumitext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:����������
`text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R �
\text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2itext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:����������
>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2LookupTableFindV2Ktext_vectorization_string_lookup_none_lookup_lookuptablefindv2_table_handle5text_vectorization/StringSplit/StringSplitV2:values:0Ltext_vectorization_string_lookup_none_lookup_lookuptablefindv2_default_value*	
Tin0*

Tout0	*#
_output_shapes
:����������
&text_vectorization/string_lookup/EqualEqual5text_vectorization/StringSplit/StringSplitV2:values:0(text_vectorization_string_lookup_equal_y*
T0*#
_output_shapes
:����������
)text_vectorization/string_lookup/SelectV2SelectV2*text_vectorization/string_lookup/Equal:z:0+text_vectorization_string_lookup_selectv2_tGtext_vectorization/string_lookup/None_Lookup/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:����������
)text_vectorization/string_lookup/IdentityIdentity2text_vectorization/string_lookup/SelectV2:output:0*
T0	*#
_output_shapes
:���������q
/text_vectorization/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R �
'text_vectorization/RaggedToTensor/ConstConst*
_output_shapes
:*
dtype0	*%
valueB	"���������       �
6text_vectorization/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor0text_vectorization/RaggedToTensor/Const:output:02text_vectorization/string_lookup/Identity:output:08text_vectorization/RaggedToTensor/default_value:output:07text_vectorization/StringSplit/strided_slice_1:output:05text_vectorization/StringSplit/strided_slice:output:0*
T0	*
Tindex0	*
Tshape0	*(
_output_shapes
:����������*
num_row_partition_tensors*7
row_partition_types 
FIRST_DIM_SIZEVALUE_ROWIDS�
"sequential/StatefulPartitionedCallStatefulPartitionedCall?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0sequential_56951sequential_56953sequential_56955*
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *N
fIRG
E__inference_sequential_layer_call_and_return_conditional_losses_56703z
IdentityIdentity+sequential/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp#^sequential/StatefulPartitionedCall?^text_vectorization/string_lookup/None_Lookup/LookupTableFindV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:���������: : : : : : : 2H
"sequential/StatefulPartitionedCall"sequential/StatefulPartitionedCall2�
>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2:] Y
#
_output_shapes
:���������
2
_user_specified_nametext_vectorization_input:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�_
�
!__inference__traced_restore_57808
file_prefix9
%assignvariableop_embedding_embeddings:
��1
assignvariableop_1_dense_kernel:+
assignvariableop_2_dense_bias:M
Cmutablehashtable_table_restore_lookuptableimportv2_mutablehashtable: &
assignvariableop_3_adam_iter:	 (
assignvariableop_4_adam_beta_1: (
assignvariableop_5_adam_beta_2: '
assignvariableop_6_adam_decay: /
%assignvariableop_7_adam_learning_rate: "
assignvariableop_8_total: "
assignvariableop_9_count: %
assignvariableop_10_total_1: %
assignvariableop_11_count_1: %
assignvariableop_12_total_2: %
assignvariableop_13_count_2: %
assignvariableop_14_total_3: %
assignvariableop_15_count_3: C
/assignvariableop_16_adam_embedding_embeddings_m:
��9
'assignvariableop_17_adam_dense_kernel_m:3
%assignvariableop_18_adam_dense_bias_m:C
/assignvariableop_19_adam_embedding_embeddings_v:
��9
'assignvariableop_20_adam_dense_kernel_v:3
%assignvariableop_21_adam_dense_bias_v:
identity_23��AssignVariableOp�AssignVariableOp_1�AssignVariableOp_10�AssignVariableOp_11�AssignVariableOp_12�AssignVariableOp_13�AssignVariableOp_14�AssignVariableOp_15�AssignVariableOp_16�AssignVariableOp_17�AssignVariableOp_18�AssignVariableOp_19�AssignVariableOp_2�AssignVariableOp_20�AssignVariableOp_21�AssignVariableOp_3�AssignVariableOp_4�AssignVariableOp_5�AssignVariableOp_6�AssignVariableOp_7�AssignVariableOp_8�AssignVariableOp_9�2MutableHashTable_table_restore/LookupTableImportV2�
RestoreV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*�
value�B�B&variables/1/.ATTRIBUTES/VARIABLE_VALUEB&variables/2/.ATTRIBUTES/VARIABLE_VALUEB&variables/3/.ATTRIBUTES/VARIABLE_VALUEBFlayer_with_weights-0/_lookup_layer/token_counts/.ATTRIBUTES/table-keysBHlayer_with_weights-0/_lookup_layer/token_counts/.ATTRIBUTES/table-valuesB>layer_with_weights-1/optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB@layer_with_weights-1/optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB@layer_with_weights-1/optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB?layer_with_weights-1/optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEBGlayer_with_weights-1/optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEBIlayer_with_weights-1/keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEBIlayer_with_weights-1/keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEBIlayer_with_weights-1/keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEBIlayer_with_weights-1/keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEBWvariables/1/.OPTIMIZER_SLOT/layer_with_weights-1/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBWvariables/2/.OPTIMIZER_SLOT/layer_with_weights-1/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBWvariables/3/.OPTIMIZER_SLOT/layer_with_weights-1/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBWvariables/1/.OPTIMIZER_SLOT/layer_with_weights-1/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBWvariables/2/.OPTIMIZER_SLOT/layer_with_weights-1/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBWvariables/3/.OPTIMIZER_SLOT/layer_with_weights-1/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH�
RestoreV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*E
value<B:B B B B B B B B B B B B B B B B B B B B B B B B B �
	RestoreV2	RestoreV2file_prefixRestoreV2/tensor_names:output:0#RestoreV2/shape_and_slices:output:0"/device:CPU:0*x
_output_shapesf
d:::::::::::::::::::::::::*'
dtypes
2		[
IdentityIdentityRestoreV2:tensors:0"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOpAssignVariableOp%assignvariableop_embedding_embeddingsIdentity:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_1IdentityRestoreV2:tensors:1"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_1AssignVariableOpassignvariableop_1_dense_kernelIdentity_1:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_2IdentityRestoreV2:tensors:2"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_2AssignVariableOpassignvariableop_2_dense_biasIdentity_2:output:0"/device:CPU:0*
_output_shapes
 *
dtype0�
2MutableHashTable_table_restore/LookupTableImportV2LookupTableImportV2Cmutablehashtable_table_restore_lookuptableimportv2_mutablehashtableRestoreV2:tensors:3RestoreV2:tensors:4*	
Tin0*

Tout0	*#
_class
loc:@MutableHashTable*
_output_shapes
 ]

Identity_3IdentityRestoreV2:tensors:5"/device:CPU:0*
T0	*
_output_shapes
:�
AssignVariableOp_3AssignVariableOpassignvariableop_3_adam_iterIdentity_3:output:0"/device:CPU:0*
_output_shapes
 *
dtype0	]

Identity_4IdentityRestoreV2:tensors:6"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_4AssignVariableOpassignvariableop_4_adam_beta_1Identity_4:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_5IdentityRestoreV2:tensors:7"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_5AssignVariableOpassignvariableop_5_adam_beta_2Identity_5:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_6IdentityRestoreV2:tensors:8"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_6AssignVariableOpassignvariableop_6_adam_decayIdentity_6:output:0"/device:CPU:0*
_output_shapes
 *
dtype0]

Identity_7IdentityRestoreV2:tensors:9"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_7AssignVariableOp%assignvariableop_7_adam_learning_rateIdentity_7:output:0"/device:CPU:0*
_output_shapes
 *
dtype0^

Identity_8IdentityRestoreV2:tensors:10"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_8AssignVariableOpassignvariableop_8_totalIdentity_8:output:0"/device:CPU:0*
_output_shapes
 *
dtype0^

Identity_9IdentityRestoreV2:tensors:11"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_9AssignVariableOpassignvariableop_9_countIdentity_9:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_10IdentityRestoreV2:tensors:12"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_10AssignVariableOpassignvariableop_10_total_1Identity_10:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_11IdentityRestoreV2:tensors:13"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_11AssignVariableOpassignvariableop_11_count_1Identity_11:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_12IdentityRestoreV2:tensors:14"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_12AssignVariableOpassignvariableop_12_total_2Identity_12:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_13IdentityRestoreV2:tensors:15"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_13AssignVariableOpassignvariableop_13_count_2Identity_13:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_14IdentityRestoreV2:tensors:16"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_14AssignVariableOpassignvariableop_14_total_3Identity_14:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_15IdentityRestoreV2:tensors:17"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_15AssignVariableOpassignvariableop_15_count_3Identity_15:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_16IdentityRestoreV2:tensors:18"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_16AssignVariableOp/assignvariableop_16_adam_embedding_embeddings_mIdentity_16:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_17IdentityRestoreV2:tensors:19"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_17AssignVariableOp'assignvariableop_17_adam_dense_kernel_mIdentity_17:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_18IdentityRestoreV2:tensors:20"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_18AssignVariableOp%assignvariableop_18_adam_dense_bias_mIdentity_18:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_19IdentityRestoreV2:tensors:21"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_19AssignVariableOp/assignvariableop_19_adam_embedding_embeddings_vIdentity_19:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_20IdentityRestoreV2:tensors:22"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_20AssignVariableOp'assignvariableop_20_adam_dense_kernel_vIdentity_20:output:0"/device:CPU:0*
_output_shapes
 *
dtype0_
Identity_21IdentityRestoreV2:tensors:23"/device:CPU:0*
T0*
_output_shapes
:�
AssignVariableOp_21AssignVariableOp%assignvariableop_21_adam_dense_bias_vIdentity_21:output:0"/device:CPU:0*
_output_shapes
 *
dtype01
NoOpNoOp"/device:CPU:0*
_output_shapes
 �
Identity_22Identityfile_prefix^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_93^MutableHashTable_table_restore/LookupTableImportV2^NoOp"/device:CPU:0*
T0*
_output_shapes
: W
Identity_23IdentityIdentity_22:output:0^NoOp_1*
T0*
_output_shapes
: �
NoOp_1NoOp^AssignVariableOp^AssignVariableOp_1^AssignVariableOp_10^AssignVariableOp_11^AssignVariableOp_12^AssignVariableOp_13^AssignVariableOp_14^AssignVariableOp_15^AssignVariableOp_16^AssignVariableOp_17^AssignVariableOp_18^AssignVariableOp_19^AssignVariableOp_2^AssignVariableOp_20^AssignVariableOp_21^AssignVariableOp_3^AssignVariableOp_4^AssignVariableOp_5^AssignVariableOp_6^AssignVariableOp_7^AssignVariableOp_8^AssignVariableOp_93^MutableHashTable_table_restore/LookupTableImportV2*"
_acd_function_control_output(*
_output_shapes
 "#
identity_23Identity_23:output:0*C
_input_shapes2
0: : : : : : : : : : : : : : : : : : : : : : : : 2$
AssignVariableOpAssignVariableOp2(
AssignVariableOp_1AssignVariableOp_12*
AssignVariableOp_10AssignVariableOp_102*
AssignVariableOp_11AssignVariableOp_112*
AssignVariableOp_12AssignVariableOp_122*
AssignVariableOp_13AssignVariableOp_132*
AssignVariableOp_14AssignVariableOp_142*
AssignVariableOp_15AssignVariableOp_152*
AssignVariableOp_16AssignVariableOp_162*
AssignVariableOp_17AssignVariableOp_172*
AssignVariableOp_18AssignVariableOp_182*
AssignVariableOp_19AssignVariableOp_192(
AssignVariableOp_2AssignVariableOp_22*
AssignVariableOp_20AssignVariableOp_202*
AssignVariableOp_21AssignVariableOp_212(
AssignVariableOp_3AssignVariableOp_32(
AssignVariableOp_4AssignVariableOp_42(
AssignVariableOp_5AssignVariableOp_52(
AssignVariableOp_6AssignVariableOp_62(
AssignVariableOp_7AssignVariableOp_72(
AssignVariableOp_8AssignVariableOp_82(
AssignVariableOp_9AssignVariableOp_92h
2MutableHashTable_table_restore/LookupTableImportV22MutableHashTable_table_restore/LookupTableImportV2:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:)%
#
_class
loc:@MutableHashTable
�
,
__inference__destroyer_57564
identityG
ConstConst*
_output_shapes
: *
dtype0*
value	B :E
IdentityIdentityConst:output:0*
T0*
_output_shapes
: "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes 
�
T
8__inference_global_average_pooling1d_layer_call_fn_57483

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *0
_output_shapes
:������������������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *\
fWRU
S__inference_global_average_pooling1d_layer_call_and_return_conditional_losses_56411i
IdentityIdentityPartitionedCall:output:0*
T0*0
_output_shapes
:������������������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*<
_input_shapes+
):'���������������������������:e a
=
_output_shapes+
):'���������������������������
 
_user_specified_nameinputs
�
C
'__inference_dropout_layer_call_fn_57456

inputs
identity�
PartitionedCallPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *4
_output_shapes"
 :������������������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *K
fFRD
B__inference_dropout_layer_call_and_return_conditional_losses_56440m
IdentityIdentityPartitionedCall:output:0*
T0*4
_output_shapes"
 :������������������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :������������������:\ X
4
_output_shapes"
 :������������������
 
_user_specified_nameinputs
�7
�	
__inference__traced_save_57729
file_prefix3
/savev2_embedding_embeddings_read_readvariableop+
'savev2_dense_kernel_read_readvariableop)
%savev2_dense_bias_read_readvariableopJ
Fsavev2_mutablehashtable_lookup_table_export_values_lookuptableexportv2L
Hsavev2_mutablehashtable_lookup_table_export_values_lookuptableexportv2_1	(
$savev2_adam_iter_read_readvariableop	*
&savev2_adam_beta_1_read_readvariableop*
&savev2_adam_beta_2_read_readvariableop)
%savev2_adam_decay_read_readvariableop1
-savev2_adam_learning_rate_read_readvariableop$
 savev2_total_read_readvariableop$
 savev2_count_read_readvariableop&
"savev2_total_1_read_readvariableop&
"savev2_count_1_read_readvariableop&
"savev2_total_2_read_readvariableop&
"savev2_count_2_read_readvariableop&
"savev2_total_3_read_readvariableop&
"savev2_count_3_read_readvariableop:
6savev2_adam_embedding_embeddings_m_read_readvariableop2
.savev2_adam_dense_kernel_m_read_readvariableop0
,savev2_adam_dense_bias_m_read_readvariableop:
6savev2_adam_embedding_embeddings_v_read_readvariableop2
.savev2_adam_dense_kernel_v_read_readvariableop0
,savev2_adam_dense_bias_v_read_readvariableop
savev2_const_6

identity_1��MergeV2Checkpointsw
StaticRegexFullMatchStaticRegexFullMatchfile_prefix"/device:CPU:**
_output_shapes
: *
pattern
^s3://.*Z
ConstConst"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B.parta
Const_1Const"/device:CPU:**
_output_shapes
: *
dtype0*
valueB B
_temp/part�
SelectSelectStaticRegexFullMatch:output:0Const:output:0Const_1:output:0"/device:CPU:**
T0*
_output_shapes
: f

StringJoin
StringJoinfile_prefixSelect:output:0"/device:CPU:**
N*
_output_shapes
: L

num_shardsConst*
_output_shapes
: *
dtype0*
value	B :f
ShardedFilename/shardConst"/device:CPU:0*
_output_shapes
: *
dtype0*
value	B : �
ShardedFilenameShardedFilenameStringJoin:output:0ShardedFilename/shard:output:0num_shards:output:0"/device:CPU:0*
_output_shapes
: �
SaveV2/tensor_namesConst"/device:CPU:0*
_output_shapes
:*
dtype0*�
value�B�B&variables/1/.ATTRIBUTES/VARIABLE_VALUEB&variables/2/.ATTRIBUTES/VARIABLE_VALUEB&variables/3/.ATTRIBUTES/VARIABLE_VALUEBFlayer_with_weights-0/_lookup_layer/token_counts/.ATTRIBUTES/table-keysBHlayer_with_weights-0/_lookup_layer/token_counts/.ATTRIBUTES/table-valuesB>layer_with_weights-1/optimizer/iter/.ATTRIBUTES/VARIABLE_VALUEB@layer_with_weights-1/optimizer/beta_1/.ATTRIBUTES/VARIABLE_VALUEB@layer_with_weights-1/optimizer/beta_2/.ATTRIBUTES/VARIABLE_VALUEB?layer_with_weights-1/optimizer/decay/.ATTRIBUTES/VARIABLE_VALUEBGlayer_with_weights-1/optimizer/learning_rate/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEB4keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEBIlayer_with_weights-1/keras_api/metrics/0/total/.ATTRIBUTES/VARIABLE_VALUEBIlayer_with_weights-1/keras_api/metrics/0/count/.ATTRIBUTES/VARIABLE_VALUEBIlayer_with_weights-1/keras_api/metrics/1/total/.ATTRIBUTES/VARIABLE_VALUEBIlayer_with_weights-1/keras_api/metrics/1/count/.ATTRIBUTES/VARIABLE_VALUEBWvariables/1/.OPTIMIZER_SLOT/layer_with_weights-1/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBWvariables/2/.OPTIMIZER_SLOT/layer_with_weights-1/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBWvariables/3/.OPTIMIZER_SLOT/layer_with_weights-1/optimizer/m/.ATTRIBUTES/VARIABLE_VALUEBWvariables/1/.OPTIMIZER_SLOT/layer_with_weights-1/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBWvariables/2/.OPTIMIZER_SLOT/layer_with_weights-1/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEBWvariables/3/.OPTIMIZER_SLOT/layer_with_weights-1/optimizer/v/.ATTRIBUTES/VARIABLE_VALUEB_CHECKPOINTABLE_OBJECT_GRAPH�
SaveV2/shape_and_slicesConst"/device:CPU:0*
_output_shapes
:*
dtype0*E
value<B:B B B B B B B B B B B B B B B B B B B B B B B B B �	
SaveV2SaveV2ShardedFilename:filename:0SaveV2/tensor_names:output:0 SaveV2/shape_and_slices:output:0/savev2_embedding_embeddings_read_readvariableop'savev2_dense_kernel_read_readvariableop%savev2_dense_bias_read_readvariableopFsavev2_mutablehashtable_lookup_table_export_values_lookuptableexportv2Hsavev2_mutablehashtable_lookup_table_export_values_lookuptableexportv2_1$savev2_adam_iter_read_readvariableop&savev2_adam_beta_1_read_readvariableop&savev2_adam_beta_2_read_readvariableop%savev2_adam_decay_read_readvariableop-savev2_adam_learning_rate_read_readvariableop savev2_total_read_readvariableop savev2_count_read_readvariableop"savev2_total_1_read_readvariableop"savev2_count_1_read_readvariableop"savev2_total_2_read_readvariableop"savev2_count_2_read_readvariableop"savev2_total_3_read_readvariableop"savev2_count_3_read_readvariableop6savev2_adam_embedding_embeddings_m_read_readvariableop.savev2_adam_dense_kernel_m_read_readvariableop,savev2_adam_dense_bias_m_read_readvariableop6savev2_adam_embedding_embeddings_v_read_readvariableop.savev2_adam_dense_kernel_v_read_readvariableop,savev2_adam_dense_bias_v_read_readvariableopsavev2_const_6"/device:CPU:0*
_output_shapes
 *'
dtypes
2		�
&MergeV2Checkpoints/checkpoint_prefixesPackShardedFilename:filename:0^SaveV2"/device:CPU:0*
N*
T0*
_output_shapes
:�
MergeV2CheckpointsMergeV2Checkpoints/MergeV2Checkpoints/checkpoint_prefixes:output:0file_prefix"/device:CPU:0*
_output_shapes
 f
IdentityIdentityfile_prefix^MergeV2Checkpoints"/device:CPU:0*
T0*
_output_shapes
: Q

Identity_1IdentityIdentity:output:0^NoOp*
T0*
_output_shapes
: [
NoOpNoOp^MergeV2Checkpoints*"
_acd_function_control_output(*
_output_shapes
 "!

identity_1Identity_1:output:0*�
_input_shapes|
z: :
��::::: : : : : : : : : : : : : :
��:::
��::: 2(
MergeV2CheckpointsMergeV2Checkpoints:C ?

_output_shapes
: 
%
_user_specified_namefile_prefix:&"
 
_output_shapes
:
��:$ 

_output_shapes

:: 

_output_shapes
::

_output_shapes
::

_output_shapes
::

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :	

_output_shapes
: :


_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :

_output_shapes
: :&"
 
_output_shapes
:
��:$ 

_output_shapes

:: 

_output_shapes
::&"
 
_output_shapes
:
��:$ 

_output_shapes

:: 

_output_shapes
::

_output_shapes
: 
�
�
*__inference_sequential_layer_call_fn_57291

inputs
unknown:
��
	unknown_0:
	unknown_1:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *N
fIRG
E__inference_sequential_layer_call_and_return_conditional_losses_56473o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":������������������: : : 22
StatefulPartitionedCallStatefulPartitionedCall:X T
0
_output_shapes
:������������������
 
_user_specified_nameinputs
�
b
D__inference_dropout_1_layer_call_and_return_conditional_losses_57515

inputs

identity_1N
IdentityIdentityinputs*
T0*'
_output_shapes
:���������[

Identity_1IdentityIdentity:output:0*
T0*'
_output_shapes
:���������"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime*&
_input_shapes
:���������:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs
�
`
B__inference_dropout_layer_call_and_return_conditional_losses_56440

inputs

identity_1[
IdentityIdentityinputs*
T0*4
_output_shapes"
 :������������������h

Identity_1IdentityIdentity:output:0*
T0*4
_output_shapes"
 :������������������"!

identity_1Identity_1:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :������������������:\ X
4
_output_shapes"
 :������������������
 
_user_specified_nameinputs
�	
�
@__inference_dense_layer_call_and_return_conditional_losses_57546

inputs0
matmul_readvariableop_resource:-
biasadd_readvariableop_resource:
identity��BiasAdd/ReadVariableOp�MatMul/ReadVariableOpt
MatMul/ReadVariableOpReadVariableOpmatmul_readvariableop_resource*
_output_shapes

:*
dtype0i
MatMulMatMulinputsMatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������r
BiasAdd/ReadVariableOpReadVariableOpbiasadd_readvariableop_resource*
_output_shapes
:*
dtype0v
BiasAddBiasAddMatMul:product:0BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������_
IdentityIdentityBiasAdd:output:0^NoOp*
T0*'
_output_shapes
:���������w
NoOpNoOp^BiasAdd/ReadVariableOp^MatMul/ReadVariableOp*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 20
BiasAdd/ReadVariableOpBiasAdd/ReadVariableOp2.
MatMul/ReadVariableOpMatMul/ReadVariableOp:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs
�[
�
G__inference_sequential_1_layer_call_and_return_conditional_losses_56712

inputsO
Ktext_vectorization_string_lookup_none_lookup_lookuptablefindv2_table_handleP
Ltext_vectorization_string_lookup_none_lookup_lookuptablefindv2_default_value	,
(text_vectorization_string_lookup_equal_y/
+text_vectorization_string_lookup_selectv2_t	$
sequential_56704:
��"
sequential_56706:
sequential_56708:
identity��"sequential/StatefulPartitionedCall�>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2Z
text_vectorization/StringLowerStringLowerinputs*#
_output_shapes
:����������
%text_vectorization/StaticRegexReplaceStaticRegexReplace'text_vectorization/StringLower:output:0*#
_output_shapes
:���������*6
pattern+)[!"#$%&()\*\+,-\./:;<=>?@\[\\\]^_`{|}~\']*
rewrite e
$text_vectorization/StringSplit/ConstConst*
_output_shapes
: *
dtype0*
valueB B �
,text_vectorization/StringSplit/StringSplitV2StringSplitV2.text_vectorization/StaticRegexReplace:output:0-text_vectorization/StringSplit/Const:output:0*<
_output_shapes*
(:���������:���������:�
2text_vectorization/StringSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        �
4text_vectorization/StringSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       �
4text_vectorization/StringSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      �
,text_vectorization/StringSplit/strided_sliceStridedSlice6text_vectorization/StringSplit/StringSplitV2:indices:0;text_vectorization/StringSplit/strided_slice/stack:output:0=text_vectorization/StringSplit/strided_slice/stack_1:output:0=text_vectorization/StringSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:���������*

begin_mask*
end_mask*
shrink_axis_mask~
4text_vectorization/StringSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: �
6text_vectorization/StringSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
6text_vectorization/StringSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
.text_vectorization/StringSplit/strided_slice_1StridedSlice4text_vectorization/StringSplit/StringSplitV2:shape:0=text_vectorization/StringSplit/strided_slice_1/stack:output:0?text_vectorization/StringSplit/strided_slice_1/stack_1:output:0?text_vectorization/StringSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask�
Utext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCast5text_vectorization/StringSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:����������
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Cast7text_vectorization/StringSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: �
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShapeYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:�
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
^text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdhtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0htext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: �
ctext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreatergtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0ltext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: �
^text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCastetext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: �
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMaxYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0jtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: �
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :�
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2ftext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0htext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: �
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMulbtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximum[text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimum[text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 �
btext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincountYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0jtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:����������
\text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumitext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:����������
`text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R �
\text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2itext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:����������
>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2LookupTableFindV2Ktext_vectorization_string_lookup_none_lookup_lookuptablefindv2_table_handle5text_vectorization/StringSplit/StringSplitV2:values:0Ltext_vectorization_string_lookup_none_lookup_lookuptablefindv2_default_value*	
Tin0*

Tout0	*#
_output_shapes
:����������
&text_vectorization/string_lookup/EqualEqual5text_vectorization/StringSplit/StringSplitV2:values:0(text_vectorization_string_lookup_equal_y*
T0*#
_output_shapes
:����������
)text_vectorization/string_lookup/SelectV2SelectV2*text_vectorization/string_lookup/Equal:z:0+text_vectorization_string_lookup_selectv2_tGtext_vectorization/string_lookup/None_Lookup/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:����������
)text_vectorization/string_lookup/IdentityIdentity2text_vectorization/string_lookup/SelectV2:output:0*
T0	*#
_output_shapes
:���������q
/text_vectorization/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R �
'text_vectorization/RaggedToTensor/ConstConst*
_output_shapes
:*
dtype0	*%
valueB	"���������       �
6text_vectorization/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor0text_vectorization/RaggedToTensor/Const:output:02text_vectorization/string_lookup/Identity:output:08text_vectorization/RaggedToTensor/default_value:output:07text_vectorization/StringSplit/strided_slice_1:output:05text_vectorization/StringSplit/strided_slice:output:0*
T0	*
Tindex0	*
Tshape0	*(
_output_shapes
:����������*
num_row_partition_tensors*7
row_partition_types 
FIRST_DIM_SIZEVALUE_ROWIDS�
"sequential/StatefulPartitionedCallStatefulPartitionedCall?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0sequential_56704sequential_56706sequential_56708*
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *N
fIRG
E__inference_sequential_layer_call_and_return_conditional_losses_56703z
IdentityIdentity+sequential/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp#^sequential/StatefulPartitionedCall?^text_vectorization/string_lookup/None_Lookup/LookupTableFindV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:���������: : : : : : : 2H
"sequential/StatefulPartitionedCall"sequential/StatefulPartitionedCall2�
>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2:K G
#
_output_shapes
:���������
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
o
S__inference_global_average_pooling1d_layer_call_and_return_conditional_losses_57494

inputs
identityX
Mean/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :p
MeanMeaninputsMean/reduction_indices:output:0*
T0*0
_output_shapes
:������������������^
IdentityIdentityMean:output:0*
T0*0
_output_shapes
:������������������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*<
_input_shapes+
):'���������������������������:e a
=
_output_shapes+
):'���������������������������
 
_user_specified_nameinputs
�%
�
E__inference_sequential_layer_call_and_return_conditional_losses_57378

inputs4
 embedding_embedding_lookup_57348:
��6
$dense_matmul_readvariableop_resource:3
%dense_biasadd_readvariableop_resource:
identity��dense/BiasAdd/ReadVariableOp�dense/MatMul/ReadVariableOp�embedding/embedding_lookuph
embedding/CastCastinputs*

DstT0*

SrcT0*0
_output_shapes
:�������������������
embedding/embedding_lookupResourceGather embedding_embedding_lookup_57348embedding/Cast:y:0*
Tindices0*3
_class)
'%loc:@embedding/embedding_lookup/57348*4
_output_shapes"
 :������������������*
dtype0�
#embedding/embedding_lookup/IdentityIdentity#embedding/embedding_lookup:output:0*
T0*3
_class)
'%loc:@embedding/embedding_lookup/57348*4
_output_shapes"
 :�������������������
%embedding/embedding_lookup/Identity_1Identity,embedding/embedding_lookup/Identity:output:0*
T0*4
_output_shapes"
 :������������������Z
dropout/dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
dropout/dropout/MulMul.embedding/embedding_lookup/Identity_1:output:0dropout/dropout/Const:output:0*
T0*4
_output_shapes"
 :������������������s
dropout/dropout/ShapeShape.embedding/embedding_lookup/Identity_1:output:0*
T0*
_output_shapes
:�
,dropout/dropout/random_uniform/RandomUniformRandomUniformdropout/dropout/Shape:output:0*
T0*4
_output_shapes"
 :������������������*
dtype0c
dropout/dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *��L>�
dropout/dropout/GreaterEqualGreaterEqual5dropout/dropout/random_uniform/RandomUniform:output:0'dropout/dropout/GreaterEqual/y:output:0*
T0*4
_output_shapes"
 :�������������������
dropout/dropout/CastCast dropout/dropout/GreaterEqual:z:0*

DstT0*

SrcT0
*4
_output_shapes"
 :�������������������
dropout/dropout/Mul_1Muldropout/dropout/Mul:z:0dropout/dropout/Cast:y:0*
T0*4
_output_shapes"
 :������������������q
/global_average_pooling1d/Mean/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :�
global_average_pooling1d/MeanMeandropout/dropout/Mul_1:z:08global_average_pooling1d/Mean/reduction_indices:output:0*
T0*'
_output_shapes
:���������\
dropout_1/dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
dropout_1/dropout/MulMul&global_average_pooling1d/Mean:output:0 dropout_1/dropout/Const:output:0*
T0*'
_output_shapes
:���������m
dropout_1/dropout/ShapeShape&global_average_pooling1d/Mean:output:0*
T0*
_output_shapes
:�
.dropout_1/dropout/random_uniform/RandomUniformRandomUniform dropout_1/dropout/Shape:output:0*
T0*'
_output_shapes
:���������*
dtype0e
 dropout_1/dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *��L>�
dropout_1/dropout/GreaterEqualGreaterEqual7dropout_1/dropout/random_uniform/RandomUniform:output:0)dropout_1/dropout/GreaterEqual/y:output:0*
T0*'
_output_shapes
:����������
dropout_1/dropout/CastCast"dropout_1/dropout/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:����������
dropout_1/dropout/Mul_1Muldropout_1/dropout/Mul:z:0dropout_1/dropout/Cast:y:0*
T0*'
_output_shapes
:����������
dense/MatMul/ReadVariableOpReadVariableOp$dense_matmul_readvariableop_resource*
_output_shapes

:*
dtype0�
dense/MatMulMatMuldropout_1/dropout/Mul_1:z:0#dense/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������~
dense/BiasAdd/ReadVariableOpReadVariableOp%dense_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
dense/BiasAddBiasAdddense/MatMul:product:0$dense/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������e
IdentityIdentitydense/BiasAdd:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp^dense/BiasAdd/ReadVariableOp^dense/MatMul/ReadVariableOp^embedding/embedding_lookup*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":������������������: : : 2<
dense/BiasAdd/ReadVariableOpdense/BiasAdd/ReadVariableOp2:
dense/MatMul/ReadVariableOpdense/MatMul/ReadVariableOp28
embedding/embedding_lookupembedding/embedding_lookup:X T
0
_output_shapes
:������������������
 
_user_specified_nameinputs
�	
c
D__inference_dropout_1_layer_call_and_return_conditional_losses_56512

inputs
identity�R
dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  �?d
dropout/MulMulinputsdropout/Const:output:0*
T0*'
_output_shapes
:���������C
dropout/ShapeShapeinputs*
T0*
_output_shapes
:�
$dropout/random_uniform/RandomUniformRandomUniformdropout/Shape:output:0*
T0*'
_output_shapes
:���������*
dtype0[
dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *��L>�
dropout/GreaterEqualGreaterEqual-dropout/random_uniform/RandomUniform:output:0dropout/GreaterEqual/y:output:0*
T0*'
_output_shapes
:���������o
dropout/CastCastdropout/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:���������i
dropout/Mul_1Muldropout/Mul:z:0dropout/Cast:y:0*
T0*'
_output_shapes
:���������Y
IdentityIdentitydropout/Mul_1:z:0*
T0*'
_output_shapes
:���������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*&
_input_shapes
:���������:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs
�
�
E__inference_sequential_layer_call_and_return_conditional_losses_56579

inputs#
embedding_56567:
��
dense_56573:
dense_56575:
identity��dense/StatefulPartitionedCall�dropout/StatefulPartitionedCall�!dropout_1/StatefulPartitionedCall�!embedding/StatefulPartitionedCall�
!embedding/StatefulPartitionedCallStatefulPartitionedCallinputsembedding_56567*
Tin
2*
Tout
2*
_collective_manager_ids
 *4
_output_shapes"
 :������������������*#
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *M
fHRF
D__inference_embedding_layer_call_and_return_conditional_losses_56431�
dropout/StatefulPartitionedCallStatefulPartitionedCall*embedding/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *4
_output_shapes"
 :������������������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *K
fFRD
B__inference_dropout_layer_call_and_return_conditional_losses_56540�
(global_average_pooling1d/PartitionedCallPartitionedCall(dropout/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *\
fWRU
S__inference_global_average_pooling1d_layer_call_and_return_conditional_losses_56447�
!dropout_1/StatefulPartitionedCallStatefulPartitionedCall1global_average_pooling1d/PartitionedCall:output:0 ^dropout/StatefulPartitionedCall*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *M
fHRF
D__inference_dropout_1_layer_call_and_return_conditional_losses_56512�
dense/StatefulPartitionedCallStatefulPartitionedCall*dropout_1/StatefulPartitionedCall:output:0dense_56573dense_56575*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *I
fDRB
@__inference_dense_layer_call_and_return_conditional_losses_56466u
IdentityIdentity&dense/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp^dense/StatefulPartitionedCall ^dropout/StatefulPartitionedCall"^dropout_1/StatefulPartitionedCall"^embedding/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":������������������: : : 2>
dense/StatefulPartitionedCalldense/StatefulPartitionedCall2B
dropout/StatefulPartitionedCalldropout/StatefulPartitionedCall2F
!dropout_1/StatefulPartitionedCall!dropout_1/StatefulPartitionedCall2F
!embedding/StatefulPartitionedCall!embedding/StatefulPartitionedCall:X T
0
_output_shapes
:������������������
 
_user_specified_nameinputs
�
�
E__inference_sequential_layer_call_and_return_conditional_losses_56629
embedding_input#
embedding_56617:
��
dense_56623:
dense_56625:
identity��dense/StatefulPartitionedCall�dropout/StatefulPartitionedCall�!dropout_1/StatefulPartitionedCall�!embedding/StatefulPartitionedCall�
!embedding/StatefulPartitionedCallStatefulPartitionedCallembedding_inputembedding_56617*
Tin
2*
Tout
2*
_collective_manager_ids
 *4
_output_shapes"
 :������������������*#
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *M
fHRF
D__inference_embedding_layer_call_and_return_conditional_losses_56431�
dropout/StatefulPartitionedCallStatefulPartitionedCall*embedding/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *4
_output_shapes"
 :������������������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *K
fFRD
B__inference_dropout_layer_call_and_return_conditional_losses_56540�
(global_average_pooling1d/PartitionedCallPartitionedCall(dropout/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *\
fWRU
S__inference_global_average_pooling1d_layer_call_and_return_conditional_losses_56447�
!dropout_1/StatefulPartitionedCallStatefulPartitionedCall1global_average_pooling1d/PartitionedCall:output:0 ^dropout/StatefulPartitionedCall*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *M
fHRF
D__inference_dropout_1_layer_call_and_return_conditional_losses_56512�
dense/StatefulPartitionedCallStatefulPartitionedCall*dropout_1/StatefulPartitionedCall:output:0dense_56623dense_56625*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *I
fDRB
@__inference_dense_layer_call_and_return_conditional_losses_56466u
IdentityIdentity&dense/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp^dense/StatefulPartitionedCall ^dropout/StatefulPartitionedCall"^dropout_1/StatefulPartitionedCall"^embedding/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":������������������: : : 2>
dense/StatefulPartitionedCalldense/StatefulPartitionedCall2B
dropout/StatefulPartitionedCalldropout/StatefulPartitionedCall2F
!dropout_1/StatefulPartitionedCall!dropout_1/StatefulPartitionedCall2F
!embedding/StatefulPartitionedCall!embedding/StatefulPartitionedCall:a ]
0
_output_shapes
:������������������
)
_user_specified_nameembedding_input
�	
�
,__inference_sequential_1_layer_call_fn_56729
text_vectorization_input
unknown
	unknown_0	
	unknown_1
	unknown_2	
	unknown_3:
��
	unknown_4:
	unknown_5:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCalltext_vectorization_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5*
Tin

2		*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *P
fKRI
G__inference_sequential_1_layer_call_and_return_conditional_losses_56712o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:���������: : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:] Y
#
_output_shapes
:���������
2
_user_specified_nametext_vectorization_input:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�

)__inference_embedding_layer_call_fn_57441

inputs
unknown:
��
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown*
Tin
2*
Tout
2*
_collective_manager_ids
 *4
_output_shapes"
 :������������������*#
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *M
fHRF
D__inference_embedding_layer_call_and_return_conditional_losses_56431|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*4
_output_shapes"
 :������������������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*1
_input_shapes 
:������������������: 22
StatefulPartitionedCallStatefulPartitionedCall:X T
0
_output_shapes
:������������������
 
_user_specified_nameinputs
�
�
*__inference_sequential_layer_call_fn_57324

inputs	
unknown:
��
	unknown_0:
	unknown_1:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1*
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *N
fIRG
E__inference_sequential_layer_call_and_return_conditional_losses_56777o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*-
_input_shapes
:����������: : : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
E__inference_sequential_layer_call_and_return_conditional_losses_57344

inputs4
 embedding_embedding_lookup_57328:
��6
$dense_matmul_readvariableop_resource:3
%dense_biasadd_readvariableop_resource:
identity��dense/BiasAdd/ReadVariableOp�dense/MatMul/ReadVariableOp�embedding/embedding_lookuph
embedding/CastCastinputs*

DstT0*

SrcT0*0
_output_shapes
:�������������������
embedding/embedding_lookupResourceGather embedding_embedding_lookup_57328embedding/Cast:y:0*
Tindices0*3
_class)
'%loc:@embedding/embedding_lookup/57328*4
_output_shapes"
 :������������������*
dtype0�
#embedding/embedding_lookup/IdentityIdentity#embedding/embedding_lookup:output:0*
T0*3
_class)
'%loc:@embedding/embedding_lookup/57328*4
_output_shapes"
 :�������������������
%embedding/embedding_lookup/Identity_1Identity,embedding/embedding_lookup/Identity:output:0*
T0*4
_output_shapes"
 :�������������������
dropout/IdentityIdentity.embedding/embedding_lookup/Identity_1:output:0*
T0*4
_output_shapes"
 :������������������q
/global_average_pooling1d/Mean/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :�
global_average_pooling1d/MeanMeandropout/Identity:output:08global_average_pooling1d/Mean/reduction_indices:output:0*
T0*'
_output_shapes
:���������x
dropout_1/IdentityIdentity&global_average_pooling1d/Mean:output:0*
T0*'
_output_shapes
:����������
dense/MatMul/ReadVariableOpReadVariableOp$dense_matmul_readvariableop_resource*
_output_shapes

:*
dtype0�
dense/MatMulMatMuldropout_1/Identity:output:0#dense/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������~
dense/BiasAdd/ReadVariableOpReadVariableOp%dense_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
dense/BiasAddBiasAdddense/MatMul:product:0$dense/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������e
IdentityIdentitydense/BiasAdd:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp^dense/BiasAdd/ReadVariableOp^dense/MatMul/ReadVariableOp^embedding/embedding_lookup*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":������������������: : : 2<
dense/BiasAdd/ReadVariableOpdense/BiasAdd/ReadVariableOp2:
dense/MatMul/ReadVariableOpdense/MatMul/ReadVariableOp28
embedding/embedding_lookupembedding/embedding_lookup:X T
0
_output_shapes
:������������������
 
_user_specified_nameinputs
�
o
S__inference_global_average_pooling1d_layer_call_and_return_conditional_losses_57500

inputs
identityX
Mean/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :g
MeanMeaninputsMean/reduction_indices:output:0*
T0*'
_output_shapes
:���������U
IdentityIdentityMean:output:0*
T0*'
_output_shapes
:���������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :������������������:\ X
4
_output_shapes"
 :������������������
 
_user_specified_nameinputs
�	
�
,__inference_sequential_1_layer_call_fn_56901
text_vectorization_input
unknown
	unknown_0	
	unknown_1
	unknown_2	
	unknown_3:
��
	unknown_4:
	unknown_5:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCalltext_vectorization_inputunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5*
Tin

2		*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *P
fKRI
G__inference_sequential_1_layer_call_and_return_conditional_losses_56865o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:���������: : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:] Y
#
_output_shapes
:���������
2
_user_specified_nametext_vectorization_input:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
*__inference_sequential_layer_call_fn_57302

inputs
unknown:
��
	unknown_0:
	unknown_1:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *N
fIRG
E__inference_sequential_layer_call_and_return_conditional_losses_56579o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":������������������: : : 22
StatefulPartitionedCallStatefulPartitionedCall:X T
0
_output_shapes
:������������������
 
_user_specified_nameinputs
�
�
*__inference_sequential_layer_call_fn_57313

inputs	
unknown:
��
	unknown_0:
	unknown_1:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1*
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *N
fIRG
E__inference_sequential_layer_call_and_return_conditional_losses_56703o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*-
_input_shapes
:����������: : : 22
StatefulPartitionedCallStatefulPartitionedCall:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
�
E__inference_sequential_layer_call_and_return_conditional_losses_56473

inputs#
embedding_56432:
��
dense_56467:
dense_56469:
identity��dense/StatefulPartitionedCall�!embedding/StatefulPartitionedCall�
!embedding/StatefulPartitionedCallStatefulPartitionedCallinputsembedding_56432*
Tin
2*
Tout
2*
_collective_manager_ids
 *4
_output_shapes"
 :������������������*#
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *M
fHRF
D__inference_embedding_layer_call_and_return_conditional_losses_56431�
dropout/PartitionedCallPartitionedCall*embedding/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *4
_output_shapes"
 :������������������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *K
fFRD
B__inference_dropout_layer_call_and_return_conditional_losses_56440�
(global_average_pooling1d/PartitionedCallPartitionedCall dropout/PartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *\
fWRU
S__inference_global_average_pooling1d_layer_call_and_return_conditional_losses_56447�
dropout_1/PartitionedCallPartitionedCall1global_average_pooling1d/PartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *M
fHRF
D__inference_dropout_1_layer_call_and_return_conditional_losses_56454�
dense/StatefulPartitionedCallStatefulPartitionedCall"dropout_1/PartitionedCall:output:0dense_56467dense_56469*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *I
fDRB
@__inference_dense_layer_call_and_return_conditional_losses_56466u
IdentityIdentity&dense/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp^dense/StatefulPartitionedCall"^embedding/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":������������������: : : 2>
dense/StatefulPartitionedCalldense/StatefulPartitionedCall2F
!embedding/StatefulPartitionedCall!embedding/StatefulPartitionedCall:X T
0
_output_shapes
:������������������
 
_user_specified_nameinputs
�

a
B__inference_dropout_layer_call_and_return_conditional_losses_56540

inputs
identity�R
dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  �?q
dropout/MulMulinputsdropout/Const:output:0*
T0*4
_output_shapes"
 :������������������C
dropout/ShapeShapeinputs*
T0*
_output_shapes
:�
$dropout/random_uniform/RandomUniformRandomUniformdropout/Shape:output:0*
T0*4
_output_shapes"
 :������������������*
dtype0[
dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *��L>�
dropout/GreaterEqualGreaterEqual-dropout/random_uniform/RandomUniform:output:0dropout/GreaterEqual/y:output:0*
T0*4
_output_shapes"
 :������������������|
dropout/CastCastdropout/GreaterEqual:z:0*

DstT0*

SrcT0
*4
_output_shapes"
 :������������������v
dropout/Mul_1Muldropout/Mul:z:0dropout/Cast:y:0*
T0*4
_output_shapes"
 :������������������f
IdentityIdentitydropout/Mul_1:z:0*
T0*4
_output_shapes"
 :������������������"
identityIdentity:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :������������������:\ X
4
_output_shapes"
 :������������������
 
_user_specified_nameinputs
�
�
__inference_<lambda>_576217
3key_value_init3912_lookuptableimportv2_table_handle/
+key_value_init3912_lookuptableimportv2_keys1
-key_value_init3912_lookuptableimportv2_values	
identity��&key_value_init3912/LookupTableImportV2�
&key_value_init3912/LookupTableImportV2LookupTableImportV23key_value_init3912_lookuptableimportv2_table_handle+key_value_init3912_lookuptableimportv2_keys-key_value_init3912_lookuptableimportv2_values*	
Tin0*

Tout0	*
_output_shapes
 J
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  �?L
IdentityIdentityConst:output:0^NoOp*
T0*
_output_shapes
: o
NoOpNoOp'^key_value_init3912/LookupTableImportV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*%
_input_shapes
: :��:��2P
&key_value_init3912/LookupTableImportV2&key_value_init3912/LookupTableImportV2:"

_output_shapes

:��:"

_output_shapes

:��
�
:
__inference__creator_57551
identity��
hash_tablel

hash_tableHashTableV2*
_output_shapes
: *
	key_dtype0*
shared_name3913*
value_dtype0	W
IdentityIdentityhash_table:table_handle:0^NoOp*
T0*
_output_shapes
: S
NoOpNoOp^hash_table*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes 2

hash_table
hash_table
�
�
%__inference_dense_layer_call_fn_57536

inputs
unknown:
	unknown_0:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *I
fDRB
@__inference_dense_layer_call_and_return_conditional_losses_56466o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime**
_input_shapes
:���������: : 22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs
�~
�
G__inference_sequential_1_layer_call_and_return_conditional_losses_57226

inputsO
Ktext_vectorization_string_lookup_none_lookup_lookuptablefindv2_table_handleP
Ltext_vectorization_string_lookup_none_lookup_lookuptablefindv2_default_value	,
(text_vectorization_string_lookup_equal_y/
+text_vectorization_string_lookup_selectv2_t	?
+sequential_embedding_embedding_lookup_57196:
��A
/sequential_dense_matmul_readvariableop_resource:>
0sequential_dense_biasadd_readvariableop_resource:
identity��'sequential/dense/BiasAdd/ReadVariableOp�&sequential/dense/MatMul/ReadVariableOp�%sequential/embedding/embedding_lookup�>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2Z
text_vectorization/StringLowerStringLowerinputs*#
_output_shapes
:����������
%text_vectorization/StaticRegexReplaceStaticRegexReplace'text_vectorization/StringLower:output:0*#
_output_shapes
:���������*6
pattern+)[!"#$%&()\*\+,-\./:;<=>?@\[\\\]^_`{|}~\']*
rewrite e
$text_vectorization/StringSplit/ConstConst*
_output_shapes
: *
dtype0*
valueB B �
,text_vectorization/StringSplit/StringSplitV2StringSplitV2.text_vectorization/StaticRegexReplace:output:0-text_vectorization/StringSplit/Const:output:0*<
_output_shapes*
(:���������:���������:�
2text_vectorization/StringSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        �
4text_vectorization/StringSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       �
4text_vectorization/StringSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      �
,text_vectorization/StringSplit/strided_sliceStridedSlice6text_vectorization/StringSplit/StringSplitV2:indices:0;text_vectorization/StringSplit/strided_slice/stack:output:0=text_vectorization/StringSplit/strided_slice/stack_1:output:0=text_vectorization/StringSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:���������*

begin_mask*
end_mask*
shrink_axis_mask~
4text_vectorization/StringSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: �
6text_vectorization/StringSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
6text_vectorization/StringSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
.text_vectorization/StringSplit/strided_slice_1StridedSlice4text_vectorization/StringSplit/StringSplitV2:shape:0=text_vectorization/StringSplit/strided_slice_1/stack:output:0?text_vectorization/StringSplit/strided_slice_1/stack_1:output:0?text_vectorization/StringSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask�
Utext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCast5text_vectorization/StringSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:����������
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Cast7text_vectorization/StringSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: �
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShapeYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:�
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
^text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdhtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0htext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: �
ctext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreatergtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0ltext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: �
^text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCastetext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: �
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMaxYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0jtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: �
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :�
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2ftext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0htext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: �
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMulbtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximum[text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimum[text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 �
btext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincountYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0jtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:����������
\text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumitext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:����������
`text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R �
\text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2itext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:����������
>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2LookupTableFindV2Ktext_vectorization_string_lookup_none_lookup_lookuptablefindv2_table_handle5text_vectorization/StringSplit/StringSplitV2:values:0Ltext_vectorization_string_lookup_none_lookup_lookuptablefindv2_default_value*	
Tin0*

Tout0	*#
_output_shapes
:����������
&text_vectorization/string_lookup/EqualEqual5text_vectorization/StringSplit/StringSplitV2:values:0(text_vectorization_string_lookup_equal_y*
T0*#
_output_shapes
:����������
)text_vectorization/string_lookup/SelectV2SelectV2*text_vectorization/string_lookup/Equal:z:0+text_vectorization_string_lookup_selectv2_tGtext_vectorization/string_lookup/None_Lookup/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:����������
)text_vectorization/string_lookup/IdentityIdentity2text_vectorization/string_lookup/SelectV2:output:0*
T0	*#
_output_shapes
:���������q
/text_vectorization/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R �
'text_vectorization/RaggedToTensor/ConstConst*
_output_shapes
:*
dtype0	*%
valueB	"���������       �
6text_vectorization/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor0text_vectorization/RaggedToTensor/Const:output:02text_vectorization/string_lookup/Identity:output:08text_vectorization/RaggedToTensor/default_value:output:07text_vectorization/StringSplit/strided_slice_1:output:05text_vectorization/StringSplit/strided_slice:output:0*
T0	*
Tindex0	*
Tshape0	*(
_output_shapes
:����������*
num_row_partition_tensors*7
row_partition_types 
FIRST_DIM_SIZEVALUE_ROWIDS�
sequential/CastCast?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0*

DstT0*

SrcT0	*(
_output_shapes
:����������x
sequential/embedding/CastCastsequential/Cast:y:0*

DstT0*

SrcT0*(
_output_shapes
:�����������
%sequential/embedding/embedding_lookupResourceGather+sequential_embedding_embedding_lookup_57196sequential/embedding/Cast:y:0*
Tindices0*>
_class4
20loc:@sequential/embedding/embedding_lookup/57196*,
_output_shapes
:����������*
dtype0�
.sequential/embedding/embedding_lookup/IdentityIdentity.sequential/embedding/embedding_lookup:output:0*
T0*>
_class4
20loc:@sequential/embedding/embedding_lookup/57196*,
_output_shapes
:�����������
0sequential/embedding/embedding_lookup/Identity_1Identity7sequential/embedding/embedding_lookup/Identity:output:0*
T0*,
_output_shapes
:����������e
 sequential/dropout/dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
sequential/dropout/dropout/MulMul9sequential/embedding/embedding_lookup/Identity_1:output:0)sequential/dropout/dropout/Const:output:0*
T0*,
_output_shapes
:�����������
 sequential/dropout/dropout/ShapeShape9sequential/embedding/embedding_lookup/Identity_1:output:0*
T0*
_output_shapes
:�
7sequential/dropout/dropout/random_uniform/RandomUniformRandomUniform)sequential/dropout/dropout/Shape:output:0*
T0*,
_output_shapes
:����������*
dtype0n
)sequential/dropout/dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *��L>�
'sequential/dropout/dropout/GreaterEqualGreaterEqual@sequential/dropout/dropout/random_uniform/RandomUniform:output:02sequential/dropout/dropout/GreaterEqual/y:output:0*
T0*,
_output_shapes
:�����������
sequential/dropout/dropout/CastCast+sequential/dropout/dropout/GreaterEqual:z:0*

DstT0*

SrcT0
*,
_output_shapes
:�����������
 sequential/dropout/dropout/Mul_1Mul"sequential/dropout/dropout/Mul:z:0#sequential/dropout/dropout/Cast:y:0*
T0*,
_output_shapes
:����������|
:sequential/global_average_pooling1d/Mean/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :�
(sequential/global_average_pooling1d/MeanMean$sequential/dropout/dropout/Mul_1:z:0Csequential/global_average_pooling1d/Mean/reduction_indices:output:0*
T0*'
_output_shapes
:���������g
"sequential/dropout_1/dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
 sequential/dropout_1/dropout/MulMul1sequential/global_average_pooling1d/Mean:output:0+sequential/dropout_1/dropout/Const:output:0*
T0*'
_output_shapes
:����������
"sequential/dropout_1/dropout/ShapeShape1sequential/global_average_pooling1d/Mean:output:0*
T0*
_output_shapes
:�
9sequential/dropout_1/dropout/random_uniform/RandomUniformRandomUniform+sequential/dropout_1/dropout/Shape:output:0*
T0*'
_output_shapes
:���������*
dtype0p
+sequential/dropout_1/dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *��L>�
)sequential/dropout_1/dropout/GreaterEqualGreaterEqualBsequential/dropout_1/dropout/random_uniform/RandomUniform:output:04sequential/dropout_1/dropout/GreaterEqual/y:output:0*
T0*'
_output_shapes
:����������
!sequential/dropout_1/dropout/CastCast-sequential/dropout_1/dropout/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:����������
"sequential/dropout_1/dropout/Mul_1Mul$sequential/dropout_1/dropout/Mul:z:0%sequential/dropout_1/dropout/Cast:y:0*
T0*'
_output_shapes
:����������
&sequential/dense/MatMul/ReadVariableOpReadVariableOp/sequential_dense_matmul_readvariableop_resource*
_output_shapes

:*
dtype0�
sequential/dense/MatMulMatMul&sequential/dropout_1/dropout/Mul_1:z:0.sequential/dense/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:����������
'sequential/dense/BiasAdd/ReadVariableOpReadVariableOp0sequential_dense_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
sequential/dense/BiasAddBiasAdd!sequential/dense/MatMul:product:0/sequential/dense/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������p
IdentityIdentity!sequential/dense/BiasAdd:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp(^sequential/dense/BiasAdd/ReadVariableOp'^sequential/dense/MatMul/ReadVariableOp&^sequential/embedding/embedding_lookup?^text_vectorization/string_lookup/None_Lookup/LookupTableFindV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:���������: : : : : : : 2R
'sequential/dense/BiasAdd/ReadVariableOp'sequential/dense/BiasAdd/ReadVariableOp2P
&sequential/dense/MatMul/ReadVariableOp&sequential/dense/MatMul/ReadVariableOp2N
%sequential/embedding/embedding_lookup%sequential/embedding/embedding_lookup2�
>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2:K G
#
_output_shapes
:���������
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
*__inference_sequential_layer_call_fn_56482
embedding_input
unknown:
��
	unknown_0:
	unknown_1:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallembedding_inputunknown	unknown_0	unknown_1*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *N
fIRG
E__inference_sequential_layer_call_and_return_conditional_losses_56473o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":������������������: : : 22
StatefulPartitionedCallStatefulPartitionedCall:a ]
0
_output_shapes
:������������������
)
_user_specified_nameembedding_input
�
�
__inference__initializer_575597
3key_value_init3912_lookuptableimportv2_table_handle/
+key_value_init3912_lookuptableimportv2_keys1
-key_value_init3912_lookuptableimportv2_values	
identity��&key_value_init3912/LookupTableImportV2�
&key_value_init3912/LookupTableImportV2LookupTableImportV23key_value_init3912_lookuptableimportv2_table_handle+key_value_init3912_lookuptableimportv2_keys-key_value_init3912_lookuptableimportv2_values*	
Tin0*

Tout0	*
_output_shapes
 G
ConstConst*
_output_shapes
: *
dtype0*
value	B :L
IdentityIdentityConst:output:0^NoOp*
T0*
_output_shapes
: o
NoOpNoOp'^key_value_init3912/LookupTableImportV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*%
_input_shapes
: :��:��2P
&key_value_init3912/LookupTableImportV2&key_value_init3912/LookupTableImportV2:"

_output_shapes

:��:"

_output_shapes

:��
�[
�
G__inference_sequential_1_layer_call_and_return_conditional_losses_57017
text_vectorization_inputO
Ktext_vectorization_string_lookup_none_lookup_lookuptablefindv2_table_handleP
Ltext_vectorization_string_lookup_none_lookup_lookuptablefindv2_default_value	,
(text_vectorization_string_lookup_equal_y/
+text_vectorization_string_lookup_selectv2_t	$
sequential_57009:
��"
sequential_57011:
sequential_57013:
identity��"sequential/StatefulPartitionedCall�>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2l
text_vectorization/StringLowerStringLowertext_vectorization_input*#
_output_shapes
:����������
%text_vectorization/StaticRegexReplaceStaticRegexReplace'text_vectorization/StringLower:output:0*#
_output_shapes
:���������*6
pattern+)[!"#$%&()\*\+,-\./:;<=>?@\[\\\]^_`{|}~\']*
rewrite e
$text_vectorization/StringSplit/ConstConst*
_output_shapes
: *
dtype0*
valueB B �
,text_vectorization/StringSplit/StringSplitV2StringSplitV2.text_vectorization/StaticRegexReplace:output:0-text_vectorization/StringSplit/Const:output:0*<
_output_shapes*
(:���������:���������:�
2text_vectorization/StringSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        �
4text_vectorization/StringSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       �
4text_vectorization/StringSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      �
,text_vectorization/StringSplit/strided_sliceStridedSlice6text_vectorization/StringSplit/StringSplitV2:indices:0;text_vectorization/StringSplit/strided_slice/stack:output:0=text_vectorization/StringSplit/strided_slice/stack_1:output:0=text_vectorization/StringSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:���������*

begin_mask*
end_mask*
shrink_axis_mask~
4text_vectorization/StringSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: �
6text_vectorization/StringSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
6text_vectorization/StringSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
.text_vectorization/StringSplit/strided_slice_1StridedSlice4text_vectorization/StringSplit/StringSplitV2:shape:0=text_vectorization/StringSplit/strided_slice_1/stack:output:0?text_vectorization/StringSplit/strided_slice_1/stack_1:output:0?text_vectorization/StringSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask�
Utext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCast5text_vectorization/StringSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:����������
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1Cast7text_vectorization/StringSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: �
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShapeYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:�
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
^text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdhtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0htext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: �
ctext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreatergtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0ltext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: �
^text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCastetext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: �
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMaxYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0jtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: �
_text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :�
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2ftext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0htext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: �
]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMulbtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximum[text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimum[text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: �
atext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 �
btext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincountYtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0jtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:����������
\text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumitext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:����������
`text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R �
\text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : �
Wtext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2itext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0]text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0etext_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:����������
>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2LookupTableFindV2Ktext_vectorization_string_lookup_none_lookup_lookuptablefindv2_table_handle5text_vectorization/StringSplit/StringSplitV2:values:0Ltext_vectorization_string_lookup_none_lookup_lookuptablefindv2_default_value*	
Tin0*

Tout0	*#
_output_shapes
:����������
&text_vectorization/string_lookup/EqualEqual5text_vectorization/StringSplit/StringSplitV2:values:0(text_vectorization_string_lookup_equal_y*
T0*#
_output_shapes
:����������
)text_vectorization/string_lookup/SelectV2SelectV2*text_vectorization/string_lookup/Equal:z:0+text_vectorization_string_lookup_selectv2_tGtext_vectorization/string_lookup/None_Lookup/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:����������
)text_vectorization/string_lookup/IdentityIdentity2text_vectorization/string_lookup/SelectV2:output:0*
T0	*#
_output_shapes
:���������q
/text_vectorization/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R �
'text_vectorization/RaggedToTensor/ConstConst*
_output_shapes
:*
dtype0	*%
valueB	"���������       �
6text_vectorization/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor0text_vectorization/RaggedToTensor/Const:output:02text_vectorization/string_lookup/Identity:output:08text_vectorization/RaggedToTensor/default_value:output:07text_vectorization/StringSplit/strided_slice_1:output:05text_vectorization/StringSplit/strided_slice:output:0*
T0	*
Tindex0	*
Tshape0	*(
_output_shapes
:����������*
num_row_partition_tensors*7
row_partition_types 
FIRST_DIM_SIZEVALUE_ROWIDS�
"sequential/StatefulPartitionedCallStatefulPartitionedCall?text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0sequential_57009sequential_57011sequential_57013*
Tin
2	*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *N
fIRG
E__inference_sequential_layer_call_and_return_conditional_losses_56777z
IdentityIdentity+sequential/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp#^sequential/StatefulPartitionedCall?^text_vectorization/string_lookup/None_Lookup/LookupTableFindV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:���������: : : : : : : 2H
"sequential/StatefulPartitionedCall"sequential/StatefulPartitionedCall2�
>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2>text_vectorization/string_lookup/None_Lookup/LookupTableFindV2:] Y
#
_output_shapes
:���������
2
_user_specified_nametext_vectorization_input:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
__inference_save_fn_57598
checkpoint_keyP
Lmutablehashtable_lookup_table_export_values_lookuptableexportv2_table_handle
identity

identity_1

identity_2

identity_3

identity_4

identity_5	��?MutableHashTable_lookup_table_export_values/LookupTableExportV2�
?MutableHashTable_lookup_table_export_values/LookupTableExportV2LookupTableExportV2Lmutablehashtable_lookup_table_export_values_lookuptableexportv2_table_handle",/job:localhost/replica:0/task:0/device:CPU:0*
Tkeys0*
Tvalues0	*
_output_shapes

::K
add/yConst*
_output_shapes
: *
dtype0*
valueB B-keysK
addAddcheckpoint_keyadd/y:output:0*
T0*
_output_shapes
: O
add_1/yConst*
_output_shapes
: *
dtype0*
valueB B-valuesO
add_1Addcheckpoint_keyadd_1/y:output:0*
T0*
_output_shapes
: E
IdentityIdentityadd:z:0^NoOp*
T0*
_output_shapes
: F
ConstConst*
_output_shapes
: *
dtype0*
valueB B N

Identity_1IdentityConst:output:0^NoOp*
T0*
_output_shapes
: �

Identity_2IdentityFMutableHashTable_lookup_table_export_values/LookupTableExportV2:keys:0^NoOp*
T0*
_output_shapes
:I

Identity_3Identity	add_1:z:0^NoOp*
T0*
_output_shapes
: H
Const_1Const*
_output_shapes
: *
dtype0*
valueB B P

Identity_4IdentityConst_1:output:0^NoOp*
T0*
_output_shapes
: �

Identity_5IdentityHMutableHashTable_lookup_table_export_values/LookupTableExportV2:values:0^NoOp*
T0	*
_output_shapes
:�
NoOpNoOp@^MutableHashTable_lookup_table_export_values/LookupTableExportV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0"!

identity_1Identity_1:output:0"!

identity_2Identity_2:output:0"!

identity_3Identity_3:output:0"!

identity_4Identity_4:output:0"!

identity_5Identity_5:output:0*(
_construction_contextkEagerRuntime*
_input_shapes
: : 2�
?MutableHashTable_lookup_table_export_values/LookupTableExportV2?MutableHashTable_lookup_table_export_values/LookupTableExportV2:F B

_output_shapes
: 
(
_user_specified_namecheckpoint_key
�
�
__inference_restore_fn_57606
restored_tensors_0
restored_tensors_1	C
?mutablehashtable_table_restore_lookuptableimportv2_table_handle
identity��2MutableHashTable_table_restore/LookupTableImportV2�
2MutableHashTable_table_restore/LookupTableImportV2LookupTableImportV2?mutablehashtable_table_restore_lookuptableimportv2_table_handlerestored_tensors_0restored_tensors_1",/job:localhost/replica:0/task:0/device:CPU:0*	
Tin0*

Tout0	*
_output_shapes
 G
ConstConst*
_output_shapes
: *
dtype0*
value	B :L
IdentityIdentityConst:output:0^NoOp*
T0*
_output_shapes
: {
NoOpNoOp3^MutableHashTable_table_restore/LookupTableImportV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes

::: 2h
2MutableHashTable_table_restore/LookupTableImportV22MutableHashTable_table_restore/LookupTableImportV2:L H

_output_shapes
:
,
_user_specified_namerestored_tensors_0:LH

_output_shapes
:
,
_user_specified_namerestored_tensors_1
�	
�
D__inference_embedding_layer_call_and_return_conditional_losses_56431

inputs*
embedding_lookup_56425:
��
identity��embedding_lookup^
CastCastinputs*

DstT0*

SrcT0*0
_output_shapes
:�������������������
embedding_lookupResourceGatherembedding_lookup_56425Cast:y:0*
Tindices0*)
_class
loc:@embedding_lookup/56425*4
_output_shapes"
 :������������������*
dtype0�
embedding_lookup/IdentityIdentityembedding_lookup:output:0*
T0*)
_class
loc:@embedding_lookup/56425*4
_output_shapes"
 :�������������������
embedding_lookup/Identity_1Identity"embedding_lookup/Identity:output:0*
T0*4
_output_shapes"
 :�������������������
IdentityIdentity$embedding_lookup/Identity_1:output:0^NoOp*
T0*4
_output_shapes"
 :������������������Y
NoOpNoOp^embedding_lookup*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*1
_input_shapes 
:������������������: 2$
embedding_lookupembedding_lookup:X T
0
_output_shapes
:������������������
 
_user_specified_nameinputs
�%
�
E__inference_sequential_layer_call_and_return_conditional_losses_57434

inputs	4
 embedding_embedding_lookup_57404:
��6
$dense_matmul_readvariableop_resource:3
%dense_biasadd_readvariableop_resource:
identity��dense/BiasAdd/ReadVariableOp�dense/MatMul/ReadVariableOp�embedding/embedding_lookupV
CastCastinputs*

DstT0*

SrcT0	*(
_output_shapes
:����������b
embedding/CastCastCast:y:0*

DstT0*

SrcT0*(
_output_shapes
:�����������
embedding/embedding_lookupResourceGather embedding_embedding_lookup_57404embedding/Cast:y:0*
Tindices0*3
_class)
'%loc:@embedding/embedding_lookup/57404*,
_output_shapes
:����������*
dtype0�
#embedding/embedding_lookup/IdentityIdentity#embedding/embedding_lookup:output:0*
T0*3
_class)
'%loc:@embedding/embedding_lookup/57404*,
_output_shapes
:�����������
%embedding/embedding_lookup/Identity_1Identity,embedding/embedding_lookup/Identity:output:0*
T0*,
_output_shapes
:����������Z
dropout/dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
dropout/dropout/MulMul.embedding/embedding_lookup/Identity_1:output:0dropout/dropout/Const:output:0*
T0*,
_output_shapes
:����������s
dropout/dropout/ShapeShape.embedding/embedding_lookup/Identity_1:output:0*
T0*
_output_shapes
:�
,dropout/dropout/random_uniform/RandomUniformRandomUniformdropout/dropout/Shape:output:0*
T0*,
_output_shapes
:����������*
dtype0c
dropout/dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *��L>�
dropout/dropout/GreaterEqualGreaterEqual5dropout/dropout/random_uniform/RandomUniform:output:0'dropout/dropout/GreaterEqual/y:output:0*
T0*,
_output_shapes
:�����������
dropout/dropout/CastCast dropout/dropout/GreaterEqual:z:0*

DstT0*

SrcT0
*,
_output_shapes
:�����������
dropout/dropout/Mul_1Muldropout/dropout/Mul:z:0dropout/dropout/Cast:y:0*
T0*,
_output_shapes
:����������q
/global_average_pooling1d/Mean/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :�
global_average_pooling1d/MeanMeandropout/dropout/Mul_1:z:08global_average_pooling1d/Mean/reduction_indices:output:0*
T0*'
_output_shapes
:���������\
dropout_1/dropout/ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  �?�
dropout_1/dropout/MulMul&global_average_pooling1d/Mean:output:0 dropout_1/dropout/Const:output:0*
T0*'
_output_shapes
:���������m
dropout_1/dropout/ShapeShape&global_average_pooling1d/Mean:output:0*
T0*
_output_shapes
:�
.dropout_1/dropout/random_uniform/RandomUniformRandomUniform dropout_1/dropout/Shape:output:0*
T0*'
_output_shapes
:���������*
dtype0e
 dropout_1/dropout/GreaterEqual/yConst*
_output_shapes
: *
dtype0*
valueB
 *��L>�
dropout_1/dropout/GreaterEqualGreaterEqual7dropout_1/dropout/random_uniform/RandomUniform:output:0)dropout_1/dropout/GreaterEqual/y:output:0*
T0*'
_output_shapes
:����������
dropout_1/dropout/CastCast"dropout_1/dropout/GreaterEqual:z:0*

DstT0*

SrcT0
*'
_output_shapes
:����������
dropout_1/dropout/Mul_1Muldropout_1/dropout/Mul:z:0dropout_1/dropout/Cast:y:0*
T0*'
_output_shapes
:����������
dense/MatMul/ReadVariableOpReadVariableOp$dense_matmul_readvariableop_resource*
_output_shapes

:*
dtype0�
dense/MatMulMatMuldropout_1/dropout/Mul_1:z:0#dense/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������~
dense/BiasAdd/ReadVariableOpReadVariableOp%dense_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
dense/BiasAddBiasAdddense/MatMul:product:0$dense/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������e
IdentityIdentitydense/BiasAdd:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp^dense/BiasAdd/ReadVariableOp^dense/MatMul/ReadVariableOp^embedding/embedding_lookup*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*-
_input_shapes
:����������: : : 2<
dense/BiasAdd/ReadVariableOpdense/BiasAdd/ReadVariableOp2:
dense/MatMul/ReadVariableOpdense/MatMul/ReadVariableOp28
embedding/embedding_lookupembedding/embedding_lookup:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
b
)__inference_dropout_1_layer_call_fn_57510

inputs
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *M
fHRF
D__inference_dropout_1_layer_call_and_return_conditional_losses_56512o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*&
_input_shapes
:���������22
StatefulPartitionedCallStatefulPartitionedCall:O K
'
_output_shapes
:���������
 
_user_specified_nameinputs
�	
�
D__inference_embedding_layer_call_and_return_conditional_losses_57451

inputs*
embedding_lookup_57445:
��
identity��embedding_lookup^
CastCastinputs*

DstT0*

SrcT0*0
_output_shapes
:�������������������
embedding_lookupResourceGatherembedding_lookup_57445Cast:y:0*
Tindices0*)
_class
loc:@embedding_lookup/57445*4
_output_shapes"
 :������������������*
dtype0�
embedding_lookup/IdentityIdentityembedding_lookup:output:0*
T0*)
_class
loc:@embedding_lookup/57445*4
_output_shapes"
 :�������������������
embedding_lookup/Identity_1Identity"embedding_lookup/Identity:output:0*
T0*4
_output_shapes"
 :�������������������
IdentityIdentity$embedding_lookup/Identity_1:output:0^NoOp*
T0*4
_output_shapes"
 :������������������Y
NoOpNoOp^embedding_lookup*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*1
_input_shapes 
:������������������: 2$
embedding_lookupembedding_lookup:X T
0
_output_shapes
:������������������
 
_user_specified_nameinputs
�
�
E__inference_sequential_layer_call_and_return_conditional_losses_56614
embedding_input#
embedding_56602:
��
dense_56608:
dense_56610:
identity��dense/StatefulPartitionedCall�!embedding/StatefulPartitionedCall�
!embedding/StatefulPartitionedCallStatefulPartitionedCallembedding_inputembedding_56602*
Tin
2*
Tout
2*
_collective_manager_ids
 *4
_output_shapes"
 :������������������*#
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *M
fHRF
D__inference_embedding_layer_call_and_return_conditional_losses_56431�
dropout/PartitionedCallPartitionedCall*embedding/StatefulPartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *4
_output_shapes"
 :������������������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *K
fFRD
B__inference_dropout_layer_call_and_return_conditional_losses_56440�
(global_average_pooling1d/PartitionedCallPartitionedCall dropout/PartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *\
fWRU
S__inference_global_average_pooling1d_layer_call_and_return_conditional_losses_56447�
dropout_1/PartitionedCallPartitionedCall1global_average_pooling1d/PartitionedCall:output:0*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *M
fHRF
D__inference_dropout_1_layer_call_and_return_conditional_losses_56454�
dense/StatefulPartitionedCallStatefulPartitionedCall"dropout_1/PartitionedCall:output:0dense_56608dense_56610*
Tin
2*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*$
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *I
fDRB
@__inference_dense_layer_call_and_return_conditional_losses_56466u
IdentityIdentity&dense/StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp^dense/StatefulPartitionedCall"^embedding/StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*5
_input_shapes$
":������������������: : : 2>
dense/StatefulPartitionedCalldense/StatefulPartitionedCall2F
!embedding/StatefulPartitionedCall!embedding/StatefulPartitionedCall:a ]
0
_output_shapes
:������������������
)
_user_specified_nameembedding_input
�	
�
,__inference_sequential_1_layer_call_fn_57076

inputs
unknown
	unknown_0	
	unknown_1
	unknown_2	
	unknown_3:
��
	unknown_4:
	unknown_5:
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputsunknown	unknown_0	unknown_1	unknown_2	unknown_3	unknown_4	unknown_5*
Tin

2		*
Tout
2*
_collective_manager_ids
 *'
_output_shapes
:���������*%
_read_only_resource_inputs
*-
config_proto

CPU

GPU 2J 8� *P
fKRI
G__inference_sequential_1_layer_call_and_return_conditional_losses_56865o
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*'
_output_shapes
:���������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:���������: : : : : : : 22
StatefulPartitionedCallStatefulPartitionedCall:K G
#
_output_shapes
:���������
 
_user_specified_nameinputs:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
F
__inference__creator_57569
identity: ��MutableHashTable}
MutableHashTableMutableHashTableV2*
_output_shapes
: *
	key_dtype0*
shared_name
table_63*
value_dtype0	]
IdentityIdentityMutableHashTable:table_handle:0^NoOp*
T0*
_output_shapes
: Y
NoOpNoOp^MutableHashTable*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes 2$
MutableHashTableMutableHashTable
�{
�
 __inference__wrapped_model_56401
text_vectorization_input\
Xsequential_1_text_vectorization_string_lookup_none_lookup_lookuptablefindv2_table_handle]
Ysequential_1_text_vectorization_string_lookup_none_lookup_lookuptablefindv2_default_value	9
5sequential_1_text_vectorization_string_lookup_equal_y<
8sequential_1_text_vectorization_string_lookup_selectv2_t	L
8sequential_1_sequential_embedding_embedding_lookup_56385:
��N
<sequential_1_sequential_dense_matmul_readvariableop_resource:K
=sequential_1_sequential_dense_biasadd_readvariableop_resource:
identity��4sequential_1/sequential/dense/BiasAdd/ReadVariableOp�3sequential_1/sequential/dense/MatMul/ReadVariableOp�2sequential_1/sequential/embedding/embedding_lookup�Ksequential_1/text_vectorization/string_lookup/None_Lookup/LookupTableFindV2y
+sequential_1/text_vectorization/StringLowerStringLowertext_vectorization_input*#
_output_shapes
:����������
2sequential_1/text_vectorization/StaticRegexReplaceStaticRegexReplace4sequential_1/text_vectorization/StringLower:output:0*#
_output_shapes
:���������*6
pattern+)[!"#$%&()\*\+,-\./:;<=>?@\[\\\]^_`{|}~\']*
rewrite r
1sequential_1/text_vectorization/StringSplit/ConstConst*
_output_shapes
: *
dtype0*
valueB B �
9sequential_1/text_vectorization/StringSplit/StringSplitV2StringSplitV2;sequential_1/text_vectorization/StaticRegexReplace:output:0:sequential_1/text_vectorization/StringSplit/Const:output:0*<
_output_shapes*
(:���������:���������:�
?sequential_1/text_vectorization/StringSplit/strided_slice/stackConst*
_output_shapes
:*
dtype0*
valueB"        �
Asequential_1/text_vectorization/StringSplit/strided_slice/stack_1Const*
_output_shapes
:*
dtype0*
valueB"       �
Asequential_1/text_vectorization/StringSplit/strided_slice/stack_2Const*
_output_shapes
:*
dtype0*
valueB"      �
9sequential_1/text_vectorization/StringSplit/strided_sliceStridedSliceCsequential_1/text_vectorization/StringSplit/StringSplitV2:indices:0Hsequential_1/text_vectorization/StringSplit/strided_slice/stack:output:0Jsequential_1/text_vectorization/StringSplit/strided_slice/stack_1:output:0Jsequential_1/text_vectorization/StringSplit/strided_slice/stack_2:output:0*
Index0*
T0	*#
_output_shapes
:���������*

begin_mask*
end_mask*
shrink_axis_mask�
Asequential_1/text_vectorization/StringSplit/strided_slice_1/stackConst*
_output_shapes
:*
dtype0*
valueB: �
Csequential_1/text_vectorization/StringSplit/strided_slice_1/stack_1Const*
_output_shapes
:*
dtype0*
valueB:�
Csequential_1/text_vectorization/StringSplit/strided_slice_1/stack_2Const*
_output_shapes
:*
dtype0*
valueB:�
;sequential_1/text_vectorization/StringSplit/strided_slice_1StridedSliceAsequential_1/text_vectorization/StringSplit/StringSplitV2:shape:0Jsequential_1/text_vectorization/StringSplit/strided_slice_1/stack:output:0Lsequential_1/text_vectorization/StringSplit/strided_slice_1/stack_1:output:0Lsequential_1/text_vectorization/StringSplit/strided_slice_1/stack_2:output:0*
Index0*
T0	*
_output_shapes
: *
shrink_axis_mask�
bsequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CastCastBsequential_1/text_vectorization/StringSplit/strided_slice:output:0*

DstT0*

SrcT0	*#
_output_shapes
:����������
dsequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1CastDsequential_1/text_vectorization/StringSplit/strided_slice_1:output:0*

DstT0*

SrcT0	*
_output_shapes
: �
lsequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ShapeShapefsequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0*
T0*
_output_shapes
:�
lsequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ConstConst*
_output_shapes
:*
dtype0*
valueB: �
ksequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/ProdProdusequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Shape:output:0usequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const:output:0*
T0*
_output_shapes
: �
psequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/yConst*
_output_shapes
: *
dtype0*
value	B : �
nsequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/GreaterGreatertsequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Prod:output:0ysequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater/y:output:0*
T0*
_output_shapes
: �
ksequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/CastCastrsequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Greater:z:0*

DstT0*

SrcT0
*
_output_shapes
: �
nsequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1Const*
_output_shapes
:*
dtype0*
valueB: �
jsequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaxMaxfsequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0wsequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_1:output:0*
T0*
_output_shapes
: �
lsequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/yConst*
_output_shapes
: *
dtype0*
value	B :�
jsequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/addAddV2ssequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Max:output:0usequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add/y:output:0*
T0*
_output_shapes
: �
jsequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mulMulosequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Cast:y:0nsequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/add:z:0*
T0*
_output_shapes
: �
nsequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MaximumMaximumhsequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0nsequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/mul:z:0*
T0*
_output_shapes
: �
nsequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/MinimumMinimumhsequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast_1:y:0rsequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Maximum:z:0*
T0*
_output_shapes
: �
nsequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2Const*
_output_shapes
: *
dtype0	*
valueB	 �
osequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/BincountBincountfsequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cast:y:0rsequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Minimum:z:0wsequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Const_2:output:0*
T0	*#
_output_shapes
:����������
isequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axisConst*
_output_shapes
: *
dtype0*
value	B : �
dsequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/CumsumCumsumvsequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/bincount/Bincount:bins:0rsequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum/axis:output:0*
T0	*#
_output_shapes
:����������
msequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0Const*
_output_shapes
:*
dtype0	*
valueB	R �
isequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axisConst*
_output_shapes
: *
dtype0*
value	B : �
dsequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concatConcatV2vsequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/values_0:output:0jsequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/Cumsum:out:0rsequential_1/text_vectorization/StringSplit/RaggedFromValueRowIds/RowPartitionFromValueRowIds/concat/axis:output:0*
N*
T0	*#
_output_shapes
:����������
Ksequential_1/text_vectorization/string_lookup/None_Lookup/LookupTableFindV2LookupTableFindV2Xsequential_1_text_vectorization_string_lookup_none_lookup_lookuptablefindv2_table_handleBsequential_1/text_vectorization/StringSplit/StringSplitV2:values:0Ysequential_1_text_vectorization_string_lookup_none_lookup_lookuptablefindv2_default_value*	
Tin0*

Tout0	*#
_output_shapes
:����������
3sequential_1/text_vectorization/string_lookup/EqualEqualBsequential_1/text_vectorization/StringSplit/StringSplitV2:values:05sequential_1_text_vectorization_string_lookup_equal_y*
T0*#
_output_shapes
:����������
6sequential_1/text_vectorization/string_lookup/SelectV2SelectV27sequential_1/text_vectorization/string_lookup/Equal:z:08sequential_1_text_vectorization_string_lookup_selectv2_tTsequential_1/text_vectorization/string_lookup/None_Lookup/LookupTableFindV2:values:0*
T0	*#
_output_shapes
:����������
6sequential_1/text_vectorization/string_lookup/IdentityIdentity?sequential_1/text_vectorization/string_lookup/SelectV2:output:0*
T0	*#
_output_shapes
:���������~
<sequential_1/text_vectorization/RaggedToTensor/default_valueConst*
_output_shapes
: *
dtype0	*
value	B	 R �
4sequential_1/text_vectorization/RaggedToTensor/ConstConst*
_output_shapes
:*
dtype0	*%
valueB	"���������       �
Csequential_1/text_vectorization/RaggedToTensor/RaggedTensorToTensorRaggedTensorToTensor=sequential_1/text_vectorization/RaggedToTensor/Const:output:0?sequential_1/text_vectorization/string_lookup/Identity:output:0Esequential_1/text_vectorization/RaggedToTensor/default_value:output:0Dsequential_1/text_vectorization/StringSplit/strided_slice_1:output:0Bsequential_1/text_vectorization/StringSplit/strided_slice:output:0*
T0	*
Tindex0	*
Tshape0	*(
_output_shapes
:����������*
num_row_partition_tensors*7
row_partition_types 
FIRST_DIM_SIZEVALUE_ROWIDS�
sequential_1/sequential/CastCastLsequential_1/text_vectorization/RaggedToTensor/RaggedTensorToTensor:result:0*

DstT0*

SrcT0	*(
_output_shapes
:�����������
&sequential_1/sequential/embedding/CastCast sequential_1/sequential/Cast:y:0*

DstT0*

SrcT0*(
_output_shapes
:�����������
2sequential_1/sequential/embedding/embedding_lookupResourceGather8sequential_1_sequential_embedding_embedding_lookup_56385*sequential_1/sequential/embedding/Cast:y:0*
Tindices0*K
_classA
?=loc:@sequential_1/sequential/embedding/embedding_lookup/56385*,
_output_shapes
:����������*
dtype0�
;sequential_1/sequential/embedding/embedding_lookup/IdentityIdentity;sequential_1/sequential/embedding/embedding_lookup:output:0*
T0*K
_classA
?=loc:@sequential_1/sequential/embedding/embedding_lookup/56385*,
_output_shapes
:�����������
=sequential_1/sequential/embedding/embedding_lookup/Identity_1IdentityDsequential_1/sequential/embedding/embedding_lookup/Identity:output:0*
T0*,
_output_shapes
:�����������
(sequential_1/sequential/dropout/IdentityIdentityFsequential_1/sequential/embedding/embedding_lookup/Identity_1:output:0*
T0*,
_output_shapes
:�����������
Gsequential_1/sequential/global_average_pooling1d/Mean/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :�
5sequential_1/sequential/global_average_pooling1d/MeanMean1sequential_1/sequential/dropout/Identity:output:0Psequential_1/sequential/global_average_pooling1d/Mean/reduction_indices:output:0*
T0*'
_output_shapes
:����������
*sequential_1/sequential/dropout_1/IdentityIdentity>sequential_1/sequential/global_average_pooling1d/Mean:output:0*
T0*'
_output_shapes
:����������
3sequential_1/sequential/dense/MatMul/ReadVariableOpReadVariableOp<sequential_1_sequential_dense_matmul_readvariableop_resource*
_output_shapes

:*
dtype0�
$sequential_1/sequential/dense/MatMulMatMul3sequential_1/sequential/dropout_1/Identity:output:0;sequential_1/sequential/dense/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:����������
4sequential_1/sequential/dense/BiasAdd/ReadVariableOpReadVariableOp=sequential_1_sequential_dense_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
%sequential_1/sequential/dense/BiasAddBiasAdd.sequential_1/sequential/dense/MatMul:product:0<sequential_1/sequential/dense/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������}
IdentityIdentity.sequential_1/sequential/dense/BiasAdd:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp5^sequential_1/sequential/dense/BiasAdd/ReadVariableOp4^sequential_1/sequential/dense/MatMul/ReadVariableOp3^sequential_1/sequential/embedding/embedding_lookupL^sequential_1/text_vectorization/string_lookup/None_Lookup/LookupTableFindV2*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*0
_input_shapes
:���������: : : : : : : 2l
4sequential_1/sequential/dense/BiasAdd/ReadVariableOp4sequential_1/sequential/dense/BiasAdd/ReadVariableOp2j
3sequential_1/sequential/dense/MatMul/ReadVariableOp3sequential_1/sequential/dense/MatMul/ReadVariableOp2h
2sequential_1/sequential/embedding/embedding_lookup2sequential_1/sequential/embedding/embedding_lookup2�
Ksequential_1/text_vectorization/string_lookup/None_Lookup/LookupTableFindV2Ksequential_1/text_vectorization/string_lookup/None_Lookup/LookupTableFindV2:] Y
#
_output_shapes
:���������
2
_user_specified_nametext_vectorization_input:

_output_shapes
: :

_output_shapes
: :

_output_shapes
: 
�
�
E__inference_sequential_layer_call_and_return_conditional_losses_57399

inputs	4
 embedding_embedding_lookup_57383:
��6
$dense_matmul_readvariableop_resource:3
%dense_biasadd_readvariableop_resource:
identity��dense/BiasAdd/ReadVariableOp�dense/MatMul/ReadVariableOp�embedding/embedding_lookupV
CastCastinputs*

DstT0*

SrcT0	*(
_output_shapes
:����������b
embedding/CastCastCast:y:0*

DstT0*

SrcT0*(
_output_shapes
:�����������
embedding/embedding_lookupResourceGather embedding_embedding_lookup_57383embedding/Cast:y:0*
Tindices0*3
_class)
'%loc:@embedding/embedding_lookup/57383*,
_output_shapes
:����������*
dtype0�
#embedding/embedding_lookup/IdentityIdentity#embedding/embedding_lookup:output:0*
T0*3
_class)
'%loc:@embedding/embedding_lookup/57383*,
_output_shapes
:�����������
%embedding/embedding_lookup/Identity_1Identity,embedding/embedding_lookup/Identity:output:0*
T0*,
_output_shapes
:�����������
dropout/IdentityIdentity.embedding/embedding_lookup/Identity_1:output:0*
T0*,
_output_shapes
:����������q
/global_average_pooling1d/Mean/reduction_indicesConst*
_output_shapes
: *
dtype0*
value	B :�
global_average_pooling1d/MeanMeandropout/Identity:output:08global_average_pooling1d/Mean/reduction_indices:output:0*
T0*'
_output_shapes
:���������x
dropout_1/IdentityIdentity&global_average_pooling1d/Mean:output:0*
T0*'
_output_shapes
:����������
dense/MatMul/ReadVariableOpReadVariableOp$dense_matmul_readvariableop_resource*
_output_shapes

:*
dtype0�
dense/MatMulMatMuldropout_1/Identity:output:0#dense/MatMul/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������~
dense/BiasAdd/ReadVariableOpReadVariableOp%dense_biasadd_readvariableop_resource*
_output_shapes
:*
dtype0�
dense/BiasAddBiasAdddense/MatMul:product:0$dense/BiasAdd/ReadVariableOp:value:0*
T0*'
_output_shapes
:���������e
IdentityIdentitydense/BiasAdd:output:0^NoOp*
T0*'
_output_shapes
:����������
NoOpNoOp^dense/BiasAdd/ReadVariableOp^dense/MatMul/ReadVariableOp^embedding/embedding_lookup*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*-
_input_shapes
:����������: : : 2<
dense/BiasAdd/ReadVariableOpdense/BiasAdd/ReadVariableOp2:
dense/MatMul/ReadVariableOpdense/MatMul/ReadVariableOp28
embedding/embedding_lookupembedding/embedding_lookup:P L
(
_output_shapes
:����������
 
_user_specified_nameinputs
�
`
'__inference_dropout_layer_call_fn_57461

inputs
identity��StatefulPartitionedCall�
StatefulPartitionedCallStatefulPartitionedCallinputs*
Tin
2*
Tout
2*
_collective_manager_ids
 *4
_output_shapes"
 :������������������* 
_read_only_resource_inputs
 *-
config_proto

CPU

GPU 2J 8� *K
fFRD
B__inference_dropout_layer_call_and_return_conditional_losses_56540|
IdentityIdentity StatefulPartitionedCall:output:0^NoOp*
T0*4
_output_shapes"
 :������������������`
NoOpNoOp^StatefulPartitionedCall*"
_acd_function_control_output(*
_output_shapes
 "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*3
_input_shapes"
 :������������������22
StatefulPartitionedCallStatefulPartitionedCall:\ X
4
_output_shapes"
 :������������������
 
_user_specified_nameinputs
�
*
__inference_<lambda>_57626
identityJ
ConstConst*
_output_shapes
: *
dtype0*
valueB
 *  �?E
IdentityIdentityConst:output:0*
T0*
_output_shapes
: "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes 
�
,
__inference__destroyer_57579
identityG
ConstConst*
_output_shapes
: *
dtype0*
value	B :E
IdentityIdentityConst:output:0*
T0*
_output_shapes
: "
identityIdentity:output:0*(
_construction_contextkEagerRuntime*
_input_shapes "�L
saver_filename:0StatefulPartitionedCall_2:0StatefulPartitionedCall_38"
saved_model_main_op

NoOp*>
__saved_model_init_op%#
__saved_model_init_op

NoOp*�
serving_default�
Y
text_vectorization_input=
*serving_default_text_vectorization_input:0���������@

sequential2
StatefulPartitionedCall_1:0���������tensorflow/serving/predict:��
�
layer_with_weights-0
layer-0
layer_with_weights-1
layer-1
	optimizer
	variables
trainable_variables
regularization_losses
	keras_api

signatures
t__call__
*u&call_and_return_all_conditional_losses
v_default_save_signature"
_tf_keras_sequential
P
	_lookup_layer

	keras_api
w_adapt_function"
_tf_keras_layer
�
layer_with_weights-0
layer-0
layer-1
layer-2
layer-3
layer_with_weights-1
layer-4
	optimizer
	variables
trainable_variables
regularization_losses
	keras_api
x__call__
*y&call_and_return_all_conditional_losses"
_tf_keras_sequential
"
	optimizer
5
1
2
3"
trackable_list_wrapper
5
0
1
2"
trackable_list_wrapper
 "
trackable_list_wrapper
�
non_trainable_variables

layers
metrics
layer_regularization_losses
layer_metrics
	variables
trainable_variables
regularization_losses
t__call__
v_default_save_signature
*u&call_and_return_all_conditional_losses
&u"call_and_return_conditional_losses"
_generic_user_object
,
zserving_default"
signature_map
L
lookup_table
token_counts
	keras_api"
_tf_keras_layer
"
_generic_user_object
�

embeddings
 	variables
!trainable_variables
"regularization_losses
#	keras_api
{__call__
*|&call_and_return_all_conditional_losses"
_tf_keras_layer
�
$	variables
%trainable_variables
&regularization_losses
'	keras_api
}__call__
*~&call_and_return_all_conditional_losses"
_tf_keras_layer
�
(	variables
)trainable_variables
*regularization_losses
+	keras_api
__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�
,	variables
-trainable_variables
.regularization_losses
/	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�

kernel
bias
0	variables
1trainable_variables
2regularization_losses
3	keras_api
�__call__
+�&call_and_return_all_conditional_losses"
_tf_keras_layer
�
4iter

5beta_1

6beta_2
	7decay
8learning_ratemnmompvqvrvs"
	optimizer
5
0
1
2"
trackable_list_wrapper
5
0
1
2"
trackable_list_wrapper
 "
trackable_list_wrapper
�
9non_trainable_variables

:layers
;metrics
<layer_regularization_losses
=layer_metrics
	variables
trainable_variables
regularization_losses
x__call__
*y&call_and_return_all_conditional_losses
&y"call_and_return_conditional_losses"
_generic_user_object
(:&
��2embedding/embeddings
:2dense/kernel
:2
dense/bias
 "
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
.
>0
?1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
m
@_initializer
�_create_resource
�_initialize
�_destroy_resourceR jCustom.StaticHashTable
T
�_create_resource
�_initialize
�_destroy_resourceR Z
table��
"
_generic_user_object
'
0"
trackable_list_wrapper
'
0"
trackable_list_wrapper
 "
trackable_list_wrapper
�
Anon_trainable_variables

Blayers
Cmetrics
Dlayer_regularization_losses
Elayer_metrics
 	variables
!trainable_variables
"regularization_losses
{__call__
*|&call_and_return_all_conditional_losses
&|"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
Fnon_trainable_variables

Glayers
Hmetrics
Ilayer_regularization_losses
Jlayer_metrics
$	variables
%trainable_variables
&regularization_losses
}__call__
*~&call_and_return_all_conditional_losses
&~"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
Knon_trainable_variables

Llayers
Mmetrics
Nlayer_regularization_losses
Olayer_metrics
(	variables
)trainable_variables
*regularization_losses
__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
�
Pnon_trainable_variables

Qlayers
Rmetrics
Slayer_regularization_losses
Tlayer_metrics
,	variables
-trainable_variables
.regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
.
0
1"
trackable_list_wrapper
.
0
1"
trackable_list_wrapper
 "
trackable_list_wrapper
�
Unon_trainable_variables

Vlayers
Wmetrics
Xlayer_regularization_losses
Ylayer_metrics
0	variables
1trainable_variables
2regularization_losses
�__call__
+�&call_and_return_all_conditional_losses
'�"call_and_return_conditional_losses"
_generic_user_object
:	 (2	Adam/iter
: (2Adam/beta_1
: (2Adam/beta_2
: (2
Adam/decay
: (2Adam/learning_rate
 "
trackable_list_wrapper
C
0
1
2
3
4"
trackable_list_wrapper
.
Z0
[1"
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
N
	\total
	]count
^	variables
_	keras_api"
_tf_keras_metric
^
	`total
	acount
b
_fn_kwargs
c	variables
d	keras_api"
_tf_keras_metric
"
_generic_user_object
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_list_wrapper
 "
trackable_dict_wrapper
N
	etotal
	fcount
g	variables
h	keras_api"
_tf_keras_metric
^
	itotal
	jcount
k
_fn_kwargs
l	variables
m	keras_api"
_tf_keras_metric
:  (2total
:  (2count
.
\0
]1"
trackable_list_wrapper
-
^	variables"
_generic_user_object
:  (2total
:  (2count
 "
trackable_dict_wrapper
.
`0
a1"
trackable_list_wrapper
-
c	variables"
_generic_user_object
:  (2total
:  (2count
.
e0
f1"
trackable_list_wrapper
-
g	variables"
_generic_user_object
:  (2total
:  (2count
 "
trackable_dict_wrapper
.
i0
j1"
trackable_list_wrapper
-
l	variables"
_generic_user_object
-:+
��2Adam/embedding/embeddings/m
#:!2Adam/dense/kernel/m
:2Adam/dense/bias/m
-:+
��2Adam/embedding/embeddings/v
#:!2Adam/dense/kernel/v
:2Adam/dense/bias/v
�2�
,__inference_sequential_1_layer_call_fn_56729
,__inference_sequential_1_layer_call_fn_57057
,__inference_sequential_1_layer_call_fn_57076
,__inference_sequential_1_layer_call_fn_56901�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
G__inference_sequential_1_layer_call_and_return_conditional_losses_57144
G__inference_sequential_1_layer_call_and_return_conditional_losses_57226
G__inference_sequential_1_layer_call_and_return_conditional_losses_56959
G__inference_sequential_1_layer_call_and_return_conditional_losses_57017�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�B�
 __inference__wrapped_model_56401text_vectorization_input"�
���
FullArgSpec
args� 
varargsjargs
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
__inference_adapt_step_57274�
���
FullArgSpec
args�

jiterator
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
*__inference_sequential_layer_call_fn_56482
*__inference_sequential_layer_call_fn_57291
*__inference_sequential_layer_call_fn_57302
*__inference_sequential_layer_call_fn_56599
*__inference_sequential_layer_call_fn_57313
*__inference_sequential_layer_call_fn_57324�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
E__inference_sequential_layer_call_and_return_conditional_losses_57344
E__inference_sequential_layer_call_and_return_conditional_losses_57378
E__inference_sequential_layer_call_and_return_conditional_losses_56614
E__inference_sequential_layer_call_and_return_conditional_losses_56629
E__inference_sequential_layer_call_and_return_conditional_losses_57399
E__inference_sequential_layer_call_and_return_conditional_losses_57434�
���
FullArgSpec1
args)�&
jself
jinputs

jtraining
jmask
varargs
 
varkw
 
defaults�
p 

 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�B�
#__inference_signature_wrapper_57038text_vectorization_input"�
���
FullArgSpec
args� 
varargs
 
varkwjkwargs
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
)__inference_embedding_layer_call_fn_57441�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
D__inference_embedding_layer_call_and_return_conditional_losses_57451�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
'__inference_dropout_layer_call_fn_57456
'__inference_dropout_layer_call_fn_57461�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
B__inference_dropout_layer_call_and_return_conditional_losses_57466
B__inference_dropout_layer_call_and_return_conditional_losses_57478�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
8__inference_global_average_pooling1d_layer_call_fn_57483
8__inference_global_average_pooling1d_layer_call_fn_57488�
���
FullArgSpec%
args�
jself
jinputs
jmask
varargs
 
varkw
 
defaults�

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
S__inference_global_average_pooling1d_layer_call_and_return_conditional_losses_57494
S__inference_global_average_pooling1d_layer_call_and_return_conditional_losses_57500�
���
FullArgSpec%
args�
jself
jinputs
jmask
varargs
 
varkw
 
defaults�

 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
)__inference_dropout_1_layer_call_fn_57505
)__inference_dropout_1_layer_call_fn_57510�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
D__inference_dropout_1_layer_call_and_return_conditional_losses_57515
D__inference_dropout_1_layer_call_and_return_conditional_losses_57527�
���
FullArgSpec)
args!�
jself
jinputs

jtraining
varargs
 
varkw
 
defaults�
p 

kwonlyargs� 
kwonlydefaults� 
annotations� *
 
�2�
%__inference_dense_layer_call_fn_57536�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
@__inference_dense_layer_call_and_return_conditional_losses_57546�
���
FullArgSpec
args�
jself
jinputs
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *
 
�2�
__inference__creator_57551�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�2�
__inference__initializer_57559�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�2�
__inference__destroyer_57564�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�2�
__inference__creator_57569�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�2�
__inference__initializer_57574�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�2�
__inference__destroyer_57579�
���
FullArgSpec
args� 
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *� 
�B�
__inference_save_fn_57598checkpoint_key"�
���
FullArgSpec
args�
jcheckpoint_key
varargs
 
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *�	
� 
�B�
__inference_restore_fn_57606restored_tensors_0restored_tensors_1"�
���
FullArgSpec
args� 
varargsjrestored_tensors
varkw
 
defaults
 

kwonlyargs� 
kwonlydefaults
 
annotations� *�
	�
	�	
	J
Const
J	
Const_1
J	
Const_2
J	
Const_3
J	
Const_4
J	
Const_56
__inference__creator_57551�

� 
� "� 6
__inference__creator_57569�

� 
� "� 8
__inference__destroyer_57564�

� 
� "� 8
__inference__destroyer_57579�

� 
� "� A
__inference__initializer_57559���

� 
� "� :
__inference__initializer_57574�

� 
� "� �
 __inference__wrapped_model_56401�
���=�:
3�0
.�+
text_vectorization_input���������
� "7�4
2

sequential$�!

sequential���������j
__inference_adapt_step_57274J�?�<
5�2
0�-�
����������IteratorSpec 
� "
 �
@__inference_dense_layer_call_and_return_conditional_losses_57546\/�,
%�"
 �
inputs���������
� "%�"
�
0���������
� x
%__inference_dense_layer_call_fn_57536O/�,
%�"
 �
inputs���������
� "�����������
D__inference_dropout_1_layer_call_and_return_conditional_losses_57515\3�0
)�&
 �
inputs���������
p 
� "%�"
�
0���������
� �
D__inference_dropout_1_layer_call_and_return_conditional_losses_57527\3�0
)�&
 �
inputs���������
p
� "%�"
�
0���������
� |
)__inference_dropout_1_layer_call_fn_57505O3�0
)�&
 �
inputs���������
p 
� "����������|
)__inference_dropout_1_layer_call_fn_57510O3�0
)�&
 �
inputs���������
p
� "�����������
B__inference_dropout_layer_call_and_return_conditional_losses_57466v@�=
6�3
-�*
inputs������������������
p 
� "2�/
(�%
0������������������
� �
B__inference_dropout_layer_call_and_return_conditional_losses_57478v@�=
6�3
-�*
inputs������������������
p
� "2�/
(�%
0������������������
� �
'__inference_dropout_layer_call_fn_57456i@�=
6�3
-�*
inputs������������������
p 
� "%�"�������������������
'__inference_dropout_layer_call_fn_57461i@�=
6�3
-�*
inputs������������������
p
� "%�"�������������������
D__inference_embedding_layer_call_and_return_conditional_losses_57451q8�5
.�+
)�&
inputs������������������
� "2�/
(�%
0������������������
� �
)__inference_embedding_layer_call_fn_57441d8�5
.�+
)�&
inputs������������������
� "%�"�������������������
S__inference_global_average_pooling1d_layer_call_and_return_conditional_losses_57494{I�F
?�<
6�3
inputs'���������������������������

 
� ".�+
$�!
0������������������
� �
S__inference_global_average_pooling1d_layer_call_and_return_conditional_losses_57500i@�=
6�3
-�*
inputs������������������

 
� "%�"
�
0���������
� �
8__inference_global_average_pooling1d_layer_call_fn_57483nI�F
?�<
6�3
inputs'���������������������������

 
� "!��������������������
8__inference_global_average_pooling1d_layer_call_fn_57488\@�=
6�3
-�*
inputs������������������

 
� "����������y
__inference_restore_fn_57606YK�H
A�>
�
restored_tensors_0
�
restored_tensors_1	
� "� �
__inference_save_fn_57598�&�#
�
�
checkpoint_key 
� "���
`�]

name�
0/name 
#

slice_spec�
0/slice_spec 

tensor�
0/tensor
`�]

name�
1/name 
#

slice_spec�
1/slice_spec 

tensor�
1/tensor	�
G__inference_sequential_1_layer_call_and_return_conditional_losses_56959z
���E�B
;�8
.�+
text_vectorization_input���������
p 

 
� "%�"
�
0���������
� �
G__inference_sequential_1_layer_call_and_return_conditional_losses_57017z
���E�B
;�8
.�+
text_vectorization_input���������
p

 
� "%�"
�
0���������
� �
G__inference_sequential_1_layer_call_and_return_conditional_losses_57144h
���3�0
)�&
�
inputs���������
p 

 
� "%�"
�
0���������
� �
G__inference_sequential_1_layer_call_and_return_conditional_losses_57226h
���3�0
)�&
�
inputs���������
p

 
� "%�"
�
0���������
� �
,__inference_sequential_1_layer_call_fn_56729m
���E�B
;�8
.�+
text_vectorization_input���������
p 

 
� "�����������
,__inference_sequential_1_layer_call_fn_56901m
���E�B
;�8
.�+
text_vectorization_input���������
p

 
� "�����������
,__inference_sequential_1_layer_call_fn_57057[
���3�0
)�&
�
inputs���������
p 

 
� "�����������
,__inference_sequential_1_layer_call_fn_57076[
���3�0
)�&
�
inputs���������
p

 
� "�����������
E__inference_sequential_layer_call_and_return_conditional_losses_56614wI�F
?�<
2�/
embedding_input������������������
p 

 
� "%�"
�
0���������
� �
E__inference_sequential_layer_call_and_return_conditional_losses_56629wI�F
?�<
2�/
embedding_input������������������
p

 
� "%�"
�
0���������
� �
E__inference_sequential_layer_call_and_return_conditional_losses_57344n@�=
6�3
)�&
inputs������������������
p 

 
� "%�"
�
0���������
� �
E__inference_sequential_layer_call_and_return_conditional_losses_57378n@�=
6�3
)�&
inputs������������������
p

 
� "%�"
�
0���������
� �
E__inference_sequential_layer_call_and_return_conditional_losses_57399f8�5
.�+
!�
inputs����������	
p 

 
� "%�"
�
0���������
� �
E__inference_sequential_layer_call_and_return_conditional_losses_57434f8�5
.�+
!�
inputs����������	
p

 
� "%�"
�
0���������
� �
*__inference_sequential_layer_call_fn_56482jI�F
?�<
2�/
embedding_input������������������
p 

 
� "�����������
*__inference_sequential_layer_call_fn_56599jI�F
?�<
2�/
embedding_input������������������
p

 
� "�����������
*__inference_sequential_layer_call_fn_57291a@�=
6�3
)�&
inputs������������������
p 

 
� "�����������
*__inference_sequential_layer_call_fn_57302a@�=
6�3
)�&
inputs������������������
p

 
� "�����������
*__inference_sequential_layer_call_fn_57313Y8�5
.�+
!�
inputs����������	
p 

 
� "�����������
*__inference_sequential_layer_call_fn_57324Y8�5
.�+
!�
inputs����������	
p

 
� "�����������
#__inference_signature_wrapper_57038�
���Y�V
� 
O�L
J
text_vectorization_input.�+
text_vectorization_input���������"7�4
2

sequential$�!

sequential���������