# Prerequisites

The exercise session requires the following pieces of software:

1. Docker Desktop: https://www.docker.com/products/docker-desktop/
2. Python 3.9: https://www.python.org/downloads/
4. An Integrated Development Environment (IDE) for python, e.g., [VS Code](https://code.visualstudio.com) or [PyCharm](https://www.jetbrains.com/pycharm/download); PyCharm is used by the teacher.

5. Download the repository from https://gitlab.cern.ch/mmacieje/icsc2023-mlops
   - if you have git use command: `git clone https://gitlab.cern.ch/mmacieje/icsc2023-mlops.git`

Afterwards, setup your python virtual environment:
1. Open your preferred terminal and navigate to `icsc2023_mlops` folder
2. Install `virtualenv` in terminal with 
  `pip install --user virtualenv`
3. Create a virtual environment to isolate python dependencies
  `python -m venv myenv`
4. Activate virtual environment with
  `source myenv/bin/activate`
5. Install all dependencies from `requirements.txt` file
   - for MacOS M1 and later please install
     `pip install -r web/requirements_macos_m1.txt`  
     you may need install `hdf5`, too: https://stackoverflow.com/questions/70587971/errorfailed-building-wheel-for-h5pyfailed-to-build-h5pyerrorcould-not-build-wh
   - for other OS
     `pip install -r web/requirements.txt`

