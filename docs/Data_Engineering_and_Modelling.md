# Data Engineering and Modelling

The notebook is based on a `Tensorflow` example: https://www.tensorflow.org/tutorials/keras/text_classification


## Instructions
1. Open terminal and navigate to the `icsc2023_mlops` folder
2. Still in terminal open jupyter notebook with `jupyter notebook`
3. In your default browser you should see a jupyter notebook panel
![Jupyter Notebook](figures/jupyter_notebook.png)
4. Navigate to `notebook` and open `review_sentiment_prediction.ipynb`
5. Follow the instructions given in the notebook.
6. The outcome should be two models (`model_1` and `model_2`) stored in `models` directory
