# Introduction

Prepared by Michal Maciejewski, PhD


## Review Sentiment Prediction
Alice would like to develop her first machine learning model to predict a sentiment (either positive or negative) of a movie review.
She would like to share the model with her friend Bob, who is a big fan of cinema. 
With the model he will be able to skim through large databases of movie reviews and find out happy and sad movies.
However, being a seasoned Java developer he does not know Python. He does know REST API and Docker, though.
In the course of the exercise we will help Alice with building a small feed-forward neural network to classify reviews.
In fact, the model plays a secondary role here. The focus is on putting into practice the core concept learned during the lecture.

## Objectives

We will walk through the ML pipeline and put our learning into practice.
![ML pipeline](figures/ml_pipeline.png)

## Outline
During this exercise session we will look into:
1. Data Engineering and Modelling - 45 minutes
2. Local Model Deployment - 45 minutes

Let's get started!
