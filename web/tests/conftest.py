import sys
import asyncio
from typing import Any
from typing import Generator

import pytest
import numpy as np
from fastapi import FastAPI
from fastapi.testclient import TestClient

from web.app.core.configure_app import configure_routing

if sys.platform == "win32" and (3, 8, 0) <= sys.version_info < (3, 9, 0):
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())


class ModelMock:
    @staticmethod
    def predict(input_values=np.ndarray) -> np.ndarray:
        np.random.seed(0)
        return np.random.rand(len(input_values), 1)


id_to_model = {1: ModelMock(), 2: ModelMock()}


@pytest.fixture(scope="function")
def client(app: FastAPI) -> Generator[TestClient, Any, None]:
    """Creates a pytest fixture to mock client with a TestClient instance

    Args:
        app: FastAPI application instance
    Returns:
        Generator[TestClient, Any, None]: generator yielding a test client for unit tests
    """
    with TestClient(app) as client:
        yield client


@pytest.fixture(scope="function")
def app() -> Generator[FastAPI, Any, None]:
    """Creates a pytest fixture to mock client with an application instance

    Args:
        web: FastAPI application instance
    Returns:
        Generator[TestClient, Any, None]: generator yielding a test client for unit tests
    """
    _app = start_application()
    yield _app


def start_application() -> FastAPI:
    """Starts a testing application with configured routing

    Returns:
        FastAPI: a configured FastAPI instance
    """
    app = FastAPI()
    configure_routing(app)
    return app
