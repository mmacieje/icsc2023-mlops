from web.tests.conftest import id_to_model


def test_home(client, mocker):
    # Given
    mocker.patch("web.app.model.model_loader.load_models", return_value=id_to_model)

    # When
    response = client.get("/")

    # Then
    assert response.status_code == 200
