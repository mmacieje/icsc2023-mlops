import pytest

from web.app.model.predictor import convert_probability_to_sentiment, predict_sentiment
from web.app.schemas.prediction_schema import Prediction
from web.tests.conftest import ModelMock


@pytest.mark.parametrize(
    "probability,sentiment_ref",
    zip([0.0, 0.25, 0.5, 0.75, 1.0], ["neg", "neg", "pos", "pos", "pos"]),
)
def test_convert_probability_to_sentiment(probability, sentiment_ref):
    # Given

    # When
    sentiment_act = convert_probability_to_sentiment(probability)

    # Then
    assert sentiment_act == sentiment_ref


def test_predict_sentiment():
    # Given
    model = ModelMock()
    review = "It was a good movie"

    # When
    prediction = predict_sentiment(model, review)

    # Then
    assert isinstance(prediction, Prediction)
    assert abs(prediction.probability - 0.5488135039273248) < 1e-10
    assert prediction.sentiment == "pos"
