import os
from pathlib import Path

import pytest

from web.app.model.model_loader import parse_model_id, load_models, get_model, get_latest_model
from web.tests.conftest import ModelMock, id_to_model


@pytest.mark.parametrize(
    "model_subdir,model_index_ref",
    zip(
        ["model", "model-1", "model_1", "model_2", "model_1a"],
        [None, None, 1, 2, None],
    ),
)
def test_parse_model_id(model_subdir, model_index_ref):
    # Given

    # When
    model_index = parse_model_id(model_subdir)

    # Then
    assert model_index == model_index_ref


def test_load_models(mocker):
    # Given
    path = Path(os.path.dirname(__file__))
    model_root = os.path.join(path.parent, "resources", "models")
    model_mock = ModelMock()
    mocker.patch("web.app.model.model_loader.load_model", return_value=model_mock)

    # When
    id_to_model = load_models(model_root)

    # Then
    assert id_to_model == {1: model_mock, 2: model_mock, 3: model_mock}


def test_get_model(mocker):
    # Given
    mocker.patch("web.app.model.model_loader.load_models", return_value=id_to_model)

    # When
    model = get_model(1)

    # Then
    assert isinstance(model, ModelMock)


def test_get_model_missing(mocker):
    # Given
    mocker.patch("web.app.model.model_loader.load_models", return_value=id_to_model)

    # When
    model = get_model(3)

    # Then
    assert model is None


def test_get_latest_model(mocker):
    # Given
    mocker.patch("web.app.model.model_loader.load_models", return_value=id_to_model)

    # When
    model = get_latest_model()

    # Then
    assert isinstance(model, ModelMock)


def test_get_latest_model_missing(mocker):
    # Given
    mocker.patch("web.app.model.model_loader.load_models", return_value={})

    # When
    model = get_latest_model()

    # Then
    assert model is None
