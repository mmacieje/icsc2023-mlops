from web.tests.conftest import id_to_model


def test_predict_latest_model(client, mocker):
    # Given
    mocker.patch("web.app.model.model_loader.load_models", return_value=id_to_model)

    # When
    response = client.get("/That was a good movie")

    # Then
    assert response.status_code == 200
    assert response.json()["sentiment"] == "pos"


def test_predict_latest_model_error_no_models(client, mocker):
    # Given
    mocker.patch("web.app.model.model_loader.load_models", return_value={})

    # When
    response = client.get("/That was a good movie")

    # Then
    assert response.status_code == 404
    assert response.json()["detail"] == {"error": "There are no models available!"}


def test_predict_first_model(client, mocker):
    # Given
    mocker.patch("web.app.model.model_loader.load_models", return_value=id_to_model)

    # When
    response = client.get("/1/?review=Good movie")

    # Then
    assert response.status_code == 200
    assert response.json()["sentiment"] == "pos"


def test_predict_second_model(client, mocker):
    # Given
    mocker.patch("web.app.model.model_loader.load_models", return_value=id_to_model)

    # When
    response = client.get("/2/?review=Good movie")

    # Then
    assert response.status_code == 200
    assert response.json()["sentiment"] == "pos"


def test_predict_second_model_empty_review(client, mocker):
    # Given
    mocker.patch("web.app.model.model_loader.load_models", return_value=id_to_model)

    # When
    response = client.get("/2/?review=")

    # Then
    assert response.status_code == 400
    assert response.json()["detail"] == {"error": "The review message has to be non-empty"}


def test_predict_third_model(client, mocker):
    # Given
    mocker.patch("web.app.model.model_loader.load_models", return_value=id_to_model)

    # When
    response = client.get("/3/?review=Good movie")

    # Then
    assert response.status_code == 404
    assert response.json()["detail"] == {"error": "Model with id 3 does not exist!"}


def test_predict_second_model_wrong_param(client, mocker):
    # Given
    mocker.patch("web.app.model.model_loader.load_models", return_value=id_to_model)

    # When
    response = client.get("/3/?reviewer=Good movie")

    # Then
    assert response.status_code == 422
    assert response.json()["detail"] == [
        {
            "loc": ["query", "review"],
            "msg": "field required",
            "type": "value_error.missing",
        }
    ]


def test_predict_second_model_wrong_path(client, mocker):
    mocker.patch("web.app.model.model_loader.load_models", return_value=id_to_model)

    response = client.get("/3/reviewer")
    assert response.status_code == 404
    assert response.json()["detail"] == "Not Found"
