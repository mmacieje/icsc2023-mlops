import os
import functools
from typing import Optional

import keras
import tensorflow as tf

from web.app.core.config import Settings


def load_models(root_path: str) -> dict:
    """Loads tensorflow models stored in a root directory.

    It returns a dictionary mapping id to a model.
    The higher the id, the more recent version of the model.

    Args:
        root_path (str): a root directory with model subdirectories

    Returns: a dictionary mapping from model id to loaded model

    """
    return {}
    id_to_model = {}
    model_subdirs = os.listdir(root_path)
    for model_subdir in model_subdirs:
        model_index = parse_model_id(model_subdir)
        if model_index is not None:
            id_to_model[model_index] = load_model(os.path.join(root_path, model_subdir))

    return id_to_model


def parse_model_id(model_subdir: str) -> Optional[int]:
    """Parses model subdirectory name into the model id.

    It is assumed that the model subdirectories follow a naming
    convention: model_{id}, where {id} is a wildcard for a model id corresponding to its version.
    For example: model_1, model_2, model_3, etc.

    Example:
    >>> from from web.app.model_loader import parse_model_id
    >>> parse_model_id("model_1")
    1

    >>> from from web.app.model_loader import parse_model_id
    >>> parse_model_id("model1")
    None

    Args:
        model_subdir (str): a model subdirectory


    Returns:
        Optional[int]: if model_subdir was properly parsed, an integer id is returned, otherwise None
    """
    return None, None


@functools.lru_cache(128)
def load_model(model_path: str) -> keras.engine.sequential.Sequential:
    """Loads a tensorflow model for the prediction task.

    Args:
        model_path (str): an absolute path to a model

    Returns:
        keras.engine.sequential.Sequential: a loaded tensorflow model
    """
    # your code goes here
    return None


def get_latest_model() -> Optional[keras.engine.sequential.Sequential]:
    """Gets the latest model, i.e., a model with the highest id.
    It always loads the most recent models.

    Returns:
        keras.engine.sequential.Sequential: a loaded tensorflow model with the highest id
    """
    # your code goes here
    return None, None


def get_model(model_id: int) -> Optional[keras.engine.sequential.Sequential]:
    """Gets a model for the provided id.

    Args:
        model_id (int): model id

    Returns:
        keras.engine.sequential.Sequential: a loaded tensorflow model with a given id
    """
    # your code goes here
    return None, None
