import functools

import keras
import numpy as np

from web.app.schemas.prediction_schema import Prediction


@functools.lru_cache(maxsize=128, typed=False)
def predict_sentiment(model: keras.engine.sequential.Sequential, review: str) -> Prediction:
    """Function predicting sentiment for a review with a model.

    Args:
        model (keras.engine.sequential.Sequential): a tensorflow model for sentiment prediction
        review (str): a review for sentiment prediction

    Returns:
        Prediction: A prediction instance with sentiment and probability
    """
    probability: float = model.predict(np.array([review]))
    sentiment = convert_probability_to_sentiment(probability)
    return Prediction(sentiment=sentiment, probability=probability)


def convert_probability_to_sentiment(probability: float) -> str:
    """Converts probability into a sentiment string (either neg or pos)

    Args:
        probability: a probability of sentiment ([0, 0.5) - neg, [0.5, 1] - pos)

    Example:
    >>> from from web.app.predictor import convert_probability_to_sentiment
    >>> convert_probability_to_sentiment(0)
    "neg"

    >>> from from web.app.predictor import convert_probability_to_sentiment
    >>> convert_probability_to_sentiment(0.5)
    "pos"

    >>> from from web.app.predictor import convert_probability_to_sentiment
    >>> convert_probability_to_sentiment(1)
    "pos"

    Returns:
        str: sentiment (either neg or pos)

    """
    # your code goes here
    return ""
