from fastapi import FastAPI

from web.app.api import route_predictions
from web.app.core.config import Settings
from web.app.model.model_loader import load_models
from web.app.views import home


def configure(app: FastAPI) -> None:
    """Configures application at the start. It combines routing configuration and initialization of models

    Args:
        app: FastAPI instance to be configured
    Returns:
        None
    """
    configure_routing(app)
    configure_models()


def configure_routing(app: FastAPI) -> None:
    """Configured routing for an application

    Args:
        app: FastAPI instance to be configured
    Returns:
        None
    """
    app.include_router(route_predictions.router)
    app.include_router(home.router)


def configure_models() -> None:
    """Configures models. It loads the models and stores them in LRU cache memory.

    Returns:
        None
    """
    load_models(Settings.MODEL_ROOT)
