import os
from pathlib import Path
from dotenv import load_dotenv

env_path = Path("") / ".env"
load_dotenv(dotenv_path=env_path)


class Settings:
    MODEL_ROOT: str = os.getenv("MODEL_ROOT")
    PROJECT_TITLE: str = "Movie review sentiment prediction"
    PROJECT_VERSION: str = "0.0.1"
    PROJECT_DESCRIPTION: str = """The API providing two endpoints for movie review sentiment prediction"""
    PROJECT_METADATA: list = [
        {
            "name": "review",
            "description": "With the most recent model id performs a sentiment prediction for a _review_.",
        },
        {
            "name": "model_id_review",
            "description": "With a model _id_ performs a sentiment prediction for a _review_.",
        },
    ]


settings = Settings()
