from pydantic import BaseModel
from pydantic.dataclasses import dataclass


@dataclass
class Prediction:
    sentiment: str
    probability: float


class PredictionShow(BaseModel):
    sentiment: str
