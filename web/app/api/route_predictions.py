from fastapi import APIRouter, HTTPException, status

from web.app.model.model_loader import get_model, get_latest_model
from web.app.model.predictor import predict_sentiment
from web.app.schemas.prediction_schema import PredictionShow, Prediction

router = APIRouter()


@router.get(
    "/{model_id}/",
    response_model=PredictionShow,
    name="predict_with_model_id",
    tags=["model_id_review"],
)
async def get_prediction_with_id(model_id: int, review: str) -> Prediction:
    """Returns a prediction for a model with given id as a json with a schema given by the PredictionShow class.

    Args:
        review (str): a string with a movie review for sentiment prediction

    Returns:
        PredictionShow: instance of a class selecting variables to be returned to a user
    Raises:
        HTTPException: 404 in case a model with a given id is not available
        HTTPException: 400 in case an input review is empty
    """
    # your code goes here
    return None


@router.get(
    "/{review}",
    response_model=PredictionShow,
    name="predict_with_latest_model",
    tags=["review"],
)
async def get_prediction(review: str) -> Prediction:
    """Returns a prediction for the most recent model as a json with a schema given by the PredictionShow class.

    Args:
        review (str): a string with a movie review for sentiment prediction

    Returns:
        PredictionShow: instance of a class selecting variables to be returned to a user
    Raises:
        HTTPException: 404 in case no models are available
    """
    # your code goes here
    return None
