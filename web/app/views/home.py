import os
from pathlib import Path

from fastapi import APIRouter, responses

router = APIRouter()

path = Path(os.path.dirname(__file__))
with open(os.path.join(path, "home.html"), "r") as file:
    body = file.read()


@router.get("/", include_in_schema=False)
def index():
    return responses.HTMLResponse(content=body)
